import os
import time
import tkinter as tk
import traceback
import uuid

from displayer import DISP_CMD, Display, Data
from esp_serial import ESPSerial, CMD, bcol


class SerialObj(tk.Frame):
    def __init__(self, master, i, displayer, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.disp = displayer
        self.i = i
        self.uid = str(uuid.uuid4())
        self.color = "Green"

        self.serial = ESPSerial(main=self, callback=self.process_rtt)

        # prefix_label = tk.Label(self, text="%03d Prefix: " % (self.i + 1))
        # prefix_label.grid(row=0, column=0, sticky="E")

        self.prefix_input = tk.Entry(self, text="")
        self.prefix_input.grid(row=0, column=1, sticky="W")

        self.dev_var = tk.StringVar(self)
        self.dev_var.set("Select PORT")
        self.dev_dropdown = tk.OptionMenu(self, self.dev_var, "Select PORT")
        self.dev_dropdown.grid(row=0, column=2, sticky="E")

        self.connect_btn = tk.Button(self, text="Connect to PORT", command=self.connect_to_port)
        self.connect_btn.grid(row=0, column=3, sticky="W")

        self.dev_status = tk.Label(self, text="disconnected")
        self.dev_status.grid(row=0, column=4, sticky="W")

        self.line_color = tk.Label(self, text="---")
        self.line_color.grid(row=0, column=5, sticky="W")

        self.rtts = [0, 0]

    def process_rtt(self, lines:str):
        """<RTT><ID>16 d9 : 8</ID>917009<t>1105411912 4008992597 4009836012 1107172336</t></RTT>"""
        rtt = -1
        try:
            rtt = int(lines.split("</ID>")[1].split("<t>")[0])
            rtt = rtt / 1000.0 - 900
            self.append_rtt(rtt)
            data = Data(self.uid, [x for x in range(len(self.rtts))], self.rtts)
            self.disp.put(data)
        except Exception as e:
            print(bcol.WARNING + str(e) + bcol.ENDC)

    def append_rtt(self, rtt):
        self.rtts.append(rtt)
        diff = len(self.rtts) - 300
        if diff > 0:
            for i in range(diff):
                self.rtts.pop(0)

    def set_prefix_text(self, txt):
        self.prefix_input.delete(0, tk.END)
        self.prefix_input.insert(0, txt)

    def set_line_color(self, color):
        self.color = color
        self.line_color.config(fg=self.color)

    def status_connected(self):
        cmd = DISP_CMD(Display.CMD_SERIAL_REGISTER, {
            "uid": self.uid,
            "cb": self.set_line_color
        })
        self.disp.put_cmd(cmd)
        self.connect_btn.config(state=tk.NORMAL)
        self.dev_status.config(text="CONNECTED", fg="Green")

    def status_disconnected(self):
        self.dev_status.config(text="disconnected", fg="Red")
        self.dev_var.set("Select PORT")
        self.connect_btn.config(state=tk.NORMAL)

    def connect_to_port(self):
        self.dev_status.config(text="connecting...", fg="Grey")
        self.serial.put(CMD(CMD.CONNECT, self.dev_var.get()))
        self.connect_btn.config(state=tk.DISABLED)

    def update_ports(self, dev_list):
        menu = self.dev_dropdown['menu']
        menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.dev_var.set(name))
        else:
            self.dev_var.set("Select PORT")


class Vis(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, **kwargs)
        self.pack(side=tk.LEFT, fill=tk.BOTH, expand=True, padx=10, pady=10)
        self.parent = parent
        self.parent.title("RTT Vis")

        self.disp = Display(self, Display.MODE_AMP)
        self.disp.grid(row=0, column=0, columnspan=4)

        self.serials = []
        self.create_serial(self.disp)

    def remove_serial(self):
        serial = self.serials[-1]   # type: SerialObj
        self.serials.remove(serial)
        serial.serial.stop()
        serial.destroy()

    def create_serial(self, displayer):
        serial = SerialObj(self, len(self.serials), displayer)
        serial.grid(row=1 + len(self.serials), column=3, columnspan=6, sticky="W")
        serial.serial.start()
        self.serials.append(serial)


if __name__ == "__main__":
    try:
        root = tk.Tk()
        vis = Vis(root)
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)