

class bcol:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    LTGRAY = '\033[37m'

    DKGRAY = '\033[90m'
    LTRED = '\033[91m'
    LTGREEN = '\033[92m'
    LTYELLOW = '\033[93m'
    LTBLUE = '\033[94m'
    LTMAGENTA = '\033[95m'
    LTCYAN = '\033[96m'

    DEFAULT = '\033[39m'

    ENDC = '\033[0m'


class Log:
    OFF, ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL = range(8)

    def __init__(self, color, tag, level=DEBUG):
        self.color = color
        self.prefix = tag
        self.level = level

    def a(self, *txt):
        if self.level != Log.OFF and self.level <= Log.ALL:
            print("A:", self.color + self.prefix, *txt, bcol.ENDC)

    def t(self, *txt):
        if self.level != Log.OFF and self.level <= Log.TRACE:
            print("T:", self.color + self.prefix, *txt, bcol.ENDC)

    def d(self, *txt):
        if self.level != Log.OFF and self.level <= Log.DEBUG:
            print("D:", self.color + self.prefix, *txt, bcol.ENDC)

    def i(self, *txt):
        if self.level != Log.OFF and self.level <= Log.INFO:
            print("I:", self.color + self.prefix, *txt, bcol.ENDC)

    def w(self, *txt):
        if self.level != Log.OFF and self.level <= Log.WARN:
            print("W:", bcol.WARNING + self.prefix, *txt, bcol.ENDC)

    def e(self, *txt):
        if self.level != Log.OFF and self.level <= Log.ERROR:
            print("E:", bcol.BOLD + bcol.RED + self.prefix, *txt, bcol.ENDC)

    def f(self, *txt):
        if self.level != Log.OFF and self.level <= Log.FATAL:
            print("F:", bcol.BOLD + bcol.UNDERLINE + bcol.RED + self.prefix, *txt, bcol.ENDC)
