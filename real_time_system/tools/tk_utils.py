import sys


class TkUtils:
    @staticmethod
    def bind(parent, key, callback):
        if sys.platform == "win32":
            parent.bind_all(key, callback)
        else:
            parent.bind(key, callback)