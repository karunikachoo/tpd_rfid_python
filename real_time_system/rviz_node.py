import sys
import tkinter as tk
from base_node.node import Node
from displayers.rviz.rviz_displayer import RVIZDisplayer, RVIZ
from messages.cmd_msgs import CmdMessage
from messages.nav_msgs import MapMessage, StepEventStamped
from messages.plotter_msgs import Entry2D, TfPoseXY2D, ParticleCloudMsg


class RVIZNode(tk.Frame):
    CMD_RESET = -1
    def __init__(self, parent, **kwargs):
        tk.Frame.__init__(self, master=parent, **kwargs)
        self.parent = parent
        self.parent.geometry("604x700")

        self.pack()

        self.rviz = RVIZ(master=self, root=parent)
        self.rviz.grid(row=1, column=0)

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pose_req = self.node.create_request("pose_store/pose_service", "pose_store/pose_reply",
                                                 TfPoseXY2D, self.on_pose_reply)
        self.map_sub = self.node.subscribe("correction/current_map", MapMessage, self.on_map_recv)

        self.step_sub = self.node.subscribe("imu_process/step_events", StepEventStamped, self._on_step_recv)
        self.sub = self.node.subscribe('imu_process/cmd', CmdMessage, self._on_cmd_recv)

        self.request_update()
        self.node_loop()

    def request_update(self):
        self.pose_req.request(0, "")
        self.parent.after(500, self.request_update)

    def on_pose_reply(self, msg: TfPoseXY2D):
        if len(msg.x) >= 2:
            # print(msg.x)
            # print(msg.y)
            # print("\n")
            self.rviz.disp.set_paths(msg.x, msg.y, msg.ang_x, msg.ang_y)
            self.rviz.disp.update_paths()

    def on_map_recv(self, msg: MapMessage):
        self.rviz.disp.map_handler.on_map_recv(msg)

    def _on_step_recv(self, msg: StepEventStamped):
        if msg.r > 0:
            """"""
            # self.rviz.disp.set_steps(msg.r, msg.d_theta, msg.xi, msg.xy)
            # self.rviz.disp.update_steps()

    def _on_point_recv(self, msg: ParticleCloudMsg):
        self.rviz.disp.set_particles(msg.x, msg.y)

    def _on_cmd_recv(self, msg: CmdMessage):
        if msg.cmd == self.CMD_RESET:
            print("resetting")
            self.rviz.disp.xi_x = [0, 0]
            self.rviz.disp.xi_y = [0, 0]
            self.rviz.disp.step_x = [0, 0]
            self.rviz.disp.step_y = [0, 0]
            self.rviz.disp.xi_theta = [0]

    def node_loop(self):
        self.node.sleep(0.1)
        self.parent.after(100, self.node_loop)


if __name__ == "__main__":
    r = None
    try:
        root = tk.Tk()
        r = RVIZNode(root)
        root.mainloop()
    except KeyboardInterrupt:
        r.node.alive = False
