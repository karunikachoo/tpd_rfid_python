import os
import sys

from base_node.node import Node
from map.universal_map_handler import UniversalMapHandler
from messages.nav_msgs import MapMessage, StepEventStamped, PointRadius2DMessage
from particle_filter.particle_filter import ParticleFilter

"""
always get step ends, 
"""


class ParticleFilterNode:
    def __init__(self):

        self.filter = ParticleFilter()

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.map_sub = self.node.subscribe("correction/current_map", MapMessage, self._on_map_recv)
        self.step_sub = self.node.subscribe("imu_process/step_events", StepEventStamped, self._on_step_recv)

        self.gateway_sub = self.node.subscribe('gateway/point_rads', PointRadius2DMessage, self._on_gateway_triggered)

        self.node.run_loop(0.01)

    def _on_map_recv(self, msg: MapMessage):
        self.filter. on_map_recv(msg)

    def _on_step_recv(self, msg: StepEventStamped):
        self.filter.on_step_recv(msg.ts, msg.r, msg.d_theta, msg.xi)

    def _on_gateway_triggered(self, msg: PointRadius2DMessage):
        self.filter.on_gateway_recv(msg)


if __name__ == "__main__":
    d = None
    try:
        d = ParticleFilterNode()
    except KeyboardInterrupt:
        d.node.alive = False
        os._exit(0)
