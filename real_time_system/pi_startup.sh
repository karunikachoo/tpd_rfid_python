#!/bin/bash

mydir="${0%/*}"

echo "$mydir"

echo "Starting IMU Sensor Node"
lxterminal -e  bash -c "python3 ${mydir}/imu_sensor_node.py; exit"

echo "Waiting for imu to calibrate"
sleep 25

echo "Starting Giat Processor"
lxterminal -e  bash -c "python3 ${mydir}/imu_real_time_processor_node.py; exit"