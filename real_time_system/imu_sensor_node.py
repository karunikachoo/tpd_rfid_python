import os
import sys
import time

from base_node.constants import NodeConst
from base_node.node import Node
from imu.imu_sim import ImuSim
from messages.cmd_msgs import CmdMessage
from messages.sensor_msgs import ImuMessage


if "--sim" in sys.argv:
    import tkinter as tk
else:
    from tools.serial_reader import SerialReader, CMD


class ImuSensor:
    def __init__(self):
        self.root = None

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pub = self.node.create_publisher('imu_raw', ImuMessage)

        if "--sim" in sys.argv:
            i = sys.argv.index("--sim")
            path = sys.argv[i+1]

            self.root = tk.Tk()
            self.root.title("IMU_SIM")
            self.root.geometry("200x120")

            self.cor_reset_pub = self.node.create_publisher('correction/cmd', CmdMessage)
            self.gait_reset_pub = self.node.create_publisher('imu_process/cmd', CmdMessage)
            self.store_reset_pub = self.node.create_publisher('pose_store/cmd', CmdMessage)

            ImuSim(self.root, path, self.process_line, self._on_sim_reset_nodes, 0.2)

            self.node_loop()
            self.root.mainloop()
        else:
            port = "/dev/ttyACM0"
            if sys.platform.startswith('win'):
                port = "COM18"
            elif sys.platform.startswith("darwin"):
                port = "/dev/cu.usbmodem14101"

            self.serial = SerialReader(self.process_line)
            self.serial.put(CMD(CMD.CONNECT, port))

            self.serial.start()

            self.node.run_loop(0.1)

    def _on_sim_reset_nodes(self):
        msg = CmdMessage()
        msg.cmd = -1

        self.cor_reset_pub.publish(msg)
        self.gait_reset_pub.publish(msg)
        self.store_reset_pub.publish(msg)

    def node_loop(self):
        self.node.sleep(0.001)
        self.root.after(100, self.node_loop)

    def process_line(self, line):
        ms, ax, ay, az, _, _, _, _, _, _, qw, qx, qy, qz, _, _, _, _ = [float(x) for x in line.split(",")]

        msg = ImuMessage()

        msg.ts = time.time()
        msg.ms = ms
        msg.a = [ax, ay, az]
        msg.q = [qw, qx, qy, qz]

        # print(msg.a)
        self.pub.publish(msg)


if __name__ == '__main__':
    try:
        sensor = ImuSensor()
    except KeyboardInterrupt:
        os._exit(1)