start "CORE" python core.py

TIMEOUT 5

start "IMU_SIM" python imu_sensor_sim_node.py
TIMEOUT 1
start "GAIT_PROCESSING" python imu_gait_processing_node.py

TIMEOUT 5

start "GATEWAY_HANDLER" python gateway_handler_node.py
TIMEOUT 1
start "GATEWAY_SIM" python gateway_sim_panel_node.py

TIMEOUT 5

start "MAP_SERVER" python map_server.py
TIMEOUT 1
start "POSE_CORRECTION" python pose_correction_node.py
TIMEOUT 1
start "POSE_STORAGE" python pose_storage_node.py

TIMEOUT 5

start "RVIZ" python rviz_node.py
