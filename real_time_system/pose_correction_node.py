import json
import sys, os
import time

from base_node.node import Node
from map.universal_map_handler import UniversalMapHandler
from messages.cmd_msgs import CmdMessage
from pose_correction.correction_classes import TrackedTransform
from pose_correction.pose_constraint_handler import PoseConstraintHandler
from pose_correction.pose_correction import PoseCorrection
from map_server_node import MapServerNode
from messages.nav_msgs import MapMessage, PointRadius2DMessage, PoseStamped
from pose_correction.segmented_transform import SegmentedTransform
from pose_correction.total_path_transform import TotalPathTransform

"""
START OFF NOT KNOWING WHERE THE USER IS UNTIL THE PERSON GOES THROUGH THE FIRST RESET
- Stores location data without providing correction

WHEN RECEIVING FIRST CORRECTION (EITHER VIA TOF/PROXIMITY/GATEWAY):
- POLLS THE DATABASE FOR THE MAP THE PROXIMITY/GATEWAY IS ASSIGNED TO
    - TOF/RSSI WILL HAVE ITS OWN AP->MAP ASSIGNMENT AND CAN RESPOND TO MAP ID REQUESTS 
- PULL & LOAD MAP DATA WITH THE PROXIMITY/GATEWAY/LOCATION IN QUESTION
- PIN POSE DATA POINT TO LOCATION ESTIMATE XY WITH UNCERTAINTY RADIUS
    - SMALLER RAD FOR GATEWAY VS LARGER RAD FOR PROXIMITY/TOF/RSSI 

WHEN RECEIVING SECOND CORRECTION:
- path from beginning & between PIN 1 : PIN 2 will be evaluated together to ensure it fits within the map.

SUBSEQUENT CORRECTIONS:
- only path between PIN n-1 : PIN n will be evaluated to fit within map.
"""


class PoseCorrectionNode:
    CMD_RESET = -1

    def __init__(self):

        self.map_handler = UniversalMapHandler()
        self.constraint_handler = PoseConstraintHandler()
        self.transform = TotalPathTransform(self.constraint_handler, -1, self.on_transform)

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.map_req = self.node.create_request('map_server/map_req_serv', "map_server/map_req_reply",
                                                MapMessage, self._on_map_recv)
        self.gateway_sub = self.node.subscribe('gateway/point_rads', PointRadius2DMessage, self._on_gateway_triggered)
        self.pose_sub = self.node.subscribe('imu_pose', PoseStamped, self._on_pose_recv)
        self.cmd_sub = self.node.subscribe('correction/cmd', CmdMessage, self._on_cmd_recv)

        self.cur_map_pub = self.node.create_publisher("correction/current_map", MapMessage, latch=True)
        self.tf_pub = self.node.create_publisher("correction/transform", PoseStamped, latch=True)

        self.process_constraint_handler()
        self.process_corrections()
        self.node.run_loop(0.001)

    def process_constraint_handler(self):
        self.constraint_handler.process()
        self.node.after(0.01, self.process_constraint_handler)

    def process_corrections(self):
        self.transform.process()
        self.node.after(0.02, self.process_corrections)

    def on_transform(self, t: TrackedTransform):
        p = PoseStamped()
        p.ts = time.time()
        p.point = t.d.copy()
        p.q = [float(t.q[0]), float(t.q[1]), float(t.q[2]), float(t.q[3])]

        # for key in p.__dict__:
        #     print(key, p.__dict__[key], type(p.__dict__[key]))

        self.tf_pub.publish(p)

    def _on_pose_recv(self, msg: PoseStamped):
        self.constraint_handler.add_entry(msg.id, msg.ts, msg.point, msg.q)

    def _on_map_recv(self, msg: MapMessage):
        self.map_handler.on_map_recv(msg)
        self.cur_map_pub.publish(msg)

    def _on_gateway_triggered(self, msg: PointRadius2DMessage):
        if not self.map_handler.is_current_map(msg.building_id, msg.floor, msg.map_id):
            data = {
                'building_id': msg.building_id,
                'floor': msg.floor,
                'map_id': msg.map_id
            }
            self.map_req.request(MapServerNode.CMD_MAP_REQ, json.dumps(data))
        print(msg.source_id)
        self.constraint_handler.add_constraint(msg.source_id, msg.ts, msg.point, msg.radius)

    def _on_cmd_recv(self, msg: CmdMessage):
        if msg.cmd == self.CMD_RESET:
            print("resetting")
            self.map_handler.reset_map()
            self.constraint_handler.reset()
            self.transform.reset()


if __name__ == "__main__":
    d = None
    try:
        d = PoseCorrectionNode()
    except KeyboardInterrupt:
        d.node.alive = False
        os._exit(0)