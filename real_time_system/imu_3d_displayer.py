import os
import sys
import time
import tkinter as tk
from base_node.node import Node
from imu.quaternion import Quaternion
from messages.nav_msgs import PoseStamped
from imu.real_time_plotter import Plotter


G = 9.80665     # m/s^2
FAST_SLEEP = 0.0001
SAMPLE_PERIOD = 1/100


class IMUDisplayer(tk.Frame):
    def __init__(self, parent, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)
        self.parent = parent
        self.pack()

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.plotter = Plotter(self, self.parent)

        self.sub = self.node.subscribe('imu_pose', PoseStamped, self.update_position)
        self.sub2 = self.node.subscribe('imu_pose_correction', PoseStamped, self.update_position)

        self.after(100, self.node_loop)

    def node_loop(self):
        self.node.sleep(0.001)
        self.after(100, self.node_loop)

    def update_position(self, msg: PoseStamped):
        self.plotter.update_data(msg.id, msg.point, Quaternion(*msg.q))
        """"""

    def correct_position(self, msg: PoseStamped):
        self.plotter.correct_data(msg.id, msg.point)


if __name__ == "__main__":
    try:
        root = tk.Tk()
        d = IMUDisplayer(root)
        root.mainloop()
    except KeyboardInterrupt:
        os._exit(0)