import collections
import math
import os
import sys
import time
import traceback

from pathlib import Path
from base_node.node import Node
from imu.quaternion import Quaternion
from messages.cmd_msgs import CmdMessage
from messages.nav_msgs import PoseStamped, MapMessage
from messages.plotter_msgs import Entry2D, TfPoseXY2D
from pose_correction.correction_classes import Point
from pose_correction.transform import Transform
from tools.print_utils import Log, bcol

TAG = "PoseStorageNode:"
log = Log(bcol.DEFAULT, TAG)


class PoseStorageNode:
    CMD_SAVE_RAW, CMD_SAVE_TF_PTS, CMD_SAVE_TF, CMD_CLEAR_PTS, CMD_CLEAR_TF = range(5)
    CMD_RESET = -1

    def __init__(self):
        self.poses = []

        self.x = []
        self.y = []
        self.ang_x = []
        self.ang_y = []
        self.tf_changed = True
        self.tf = Transform()

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pose_serv = self.node.create_service("pose_store/pose_service", "pose_store/pose_reply",
                                                  self.on_pose_request_recv)

        self.store_cmd = self.node.subscribe("pose_store/cmd", CmdMessage, self.on_cmd_recv)

        self.pose_sub = self.node.subscribe("imu_pose", PoseStamped, self.on_pose_recv)
        self.tf_sub = self.node.subscribe("correction/transform", PoseStamped, self.on_tf_recv)

        self.process_points()
        self.node.run_loop(0.001)

    def process_points(self):
        if self.tf_changed:
            x = []
            y = []

            for pose in self.poses.copy():             # type: PoseStamped
                p = self.tf.transform_point(*pose.point)
                x.append(p[0])
                y.append(p[1])

                c = Quaternion(0, 0.5, 0, 0)
                q = Quaternion(*pose.q)
                v = q * c * q.conj()
                xyz = (self.tf.q * v * self.tf.q.conj())[1:]
                self.ang_x.append(xyz[0] + p[0])
                self.ang_y.append(xyz[1] + p[1])
                # print(pose.point[:2], p[:2])

            self.x = x
            self.y = y

            self.tf_changed = False
        else:
            diff = len(self.poses) - len(self.x)
            if diff > 0:
                for i in range(len(self.x), len(self.poses)):
                    p = self.tf.transform_point(*self.poses[i].point)
                    self.x.append(p[0])
                    self.y.append(p[1])

                    c = Quaternion(0, 0.5, 0, 0)
                    q = Quaternion(*self.poses[i].q)
                    v = q * c * q.conj()
                    xyz = (self.tf.q * v * self.tf.q.conj())[1:]
                    self.ang_x.append(xyz[0] + p[0])
                    self.ang_y.append(xyz[1] + p[1])

                    # print(self.poses[i].point[:2], p[:2])

        self.node.after(0.02, self.process_points)

    def on_pose_request_recv(self, msg: CmdMessage):
        e = TfPoseXY2D()
        e.x = self.x
        e.y = self.y
        e.ang_x = self.ang_x
        e.ang_y = self.ang_y
        e.d = self.tf.d[:2]
        self.pose_serv.reply(msg, e)

    def on_pose_recv(self, msg: PoseStamped):
        p = Point(msg.id, msg.seq, msg.ts, msg.point, msg.q)
        self.poses.append(p)

    def on_tf_recv(self, msg: PoseStamped):
        self.tf.d = msg.point.copy()
        self.tf.q = Quaternion(*msg.q)
        self.tf_changed = True

    def on_cmd_recv(self, msg: CmdMessage):
        if msg.cmd == self.CMD_SAVE_RAW:
            try:
                p = Path(msg.data)
                path = p.resolve()

                rows = ""
                for pt in self.poses.copy():    # type: Point
                    r = "%f, %f, %f\n" % (pt.ts, pt.point[0], pt.point[1])
                    rows += r

                path.write_bytes(rows.encode())

            except FileNotFoundError:
                log.e("Path does not exist")
            except RuntimeError:
                log.e(traceback.format_exc())

        elif msg.cmd == self.CMD_SAVE_TF_PTS:
            try:
                p = Path(msg.data)
                path = p.resolve()

                x = self.x.copy()
                y = self.y.copy()

                rows = ""
                for i in range(len(x)):
                    r = "%f, %f\n" % (x[i], y[i])
                    rows += r

                path.write_bytes(rows.encode())

            except FileNotFoundError:
                log.e("Path does not exist")
            except RuntimeError:
                log.e(traceback.format_exc())

        elif msg.cmd == self.CMD_SAVE_TF:
            """"""
        elif msg.cmd == self.CMD_CLEAR_PTS:
            """"""
        elif msg.cmd == self.CMD_CLEAR_TF:
            """"""
        elif msg.cmd == self.CMD_RESET:
            log.i("Resetting")
            self.poses = []

            self.x = []
            self.y = []
            self.ang_x = []
            self.ang_y = []
            self.tf_changed = True
            self.tf = Transform()


if __name__ == '__main__':
    d = None
    try:
        d = PoseStorageNode()
    except KeyboardInterrupt:
        d.node.alive = False
        os._exit(0)