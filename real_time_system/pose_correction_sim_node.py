import math
import os
import sys
import time
import tkinter as tk

from base_node.node import Node
from messages.nav_msgs import PoseStamped
from imu.quaternion import Quaternion
from tools.tk_utils import TkUtils


class PoseCorrectionSim(tk.Canvas):
    I_RAD = 140
    O_RAD = 170
    T_RAD = 185

    K_RAD = I_RAD - 5
    KI_RAD = K_RAD - 50

    W = 500
    H = 500

    D_MULT = 0.1

    def __init__(self, parent: tk.Tk, **kwargs):
        tk.Canvas.__init__(self, parent, width=500, height=500, **kwargs)
        self.parent = parent
        self.pack()

        self.angle = 0
        self.d = [0.0, 0.0, 0.0]

        self.draw_angles()
        self.knob_line = None
        self.knob_text = None
        self.d_text = None
        self.draw_knob()
        self.buttons = []
        self.draw_buttons()

        self.bind_events()

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.tf_pub = self.node.create_publisher("correction/transform", PoseStamped, latch=True)

        self.node_loop()

    def node_loop(self):
        self.node.sleep(0.001)
        self.after(100, self.node_loop)

    def bind_events(self):
        TkUtils.bind(self, '<MouseWheel>', self._on_mousewheel)
        TkUtils.bind(self, '<Button-1>', self._on_click)

    def _publish(self, evt):
        p = PoseStamped()
        p.point = self.d
        rad = self.angle / 180.0 * math.pi
        p.q = list(Quaternion.from_angle_axis(rad, 0, 0, 1))
        p.ts = time.time()

        self.tf_pub.publish(p)

    def _on_click(self, evt):
        if evt.state & 0x01:        # SHIFT
            val = 10 * self.D_MULT
        else:
            val = 1 * self.D_MULT

        if self.in_bounds(self.buttons[0], evt.x, evt.y):   # Right
            self.d[0] += val
        elif self.in_bounds(self.buttons[1], evt.x, evt.y):   # Up
            self.d[1] += val
        elif self.in_bounds(self.buttons[2], evt.x, evt.y):  # Left
            self.d[0] -= val
        elif self.in_bounds(self.buttons[3], evt.x, evt.y):  # Down
            self.d[1] -= val

        self.update_d()
        self._publish(None)

    def _on_arrow(self, evt):
        print(evt)
        if evt.state & 0x01:        # SHIFT
            val = 10 * self.D_MULT
        else:
            val = 1 * self.D_MULT
        if evt.keysym == 'Up':
            self.d[1] += val
        elif evt.keysym == 'Down':
            self.d[1] -= val
        elif evt.keysym == 'Left':
            self.d[0] -= val
        elif evt.keysym == 'Right':
            self.d[0] += val
        self.update_d()
        self._publish(None)

    def _on_mousewheel(self, evt):
        if self.in_range(self.K_RAD, evt.x, evt.y):
            if sys.platform == 'win32':
                self.angle += evt.delta/120
            else:
                self.angle += evt.delta
            self.update_knob()
            self._publish(None)

    def draw_buttons(self):
        degs = [0, 90, 180, 270]
        for i in range(4):
            rad = degs[i] / 180.0 * math.pi
            rad1 = (degs[i] - 10) / 180.0 * math.pi
            rad2 = (degs[i] + 10) / 180.0 * math.pi
            _, _, x0, y0 = self.xy_calc(rad, 210, 240)
            x1, y1, _, _ = self.xy_calc(rad1, 210, 240)
            x2, y2, _, _ = self.xy_calc(rad2, 210, 240)
            self.create_polygon(x0, y0, x1, y1, x2, y2, x0, y0, fill='#aaa')
            x = [x0, x1, x2]
            y = [y0, y1, y2]
            self.buttons.append([min(x), min(y), max(x), max(y)])

    def draw_angles(self):
        for i in range(360):
            if i % 10 == 0:
                rad = (i + 90) / 180.0 * math.pi
                x0, y0, x1, y1, x2, y2 = self.xy_calc(rad, self.I_RAD, self.O_RAD, self.T_RAD)

                self.create_line(x0, y0, x1, y1)
                if i > 180:
                    self.create_text(x2, y2, text=i-360)
                else:
                    self.create_text(x2, y2, text=i)
        self.create_oval(self.W/2 - self.I_RAD,
                         self.W/2 - self.I_RAD,
                         self.W/2 + self.I_RAD,
                         self.W/2 + self.I_RAD, width=2)

    def draw_knob(self):
        self.create_oval(self.W / 2 - self.K_RAD,
                         self.W / 2 - self.K_RAD,
                         self.W / 2 + self.K_RAD,
                         self.W / 2 + self.K_RAD)

        self.knob_line = self.create_line(0, 0, 0, 0, width=2, fill='red')
        self.knob_text = self.create_text(self.W/2, self.W/2,
                                          text=self.angle, font=("Helvetica", 40), fill="#aaa", anchor="s")
        
        self.d_text = self.create_text(self.W/2, self.W/2,
                                       text="%.2f %.2f" % (self.d[0], self.d[1]),
                                       font=("Helvetica", 24), fill="#aaa", anchor="n")

        self.update_knob()

    def update_knob(self):
        rad = (self.angle + 90) / 180.0 * math.pi
        x0, y0, x1, y1 = self.xy_calc(rad, self.KI_RAD, self.K_RAD-2)
        self.coords(self.knob_line, x0, y0, x1, y1)

        self.itemconfig(self.knob_text, text="%d" % self.angle)

    def update_d(self):
        self.itemconfig(self.d_text, text="%.2f %.2f" % (self.d[0], self.d[1]))

    @staticmethod
    def in_bounds(rect, x, y):
        return rect[0] < x < rect[2] and rect[1] < y < rect[3]

    def in_range(self, rad, x, y):
        dx = x - self.W / 2
        dy = y - self.W / 2

        return dx * dx + dy * dy < rad * rad

    def xy_calc(self, rad, irad, orad, trad=None):
        x = math.cos(rad)
        y = math.sin(rad)
        x0 = self.W / 2 + irad * x
        y0 = self.W / 2 - irad * y

        x1 = self.W / 2 + orad * x
        y1 = self.W / 2 - orad * y

        if trad is not None:
            x2 = self.W / 2 + trad * x
            y2 = self.W / 2 - trad * y

            return x0, y0, x1, y1, x2, y2
        return x0, y0, x1, y1


if __name__ == "__main__":
    d = None
    try:
        root = tk.Tk()
        d = PoseCorrectionSim(root)
        root.mainloop()
    except KeyboardInterrupt:
        d.node.alive = False
        os._exit(0)