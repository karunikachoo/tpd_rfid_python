import os
import sys

from base_node.node import Node
from messages.nav_msgs import PointRadius2DMessage, NotifyMessage


class WifiProximityNode:
    def __init__(self):
        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pub = self.node.create_publisher('proximity/point_rads', PointRadius2DMessage)
        self.sub = self.node.subscribe('proximity/notifications', NotifyMessage, self.on_proximity_notify)

        self.node.run_loop(0.1)

    def on_proximity_notify(self, msg: NotifyMessage):
        print(msg)


if __name__ == "__main__":
    g = None
    try:
        g = WifiProximityNode()
    except KeyboardInterrupt:
        g.node.alive = False
        os._exit(0)
