import json
import os
import sys

from displayers.rviz.point import Point
import tkinter as tk
from tkinter import filedialog

from tools.print_utils import bcol, Log

TAG = "PenTool:"
log = Log(bcol.DEFAULT, TAG)


class State:
    RAW_NONE = 0
    RAW_SHIFT = 1
    RAW_CTRL = 4
    RAW_MOD1 = 8
    RAW_MOD2 = 16
    RAW_MOD3_MOD4 = 96
    RAW_BUTTON1 = 256

    WIN_ALT = 0x20000

    if sys.platform == "win32":
        NONE = RAW_MOD1
        SHIFT = RAW_SHIFT | RAW_MOD1
        CTRL = RAW_CTRL | RAW_MOD1
        MOD1 = WIN_ALT | RAW_MOD1
        MOD2 = RAW_CTRL | RAW_MOD1
        BUTTON1 = RAW_BUTTON1 | RAW_MOD1
        MOD3_MOD4 = RAW_MOD3_MOD4 | RAW_MOD1
    else:
        NONE = RAW_NONE
        SHIFT = RAW_SHIFT
        CTRL = RAW_CTRL
        MOD1 = RAW_MOD1
        MOD2 = RAW_MOD2
        BUTTON1 = RAW_BUTTON1
        MOD3_MOD4 = RAW_MOD3_MOD4


class PenTool:

    def __init__(self, canvas, root: tk.Tk, map_handler):
        from displayers.rviz.rviz_displayer import RVIZDisplayer

        self.canvas = canvas        # type: RVIZDisplayer
        self.root = root
        self.map_handler = map_handler

        self.points = []
        self.points_in_bounds = []

        self.selection = []
        self.sel_add = False
        self.sel_drag = False
        self.sel = [0, 0, 0, 0]
        self.click_xy = [0, 0]
        self.sel_marq = self.canvas.create_rectangle(self.sel, dash=(1, 1), outline="grey")

        self.hovered_pt = None
        self.mid_pt = None

        self.lines = []

        self.keys = {
            "ctrl": False,
            "alt": False,
        }

    def save_vectors(self):
        paths = self.generate_paths(dtype='m')

        rows = []
        for path in paths:
            row = ", ".join([str(p) for p in path]) + "\n"
            rows.append(row)
        fn = filedialog.asksaveasfilename(initialdir=os.getcwd(), title="Save vector file",
                                          filetypes=(("Map Vector File", "*.mapvec"),))
        if ".mapvec" not in fn:
            fn += ".mapvec"
        f = open(fn, 'w')
        f.writelines(rows)
        f.close()

    def load_vectors(self):
        fn = filedialog.askopenfilename(initialdir=os.getcwd(), title="Load vector file",
                                        filetypes=(("Map Vector File", "*.mapvec"),))

        f = open(fn, 'r')
        lines = f.readlines()
        f.close()

        for line in lines:
            path = [float(c.strip()) for c in line.split(",")]
            prev_pt = None
            for i in range(len(path)):
                r = i % 2
                ind = int(i/2)
                if r == 1:
                    if ind == 0:
                        prev_pt = self.create_point(path[i-1], path[i], None, None)
                    else:
                        prev_pt = self.create_point(path[i - 1], path[i], prev_pt, None)

        self._update_lines()

    def generate_paths(self, dtype='m', arrtype='linear'):
        checked = []
        paths = []
        for pt in self.points_in_bounds:
            if pt not in checked:
                pts = pt.list_points()
                # print(pts)
                if len(pts) > 1:
                    path = []
                    for _pt in pts:

                        if dtype == 'm':
                            if arrtype == 'linear':
                                path.extend(_pt.xy)
                            elif arrtype == '2d':
                                path.append(_pt.xy)
                        elif dtype == 'px':
                            if arrtype == 'linear':
                                path.extend(_pt.cxy)
                            elif arrtype == '2d':
                                path.append(_pt.cxy)
                        else:
                            log.e("Can only generate paths of type 'm' or 'px'. No type %s" % dtype)
                    paths.append(path)
                checked.extend(pts)
        return paths

    def raster_paths(self):
        # self.map_handler.reset_map()
        paths = self.generate_paths(dtype='px', arrtype='2d')
        for path in paths:
            for i in range(len(path) - 1):
                x0, y0 = path[i]
                x1, y1 = path[i+1]

                cx0 = self.canvas.px2cell(x0)
                cy0 = self.canvas.px2cell(y0)
                cx1 = self.canvas.px2cell(x1)
                cy1 = self.canvas.px2cell(y1)

                self.map_handler.line(cx0, cy0, cx1, cy1)

    def is_pt_selected(self, pt: Point):
        return pt in self.selection

    def _has_selected_pt(self):
        return len(self.selection) > 0

    def _get_selected_pt(self):
        if len(self.selection) > 0:
            return self.selection[-1]
        return None

    def create_point(self, x_m, y_m, prev_pt, next_pt):
        pt = Point(self, self.canvas, x_m, y_m, next_pt=next_pt, prev_pt=prev_pt)
        self.points.append(pt)
        self.points_in_bounds.append(pt)
        self.selection = [pt]
        return pt

    def delete_point(self, pt):
        pt.delete()
        if pt in self.points_in_bounds:
            self.points_in_bounds.remove(pt)
        if pt in self.points:
            self.points.remove(pt)
        if pt in self.selection:
            self.selection.remove(pt)
        # self.selection.clear()
        del pt
        self.hovered_pt = None

    def select_all(self, evt):
        self.selection = self.points.copy()
    """
    INTERACTIONS
    """
    def mouse_move(self, x_px, y_px, state):
        # log.d("move")
        self.hovered_pt = None
        self.mid_pt = None
        for pt in self.points_in_bounds:            # Point
            if pt.in_rect(x_px, y_px):
                pt.hover(True)
                self.hovered_pt = pt
            else:
                pt.hover(False)
            pt.update_selected()

            if pt.on_line(x_px, y_px):
                self.mid_pt = pt

        if state == State.NONE:
            if self.mid_pt is not None:
                self.root.configure(cursor="plus")
            else:
                self.root.configure(cursor='cross')     # default
        elif state == State.MOD2:
            self.root.configure(cursor='arrow')  # default

    def click(self, x_px, y_px, state):
        if state == State.NONE:
            sel_pt = self._get_selected_pt()
            xym = self.canvas.canvas2m(x_px, y_px)
            if self.mid_pt is not None:
                x, y = self.mid_pt.midpoint_px(x_px, y_px)
                xym2 = self.canvas.canvas2m(x, y)
                pt = self.create_point(*xym2, None, None)
                if self.mid_pt.insert(pt) is not None:
                    self.selection = [pt]
                self._update_lines()
            elif self.hovered_pt is not None:                 # Hovering

                if self.hovered_pt.is_tail():
                    if sel_pt is not None:              # point is selected
                        # if sel_pt.is_head():
                        pt = self.hovered_pt.link(sel_pt)
                        if pt is not None and self.hovered_pt != sel_pt.tail():
                            self.selection = [pt]
                        else:
                            self.selection.clear()
                elif self.hovered_pt.is_head() and self.hovered_pt != sel_pt:       # not selecting self
                    if sel_pt is not None:              # point is selected
                        pt = self.hovered_pt.link(sel_pt)                           # concat and reverse
                        if pt is not None:
                            self.selection = [pt]
                self._update_lines()
            else:                                       # not hovering
                if sel_pt is not None:  # add point to chain
                    if sel_pt.is_head():
                        self.create_point(xym[0], xym[1], sel_pt, None)

                else:  # new chain
                    self.create_point(xym[0], xym[1], None, None)
                self._update_lines()

        elif state == State.MOD2:
            if self.hovered_pt is not None:
                if self.hovered_pt not in self.selection:
                    self.selection = [self.hovered_pt]
                self.click_xy[0], self.click_xy[1] = x_px, y_px
            else:
                self.selection.clear()
                self.sel_drag = False
                # self.sel[0] = x_px
                # self.sel[1] = y_px
            self.draw()
        elif state == State.MOD2 | State.SHIFT:
            if self.hovered_pt is not None:
                if self.hovered_pt not in self.selection:
                    self.selection.append(self.hovered_pt)
                self.click_xy[0], self.click_xy[1] = x_px, y_px

    def double_click(self, x_px, y_px, state):
        """"""

    def release(self, x_px, y_px, state):
        if self.sel[0] != 0 or self.sel[1] != 0 and self.sel[2] != 0 and self.sel[3] != 0:
            if self.sel[0] > self.sel[2]:
                self.sel[0], self.sel[2] = self.sel[2], self.sel[0]
            if self.sel[1] > self.sel[3]:
                self.sel[1], self.sel[3] = self.sel[3], self.sel[1]
            for pt in self.points_in_bounds:
                if pt.in_bounds(*self.sel):
                    if pt not in self.selection:
                        self.selection.append(pt)
                elif not self.sel_add:
                    if pt in self.selection:
                        self.selection.remove(pt)
                pt.update_selected()
            self.sel = [0, 0, 0, 0]
            self.sel_drag = False
            self.canvas.coords(self.sel_marq, self.sel)

    def drag(self, x_px, y_px, state):
        # log.d("drag")
        if state == State.MOD2 | State.BUTTON1 or state == State.MOD2 | State.BUTTON1 | State.SHIFT:
            if self.hovered_pt is not None:
                xy = [self.canvas.px2m(x_px - self.click_xy[0]), -self.canvas.px2m(y_px - self.click_xy[1])]
                if len(self.selection) > 0:
                    for pt in self.selection:
                        pt.move_step(*xy)
                self._update_lines()
                self.click_xy[0], self.click_xy[1] = x_px, y_px
            else:
                if not self.sel_drag:
                    self.sel[0], self.sel[1] = x_px, y_px
                    self.sel_drag = True
                self.sel[2], self.sel[3] = x_px, y_px
                self.canvas.coords(self.sel_marq, self.sel)
                if state == State.MOD2 | State.BUTTON1 | State.SHIFT:
                    self.sel_add = True

    def arrow_keys(self, x_m, y_m, state):
        if len(self.selection) > 0:
            mult = 1.0
            if state == State.MOD3_MOD4 | State.SHIFT:
                mult = 10.0
            for pt in self.selection:
                pt.move_step(x_m * mult, y_m * mult)
            self._update_lines()

    def alt_key(self, pressed: bool):
        self.keys['alt'] = pressed

    def ctrl_key(self, pressed: bool):
        self.keys['ctrl'] = pressed

    def del_key(self):
        if len(self.selection) > 0:
            for selection in self.selection:
                self.delete_point(selection)
        elif self.hovered_pt is not None:
            self.delete_point(self.hovered_pt)
        self._update_lines()

    """
    DRAWING
    """
    def draw(self):
        for pt in self.points:  # type: Point
            pt.draw()
            in_bounds = pt.in_canvas_bounds(self.canvas.WIDTH, self.canvas.HEIGHT)
            if in_bounds:
                if pt not in self.points_in_bounds:
                    self.points_in_bounds.append(pt)
            else:
                if pt in self.points_in_bounds:
                    self.points_in_bounds.remove(pt)
        self._update_lines()

    def _update_lines(self):
        paths = self.generate_paths(dtype='px')
        # print(len(paths), len(self.lines))
        d_line = len(paths) - len(self.lines)
        if d_line > 0:
            for i in range(d_line):
                line = self.canvas.create_line(0, 0, 0, 0, width=2, fill="#333")
                self.lines.append(line)
            log.d("Added %d lines" % d_line)
        elif d_line < 0:
            for i in range(abs(d_line)):
                self.canvas.delete(self.lines[-1])
                self.lines.pop(-1)
            log.d("Removed %d lines" % abs(d_line))

        for i in range(len(paths)):
            self.canvas.coords(self.lines[i], paths[i])
