import math

from base_node.networking_src.network_utils import NetworkUtils
from tools.print_utils import bcol, Log

TAG = "PenTool.Point:"
log = Log(bcol.DEFAULT, TAG)


class Point:
    DEF_OUTL = "#3c3c3c"

    DEF_FILL = "#fff"
    SEL_FILL = "#3c3c3c"
    HOV_FILL = "#9cda22"
    TAIL_FILL = "#ff0000"

    def __init__(self, pen_tool, canvas, x_m, y_m, next_pt=None, prev_pt=None, rad=10):
        from displayers.rviz.rviz_displayer import RVIZDisplayer
        from displayers.rviz.pen_tool2 import PenTool

        self.uid = NetworkUtils.random_str(3)
        self.pen_tool = pen_tool                # type: PenTool
        self.canvas = canvas                    # type: RVIZDisplayer

        self.xy = [x_m, y_m]
        self.cxy = [0, 0]

        self.r = rad
        self.rect = [0, 0, 0, 0]

        self._next = next_pt
        self._prev = prev_pt

        if self._next is not None:
            self._next._prev = self
        if self._prev is not None:
            self._prev._next = self

        self.hov = False

        self.sq = self.canvas.create_rectangle(self.rect, outline=self.DEF_OUTL, fill=self.DEF_FILL)
        self.lb_text = self.canvas.create_text(*self.cxy, text="", anchor="sw", fill="white")
        self.lb_box = self.canvas.create_rectangle(0, 0, 0, 0, fill="#333", outline="#333")
        self.lb_dist = self.canvas.create_text(0, 0, text="", anchor="s")

        self.draw()

    def __repr__(self):
        return "Pt:" + self.uid

    def raw(self):
        return repr(self._prev) + "->" + repr(self) + "->" + repr(self._next)

    # def __str__(self):
    #     pts = self.list_points()
    #     ids = [pt.uid for pt in pts]
    #     return str(ids)

    def insert(self, pt):
        """Always to the rear"""
        ret = None
        if self._prev is not None:
            pt._next = self
            pt._prev = self._prev
            pt.draw()

            self._prev._next = pt
            self._prev.draw()

            self._prev = pt
            self.draw()
            ret = self.head()
        return ret

    def link(self, pt):
        """pt will always have a None next"""
        ret = None
        if self.is_head():
            if pt.is_head():
                self._next = pt
                pt._next = self
                pt.reverse()
                ret = self.head()
            elif pt.is_tail():
                self._next = pt
                pt._prev = self
                ret = self.head()
        elif self.is_tail():
            if pt.is_head():
                self._prev = pt
                pt._next = self
                ret = self.head()
            elif pt.is_tail():
                self._prev = pt
                pt._prev = self
                self.reverse()
                ret = self.head()
        self.draw()
        return ret

    def reverse(self):
        """
        reverse order of next and prev
        typically initiated by the head
        """
        self._next, self._prev = self._prev, self._next
        if self._next is not None:
            self._next.reverse()

    def head(self):
        pt = self
        while pt._next is not None:
            if pt._next == self:
                break
            pt = pt._next
        return pt

    def tail(self):
        pt = self
        while pt._prev is not None:
            if pt._prev == self:
                break
            pt = pt._prev
        return pt

    def list_points(self):
        if self.is_head():
            pt = self
        else:
            pt = self.head()
        caller = pt
        # print(pt, pt._prev)
        ret = [pt]
        while pt._prev is not None:
            if pt._prev == caller:
                ret.insert(0, pt._prev)
                return ret
            pt = pt._prev
            ret.insert(0, pt)
        return ret

    def dist(self):
        if self._prev is not None:
            dx = self.xy[0] - self._prev.xy[0]
            dy = self.xy[1] - self._prev.xy[1]
            return math.sqrt(dx * dx + dy * dy)

    """
    DRAWING
    """

    def update_selected(self):
        if self.pen_tool.is_pt_selected(self):
            self.canvas.itemconfigure(self.sq, fill=self.SEL_FILL)
            # self._show_label()
        else:
            self.canvas.itemconfigure(self.sq, fill="")
            # self._hide_label()

    def draw(self):
        self._update_xy()
        self.canvas.coords(self.sq, *self.rect)
        self.update_selected()
        self.update_dist()
        # self._hide_label()

    def hover(self, b: bool):
        # log.d("hover:", b)
        if b != self.hov:
            if b:
                self.canvas.itemconfigure(self.sq, fill=self.HOV_FILL)
            else:
                if self.is_tail():
                    self.canvas.itemconfigure(self.sq, fill=self.TAIL_FILL)
                else:
                    self.canvas.itemconfigure(self.sq, fill=self.DEF_FILL)

            self.hov = b
        if b:
            self._show_label()
        else:
            self._hide_label()

    def move_step(self, dx_m, dy_m):
        self.xy[0] += dx_m
        self.xy[1] += dy_m
        self.draw()
        if self._next is not None:
            self._next.update_dist()
        self._show_label()

    def move(self, x_m, y_m):
        self.xy[0] = x_m
        self.xy[1] = y_m
        self.draw()
        if self._next is not None:
            self._next.update_dist()
        self._show_label()

    def delete(self):
        # if not self.is_tail() and not self.is_head():
        #     self._prev._next = self._next
        #     self._next._prev = self._prev
        # elif self.is_head():
        #     self._prev._next = None
        # elif self.is_tail():
        #     self._next._prev = None
        if self._prev is not None:
            self._prev._next = None
        if self._next is not None:
            self._next._prev = None
            self._next.update_dist()

        self.canvas.delete(self.sq)
        self.canvas.delete(self.lb_text)
        self.canvas.delete(self.lb_box)
        self.canvas.delete(self.lb_dist)

    def _update_xy(self):
        self.cxy[0] = self.xy[0] * self.canvas.px_per_m + self.canvas.offset[0]
        self.cxy[1] = -self.xy[1] * self.canvas.px_per_m + self.canvas.offset[1]

        self.rect = [self.cxy[0] - self.r / 2,
                     self.cxy[1] - self.r / 2,
                     self.cxy[0] + self.r / 2,
                     self.cxy[1] + self.r / 2]

    def update_dist(self):
        if self._prev is not None:
            dx = self.cxy[0] - self._prev.cxy[0]
            dy = self.cxy[1] - self._prev.cxy[1]

            x = self._prev.cxy[0] + dx/2
            y = self._prev.cxy[1] + dy/2

            angle = math.atan2(dy, dx) / math.pi * 180.0

            self.canvas.coords(self.lb_dist, x, y)
            self.canvas.itemconfigure(self.lb_dist, text="%.2f" % self.dist(), angle=360.0-angle)
            self.canvas.tag_lower(self.lb_dist)
        else:
            self.canvas.itemconfigure(self.lb_dist, text="")

    def _hide_label(self):
        self.canvas.itemconfigure(self.lb_text, text="")
        self.canvas.coords(self.lb_box, 0, 0, 0, 0)

    def _show_label(self):
        self.canvas.coords(self.lb_text, self.cxy[0] + 5, self.cxy[1] - 5)
        self.canvas.itemconfigure(self.lb_text, text="(%.2f, %.2f)" % (self.xy[0], self.xy[1]))
        self.canvas.coords(self.lb_box, self.canvas.bbox(self.lb_text))
        self.canvas.tag_raise(self.lb_text, self.lb_box)

    """
    CHECKS
    """

    def is_head(self):
        return self._next is None

    def is_tail(self):
        return self._prev is None

    def is_orphan(self):
        return self._prev is None and self._next is None

    def in_bounds(self, w0, h0, w1, h1):
        return w0 < self.cxy[0] < w1 and h0 < self.cxy[1] < h1

    def in_canvas_bounds(self, w, h):
        return 0 < self.cxy[0] < w and 0 < self.cxy[1] < h

    def in_rect(self, x_px, y_px):
        return self.rect[0] < x_px < self.rect[2] and self.rect[1] < y_px < self.rect[3]

    def midpoint_px(self, x_px, y_px):
        if self._prev is not None:
            dx = self.cxy[0] - self._prev.cxy[0]
            dy = self.cxy[1] - self._prev.cxy[1]

            x = self._prev.cxy[0] + dx / 2
            y = self._prev.cxy[1] + dy / 2
            return x, y
        return x_px, y_px

    def on_line(self, x_px, y_px):
        if self._prev is not None:
            dx = self.cxy[0] - self._prev.cxy[0]
            dy = self.cxy[1] - self._prev.cxy[1]

            x = self._prev.cxy[0] + dx/2
            y = self._prev.cxy[1] + dy/2

            ddx = x_px - x
            ddy = y_px - y

            return ddx * ddx + ddy * ddy < self.r * self.r
        return False
