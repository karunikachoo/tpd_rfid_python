import base64
import os
import time
from tkinter import filedialog

import numpy as np

from base_node.networking_src.network_utils import NetworkUtils
from map.map_constants import MapConst
from messages.nav_msgs import MapMessage

from PIL import Image


class MapHandler:
    def __init__(self, canvas):
        from displayers.rviz.rviz_displayer import RVIZDisplayer
        self.canvas = canvas        # type: RVIZDisplayer
        self.map = np.full((MapConst.WIDTH, MapConst.HEIGHT), MapConst.BLANK, dtype=int)

    def reset_map(self):
        self.map.fill(MapConst.BLANK)

    def get_start_index(self):
        ii = self.map.shape[0] / 2 - self.canvas.px2cell(self.canvas.offset[1])
        jj = self.map.shape[1] / 2 - self.canvas.px2cell(self.canvas.offset[0])

        return int(ii), int(jj)

    def get_start_index_for_size(self, n_row_cells, n_col_cells):
        ii = self.map.shape[0] / 2 - n_row_cells / 2
        jj = self.map.shape[1] / 2 - n_col_cells / 2

        return int(ii), int(jj)

    def _get_b64(self, ii, jj, match_value):
        i64 = self.map[ii*8:(ii+1)*8, jj*8:(jj+1)*8]
        b64 = bytearray()

        for j in range(i64.shape[1]):
            b = 0x00
            for i in range(8):
                if i64[i, j] & match_value == match_value:
                    b |= (1 << i)
                else:
                    b &= ~(1 << i)
            b64.append(b)
        return b64

    @staticmethod
    def _load_b64(ba: bytearray, value):
        if len(ba) != 8:
            print("bad b64")

        i64 = np.full((8, 8), MapConst.BLANK, dtype='uint8')

        for j in range(8):
            for i in range(8):
                if ba[j] & 1 << i != 0:
                    i64[i, j] = value
                else:
                    i64[i, j] = MapConst.BLANK
        return i64

    def export_maps(self):
        fn = filedialog.asksaveasfilename(initialdir=os.getcwd(), title="Export Map File",
                                          filetypes=(("Map File", "*.map"),))
        if ".map" not in fn:
            fn += ".map"

        name = fn.split(".map")

        for layer in MapConst.LAYERS:
            if layer in self.map & layer:
                self.export(layer, fname="%s%s.map" % (name[0], MapConst.LAYERS[layer]))
                print(MapConst.LAYERS[layer] + " exported")

    def export(self, brush_type, fname=None):
        n_cols = int(self.map.shape[1] / 8)
        n_rows = int(self.map.shape[0] / 8)

        sections = bytearray()
        sections.append(brush_type)
        sections.extend(NetworkUtils.int_to_n_bytes(4, self.map.shape[0]))
        sections.extend(NetworkUtils.int_to_n_bytes(4, self.map.shape[1]))

        for i in range(n_rows):
            si = i
            sj = 0
            data = bytearray()
            for j in range(n_cols):
                b64 = self._get_b64(i, j, brush_type)

                if sum(b64) > 0:
                    # print(b64)
                    if len(data) == 0:
                        sj = j
                    data.extend(b64)
                else:
                    if len(data) > 0:
                        section = bytearray([0xaa])
                        section.extend(NetworkUtils.int_to_n_bytes(2, si))              # b64 row
                        section.extend(NetworkUtils.int_to_n_bytes(2, sj))              # b64 column
                        section.extend(NetworkUtils.int_to_n_bytes(4, len(data)))       # len of data (n bytes to read)
                        section.extend(data)                                            # data
                        section.append(0xcc)

                        # print(section)
                        sections.extend(section)
                        data.clear()
        # print(sections)
        fn = fname
        if fn is None:
            fn = filedialog.asksaveasfilename(initialdir=os.getcwd(), title="Export Map File",
                                              filetypes=(("Map File", "*.map"),))
            if ".map" not in fn:
                fn += ".map"
        f = open(fn, 'wb')
        f.write(sections)
        f.close()

    def import_map(self):
        fns = filedialog.askopenfilenames(initialdir=os.getcwd(), title="Import Map File",
                                          filetypes=(("Map File", "*.map"),))
        for fn in fns:
            self._import_map(fn)

    def _import_map(self, fn):
        if ".map" not in fn:
            fn += ".map"
        f = open(fn, 'rb')
        sections = bytearray(f.read())
        f.close()

        h = NetworkUtils.n_bytes_to_int(4, sections[1:5])
        w = NetworkUtils.n_bytes_to_int(4, sections[5:9])

        temp_map = self._load_map(sections)

        ii, jj = self.get_start_index_for_size(*temp_map.shape)
        self.map[ii:ii+h, jj:jj+w] |= temp_map

    @staticmethod
    def _load_map(sections: bytearray):
        b = sections[0]
        h = NetworkUtils.n_bytes_to_int(4, sections[1:5])
        w = NetworkUtils.n_bytes_to_int(4, sections[5:9])

        temp_map = np.full((h, w), MapConst.BLANK, dtype='uint8')
        if sections[9] == 0xaa:  # we has data
            # if sections[8] == 0xaa:     # we has data
            # sections = sections[8:]
            sections = sections[9:]
            while 0xaa in sections:
                # print(repr(sections))
                ii = NetworkUtils.n_bytes_to_int(2, sections[1:3])
                jj = NetworkUtils.n_bytes_to_int(2, sections[3:5])
                n = NetworkUtils.n_bytes_to_int(4, sections[5:9])

                data = sections[9:9 + n]
                # print(ii, jj, n, data)
                for i in range(int(n / 8)):
                    i64 = MapHandler._load_b64(data[:8], b)
                    temp_map[ii * 8:(ii + 1) * 8, (jj + i) * 8:(jj + i + 1) * 8] = i64
                    data = data[8:]

                sections = sections[9 + n + 1:]
        return temp_map

    def save_img(self):
        fn = filedialog.asksaveasfilename(initialdir=os.getcwd(), title="Save as Image",
                                          filetypes=(("PNG File", "*.png"),))
        if ".png" not in fn:
            fn += ".png"

        im = Image.fromarray(self.map.astype('uint8'), 'P')
        im.putpalette(self.canvas.palette.value)

        im.save(fn)

    def fill(self, i, j, val):
        ii, jj = self.get_start_index()

        r, c = int(ii + i), int(jj + j)

        curr_col = self.map[r, c]
        print("Filling...")
        self._frontier_fill(r, c, curr_col, val)
        print("Done!")

    def _frontier_fill(self, i, j, prev, new):
        to_fill = set()

        to_fill.add((i, j))

        r_max = self.map.shape[0]
        c_max = self.map.shape[1]

        while len(to_fill) > 0:
            (r, c) = to_fill.pop()
            if 0 <= r < r_max and 0 <= c < c_max:
                if self.map[r, c] == prev:
                    self.map[r, c] = new

                    to_fill.add((r-1, c))
                    to_fill.add((r+1, c))
                    to_fill.add((r, c-1))
                    to_fill.add((r, c+1))

    def _flood_fill(self, i, j, prev, new):
        if i < 0 or j < 0 or i >= self.map.shape[0] or j >= self.map.shape[1]:
            return

        if self.map[i, j] != prev or self.map[i, j] == new:
            return

        # replace colour at map[i, j]
        self.map[i, j] = new

        self._flood_fill(i + 1, j, prev, new)
        self._flood_fill(i - 1, j, prev, new)
        self._flood_fill(i, j + 1, prev, new)
        self._flood_fill(i, j - 1, prev, new)

    def draw(self, i, j, val):
        ii, jj = self.get_start_index()
        self.map[int(ii + i), int(jj + j)] = val
        # print(self.map[int(ii + i), int(jj + j)])

    def crop(self):
        si, sj = self.get_start_index()
        ei = int(si + self.canvas.HEIGHT / self.canvas.cs)
        ej = int(sj + self.canvas.WIDTH / self.canvas.cs)

        return self.map[si:ei, sj:ej]

    def line(self, x0, y0, x1, y1):
        # print(x0, y0, x1, y1)
        """Bresenham's line algorithm"""
        dx = abs(x1 - x0)
        dy = abs(y1 - y0)
        x, y = x0, y0
        sx = -1 if x0 > x1 else 1
        sy = -1 if y0 > y1 else 1
        if dx > dy:
            err = dx / 2.0
            while x != x1:
                self.draw(y, x, MapConst.WALL)
                err -= dy
                if err < 0:
                    y += sy
                    err += dx
                x += sx
        else:
            err = dy / 2.0
            while y != y1:
                self.draw(y, x, MapConst.WALL)
                err -= dx
                if err < 0:
                    x += sx
                    err += dy
                y += sy
        self.draw(y, x, 0)

    def on_map_recv(self, msg: MapMessage):
        self.reset_map()
        for layer in msg.layers:
            ba = bytearray(base64.b64decode(layer))
            temp_map = self._load_map(ba)
            if temp_map.shape[0] > self.map.shape[0] or temp_map.shape[1] > self.map.shape[1]:
                print("Map replaced")
                self.map = temp_map
            else:
                print("Map Updated")
                ii, jj = self.get_start_index_for_size(*temp_map.shape)
                self.map[ii:ii + temp_map.shape[0], jj:jj + temp_map.shape[1]] |= temp_map

