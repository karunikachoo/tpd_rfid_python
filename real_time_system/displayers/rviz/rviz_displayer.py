import math
import sys
import tkinter as tk
from typing import Iterable, Union

from PIL import Image, ImageTk
import numpy as np

from displayers.rviz.grids import Grids
from displayers.rviz.map_handler import MapHandler
from displayers.rviz.palette import Palette
from displayers.rviz.pen_tool2 import PenTool, State
from map.map_constants import MapConst

from tools.print_utils import bcol, Log
from tools.tk_utils import TkUtils

TAG = "RVIZDisplayer:"
log = Log(bcol.DEFAULT, TAG)

"""
TODO:
Urgent:
- [DONE] Add pixel map export function
- [DONE] Pixel map compression for efficient OTA transfer
    - 2048 x 2048 cell map ~~ +/- 50m -> 4 MB 
    - 8 x 1 -> 1 byte compression -> 500 kb
    - b64 compression -> sub 100 kb 
- Build a map + pose processor node to compute the correction from the nav frame to the map frame
- Migrate from m -> cm
- Implement correction algorithms

Optional:
- Brush sizes
- Layer system for multiple floors
- Contextual layer with information such as location of stairs to let the system know the person might be
  switching floors
"""


class RVIZDisplayer(tk.Canvas):
    WIDTH = 600
    HEIGHT = 600

    AXIS_COLOR = ["#cc0c0c", "#74c934"]
    AXIS_LEN = 100

    GREY = "#c9c9c9"

    PEN_KEY = 'p'
    POS_RES_KEY = 'x'
    PAN_KEY = 'v'

    ERASER_KEY = 'e'
    BRUSH_KEY = 'b'

    CIRCLE_KEY = 'l'
    AP_BRUSH_KEY = 'a'
    GATEWAY_KEY = 'g'
    STAIRS_KEY = 's'
    WALK_KEY = 'w'

    TOOL_KEYS = {
        PEN_KEY: 'Pen Tool',
        POS_RES_KEY: 'Position Reset Tool',
        PAN_KEY: 'Move Tool',
        ERASER_KEY: 'Eraser',
        BRUSH_KEY: 'Brush',
        CIRCLE_KEY: 'Circle Tool',
        AP_BRUSH_KEY: 'AP Brush',
        GATEWAY_KEY: 'Gateway Brush',
        STAIRS_KEY: 'Stairs Brush',
        WALK_KEY: 'Walkable Brush',
    }

    N_PARTICLES = 500

    def __init__(self, parent, root):
        tk.Canvas.__init__(self, master=parent, width=self.WIDTH, height=self.HEIGHT)
        self.parent = parent

        self.px_per_m = 100  # px
        self.m_per_cell = 0.05  # m
        self.cs = self.m2px(self.m_per_cell)

        self.offset = [self.WIDTH / 2, self.HEIGHT / 2]  # px

        self.xy = [0, 0]
        self.prev_offset = [0.0, 0.0]

        self.palette = Palette(8)
        self.palette.generate_custom_palette()

        self.map_handler = MapHandler(self)

        self.map_data = np.full((self.WIDTH, self.HEIGHT), 255, dtype=int)
        # self.map_data = np.array(np.random.random((600, 600)) * 255, dtype=int)
        self.im = None
        self.photo = None
        self.map_img = None
        self.draw_map()

        self.grids = Grids(self, self.WIDTH, self.HEIGHT, color=self.GREY)
        self.grids.update(self.px_per_m, self.offset)

        self.pen_tool = PenTool(self, root, self.map_handler)

        self.tool_key = 'v'
        self.status_label = self.create_text(10, self.HEIGHT - 10, text="", anchor='sw', fill='grey')
        self.update_label()

        self.path_x = [0, 0]
        self.path_y = [0, 0]
        self.path_line = self.create_line(0, 0, 0, 0, fill='red', width=2)

        self.ang_x = [0, 0]
        self.ang_y = [0, 0]
        self.angs = []

        self.step_x = [0, 0]
        self.step_y = [0, 0]
        self.step_line = self.create_line(0, 0, 0, 0, fill="red")

        self.xi_theta = [0]
        self.xi_x = [0, 0]
        self.xi_y = [0, 0]
        self.xi_line = self.create_line(0, 0, 0, 0, fill="black")

        self.particle_x = []
        self.particle_y = []
        self.particles = []
        self.unused_particles = [self.create_line(0, 0, 0, 0, fill='orange') for _ in range(self.N_PARTICLES)]

        self.bind('<Motion>', self.handle_mouse)
        TkUtils.bind(self, '<Button-1>', self.handle_click)
        TkUtils.bind(self, '<ButtonRelease-1>', self.handle_release)
        TkUtils.bind(self, '<Double-Button-1>', self.handle_dbl_click)
        self.bind('<B1-Motion>', self.handle_drag)
        TkUtils.bind(self, '<MouseWheel>', self.scroll)

        self.focus_set()

        self.after(40, self.draw)

    def draw(self):
        self.update_map()
        # self.update_grids()
        self.after(40, self.draw)

    def update_map(self):
        data = self.map_handler.crop()
        im = Image.fromarray(data.astype('uint8'), 'P')
        im.putpalette(self.palette.value)

        nw = data.shape[1] * self.cs
        nh = data.shape[0] * self.cs

        if nw <= 0:
            nw = 1
        if nh <= 0:
            nh = 1

        self.im = im.resize((int(nw), int(nh)), Image.NEAREST)

        self.photo = ImageTk.PhotoImage(image=self.im)
        if self.map_img is not None:
            self.itemconfigure(self.map_img, image=self.photo)
        else:
            self.map_img = self.create_image(0, 0, image=self.photo, anchor=tk.NW)
        self.tag_lower(self.map_img)

    def draw_map(self):

        self.im = Image.fromarray(self.map_data.astype('uint8'), 'P')
        self.im.putpalette(self.palette.value)
        self.photo = ImageTk.PhotoImage(image=self.im)
        if self.map_img is not None:
            self.itemconfigure(self.map_img, image=self.photo)
        else:
            self.map_img = self.create_image(0, 0, image=self.photo, anchor=tk.NW)
        self.tag_lower(self.map_img)

    def set_particles(self, x: list, y: list):
        self.particle_x = x
        self.particle_y = y

        self.update_particles()

    def update_particles(self):
        coords = self.coords
        x = self.particle_x[:self.N_PARTICLES]
        y = self.particle_y[:self.N_PARTICLES]
        particles = self.particles
        unused = self.unused_particles

        ppop = particles.pop
        upop = unused.pop
        pappend = particles.append
        uappend = unused.append

        diff = len(x) - len(particles)
        if diff > 0 & diff <= self.N_PARTICLES:    # add particles
            for i in range(diff):
                pappend(upop())
        elif diff < 0 & abs(diff) <= len(particles):
            for i in range(abs(diff)):
                p = ppop()
                coords(p, 0, 0, 0)
                uappend(p)

        for i in range(len(x)):
            xi = x[i]
            yi = y[i]
            coords(particles[i], xi, yi, xi+1, yi)

    def set_paths(self, x, y, ang_x, ang_y):
        self.path_x = x
        self.path_y = y
        self.ang_x = ang_x
        self.ang_y = ang_y

    def update_paths(self):
        coords = []
        for i in range(len(self.path_x)):
            px = self.m2px(self.path_x[i]) + self.offset[0]
            py = -self.m2px(self.path_y[i]) + self.offset[1]
            coords.append(px)
            coords.append(py)

        # print(coords)
        self.coords(self.path_line, *coords)

    def set_steps(self, r, d_theta, xi, xy):
        theta = (self.xi_theta[-1] + d_theta)

        x = self.xi_x[-1] + r * math.cos(theta - xi)
        y = self.xi_y[-1] + r * math.sin(theta - xi)

        self.xi_theta.append(theta)
        self.xi_x.append(x)
        self.xi_y.append(y)

        self.step_x.append(xy[0])
        self.step_y.append(xy[1])
        #
        # xn, yn = self._rotate_about_origin(0.5, 0, theta)
        # xx = [x, x + xn]
        # yy = [y, y + yn]
        # coords = []
        # for i in range(2):
        #     coords.append(self.m2px(xx[i]) + self.offset[0])
        #     coords.append(-self.m2px(yy[i]) + self.offset[1])
        # line = self.create_line(coords, fill="pink")
        # self.step_head.append(line)

    def update_steps(self):
        coords = []
        for i in range(len(self.step_x)):
            coords.append(self.m2px(self.step_x[i]) + self.offset[0])
            coords.append(-self.m2px(self.step_y[i]) + self.offset[1])

        self.coords(self.step_line, *coords)

        coords = []
        for i in range(len(self.xi_x)):
            coords.append(self.m2px(self.xi_x[i]) + self.offset[0])
            coords.append(-self.m2px(self.xi_y[i]) + self.offset[1])
        self.coords(self.xi_line, *coords)

    def update_grids(self):
        self.grids.update(self.px_per_m, self.offset)
        self.pen_tool.draw()

    def update_label(self):
        text = "Tool: %s | x: %.2f y: %.2f" % (self.TOOL_KEYS[self.tool_key], *self.canvas2m(*self.xy))
        self.itemconfigure(self.status_label, text=text)

    def update_cs(self):
        self.cs = self.m2px(self.m_per_cell)

    @staticmethod
    def _rotate_about_origin(x, y, rad):
        c = math.cos(rad)
        s = math.sin(rad)

        xn = x * c - y * s
        yn = x * s + y * c
        return xn, yn

    """
    MOUSE/KB
    """

    def handle_mouse(self, evt):
        # if self.tool_key == 'x' or self.tool_key == 'z':
        self.xy[0] = evt.x
        self.xy[1] = evt.y

        self.update_label()
        if self.tool_key == self.PEN_KEY:
            # print(evt.state, repr(evt))
            self.pen_tool.mouse_move(*self.xy, evt.state)

    def scroll(self, evt):
        # print(repr(evt), evt.state)
        if evt.state == 4:  # CTRL + up down
            self.px_per_m += evt.delta
            if self.px_per_m < 1:
                self.px_per_m = 1
            elif self.px_per_m > 300:
                self.px_per_m = 300

            self.parent.sc.set(self.px_per_m)
            self.update_cs()
        elif evt.state == 1:  # SHIFT (LR)
            self.offset[0] += int(evt.delta * 3)
        elif evt.state == 0:
            self.offset[1] += int(evt.delta * 3)

        self.update_grids()
        self.update_paths()
        self.update_steps()

    def handle_click(self, evt):
        if evt.widget != self:
            return
        self.xy[0] = evt.x
        self.xy[1] = evt.y
        self.prev_offset[0] = self.offset[0]
        self.prev_offset[1] = self.offset[1]

        if self.tool_key == self.PEN_KEY:
            print(evt)
            self.pen_tool.click(*self.xy, evt.state)
        elif self.tool_key == self.ERASER_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.BLANK)
        elif self.tool_key == self.BRUSH_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.WALL)
        elif self.tool_key == self.AP_BRUSH_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.AP)
        elif self.tool_key == self.GATEWAY_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.GATEWAY)
        elif self.tool_key == self.STAIRS_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.STAIRS)
        elif self.tool_key == self.WALK_KEY:
            if evt.state & State.CTRL:
                self.map_handler.fill(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.WALKABLE)
            else:
                self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.WALKABLE)

    def handle_release(self, evt):
        if self.tool_key == self.PEN_KEY:
            self.pen_tool.release(evt.x, evt.y, evt.state)

    def handle_dbl_click(self, evt):
        self.xy[0] = evt.x
        self.xy[1] = evt.y
        if self.tool_key == self.PEN_KEY:
            self.pen_tool.double_click(*self.xy, evt.state)

    def handle_drag(self, evt):
        if self.tool_key == self.PAN_KEY:
            self.offset[0] = self.prev_offset[0] + evt.x - self.xy[0]
            self.offset[1] = self.prev_offset[1] + evt.y - self.xy[1]
            self.update_grids()
            self.update_paths()
            self.update_steps()
            # log.d(self.offset)
        elif self.tool_key == self.PEN_KEY:
            # print(repr(evt), evt.state)
            self.xy[0] = evt.x
            self.xy[1] = evt.y
            self.update_label()
            self.pen_tool.drag(evt.x, evt.y, evt.state)
        elif self.tool_key == self.ERASER_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.BLANK)
        elif self.tool_key == self.BRUSH_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.WALL)
        elif self.tool_key == self.AP_BRUSH_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.AP)
        elif self.tool_key == self.GATEWAY_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.GATEWAY)
        elif self.tool_key == self.STAIRS_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.STAIRS)
        elif self.tool_key == self.WALK_KEY:
            self.map_handler.draw(self.px2cell(evt.y), self.px2cell(evt.x), MapConst.WALKABLE)

    def handle_tool_keys(self, evt):
        if evt.keysym in self.TOOL_KEYS:
            self.tool_key = evt.keysym

        self.update_label()

    def handle_alt_key(self, evt):
        if self.tool_key == self.PEN_KEY:
            if evt.type == tk.EventType.KeyPress:
                self.pen_tool.alt_key(True)
            elif evt.type == tk.EventType.KeyRelease:
                self.pen_tool.alt_key(False)

    def handle_ctrl_key(self, evt):
        if self.tool_key == self.PEN_KEY:
            if evt.type == tk.EventType.KeyPress:
                self.pen_tool.ctrl_key(True)
            elif evt.type == tk.EventType.KeyRelease:
                self.pen_tool.ctrl_key(False)

    def handle_del(self, evt):
        if self.tool_key == self.PEN_KEY:
            self.pen_tool.del_key()
            # self.pen_tool.delete_pt()

    def handle_arrow_keys(self, evt):
        if self.tool_key == self.PAN_KEY:
            val = [0, 0]
            if str(evt.keysym) == "Up":
                val[1] = 10
            elif str(evt.keysym) == "Down":
                val[1] = -10
            elif str(evt.keysym) == "Left":
                val[0] = 10
            elif str(evt.keysym) == "Right":
                val[0] = -10

            self.offset[0] += val[0]
            self.offset[1] += val[1]
            self.update_grids()
            self.update_paths()
            self.update_steps()
        elif self.tool_key == self.PEN_KEY:
            # print(repr(evt))
            if str(evt.keysym) == "Up":
                self.pen_tool.arrow_keys(0, 0.01, evt.state)
            elif str(evt.keysym) == "Down":
                self.pen_tool.arrow_keys(0, -0.01, evt.state)
            elif str(evt.keysym) == "Left":
                self.pen_tool.arrow_keys(-0.01, 0, evt.state)
            elif str(evt.keysym) == "Right":
                self.pen_tool.arrow_keys(0.01, 0, evt.state)

    """
    CONVERSION
    """

    def m2px(self, m):
        return float(m) * self.px_per_m

    def px2m(self, p):
        return float(p) / float(self.px_per_m)

    def px2cell(self, p):
        return int(p / self.cs)

    def cell2px(self, c):
        return int(c * self.cs)

    def cell2m(self, c):
        return float(c) * self.m_per_cell

    def m2cell(self, m):
        return float(m) / self.m_per_cell

    def canvas2m(self, px_x, px_y, digits=2) -> Iterable[Union[float, float]]:  # canvas xy to m
        return round(self.px2m(px_x - self.offset[0]), digits), round(-self.px2m(px_y - self.offset[1]), digits)


class RVIZ(tk.Frame):
    def __init__(self, master, root, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        self.sc = tk.Scale(self, from_=0, to=300, orient=tk.HORIZONTAL)
        self.sc.grid(row=0, column=0, columnspan=5, sticky='we')
        self.sc.bind('<ButtonRelease-1>', self.zoom)

        # self.zoom_set = tk.Button(self, text="Set", command=self.set_zoom, width=1)
        # self.zoom_set.grid(row=0, column=5, sticky='we')

        self.disp = RVIZDisplayer(self, root=root)
        self.disp.grid(row=1, column=0, columnspan=5, sticky='wesn')
        self.sc.set(self.disp.px_per_m)

        self.save_vec = tk.Button(self, text="Save Vector", command=self.disp.pen_tool.save_vectors)
        self.save_vec.grid(row=2, column=0, sticky="we")

        self.load_vec = tk.Button(self, text="Load Vector", command=self.disp.pen_tool.load_vectors)
        self.load_vec.grid(row=3, column=0, sticky="we")

        self.raster = tk.Button(self, text="Raster Paths", command=self.disp.pen_tool.raster_paths)
        self.raster.grid(row=2, column=1, sticky="we")

        self.reset = tk.Button(self, text="Clear Raster", command=self.disp.map_handler.reset_map)
        self.reset.grid(row=3, column=1, sticky="we")

        self.export_map = tk.Button(self, text="Export Map", command=self.disp.map_handler.export_maps)
        self.export_map.grid(row=2, column=2, sticky='we')

        self.import_map = tk.Button(self, text="Import Map", command=self.disp.map_handler.import_map)
        self.import_map.grid(row=3, column=2, sticky='we')

        self.save_as_image = tk.Button(self, text="Save Image", command=self.disp.map_handler.save_img)
        self.save_as_image.grid(row=2, column=3, sticky='we')

        root.bind('<Up>', self.disp.handle_arrow_keys)
        root.bind('<Left>', self.disp.handle_arrow_keys)
        root.bind('<Down>', self.disp.handle_arrow_keys)
        root.bind('<Right>', self.disp.handle_arrow_keys)
        root.bind(self.disp.POS_RES_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.PEN_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.PAN_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.ERASER_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.BRUSH_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.AP_BRUSH_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.GATEWAY_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.STAIRS_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.CIRCLE_KEY, self.disp.handle_tool_keys)
        root.bind(self.disp.WALK_KEY, self.disp.handle_tool_keys)
        root.bind('<Delete>', self.disp.handle_del)
        root.bind('<BackSpace>', self.disp.handle_del)
        root.bind('<Alt_L>', self.disp.handle_alt_key)
        root.bind('<Control_L>', self.disp.handle_ctrl_key)
        root.bind('<Control-a>', self.disp.pen_tool.select_all)
        root.bind('<KeyRelease>', self.key_release)

        self.grid_columnconfigure(4, weight=0)

    def key_release(self, evt):
        # print(evt.__dict__)
        if evt.keysym == "Alt_L":
            self.disp.handle_alt_key(evt)
        elif evt.keysym == "Control_L":
            self.disp.handle_ctrl_key(evt)

    def zoom(self, evt):
        self.disp.px_per_m = int(self.sc.get())
        self.disp.update_cs()
        self.disp.update_grids()
        self.disp.update_paths()
        self.disp.update_steps()
