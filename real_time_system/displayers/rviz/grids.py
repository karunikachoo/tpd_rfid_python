import math
import tkinter as tk

"""
TODO: add grid numbers
"""


class Grids:
    def __init__(self, canvas: tk.Canvas, w, h, color='#c9c9c9'):
        self.c = canvas
        self.w = w
        self.h = h
        self.color = color

        self.rows = []
        self.cols = []

        self.cs_x = self.c.create_line(0, 0, 0, self.h, fill="#333", width=2)
        self.cs_y = self.c.create_line(0, 0, self.w, 0, fill="#333", width=2)

    def update(self, px_p_m, offset: list):
        n_cols = int(math.ceil(self.w / px_p_m))
        n_rows = int(math.ceil(self.h / px_p_m))

        d_rows = n_rows - len(self.rows)
        d_cols = n_cols - len(self.cols)

        if d_rows > 0:
            for i in range(d_rows):
                line = self.c.create_line(0, 0, 0, 0, fill=self.color)
                self.rows.append(line)
        elif d_rows < 0:
            for i in range(abs(d_rows)):
                self.c.delete(self.rows[-1])
                self.rows.pop(-1)

        if d_cols > 0:
            for i in range(d_cols):
                line = self.c.create_line(0, 0, 0, 0, fill=self.color)
                self.cols.append(line)
        elif d_cols < 0:
            for i in range(abs(d_cols)):
                self.c.delete(self.cols[-1])
                self.cols.pop(-1)

        x_off = offset[0] % px_p_m
        y_off = offset[1] % px_p_m

        for i in range(len(self.rows)):
            y = i * px_p_m + y_off
            c = [0, y, self.w, y]
            self.c.coords(self.rows[i], c)
            # self.c.tag_lower(self.rows[i])

        for i in range(len(self.cols)):
            x = i * px_p_m + x_off
            c = [x, 0, x, self.h]
            self.c.coords(self.cols[i], c)
            # self.c.tag_lower(self.cols[i])

        self.c.coords(self.cs_x, [offset[0], 0, offset[0], self.h])
        self.c.coords(self.cs_y, [0, offset[1], self.w, offset[1]])
