import numpy as np

from map.map_constants import MapConst


class Palette:
    BIT_SPLIT = [3, 3, 2]

    def __init__(self, bit_depth):
        self.bit_depth = bit_depth
        self.value = []

    def set_color(self, index, color):
        if index >= 2**self.bit_depth or index < 0:
            raise(ValueError('Incorrect Value: Must be between 0 an %d for a %d-bit colour' % (2**self.bit_depth - 1, self.bit_depth)))

        i = index * 3
        self.value[i] = color[0]
        self.value[i + 1] = color[1]
        self.value[i + 2] = color[2]

    def generate_custom_palette(self):
        arr = np.full((2**self.bit_depth, 3), 255, dtype=int)
        arr[MapConst.WALL] = np.asarray([0, 0, 0], dtype=int)           # WALL
        arr[MapConst.AP] = np.asarray([224, 159, 10], dtype=int)
        arr[MapConst.GATEWAY] = np.asarray([46, 196, 182], dtype=int)
        arr[MapConst.STAIRS] = np.asarray([219, 175, 193], dtype=int)
        arr[MapConst.WALKABLE] = np.asarray([176, 208, 211], dtype=int)


        self.value = arr.reshape((1, -1)).tolist()[0]

    def create_costmap_2d_palette(self):
        arr = np.full((2**self.bit_depth, 3), 240, dtype=int)

        arr[254] = np.asarray([234, 131, 146], dtype=int)

        for n in range(128, 254):
            arr[n] = np.asarray([124, 124, n], dtype=int)
        self.value = arr.reshape((1, -1)).tolist()[0]
