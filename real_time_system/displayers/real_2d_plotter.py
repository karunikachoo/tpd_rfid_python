import collections
import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import matplotlib.animation as animation
# from multiprocessing import Process


class LineObj:
    def __init__(self, line, limit):
        self.line = line
        self.can_update = True
        self.x = collections.deque([], limit)
        self.y = collections.deque([], limit)

    def extend(self, x: list, y: list):
        self.x.extend(x)
        self.y.extend(y)

        self.can_update = True

    def push(self, x, y):
        self.x.append(x)
        self.y.append(y)

        self.can_update = True


class Plotter2D(tk.Toplevel):
    def __init__(self, master, title="2DPlotter", marker=None, linestyle=None, limit=500):
        tk.Toplevel.__init__(self, master)
        self.title(title)
        self.marker = marker
        self.linestyle = linestyle
        self.limit = limit

        self.lines = []

        self.can_update = True

        self.fig = plt.figure()
        self.ax = self.fig.gca()

        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        # print(self.line)
        self.ani = animation.FuncAnimation(self.fig, self.draw_plot, interval=50)

    def add_line(self, alpha=1.0, label=""):
        line = self.ax.plot([0], [0], alpha=alpha, label=label)[0]
        o = LineObj(line, self.limit)

        self.lines.append(o)
        self.ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=len(self.lines))

        return o

    def draw_plot(self, ii):
        # update = []

        xlims = []
        ylims = []

        for line in self.lines:
            if len(line.x) > 0 and line.can_update:
                # print(self.line)
                line.line.set_data(list(line.x), list(line.y))
                line.can_update = False

                xlim, ylim = self.compute_lims(line.x, line.y)
                xlims.extend(xlim)
                ylims.extend(ylim)

                # update.append(line.line)

        if len(xlims) > 0:
            self.ax.set_xlim(min(xlims), max(xlims))
            self.ax.set_ylim(min(ylims), max(ylims))

        # return update
        # plt.draw()
        # plt.pause(0.001)

    @staticmethod
    def compute_lims(x, y):
        try:
            mx = [min(x), max(x) + 0.01]
            my = [min(y), max(y) + 0.01]
            return mx, my
        except Exception as e:
            print(e)
            print(x,y)

        return [0, 0.01], [0, 0.01]
