import time

from map.map_constants import MapConst
from map.universal_map_handler import UniversalMapHandler
from messages.nav_msgs import MapMessage, PointRadius2DMessage
import numpy as np
import math
import collections

from particle_filter.pf_classes import Particle, StepEvent, Gateway
from pose_correction.correction_classes import Constraint

from tools.print_utils import bcol, Log

TAG = "ParticleFilter:"
log = Log(bcol.DEFAULT, TAG)


class ParticleFilter:
    DIST_STDEV = 0.1  # m
    HEAD_STDEV = 0.6  # deg

    UNKNOWN_HEADING_REPEATS = 3

    def __init__(self):
        self.map_handler = UniversalMapHandler()

        self.walkable = []
        self.unused_pts = []
        self.pts = []

        self.step_history = collections.deque([], maxlen=10)
        self.gateway_buffer = collections.deque([], maxlen=10)

        self.initialised = False

    def on_map_recv(self, msg: MapMessage):
        self.map_handler.on_map_recv(msg)

        self.walkable = np.argwhere(self.map_handler.map > MapConst.WALL)

        if not self.initialised:
            self.initialised = True
            self.generate_uniform_walkable_points(self.UNKNOWN_HEADING_REPEATS)

    def on_step_recv(self, ts, r, d_theta, xi):
        d = len(self.pts)
        log.d(ts, r, d_theta, xi)
        s = StepEvent()
        s.ts = ts
        s.r = r
        s.d_theta = d_theta
        s.xi = xi

        self.step_history.append(s)
        self._gateway_processing()

        self._step_propagation(s)
        self._resample_particles()
        log.d(d, "->", len(self.pts))

    def _add_particle(self, x, y, i=None, j=None, heading=None, n=1):
        if i is None or j is None:
            i, j = self.map_handler.xy2map(x, y)

        for k in range(n):
            if len(self.unused_pts) > 0:
                p = self.unused_pts[0]
                self.unused_pts.remove(p)
            else:
                p = Particle()
            p.x = x
            p.y = y
            p.h = (math.pi * 2 / n * k) if heading is None else heading
            p.i = i
            p.j = j

            self.pts.append(p)

    def _resample_particles(self):
        """systematic resampling"""
        new_samples = []

        for j in range(5):
            try:
                i = np.random.randint(0, len(self.pts))

                draw = self.pts[i:]
                draw.extend(self.pts[:i])
                new_samples.extend([draw[k] for k in range(0, len(draw), 10)])
            except Exception as e:
                log.e(e, len(self.pts))

        self.pts = new_samples

    def _step_propagation(self, s: StepEvent):
        t = time.time()
        r_prime = s.r + self.pull_dist()
        d_theta_prime = s.d_theta + self.pull_head()

        recycle = []

        t2 = time.time()
        for pt in self.pts:         # type: Particle
            t0 = time.time()
            theta = pt.h + d_theta_prime
            x = pt.x + r_prime * math.cos(theta - s.xi)
            y = pt.y + r_prime * math.sin(theta - s.xi)

            ii, jj = self.map_handler.xy2map(x, y)

            t1 = time.time()
            if self.map_handler.wall_check(pt.i, pt.j, ii, jj):
                # Has wall
                recycle.append(pt)
            else:
                pt.h = theta
                pt.x = x
                pt.y = y
                pt.i = ii
                pt.j = jj
            print("t1:", time.time() - t1, "t0:", t1-t0)
        print("t2:", time.time()-t2)

        t3 = time.time()
        for pt in recycle:
            self.pts.remove(pt)
            self.unused_pts.append(pt)
        print("t3", time.time()-t3)
        log.w("_step_propagation:", time.time() - t)

    def _gateway_processing(self):
        delete = []

        if len(self.gateway_buffer) > 0:
            for i in range(len(self.gateway_buffer)):
                g = self.gateway_buffer[i]      # type: Gateway

                if g.ts > self.step_history[-1].ts:
                    log.d("%30s:" % g.source_id, "Gateway arrived after step/early. Not bounded, checking others...")
                    continue
                if self.step_history[-2].ts <= g.ts < self.step_history[-1].ts:
                    log.d("%30s:" % g.source_id, "Gateway bounded. Processing gateway...")
                    """ add a bunch of points near the gateway """
                    self.add_gateway_samples(g.xy[0], g.xy[1], g.radius)
                    delete.append(g)
                else:
                    log.e("%30s:" % g.source_id, "Gateway arrived too late. Deleting...")
                    delete.append(g)

        for g in delete:
            self.gateway_buffer.remove(g)

    def add_gateway_samples(self, x, y, r):
        n = self.map_handler.m2cell(r) * 2
        stdev = r / 3
        points = self._2d_normal_dist(x, y, stdev, n)

        t = time.time()
        for i in range(n):
            for j in range(n):
                pt = points[i, j]

                self._add_particle(pt[0], pt[1], heading=None, n=ParticleFilter.UNKNOWN_HEADING_REPEATS * 30)
        log.w("add_gateway_samples:", time.time()-t)

    @staticmethod
    def _2d_normal_dist(x, y, stdev, n):
        return np.random.multivariate_normal([x, y], [[stdev, 0], [0, stdev]], (n, n))

    def on_gateway_recv(self, msg: PointRadius2DMessage):
        if self.map_handler.is_current_map(msg.building_id, msg.floor, msg.map_id):
            c = Gateway(msg.source_id, msg.ts, msg.point[:2], msg.radius)
            self.gateway_buffer.append(c)

    @staticmethod
    def pull_dist():
        return np.random.normal(0.0, ParticleFilter.DIST_STDEV)

    @staticmethod
    def pull_head(deg=False):
        ang = np.random.normal(0.0, ParticleFilter.HEAD_STDEV)
        if deg:
            return ang
        else:
            return ang / 180.0 * math.pi

    def generate_uniform_walkable_points(self, particles_per_cell):
        len1 = len(self.pts)
        for cell in self.walkable:
            i, j = cell

            x, y = self.map_handler.map2xy(i, j)

            self._add_particle(x, y, i=i, j=j, heading=None, n=ParticleFilter.UNKNOWN_HEADING_REPEATS)

        log.d(len(self.pts) - len1, "particles generated")





