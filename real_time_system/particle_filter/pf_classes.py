class Particle:
    __slots__ = ["x", "y", "h", "i", "j"]

    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.h = 0.0
        self.i = 0
        self.j = 0

    def __repr__(self):
        return "<Particle %.3f:%.3f | %d:%d>" % (self.x, self.y, self.i, self.j)


class StepEvent:
    __slots__ = ["ts", "r", "d_theta", "xi"]

    def __init__(self):
        self.ts = 0.0
        self.r = 0.0
        self.d_theta = 0.0
        self.xi = 0.0


class Gateway:
    __slots__ = ["source_id", "ts", "xy", "radius"]

    def __init__(self, source_id, ts: float, xy: list, radius: float):
        self.source_id = source_id
        self.ts = ts
        self.xy = xy
        self.radius = radius
