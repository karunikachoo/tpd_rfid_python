import json
import os
import sys
import json
import time
import tkinter as tk
from tkinter import filedialog

from base_node.node import Node
from messages.nav_msgs import NotifyMessage
from messages.sensor_msgs import ImuMessage


class Gateway(tk.Frame):
    def __init__(self, parent, on_click, **kwargs):
        tk.Frame.__init__(self, master=parent, **kwargs)
        self.parent = parent
        self.cb = on_click

        self.id = ""
        self.building_id = ""
        self.floor = ""
        self.map_id = ""
        self.point = []
        self.radius = 0.0

        self.title = tk.Label(self, text="", font='Helvetica 12 bold')
        self.title.grid(row=0, column=0, sticky='w', pady=(4, 0))

        self.map = tk.Label(self, text="")
        self.map.grid(row=1, column=0, sticky='w')

        self.loc = tk.Label(self, text="")
        self.loc.grid(row=2, column=0, sticky='ws')

        self.btn = tk.Button(self, text="Send", command=self.click, height=2, width=10)
        self.btn.grid(row=0, column=1, sticky='es', padx=(2, 0), rowspan=2)

        tk.Grid.columnconfigure(self, 0, weight=1)
        tk.Grid.columnconfigure(self, 1, weight=1)

    def update_label(self):
        self.title.configure(text="%s" % self.id)
        self.map.configure(text="%s : %s : %s" % (self.building_id, self.floor, self.map_id))
        self.loc.configure(text="%.2f : %.2f @ %.2f" % (self.point[0], self.point[1], self.radius))

    def click(self):
        self.cb(self.id)

    def load_config(self, _dict):
        self.id = _dict['id']
        self.building_id = _dict['building_id']
        self.floor = _dict['floor']
        self.map_id = _dict['map_id']
        self.point = _dict['point']
        self.radius = _dict['radius']
        self.update_label()


class GatewayProcessorPanelNode(tk.Frame):
    def __init__(self, parent, config, **kwargs):
        tk.Frame.__init__(self, master=parent, **kwargs)

        self.pack()

        self.config = self.load_config(config)
        self.gateways = []

        self.parse_gateways()

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pub = self.node.create_publisher('gateway/notifications', NotifyMessage)
        self.sub = self.node.subscribe('imu_raw', ImuMessage, self._on_imu_recv)

        self.load_sim_btn = tk.Button(self, text="Load Sim", command=self._load_gateway_sim)
        self.load_sim_btn.grid(row=0, column=0)

        self.sim_label = tk.Label(self, text="")
        self.sim_label.grid(row=0, column=1)

        self.sim_loaded = False
        self.gateway_triggers = {}

        self.node_loop()

    def parse_gateways(self):
        for gateway_id in self.config['gateways']:
            g = Gateway(self, self.on_click)
            g.load_config(self.config['gateways'][gateway_id])
            g.grid(row=len(self.gateways) + 1, column=0, sticky='we', columnspan=2)

            self.gateways.append(g)
        tk.Grid.columnconfigure(self, 0, weight=1)

    def on_click(self, uid):
        msg = NotifyMessage()
        msg.ts = time.time()
        msg.source_id = uid
        print(uid)
        self.pub.publish(msg)

    def _on_imu_recv(self, msg: ImuMessage):
        if self.sim_loaded:
            ms = int(msg.ms)
            if ms in self.gateway_triggers:
                self.on_click(self.gateway_triggers[ms])

    def _load_gateway_sim(self):
        fn = filedialog.askopenfilename(initialdir=os.getcwd(), title="Import Sim File",
                                        filetypes=(("Sim File", "*.sim"), ("Text File", "*.txt"),))

        if fn != "" and fn is not None:
            if '.sim' not in fn:
                fn += '.sim'
            f = open(fn, 'r')
            lines = f.readlines()
            f.close()

            for line in lines:
                data = line.split(",")
                ms = int(data[0].strip())
                uid = str(data[1].strip())

                self.gateway_triggers[ms] = uid
            self.sim_loaded = True
            print("%s loaded" % fn)

            name = fn.split("/")[-1]
            self.sim_label.config(text=name)
            # print(self.gateway_triggers)

    @staticmethod
    def load_config(config):
        f = open(config, 'r')
        config = json.loads(f.read())
        f.close()
        return config

    def node_loop(self):
        self.node.sleep(0.1)
        self.after(100, self.node_loop)


if __name__ == "__main__":
    p = None
    try:
        root = tk.Tk()
        root.title("Gateway Simulator")
        p = GatewayProcessorPanelNode(root, "gateway/gateway_config.json")
        root.mainloop()

    except KeyboardInterrupt:
        p.node.alive = False
        os._exit(0)