class MapConst:
    BLANK = 0
    WALL = 1
    AP = 2**1        # 2
    GATEWAY = 2**2   # 4
    STAIRS = 2**3    # 8
    WALKABLE = 2**4

    LAYERS = {
        WALL: ".wall",
        AP: ".ap",
        GATEWAY: ".gateway",
        STAIRS: ".stairs",
        WALKABLE: ".walk"
    }

    WIDTH = 2048  # multiples of 8
    HEIGHT = 2048  # 50 ish m per side
