import base64
import math

import numpy as np
from base_node.networking_src.network_utils import NetworkUtils
from map.map_constants import MapConst
from messages.nav_msgs import MapMessage

from tools.print_utils import bcol, Log

TAG = "UnivMapHandler:"
log = Log(bcol.DEFAULT, TAG, level=Log.INFO)


class UniversalMapHandler:
    def __init__(self, w=MapConst.WIDTH, h=MapConst.HEIGHT):
        self.map = np.full((w, h), MapConst.BLANK, dtype=int)
        self.building_id = ""
        self.floor = ""
        self.map_id = ""

        self.m_per_cell = 0.05

    def cell2m(self, c):
        return float(c) * self.m_per_cell

    def m2cell(self, m):
        return int(math.floor(m/self.m_per_cell))

    def xy2map(self, x, y):
        ii, jj = self.get_origin_index()
        cx = self.m2cell(x)
        cy = self.m2cell(y)
        return ii - cy, jj + cx

    def map2xy(self, i, j):
        ii, jj = self.get_origin_index()
        cx = j - jj
        cy = i - ii
        mx = self.cell2m(cx)
        my = self.cell2m(-cy)

        return mx + self.m_per_cell/2.0, my + self.m_per_cell/2.0

    def get_origin_index(self):
        ii = self.map.shape[0] / 2
        jj = self.map.shape[0] / 2
        return int(ii), int(jj)

    def on_map_recv(self, msg: MapMessage):
        self.reset_map()
        for layer in msg.layers:
            ba = bytearray(base64.b64decode(layer))
            temp_map = self._load_map(ba)
            if temp_map.shape[0] > self.map.shape[0] or temp_map.shape[1] > self.map.shape[1]:
                log.i("Map replaced")
                self.map = temp_map
            else:
                log.i("Map Updated")
                ii, jj = self.get_start_index(*temp_map.shape)
                self.map[ii:ii + temp_map.shape[0], jj:jj + temp_map.shape[1]] |= temp_map

        self.building_id = msg.building_id
        self.floor = msg.floor
        self.map_id = msg.map_id

    def is_current_map(self, building_id, floor, map_id):
        return self.building_id == building_id and self.floor == floor and self.map_id == map_id

    def reset_map(self):
        self.map.fill(MapConst.BLANK)

    def get_start_index(self, h, w):
        ii = self.map.shape[0] / 2 - h / 2
        jj = self.map.shape[1] / 2 - w / 2

        return int(ii), int(jj)

    @staticmethod
    def _load_b64(ba: bytearray, value):
        if len(ba) != 8:
            log.e("bad b64")

        i64 = np.full((8, 8), MapConst.BLANK, dtype='uint8')

        for j in range(8):
            for i in range(8):
                if ba[j] & 1 << i != 0:
                    i64[i, j] = value
                else:
                    i64[i, j] = MapConst.BLANK
        return i64

    @staticmethod
    def _load_map(sections: bytearray):
        b = sections[0]
        h = NetworkUtils.n_bytes_to_int(4, sections[1:5])
        w = NetworkUtils.n_bytes_to_int(4, sections[5:9])

        temp_map = np.full((h, w), MapConst.BLANK, dtype='uint8')
        if sections[9] == 0xaa:  # we has data
            # if sections[8] == 0xaa:     # we has data
            # sections = sections[8:]
            sections = sections[9:]
            while 0xaa in sections:
                # print(repr(sections))
                ii = NetworkUtils.n_bytes_to_int(2, sections[1:3])
                jj = NetworkUtils.n_bytes_to_int(2, sections[3:5])
                n = NetworkUtils.n_bytes_to_int(4, sections[5:9])

                data = sections[9:9 + n]
                # print(ii, jj, n, data)
                for i in range(int(n / 8)):
                    i64 = UniversalMapHandler._load_b64(data[:8], b)
                    temp_map[ii * 8:(ii + 1) * 8, (jj + i) * 8:(jj + i + 1) * 8] = i64
                    data = data[8:]

                sections = sections[9 + n + 1:]
        return temp_map

    def wall_check(self, i0, j0, i1, j1):
        # print(x0, y0, x1, y1)
        """
        Bresenham's line algorithm modified to check for walls and diagonals
        returns true if wall else false for no wall.
        """
        di = abs(i1 - i0)
        dj = abs(j1 - j0)
        i, j = i0, j0
        si = -1 if i0 > i1 else 1
        sj = -1 if j0 > j1 else 1
        if di > dj:
            err = di / 2.0
            while i != i1:
                if self.map[i, j] == MapConst.WALL or not self.map[i, j] > MapConst.WALL:
                    log.d("[%d, %d]" % (i, j), "Wall Found")
                    return True
                err -= dj

                if err < 0:     # shifting y by sy
                    fi = i + si
                    fj = j + sj

                    if fi != i1:
                        # check diagonals
                        if self.map[i, fj] == MapConst.WALL and self.map[fi, j] == MapConst.WALL:
                            log.d("[%d, %d]" % (i, j), "Diagonal Wall Found")
                            return True

                    j = fj
                    err += di
                i += si
        else:
            err = dj / 2.0
            while j != j1:
                if self.map[i, j] == MapConst.WALL or not self.map[i, j] > MapConst.WALL:
                    log.d("[%d, %d]" % (i, j), "Wall Found")
                    return True
                err -= di

                if err < 0:     # shifting x by sx
                    fi = i + si
                    fj = j + sj

                    if fj != j1:
                        # check diagonals
                        if self.map[i, fj] == MapConst.WALL and self.map[fi, j] == MapConst.WALL:
                            log.d("[%d, %d]" % (i, j), "Diagonal Wall Found")
                            return True

                    i = fi
                    err += dj
                j += sj
        if self.map[i, j] == MapConst.WALL or not self.map[i, j] > MapConst.WALL:
            log.d("[%d, %d]" % (i, j), "Wall Found")
            return True
        return False
