import os
import sys
import threading

import tkinter as tk
from base_node.node import Node
from imu.quaternion import Quaternion
from messages.cmd_msgs import CmdMessage

if "--disp" in sys.argv:
    from displayers.real_2d_plotter import Plotter2D

from messages.nav_msgs import PoseStamped, StepEventStamped
from messages.plotter_msgs import Entry2D, Entry3D
from messages.sensor_msgs import ImuMessage
from imu.post_processor import PostProcessor
from tools.print_utils import Log, bcol

TAG = "PostProcessNode:"
log = Log(bcol.DEFAULT, TAG)


class IMUProcessor:
    CMD_RESET = -1

    def __init__(self):

        self.seq = 0

        self.root = None
        if "--debug" in sys.argv:
            self.root = tk.Tk()
            self.plotter = Plotter2D(self.root)
            self.mag = self.plotter.add_line()
            self.sta = self.plotter.add_line()

        self.post_processor = PostProcessor(block_callback=self.block_processed, simplify=True,
                                            step_callback=self.step_processed)
        # self.post_processor = PostProcessor(block_callback=self.block_processed, plotter=[])

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pub = self.node.create_publisher('imu_pose', PoseStamped)
        self.pub_cor = self.node.create_publisher('imu_pose_corrected', Entry3D)

        self.mag_pub = self.node.create_publisher('imu_process/mag', Entry2D)
        self.acc_pub = self.node.create_publisher('imu_process/acc', Entry2D)
        self.sta_pub = self.node.create_publisher('imu_process/sta', Entry2D)
        self.vel_pub = self.node.create_publisher('imu_process/vel', Entry2D)
        self.vel_cor_pub = self.node.create_publisher('imu_process/vel_cor', Entry2D)

        self.step_evt_pub = self.node.create_publisher('imu_process/step_events', StepEventStamped)

        self.sub = self.node.subscribe('imu_raw', ImuMessage, self.process_imu)
        self.sub = self.node.subscribe('imu_process/cmd', CmdMessage, self._on_cmd_recv)

        if "--debug" in sys.argv:
            self.root.after(10, self.node_loop)
            self.root.mainloop()
        else:
            self.node.run_loop(0.1)

    def node_loop(self):
        self.node.sleep(0.1)
        self.root.after(10, self.node_loop)

    def block_processed(self, msgs, data):
        vel_cor = Entry2D()
        ms, vx, vy, vz, sx, sy, sz = data
        #
        vel_cor.x = ms
        vel_cor.y = [vx, vy, vz]
        self.vel_cor_pub.publish(vel_cor)

        threading.Thread(target=self.publish_poses, args=(msgs,)).start()

    def step_processed(self, ts, r, d_theta, xi, pt):
        msg = StepEventStamped()
        msg.ts = ts
        msg.r = r
        msg.d_theta = d_theta
        msg.xi = xi
        msg.xy = pt

        self.step_evt_pub.publish(msg)

    def publish_poses(self, msgs):
        for msg in msgs:
            self.seq += 1
            msg.seq = self.seq
            self.pub.publish(msg)

    def process_imu(self, msg: ImuMessage):
        # print(msg.a)
        # t0 = time.time_ns()
        self.post_processor.add_entry(msg.ts, msg.ms, msg.a, Quaternion(*msg.q))
        # print(time.time_ns() - t0)

        if "--debug" in sys.argv:
            self.mag.push(msg.ts, self.post_processor.mags[-1])
            self.sta.push(msg.ts, self.post_processor.stationary[-1])

        mag = Entry2D()
        mag.x = [msg.ts]
        mag.y = [self.post_processor.mags[-1]]

        acc = Entry2D()
        acc.x = [msg.ts]
        acc.y = [self.post_processor.dax[-1], self.post_processor.day[-1], self.post_processor.daz[-1]]

        sta = Entry2D()
        sta.x = [msg.ts]
        sta.y = [self.post_processor.stationary[-1]]

        vel = Entry2D()
        vel.x = [msg.ts]
        vel.y = [self.post_processor.vx[-1], self.post_processor.vy[-1], self.post_processor.vz[-1]]

        self.mag_pub.publish(mag)
        self.acc_pub.publish(acc)
        self.sta_pub.publish(sta)
        self.vel_pub.publish(vel)

    def _on_cmd_recv(self, msg: CmdMessage):
        if msg.cmd == self.CMD_RESET:
            print("resetting")
            self.post_processor.reset()
            self.seq = 0


class ProcessDisp2D:
    def __init__(self, parent: tk.Tk):
        self.parent = parent

        self.mag_sta = Plotter2D(parent, title="Magnitude & Stationary")
        self.vel = Plotter2D(parent, title="Velocity")
        self.vel_cor = Plotter2D(parent, title="Corrected Velocity")
        self.acc = Plotter2D(parent, title="Dynamic Acceleration")

        self.magl = self.mag_sta.add_line(label="mag")
        self.stal = self.mag_sta.add_line(label="Stationary")

        self.vxl = self.vel.add_line(label="vx")
        self.vyl = self.vel.add_line(label="vy")
        self.vzl = self.vel.add_line(label="vz")

        self.vcxl = self.vel_cor.add_line(label="vx")
        self.vcyl = self.vel_cor.add_line(label="vy")
        self.vczl = self.vel_cor.add_line(label="vz")

        self.axl = self.acc.add_line(label="ax")
        self.ayl = self.acc.add_line(label="ay")
        self.azl = self.acc.add_line(label="az")

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master, queue_sleep=0.0001)

        self.mag_sub = self.node.subscribe('imu_process/mag', Entry2D, self.mag_cb)
        self.sta_sub = self.node.subscribe('imu_process/sta', Entry2D, self.sta_cb)
        self.acc_sub = self.node.subscribe('imu_process/acc', Entry2D, self.acc_cb)
        self.vel_sub = self.node.subscribe('imu_process/vel', Entry2D, self.vel_cb)
        self.vel_cor_sub = self.node.subscribe('imu_process/vel_cor', Entry2D, self.vel_cor_cb)

        parent.after(100, self.node_loop)

    def node_loop(self):
        self.node.sleep(0.1)
        self.parent.after(100, self.node_loop)

    def mag_cb(self, msg: Entry2D):
        self.magl.push(msg.x[0], msg.y[0])

    def sta_cb(self, msg: Entry2D):
        self.stal.push(msg.x[0], msg.y[0])

    def acc_cb(self, msg: Entry2D):
        self.axl.push(msg.x[0], msg.y[0])
        self.ayl.push(msg.x[0], msg.y[1])
        self.azl.push(msg.x[0], msg.y[2])

    def vel_cb(self, msg: Entry2D):
        self.vxl.push(msg.x[0], msg.y[0])
        self.vyl.push(msg.x[0], msg.y[1])
        self.vzl.push(msg.x[0], msg.y[2])

    def vel_cor_cb(self, msg: Entry2D):
        self.vcxl.extend(msg.x, msg.y[0])
        self.vcyl.extend(msg.x, msg.y[1])
        self.vczl.extend(msg.x, msg.y[2])


if __name__ == "__main__":
    d = None
    try:
        if "--disp" in sys.argv:
            root = tk.Tk()
            ProcessDisp2D(root)
            root.mainloop()
        else:
            d = IMUProcessor()
    except KeyboardInterrupt:
        d.node.alive = False
        os._exit(0)
