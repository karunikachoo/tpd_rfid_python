import collections
import time
import tkinter as tk
from .quaternion import Quaternion
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
import numpy as np

G = 9.80665     # m/s^2


class Plotter:
    def __init__(self, main, master, limit=2000):
        self.main = main

        self.limit = limit

        self.id = collections.deque([0], limit)
        # self.disp = np.array([[0, 0, 0]])
        self.x = collections.deque([0], limit)
        self.y = collections.deque([0], limit)
        self.z = collections.deque([0], limit)
        self.q = collections.deque([Quaternion(1, 0, 0, 0)], limit)

        self.can_update = True
        self.can_write = True

        # Plotting
        self.fig = plt.Figure()

        self.canvas = FigureCanvasTkAgg(self.fig, master=master)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        self.ax = p3.Axes3D(self.fig)

        self.ax.set_xlabel('x')
        self.ax.set_ylabel('y')
        self.ax.set_zlabel('z')

        fixed_cs = self.axis_local_to_global(Quaternion(1, 0, 0, 0), scale=0.1)
        self.axes = self.plot_cs(self.ax, [0, 0, 0], fixed_cs)
        self.disp_path, = self.ax.plot([0, 0], [0, 0], [0, 0], color='cyan')

        # self.ani = animation.FuncAnimation(self.fig, self.draw_plot, interval=125)
        self.ani = animation.FuncAnimation(self.fig, self.draw_plot, interval=50)

    def correct_data(self, _id, disp):
        self.can_write = False
        print(_id in self.id)
        if _id in self.id:
            i = self.id.index(_id)
            self.x[i] = disp[0]
            self.y[i] = disp[1]
            self.z[i] = disp[2]

        self.can_write = True
        self.can_update = True

    def update_data(self, _id, disp, q: Quaternion):
        while not self.can_write:
            time.sleep(0.001)
        self.id.append(_id)
        self.x.append(disp[0])
        self.y.append(disp[1])
        self.z.append(disp[2])
        self.q.append(q)
        self.can_update = True

    def draw_plot(self, ii):
        if self.can_update:
            # self.ax.view_init(elev=10., azim=(ii/5) % 360)
            q = self.q[-1]
            pose = self.axis_local_to_global(q, scale=0.5)
            origin = [self.x[-1], self.y[-1], self.z[-1]]
            self.update_cs(self.axes, origin, pose)

            self.disp_path.set_data(self.x, self.y)
            self.disp_path.set_3d_properties(self.z)

            if len(self.x) > 0:
                lx, ly, lz = self.compute_lims(self.x, self.y, self.z)
                self.ax.set_xlim(*lx)
                self.ax.set_ylim(*ly)
                self.ax.set_zlim(*lz)

            self.can_update = False
        # to_update = [self.disp_path, self.ax]
        # to_update.extend(self.axes)
        # return to_update

    @staticmethod
    def compute_lims(x, y, z):
        mx = [min(x), max(x)]
        my = [min(y), max(y)]
        mz = [min(z), max(z)]
        d = [mx[1] - mx[0], my[1] - my[0], mz[1] - mz[0]]
        dist = max(d)
        return [mx[0], mx[0] + dist], [my[0], my[0] + dist], [mz[0], mz[0] + dist]

    @staticmethod
    def update_cs(axes, origin, pose):
        for axis in range(3):
            xs = [origin[0], pose[axis][0] + origin[0]]
            ys = [origin[1], pose[axis][1] + origin[1]]
            zs = [origin[2], pose[axis][2] + origin[2]]
            axes[axis].set_data(xs, ys)
            axes[axis].set_3d_properties(zs)

    @staticmethod
    def plot_cs(ax, origin, pose, alpha=1.0, colors=None):
        if colors is None:
            colors = ['red', 'green', 'blue']
        ret = []
        for axis in range(3):
            xs = [origin[0], pose[axis][0] + origin[0]]
            ys = [origin[1], pose[axis][1] + origin[1]]
            zs = [origin[2], pose[axis][2] + origin[2]]
            axs, = ax.plot(xs, ys, zs, color=colors[axis], alpha=alpha)
            ret.append(axs)
        return ret

    @staticmethod
    def axis_local_to_global(q: Quaternion, scale=1.0):
        """
        generates a coordinate system around (0, 0, 0)
        """
        cs = [[scale, 0.0, 0.0], [0.0, scale, 0.0], [0.0, 0.0, scale]]
        c2 = []

        for axis in range(3):
            q2 = Quaternion(0, cs[axis][0], cs[axis][1], cs[axis][2])
            h = q * q2 * q.conj()
            c2.append(h[1:])
        return c2