import collections
import math
import traceback

import numpy as np

from base_node.networking_src.network_utils import NetworkUtils
from messages.nav_msgs import PoseStamped
from .quaternion import Quaternion
from tools.print_utils import Log, bcol

TAG = "PostProcessor:"
log = Log(bcol.DEFAULT, TAG)


class PostProcessor:
    def __init__(self, g=1.0175, g_ms2=9.81, thresh=0.08, limit=250, block_callback=None, simplify=False, step_callback=None):
        self.simplify = simplify
        self.block_callback = block_callback
        self.step_callback = step_callback
        self.g = g
        self.g_ms2 = g_ms2
        self.thresh = thresh

        self.id = collections.deque([], limit)
        self.ms = collections.deque([], limit)
        self.ts = collections.deque([], limit)

        self.ax = collections.deque([], limit)
        self.ay = collections.deque([], limit)
        self.az = collections.deque([], limit)

        self.q = collections.deque([], limit)

        self.gax = collections.deque([], limit)
        self.gay = collections.deque([], limit)
        self.gaz = collections.deque([], limit)

        self.dax = collections.deque([], limit)
        self.day = collections.deque([], limit)
        self.daz = collections.deque([], limit)

        self.mags = collections.deque([], limit)
        self.stationary = collections.deque([], limit)

        self.starts = []
        self.ends = []

        self.vx = collections.deque([0.0], limit)
        self.vy = collections.deque([0.0], limit)
        self.vz = collections.deque([0.0], limit)

        self.s = [0.0, 0.0, 0.0]

    def reset(self):
        self.__init__(g=self.g, g_ms2=self.g_ms2, thresh=self.thresh,
                      block_callback=self.block_callback, simplify=self.simplify, step_callback=self.step_callback)

    def calibrate_g(self):
        if len(self.mags) > 50:
            x = np.average(list(self.ax)[-50:])
            y = np.average(list(self.ay)[-50:])
            z = np.average(list(self.az)[-50:])

            # self.g = math.sqrt(x*x + y*y + z*z)
            self.g = math.sqrt(z*z)
            print(self.g)

    def retain_last(self, n):
        self.ms = list(self.ms)[-n:]
        self.ts = list(self.ms)[-n:]
        self.id = list(self.id)[-n:]

        self.ax = list(self.ax)[-n:]
        self.ay = list(self.ay)[-n:]
        self.az = list(self.az)[-n:]

        self.q = list(self.q)[-n:]

        self.gax = list(self.gax)[-n:]
        self.gay = list(self.gay)[-n:]
        self.gaz = list(self.gaz)[-n:]

        self.dax = list(self.dax)[-n:]
        self.day = list(self.day)[-n:]
        self.daz = list(self.daz)[-n:]

        self.mags = list(self.mags)[-n:]
        self.stationary = list(self.stationary)[-n:]

        self.starts = []
        self.ends = []

        self.vx = list(self.vx)[-n:]
        self.vy = list(self.vy)[-n:]
        self.vz = list(self.vz)[-n:]

    def get_entry(self, i):
        return self.ts[i], self.ms[i], [self.ax[i], self.ay[i], self.az[i]], self.q[i]

    def add_entry(self, ts, ms, acc, q):
        _id = NetworkUtils.random_str(6)
        self.id.append(_id)
        self.ts.append(ts)
        self.ms.append(ms / 1000.0)
        self.ax.append(acc[0])
        self.ay.append(acc[1])
        self.az.append(acc[2])
        self.q.append(q)

        self.process_entry(ms, acc, q)

        if self.is_stationary(2) and len(self.ends) > 0:
            # threading.Thread(target=self.process_block, args=(,))
            try:
                self.process_block(2)
            except Exception as e:
                log.e(traceback.format_exc())
        return _id

    def process_block(self, last_n):
        vx = list(self.vx)[:-last_n]
        vy = list(self.vy)[:-last_n]
        vz = list(self.vz)[:-last_n]
        ms = list(self.ms)[:-last_n]
        ts = list(self.ts)[:-last_n]
        q = list(self.q)[:-last_n]

        self.calc_vel_drift(vx, vy, vz, self.starts, self.ends)

        p0 = PoseStamped()
        p0.id = self.id[0]
        p0.ts = ts[0]
        p0.q = list(self.q[0])
        p0.point = self.s.copy()

        msgs = [p0]

        sx = self.s[0]
        sy = self.s[1]
        sz = self.s[2]

        s_array = [[sx], [sy], [sz]]

        for i in range(1, len(ms)):

            dt = ms[i] - ms[i-1]
            dsx = sx + vx[i] * dt
            dsy = sy + vy[i] * dt
            dsz = sz + vz[i] * dt

            sx = dsx
            sy = dsy
            sz = dsz

            s_array[0].append(dsx)
            s_array[1].append(dsy)
            s_array[2].append(dsz)

            if self.simplify:
                if i % 5 == 4 and i < len(ms) - 2:
                    p = PoseStamped()
                    p.id = self.id[i]
                    p.ts = ts[i]
                    p.q = list(self.q[i])

                    p.point = [dsx, dsy, dsz]

                    msgs.append(p)
            else:
                p = PoseStamped()
                p.id = self.id[i]
                p.ts = ts[i]
                p.q = list(self.q[i])

                p.point = [dsx, dsy, dsz]

                msgs.append(p)

        # self.step_processing(ts, q, s_array, self.starts, self.ends)
        self._step_processing(s_array)

        self.s[0] = sx
        self.s[1] = sy
        self.s[2] = sz

        self.retain_last(last_n)

        if self.block_callback is not None:
            # if self.simplify:
            #     self.block_callback([msgs[0], msgs[1], msgs[-1]],
            #                         [ms, vx, vy, vz, s_array[0], s_array[1], s_array[2]])
            # else:
            self.block_callback(msgs, [ms, vx, vy, vz, s_array[0], s_array[1], s_array[2]])

    def _step_processing(self, s_array):
        ts = self.ts[-1]

        q0 = self.q[0]
        q1 = self.q[-1]

        x0 = s_array[0][0]
        y0 = s_array[1][0]

        x1 = s_array[0][-1]
        y1 = s_array[1][-1]

        c = Quaternion(0, 1, 0, 0)
        v0 = (q0 * c * q0.conj())[1:]
        v1 = (q1 * c * q1.conj())[1:]

        theta1 = math.atan2(v1[1], v1[0])

        d_theta = theta1 - math.atan2(v0[1], v0[0])

        dx = x1 - x0
        dy = y1 - y0
        r = math.sqrt(dx * dx + dy * dy)

        theta = math.atan2(dy, dx)

        xi = theta1 - theta

        pt = [x1, y1]
        # print(ts, r, d_theta, xi)
        # print(self.step_callback)
        if self.step_callback is not None:
            self.step_callback(ts, r, d_theta, xi, pt)

    def __step_processing(self, s_array):
        s = self.starts[0]
        e = self.ends[-1] - 1

        ts = self.ts[e]

        x0 = s_array[0][s]
        y0 = s_array[1][s]

        x1 = s_array[0][s+1]
        y1 = s_array[1][s+1]

        x2 = s_array[0][e - 1]
        y2 = s_array[1][e - 1]

        x3 = s_array[0][e]
        y3 = s_array[1][e]

        dx01 = x1-x0
        dy01 = y1-y0

        dx23 = x3-x2
        dy23 = y3-y2

        ang01 = math.atan2(dy01, dx01)
        ang23 = math.atan2(dy23, dx23)

        d_theta = ang23 - ang01

        dx = x3 - x0
        dy = y3 - y0
        r = math.sqrt(dx*dx + dy*dy)

        theta = math.atan2(dy, dx)

        xi = ang23 - theta
        pt = [x3, y3]
        if self.step_callback is not None:
            self.step_callback(ts, r, d_theta, xi, pt)

    def step_processing(self, ts_array, q_array, s_array, starts: list, ends: list):
        for i in range(len(ends)):
            s = starts[i] - 1
            e = ends[i] - 1

            ts = ts_array[e]

            q0 = self.q[s]
            q1 = self.q[e+1]

            x0 = s_array[0][s]
            y0 = s_array[1][s]

            x1 = s_array[0][e]
            y1 = s_array[1][e]

            c = Quaternion(0, 1, 0, 0)
            v0 = (q0 * c * q0.conj())[1:]
            v1 = (q1 * c * q1.conj())[1:]

            theta1 = math.atan2(v1[1], v1[0])

            d_theta = theta1 - math.atan2(v0[1], v0[0])

            dx = x1 - x0
            dy = y1 - y0
            r = math.sqrt(dx*dx + dy*dy)

            theta = math.atan2(dy, dx)

            xi = theta1 - theta

            pt = [x1, y1]
            # print(ts, r, d_theta, xi)
            # print(self.step_callback)
            if self.step_callback is not None:
                self.step_callback(ts, r, d_theta, xi, pt)

    def calc_vel_drift(self, vx, vy, vz, start: list, end: list):
        for i in range(len(end)):
            _vx = 0
            _vy = 0
            _vz = 0
            try:
                _vx = vx[end[i] - 1]
                _vy = vy[end[i] - 1]
                _vz = vz[end[i] - 1]
            except Exception as e:
                log.e(e)

            elems = end[i] - start[i]

            if elems > 0:
                dr = [_vx/elems, _vy/elems, _vz/elems]

                for ind in range(0, elems):
                    vx[start[i] + ind] -= dr[0] * (ind + 1)
                    vy[start[i] + ind] -= dr[1] * (ind + 1)
                    vz[start[i] + ind] -= dr[2] * (ind + 1)

    def process_entry(self, ms, acc, q):
        # transform to global
        a = (q * Quaternion(0, *acc) * q.conj())[1:]
        self.add_global_entry(a)

        # make dynamic
        a[2] -= self.g
        self.add_dyn_entry(a)

        # calculate stationary
        mag = math.sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2])
        self.mags.append(mag)
        st = mag < self.thresh
        self.stationary.append(st)

        if len(self.stationary) > 1:
            dt = self.ms[-1] - self.ms[-2]
            if st:
                self.vx.append(0.0)
                self.vy.append(0.0)
                self.vz.append(0.0)

            else:
                vx = self.vx[-1] + self.dax[-1] * self.g_ms2 * dt
                vy = self.vy[-1] + self.day[-1] * self.g_ms2 * dt
                vz = self.vz[-1] + self.daz[-1] * self.g_ms2 * dt

                self.vx.append(vx)
                self.vy.append(vy)
                self.vz.append(vz)

            point = self.stationary[-1] - self.stationary[-2]
            if point == -1:
                self.starts.append(len(self.stationary) - 1)
            elif point == 1:
                self.ends.append(len(self.stationary) - 1)

            # if point != 0:
            #     print(self.starts, self.ends)

    def add_global_entry(self, acc):
        self.gax.append(acc[0])
        self.gay.append(acc[1])
        self.gaz.append(acc[2])

    def add_dyn_entry(self, acc):
        self.dax.append(acc[0])
        self.day.append(acc[1])
        self.daz.append(acc[2])

    def is_stationary(self, n):
        if len(self.stationary) >= n:
            past = list(self.stationary)[-n:]
            return all(past)
        return False

    def get_quaternion_columns(self):
        cols = ()
        for i in range(len(self.q)):
            q = self.q[i]
            for j in range(4):
                if len(cols) == j:
                    cols += ([], )
                cols[j].append(q[j])
        return cols


