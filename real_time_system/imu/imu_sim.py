import os
import time
import tkinter as tk
from tkinter import filedialog


class ImuSim(tk.Frame):
    START, PLAY, PAUSE, STOP = range(4)

    def __init__(self, parent, playback_file, on_read_callback, on_reset_nodes_callback, mult, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)

        self.mult = mult

        self.file_path = playback_file
        self.callback = on_read_callback
        self.on_reset_nodes_callback = on_reset_nodes_callback
        self.state = self.START
        self.i = 0
        self.dts, self.ts, self.lines = self.load_file()

        self.load_btn = tk.Button(self, text="Load", command=self.load)
        self.play_btn = tk.Button(self, text="Play", command=self.play)
        self.pause_btn = tk.Button(self, text="Pause", command=self.pause)
        self.reset_btn = tk.Button(self, text="Reset", command=self.reset)
        self.reset_nodes_btn = tk.Button(self, text="Reset Nodes", command=self.on_reset_nodes_callback)
        self.status_label = tk.Label(self, text="Waiting")

        self.pack()
        self.load_btn.grid(row=0, column=0, sticky='we')
        self.play_btn.grid(row=1, column=0, sticky='we')
        self.pause_btn.grid(row=2, column=0, sticky='we')
        self.reset_btn.grid(row=3, column=0, sticky='we')
        self.reset_nodes_btn.grid(row=3, column=1, sticky='we', padx=(3, 0))
        self.status_label.grid(row=0, column=1, sticky='we')

    def loop(self):
        if self.state == self.PLAY:
            self.status_label.config(text="Playing: %d : %d" % (self.i, self.ts[self.i]))
            self.callback(self.lines[self.i])
            self.i += 1
            if self.i < len(self.lines):
                self.after(int(self.dts[self.i] * self.mult), self.loop)
            else:
                self.stop()

    def load(self):
        fn = filedialog.askopenfilename(initialdir=os.getcwd(), title="Import Sim File",
                                        filetypes=(("Text File", "*.txt"),))

        if fn != "" and fn is not None:
            if '.txt' not in fn:
                fn += '.txt'
            self.file_path = fn
            self.dts, self.ts, self.lines = self.load_file()

    def load_file(self):
        f = open(self.file_path, 'r')
        lines = f.readlines()
        f.close()

        ts = [int(line.split(",")[0])for line in lines]
        dts = [0]
        for i in range(1, len(ts)):
            t0 = ts[i-1]
            t1 = ts[i]
            dts.append(t1-t0)
        return dts, ts, lines

    def play(self):
        self.state = self.PLAY
        self.status_label.config(text="Playing")
        self.loop()

    def pause(self):
        self.state = self.PAUSE
        self.status_label.config(text="Paused")

    def reset(self):
        self.state = self.START
        self.i = 0
        self.status_label.config(text="Reset")

    def stop(self):
        self.state = self.STOP
        self.i = 0
        self.status_label.config(text="Stopped")
