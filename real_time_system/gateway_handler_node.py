import json
import os
import sys

from base_node.node import Node
from messages.nav_msgs import PointRadius2DMessage, NotifyMessage


class GatewayProcessorNode:
    def __init__(self):

        self.config = self.load_config("gateway/gateway_config.json")
        self.gateways = self.config['gateways']

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.pub = self.node.create_publisher('gateway/point_rads', PointRadius2DMessage)
        self.sub = self.node.subscribe('gateway/notifications', NotifyMessage, self.on_gateway_notify)

        self.node.run_loop(0.1)

    def on_gateway_notify(self, msg: NotifyMessage):
        print(msg.source_id)
        if msg.source_id in self.gateways:
            gateway = self.gateways[msg.source_id]

            pt = PointRadius2DMessage()
            pt.building_id = gateway['building_id']
            pt.floor = gateway['floor']
            pt.map_id = gateway['map_id']

            pt.ts = msg.ts
            pt.source_id = msg.source_id

            pt.point = gateway['point']
            pt.radius = gateway['radius']

            self.pub.publish(pt)

    @staticmethod
    def load_config(config):
        f = open(config, 'r')
        config = json.loads(f.read())
        f.close()
        return config


if __name__ == "__main__":
    g = None
    try:
        g = GatewayProcessorNode()

    except KeyboardInterrupt:
        g.node.alive = False
        os._exit(0)
