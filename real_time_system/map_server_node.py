import base64
import json
import sys

from base_node.node import Node
from messages.cmd_msgs import CmdMessage
from messages.msg_struct import Wrapper
from messages.nav_msgs import MapMessage, PointRadius2DMessage


class MapServerNode:

    CMD_MAP_REQ = 1

    def __init__(self):

        self.config = self.load_config('map/data/config.json')

        master = Node.sys_parse(sys.argv)
        self.node = Node(*master)

        self.map_req_service = self.node.create_service("map_server/map_req_serv",
                                                        "map_server/map_req_reply", self.map_request_callback)
        self.node.run_loop(0.1)

    @staticmethod
    def read_map(path):
        f = open(path, 'rb')
        m = base64.b64encode(f.read())
        f.close()
        return m

    @staticmethod
    def load_config(config):
        f = open(config, 'r')
        data = json.loads(f.read())
        f.close()
        return data

    def check_valid_map(self, building_id, floor, map_id):
        if building_id in self.config:
            building = self.config[building_id]
            if floor in building['floors']:
                f = building['floors'][floor]
                if map_id in f:
                    m = f[map_id]
                    return m['layers'], m['resolution']
        return [], 0

    def map_request_callback(self, msg: CmdMessage):
        if msg.cmd == self.CMD_MAP_REQ:
            data = json.loads(msg.data)
            building_id = data['building_id']
            floor = data['floor']
            map_id = data['map_id']

            layers = []
            paths, res = self.check_valid_map(building_id, floor, map_id)
            for path in paths:
                b64 = self.read_map(path).decode()
                layers.append(b64)

            if len(layers) > 0:
                map_msg = MapMessage()
                map_msg.map_id = map_id
                map_msg.building_id = building_id
                map_msg.floor = floor
                map_msg.resolution = res
                map_msg.layers = layers

                self.map_req_service.reply(msg, map_msg)


if __name__ == "__main__":
    ms = None
    try:
        ms = MapServerNode()
    except KeyboardInterrupt:
        ms.node.alive = False
