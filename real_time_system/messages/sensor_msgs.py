from messages.base_msg import BaseMessage


class ImuMessage(BaseMessage):
    def __init__(self):
        super(ImuMessage, self).__init__()
        self.ts = 0
        self.ms = 0
        self.a = []
        self.q = []

