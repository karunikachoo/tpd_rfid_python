from messages.base_msg import BaseMessage


class Entry2D(BaseMessage):
    def __init__(self):
        super(Entry2D, self).__init__()
        self.x = []
        self.y = []


class Entry3D(BaseMessage):
    def __init__(self):
        super(Entry3D, self).__init__()
        self.x = []
        self.y = []
        self.z = []


class TfPoseXY2D(BaseMessage):
    def __init__(self):
        super(TfPoseXY2D, self).__init__()
        self.x = []
        self.y = []
        self.ang_x = []
        self.ang_y = []

        self.d = [0, 0]


class ParticleCloudMsg(BaseMessage):
    def __init__(self):
        super(ParticleCloudMsg, self).__init__()
        """id, x, y, max is 8125.0"""
        self.x = []
        self.y = []
