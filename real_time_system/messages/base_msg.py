import json


class BaseMessage:
    def load(self, data):
        obj = json.loads(data)
        self.__dict__ = obj
        return self

    def to_json(self):
        return json.dumps(self.__dict__)
    

class TestMsg(BaseMessage):
    def __init__(self):
        super(BaseMessage, self).__init__()
        self.test = 1023


if __name__ == "__main__":
    m = TestMsg()
    j = m.to_json()
    print(j)
    m2 = TestMsg().load(j)
    print(m2.__dict__)
