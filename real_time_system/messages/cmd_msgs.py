from messages.base_msg import BaseMessage


class CmdMessage(BaseMessage):
    def __init__(self):
        super(CmdMessage, self).__init__()
        self.uid = ""
        self.cmd = 0
        self.data = ""
