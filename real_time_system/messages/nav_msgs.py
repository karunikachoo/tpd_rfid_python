from messages.base_msg import BaseMessage


class PoseStamped(BaseMessage):
    def __init__(self):
        super(PoseStamped, self).__init__()

        self.id = ""
        self.ts = 0
        self.seq = 0
        self.frame = ""

        self.point = [0, 0, 0]
        self.q = [1, 0, 0, 0]


class StepEventStamped(BaseMessage):
    def __init__(self):
        super(StepEventStamped, self).__init__()

        self.id = ""
        self.ts = 0.0

        self.r = 0.0
        self.d_theta = 0.0      # rad
        self.xi = 0.0           # rad
        self.xy = [0.0, 0.0]


class MapMessage(BaseMessage):
    def __init__(self):
        super(MapMessage, self).__init__()

        self.building_id = ""
        self.floor = ""
        self.map_id = ""

        self.layers = []           # base64 encode of bytes
        self.resolution = 0  # metres per cell


class PointRadius2DMessage(BaseMessage):
    def __init__(self):
        super(PointRadius2DMessage, self).__init__()

        self.building_id = ""
        self.floor = ""
        self.map_id = ""

        self.ts = 0
        self.source_id = ""

        self.point = [0, 0]     # m? we'receiving poses as m as well
        self.radius = 0


class NotifyMessage(BaseMessage):
    def __init__(self):
        super(NotifyMessage, self).__init__()

        self.ts = 0
        self.source_id = ""
        self.data = ""
