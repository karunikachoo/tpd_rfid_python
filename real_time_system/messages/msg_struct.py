import json
from pprint import pprint


class Wrapper:
    """
    This serves as a wrapper around the actual message types
    """
    PUB_SUB, CMD, DATA, VERIFY, ALIVE, DISCOVER, DISCOVER_REPLY = range(7)

    def __init__(self, sender=None, msg_type=None, payload=None, uuid=None):
        self.sender = {
            'ip': '',
            'port': 0
        }
        if sender is not None:
            self.sender['ip'] = sender[0]
            self.sender['port'] = sender[1]

        self.type = msg_type
        self.payload = payload
        self.uuid = uuid

    def __repr__(self):
        return "<Wrapper " + str(id(self)) + " " + str(self.__dict__) + ">"

    def load(self, data: str):
        obj = json.loads(data)
        self.__dict__ = obj
        return self

    def to_json(self):
        return json.dumps(self.__dict__)


if __name__ == "__main__":
    m = Wrapper(sender=('127.0.0.1', 65432),
                msg_type=Wrapper.CMD,
                payload="TEST")

    print(m.to_json())