from messages.base_msg import BaseMessage


class PubSubMsg(BaseMessage):
    PUB, UNPUB, SUB_REQ, SUB_REPLY, SUB, UNSUB = range(6)

    def __init__(self, cmd=None, topic=None, data=None,):
        super(PubSubMsg, self).__init__()
        self.cmd = cmd
        self.topic = topic
        self.data = data


