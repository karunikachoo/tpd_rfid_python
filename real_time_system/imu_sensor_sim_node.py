import os
import sys

sys.argv.append('--sim')
sys.argv.append('imu/data/5a01_extended_run_1.txt')

from imu_sensor_node import ImuSensor

if __name__ == '__main__':
    try:
        sensor = ImuSensor()
    except KeyboardInterrupt:
        os._exit(1)