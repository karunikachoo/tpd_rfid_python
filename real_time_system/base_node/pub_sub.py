import collections
import time

from base_node.networking_src.network_utils import NetworkUtils
from messages.base_msg import BaseMessage
from messages.cmd_msgs import CmdMessage
from tools.print_utils import bcol, Log

from messages.msg_struct import Wrapper
from messages.pub_sub_msg import PubSubMsg

TAG = "PublisherSubscriber:"
log = Log(bcol.YELLOW, TAG)


class Publisher:
    def __init__(self, topic, msg_type, publish_callback=None, log_size=0, latch=False):
        self.topic = topic
        self.type = msg_type
        self._cb = publish_callback

        self.latch = latch
        if self.latch and log_size <= 0:
            log_size = 1

        self.logs = collections.deque([], log_size)
        self.log_size = log_size

    def publish(self, msg):
        self.log_msg(msg)
        self._cb(self.topic, msg.to_json())

    def log_msg(self, msg):
        if self.log_size > 0 or self.latch:
            self.logs.append(msg.to_json())


class Subscriber:
    def __init__(self, topic, msg_type, callback, pass_wrapper=False):
        self.topic = topic
        self.type = msg_type
        self.callback = callback

        self.pass_wrapper = pass_wrapper

        self.conns = {}

    def on_recv(self, wrap: Wrapper):
        msg = self.type().load(wrap.payload)
        if self.pass_wrapper:
            self.callback(msg, wrap)
        else:
            self.callback(msg)

    def has_server(self, server):
        for uuid in self.conns:
            if server == self.conns[uuid]:
                return True
        return False


class Service:
    """
    cmd_topic is where the service listens for request cmds (sub)
    reply_topic allows the service to send its replies there.
    """
    def __init__(self, cmd_topic, reply_topic, cmd_callback):
        self.callback = cmd_callback
        self.cmd_topic = cmd_topic
        self.reply_topic = reply_topic

        self.sub = None             # type: Subscriber
        self.pub = None             # type: Publisher

    def reply(self, cmd_msg: CmdMessage, data_msg: BaseMessage):
        cmd_msg.data = data_msg.to_json()
        self.pub.publish(cmd_msg)


class Request:
    """
    cmd_topic is the topic the requester sends the cmd to   (pub)
    the requester has to look for its uid in the reply_topic to get the reply (sub)
    """
    def __init__(self, cmd_topic, reply_topic, msg_type, callback):
        self.uid = NetworkUtils.random_str(4)
        self.callback = callback
        self.cmd_topic = cmd_topic
        self.reply_topic = reply_topic
        self.msg_type = msg_type

        self.sub = None             # type: Subscriber
        self.pub = None             # type: Publisher

    def request(self, cmd, data):
        cmd_msg = CmdMessage()
        cmd_msg.uid = self.uid
        cmd_msg.cmd = cmd
        cmd_msg.data = data

        self.pub.publish(cmd_msg)

    def recv_reply(self, cmd_msg: CmdMessage):
        if cmd_msg.uid == self.uid:
            msg = self.msg_type().load(cmd_msg.data)
            self.callback(msg)


class PublisherSubscriber:
    def __init__(self, network):
        self.network = network

        self.publishers = {}
        self.subscribers = {}

        self.publishers_in = {}
        self.subscribers_in = {}

        self.request_map = {}

        self.uuid_map = {}

        self.tested_servers = {}
        self.last_housekeeping = time.time()

    def housekeeping(self):
        if time.time() - self.last_housekeeping > 15:
            for uuid in self.tested_servers:
                self.network.client_handler.send_to_uid(uuid, Wrapper.ALIVE, "Alive?", self.publisher_still_active)

            self.last_housekeeping = time.time()

    def publisher_still_active(self, uuid, active):
        if not active:
            topics, server, client = self.tested_servers[uuid]
            for topic in topics:
                self.publishers_in[topic].remove(server)
                self.notify_requesters(topic)
                log.w("Publisher %s:%d pruned from topic \"%s\"" % (server[0], server[1], topic))
            client.close()
            del self.tested_servers[uuid]

    def create_publisher(self, topic, msg_type, latch=False):
        pub = Publisher(topic, msg_type, self.publish, latch=latch)
        self.publishers[topic] = pub
        self.register_publisher_with_master(pub)
        return pub

    def create_subscriber(self, topic, msg_type, callback, pass_wrapper=False):
        sub = Subscriber(topic, msg_type, callback, pass_wrapper=pass_wrapper)
        self.subscribers[topic] = sub
        self.request_publisher_from_master(sub)
        return sub

    def create_service(self, cmd_topic, reply_topic, callback):
        serv = Service(cmd_topic, reply_topic, callback)

        sub = self.create_subscriber(cmd_topic, CmdMessage, callback)
        serv.sub = sub
        pub = self.create_publisher(reply_topic, CmdMessage)
        serv.pub = pub

        return serv

    def create_request(self, cmd_topic, reply_topic, msg_type, callback):
        req = Request(cmd_topic, reply_topic, msg_type, callback)

        sub = self.create_subscriber(reply_topic, CmdMessage, req.recv_reply)
        req.sub = sub
        pub = self.create_publisher(cmd_topic, CmdMessage)
        req.pub = pub

        return req

    def register_publisher_with_master(self, pub: Publisher):
        port = self.network.server.get_server_port()
        msg = PubSubMsg(PubSubMsg.PUB, pub.topic,
                        (self.network.ip_addr, port))
        if not self.network.master.is_master:
            self.network.master.conn.send(Wrapper.PUB_SUB, msg.to_json())
        else:
            self.network.recv_q.put(msg.to_json())

    def request_publisher_from_master(self, sub: Subscriber):
        msg = PubSubMsg(PubSubMsg.SUB_REQ, sub.topic)
        if not self.network.master.is_master:
            self.network.master.conn.send(Wrapper.PUB_SUB, msg.to_json())
        else:
            self.network.recv_q.put(msg.to_json())

    def update_subscriber_servers(self, topic, servers: list):
        sub = self.subscribers[topic]        # type: Subscriber

        log.d("Received %s server list from master: " % topic, servers)

        for server in servers:
            if not sub.has_server(server):
                client = self.network.client_handler.conn_to(*server, self.on_subscriber_connected)
                sub.conns[client.uuid] = server
                self.uuid_map[client.uuid] = sub

    def on_subscriber_connected(self, uuid, connected):
        subs = self.match_subscribers(uuid)
        if connected:
            for sub in subs:
                msg = PubSubMsg(PubSubMsg.SUB, sub.topic)
                self.network.client_handler.send_to_uid(uuid, Wrapper.PUB_SUB, msg.to_json())
        else:
            for sub in subs:
                del sub.conns[uuid]
                del self.uuid_map[uuid]

    def publish(self, topic, msg_str):
        if topic in self.subscribers_in:
            remove = []
            for uuid in self.subscribers_in[topic]:
                client = self.network.client_handler.get_client(uuid)

                if client is not None:
                    client.send(Wrapper.DATA, msg_str)
                else:
                    remove.append(uuid)
            for uuid in remove:
                self.subscribers_in[topic].remove(uuid)

    def match_subscribers(self, uuid):
        if uuid in self.uuid_map:
            sub = self.uuid_map[uuid]
            if type(sub) == Subscriber:
                return [sub]
        ret = []
        for topic in self.subscribers:
            sub = self.subscribers[topic]
            if uuid in sub.conns:
                ret.append(sub)
        return ret

    def catchup(self, topic, uuid):
        if topic in self.publishers:
            client = self.network.client_handler.get_client(uuid)

            for _log in self.publishers[topic].logs:
                client.send(Wrapper.DATA, _log)

    """
    REMOTE IN
    """

    def subscribe_in(self, topic, uuid):
        """
        Used when another node connects to this node to subscribe
        to a particular topic
        :param topic:   Topic (str)
        :param uuid:    client uuid (str)
        :return:
        """
        if topic not in self.subscribers_in:
            self.subscribers_in[topic] = []

        if uuid not in self.subscribers_in[topic]:
            self.subscribers_in[topic].append(uuid)
            self.uuid_map[uuid] = topic
            self.network.client_handler.add_on_send_success_listener(uuid, self.subscriber_alive_check)

        self.catchup(topic, uuid)

    def subscriber_alive_check(self, uuid, alive):
        if not alive:
            self.network.client_handler.close(uuid)
            if uuid in self.uuid_map:
                key = self.uuid_map[uuid]
                if key in self.subscribers:
                    if uuid in self.subscribers[key]:
                        self.subscribers_in[key].remove(uuid)
                del self.uuid_map[uuid]

    def register_publisher(self, topic, server):
        """
        Register publisher from other nodes at master
        :param topic:   topic str
        :param server:  tuple(ip: str, port:int)
        :return:
        """
        server = tuple(server)
        if topic not in self.publishers_in:
            self.publishers_in[topic] = []

        if server not in self.publishers_in[topic]:
            self.publishers_in[topic].append(server)
            log.d(server)
            log.i("Publisher from %s : %d registered for topic \"%s\'" % (server[0], server[1], topic))

            target_uuid = None
            for uuid in self.tested_servers:
                topics, _server, client = self.tested_servers[uuid]
                if server == _server:
                    target_uuid = uuid
                    break
            if target_uuid is None:
                client = self.network.client_handler.conn_to(
                    server[0], server[1], self.publisher_still_active)
                self.tested_servers[client.uuid] = ([topic], server, client)
            else:
                self.tested_servers[target_uuid][0].append(topic)

            self.notify_requesters(topic)
        else:
            log.w("Duplicate Publisher from %s : %d in topic \"%s\"" % (server[0], server[1], topic))

    def subscriber_server_request(self, uuid, topic) -> list:
        """
        Request only to master for servers publishing topic
        :param topic:
        :return:
        """
        if topic not in self.request_map:
            self.request_map[topic] = []

        self.request_map[topic].append(uuid)

        if topic in self.publishers_in:
            return self.publishers_in[topic]
        return []

    def notify_requesters(self, topic):
        servers = []
        if topic in self.publishers_in:
            servers = self.publishers_in[topic]

        if topic in self.request_map:
            ret = PubSubMsg(
                cmd=PubSubMsg.SUB_REPLY,
                topic=topic,
                data=servers)

            for uuid in self.request_map[topic]:
                self.network.client_handler.send_to_uid(uuid, Wrapper.PUB_SUB, ret.to_json())
