import json

from base_node.pub_sub import PublisherSubscriber
from messages.msg_struct import Wrapper
from tools.print_utils import bcol, Log

from messages.pub_sub_msg import PubSubMsg

TAG = "MessageHandler:"
log = Log(bcol.LTGRAY, TAG)


class MessageHandler:
    def __init__(self, networking):
        self.networking = networking

        self.pub_sub = PublisherSubscriber(self.networking)

        self.subscriptions_out = {}
        self.subscriptions_in = {}
        self.publishers = {}

    """
    MSGS
    """

    def handle_msgs(self, m: Wrapper):
        # strip wrapper
        if type(m) != Wrapper:
            return

        if m.type == Wrapper.DATA:
            self._handle_data(m)
        elif m.type == Wrapper.PUB_SUB:
            self._handle_pub_sub(m)
        elif m.type == Wrapper.VERIFY:
            self._handle_verify(m)
        elif m.type == Wrapper.DISCOVER:
            self._handle_discover(m, False)
        elif m.type == Wrapper.DISCOVER_REPLY:
            self._handle_discover(m, True)

    def _handle_discover(self, wrap: Wrapper, reply):
        data = json.loads(wrap.payload)
        ip = data['ip']
        port = int(data['port'])
        if not reply:
            self.networking.on_discover_request_recv(ip, port)

        else:
            self.networking.on_discover_reply_recv(ip, port)

    def _handle_data(self, wrap: Wrapper):
        subs = self.pub_sub.match_subscribers(wrap.uuid)
        for sub in subs:
            sub.on_recv(wrap)

    def _handle_pub_sub(self, wrap: Wrapper):
        msg = PubSubMsg().load(wrap.payload)

        if msg.cmd == PubSubMsg.PUB:
            self.pub_sub.register_publisher(msg.topic, msg.data)

        elif msg.cmd == PubSubMsg.UNPUB:
            """"""

        elif msg.cmd == PubSubMsg.SUB_REQ:
            servers = self.pub_sub.subscriber_server_request(wrap.uuid, msg.topic)
            ret = PubSubMsg(
                cmd=PubSubMsg.SUB_REPLY,
                topic=msg.topic,
                data=servers)

            self.networking.client_handler.send_to_ip(
                wrap.sender['ip'], wrap.sender['port'],
                Wrapper.PUB_SUB,
                ret.to_json()
            )

            log.i("Sub info request from %s : %d for topic %s -> " % (wrap.sender['ip'],
                                                                      wrap.sender['port'],
                                                                      msg.topic), servers)
        elif msg.cmd == PubSubMsg.SUB_REPLY:
            self.pub_sub.update_subscriber_servers(msg.topic, msg.data)

        elif msg.cmd == PubSubMsg.SUB:
            log.i("%s : %d subbing to \"%s\"" % (wrap.sender['ip'], wrap.sender['port'],
                                                 msg.topic))
            self.pub_sub.subscribe_in(msg.topic, wrap.uuid)

        elif msg.cmd == PubSubMsg.UNSUB:
            """"""

    def _handle_verify(self, wrap: Wrapper):
        # log.i(wrap.payload)
        token = bytearray.fromhex(wrap.payload)
        token.reverse()

        # log.i(token)

        hex_rev = token.hex()

        if self.networking.master.token != hex_rev and self.networking.master.is_master:
            log.i("VERIFY->", "Am master. Convoluting token %s -> %s" % (wrap.payload, hex_rev))
            self.networking.client_handler.send_to_ip(
                wrap.sender['ip'], wrap.sender['port'], wrap.type, hex_rev
            )
        else:
            self.networking.on_master_verified(hex_rev, self.networking.master.token == hex_rev)
