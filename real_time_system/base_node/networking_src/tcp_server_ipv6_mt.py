import socket
import sys
import threading
import time
import traceback

from tools.print_utils import Log, bcol

TAG = "TcpServerIPv6:"
log = Log(bcol.DKGRAY, TAG)


class TCPServerIPv6(threading.Thread):
    def __init__(self, client_handler, port: int):
        super(TCPServerIPv6, self).__init__()
        self.client_handler = client_handler

        self.port = port
        self.socket = None
        self.clients = []

        self.alive = threading.Event()
        self.alive.set()

        self.init()

    def init(self):
        try:
            self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind(("", self.port, 0, 0))
            self.socket.listen(20)
            log.d(self.socket.getsockname())
            self.port = self.socket.getsockname()[1]
        except OSError as e:
            log.e(e)
            if e.errno == 98:
                """TODO: make kill thread"""
            self.socket.close()
            self.socket = None

    def run(self):
        try:
            while self.alive.isSet():
                try:
                    if self.socket is None:
                        self.init()
                    else:
                        self.wait_for_connection()
                except Exception as e:
                    log.e("line:54", traceback.format_exc())
                time.sleep(0.01)
        except KeyboardInterrupt:
            self.socket.close()
            self.join()

    def join(self, timeout=None):
        self.alive.clear()
        try:
            if not sys.platform.startswith("darwin") and not sys.platform.startswith("win32"):
                self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
        except Exception as e:
            log.e("line:64", traceback.format_exc())
        threading.Thread.join(self, timeout)

    def get_server_port(self):
        return self.socket.getsockname()[1]

    def wait_for_connection(self):
        # print(self.socket, type(self.socket))
        conn, addr = self.socket.accept()

        log.d("received connection from %s" % str(addr))
        if self.client_handler is not None:
            self.client_handler.add_client(conn)
