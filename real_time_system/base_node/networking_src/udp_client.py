import socket

from messages.msg_struct import Wrapper
from tools.print_utils import Log, bcol
from ..constants import NodeConst

TAG = "UPDBroadcast:"
log = Log(bcol.LTMAGENTA, TAG)


class UDPBroadcast:
    def __init__(self, port=NodeConst.MASTER_UDP_PORT):
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        try:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        except:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    def send(self, msg: str):
        w = Wrapper(
            sender=("", 0),
            msg_type=Wrapper.DISCOVER,
            payload=msg
        )
        log.i("Broadcasting to local network")
        try:
            self.socket.sendto(w.to_json().encode(), ('<broadcast>', self.port))
        except Exception as e:
            log.e(e)
