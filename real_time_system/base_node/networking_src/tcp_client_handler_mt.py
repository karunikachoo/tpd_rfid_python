import threading
import queue
import time

from tools.print_utils import bcol, Log

from messages.msg_struct import Wrapper
from .tcp_client_ipv6_mt import TCPClientIPv6
from .tcp_client_mt import TCPClientReply, SocketClientThread, TCPClientCommand

TAG = "TCP Client Handler:"
log = Log(bcol.BLUE, TAG)


class TcpClientHandler(threading.Thread):
    def __init__(self, recv_q=None):
        super(TcpClientHandler, self).__init__()

        self.alive = threading.Event()
        self.alive.set()

        self.recv_q = recv_q or queue.Queue()
        self.reply_q = queue.Queue()

        self.clients = {}

        self.conn_succ_cb = {}
        self.send_succ_cb = {}

    def run(self):
        try:
            while self.alive.isSet():
                try:
                    reply = self.reply_q.get(True, 0.001)        # type: TCPClientReply
                    self.process_reply(reply)
                except queue.Empty:
                    continue
                time.sleep(0.001)

        except KeyboardInterrupt:
            for uid in self.clients:
                self.clients[uid].close()
            self.join()

    def join(self, timeout=None):
        self.alive.clear()
        threading.Thread.join(self, timeout)

    def conn_to(self, ip, port, success_callback=None) -> SocketClientThread:
        client = SocketClientThread(reply_q=self.reply_q)
        client.start()
        log.d("Client Started")

        client.connect(ip, port)
        log.d("Client Connecting...")

        self.clients[client.uuid] = client
        if success_callback is not None:
            self.conn_succ_cb[client.uuid] = success_callback
        return client

    def send_to_uid(self, uuid, msg_type, msg: str, success_cb=None):
        client = self.get_client(uuid)
        if success_cb is not None:
            self.add_on_send_success_listener(uuid, success_cb)
        if client is not None:
            client.send(msg_type, msg)

    def send_to_ip(self, ip, port, msg_type, msg: str):
        for uuid in self.clients:     # type: SocketClientThread
            client = self.clients[uuid]
            recpt = client.get_recipient()
            if recpt[0] is None and recpt[1] == 0:
                log.f("SOCKET DEAD")
                # client.close()
                # del self.clients[uuid]
                return
            # print(recpt, ip, port)
            if str(recpt[0]) == str(ip) and int(recpt[1]) == int(port):
                log.d("Client found for %s : %d" % (ip, port))
                client.send(msg_type, msg)
                return

        log.e("%s : %d Client not found. Creating client..." % (ip, port))

        def send_msg(uuid, succ):
            if succ:
                self.send_to_uid(uuid, msg_type, msg)

        self.conn_to(ip, port, send_msg)

    def close(self, uuid):
        self.get_client(uuid).close()

    def add_client(self, conn):
        client = SocketClientThread(reply_q=self.reply_q, client_socket=conn)
        client.start()
        log.d("Client Started")

        self.clients[client.uuid] = client
        log.d("Client Added")
        return client

    def get_client(self, uid):
        if uid in self.clients:
            return self.clients[uid]
        return None

    def add_on_send_success_listener(self, uuid, callback):
        if uuid not in self.send_succ_cb:
            self.send_succ_cb[uuid] = []

        if callback not in self.send_succ_cb[uuid]:
            self.send_succ_cb[uuid].append(callback)

    def remove_on_send_success_listener(self, uuid, callback):
        if callback in self.send_succ_cb[uuid]:
            self.send_succ_cb[uuid].remove(callback)

    def process_reply(self, reply: TCPClientReply):
        if reply.cmd == TCPClientReply.ERROR:
            if type(reply.data) == TCPClientCommand:
                if reply.data.cmd == TCPClientCommand.CONNECT:
                    if reply.uuid in self.conn_succ_cb:
                        log.e("Client %s failed to connect to %s:%d" % (reply.uuid, reply.data.data[0], reply.data.data[1]))
                        self.conn_succ_cb[reply.uuid](reply.uuid, False)

                elif reply.data.cmd == TCPClientCommand.CLOSE:
                    log.e("Client %s failed to close" % reply.uuid)

                elif reply.data.cmd == TCPClientCommand.SEND or reply.data.cmd == TCPClientCommand.RECEIVE:
                    if reply.uuid in self.send_succ_cb:
                        for cb in self.send_succ_cb[reply.uuid]:
                            cb(reply.uuid, False)
                    else:
                        self.close(reply.uuid)
            else:
                log.e(reply.data)
        elif reply.cmd == TCPClientReply.SUCCESS:
            if type(reply.data) == TCPClientCommand:
                if reply.data.cmd == TCPClientCommand.CONNECT:
                    if reply.uuid in self.conn_succ_cb:
                        log.d("Client %s connected successfully to %s:%d" % (reply.uuid, reply.data.data[0], reply.data.data[1]))
                        self.conn_succ_cb[reply.uuid](reply.uuid, True)

                elif reply.data.cmd == TCPClientCommand.CLOSE:
                    log.d("Client %s closed" % reply.uuid)
                    self.clients[reply.uuid].join()
                    if reply.uuid in self.clients:
                        del self.clients[reply.uuid]
                    if reply.uuid in self.send_succ_cb:
                        del self.send_succ_cb[reply.uuid]
                    if reply.uuid in self.conn_succ_cb:
                        del self.conn_succ_cb[reply.uuid]

                elif reply.data.cmd == TCPClientCommand.SEND:
                    if reply.uuid in self.send_succ_cb:
                        for cb in self.send_succ_cb[reply.uuid]:
                            cb(reply.uuid, True)

        elif reply.cmd == TCPClientReply.RECV:
            wrap = Wrapper().load(reply.data)
            wrap.uuid = reply.uuid

            self.recv_q.put(wrap)


