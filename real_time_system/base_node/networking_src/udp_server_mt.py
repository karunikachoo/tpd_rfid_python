import queue
import socket
import threading
import time

from messages.msg_struct import Wrapper
from tools.print_utils import Log, bcol
from .network_utils import NetworkUtils
from ..constants import NodeConst

TAG = "UDPServerMT:"
log = Log(bcol.MAGENTA, TAG)


class UDPServerMt(threading.Thread):
    def __init__(self, port=NodeConst.MASTER_UDP_PORT, recv_q=None):
        super(UDPServerMt, self).__init__()

        self.recv_q = recv_q or queue.Queue()

        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        try:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        except:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(("", int(self.port)))

        self.alive = threading.Event()
        self.alive.set()

    def run(self):
        try:
            while self.alive.isSet():
                try:
                    data = self.socket.recv(NetworkUtils.BUFFER_SIZE)
                    w = Wrapper().load(data.decode())
                    self.recv_q.put(w)
                except IOError as e:
                    log.e(e)
                time.sleep(0.01)
        except KeyboardInterrupt:
            self.join()

    def join(self, timeout=None):
        self.alive.clear()
        self.socket.close()
        threading.Thread.join(self, timeout)