import socket
import struct
import sys

from base_node.constants import NodeConst
from messages.msg_struct import Wrapper
from tools.print_utils import Log, bcol

if sys.platform == 'win32':
    socket.IPPROTO_IPV6 = 41

TAG = "MulticastClient:"
log = Log(bcol.LTMAGENTA, TAG)


class MulticastClient:
    def __init__(self, group, port, ttl, iface: int):
        self.group = group
        self.port = port
        self.iface = iface

        self.addrinfo = socket.getaddrinfo(group, None)[0]

        self.socket = socket.socket(self.addrinfo[0], socket.SOCK_DGRAM, socket.IPPROTO_UDP)

        self.ttl_bin = struct.pack("@i", ttl)

        if self.addrinfo[0] == socket.AF_INET:  # IPv4
            self.socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, self.ttl_bin)
        else:
            self.socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, self.ttl_bin)
            self.socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_IF, iface)

        self.socket.connect((self.addrinfo[4][0], self.port))
        log.d(self.socket)

    def send(self, msg: str):
        w = Wrapper(
            sender=("", 0),
            msg_type=Wrapper.DISCOVER,
            payload=msg
        )

        log.i("Multicasting to", self.group)
        try:
            self.socket.send(w.to_json().encode())
        except Exception as e:
            log.e(e)


if __name__ == "__main__":
    if sys.platform == 'win32':
        c = MulticastClient(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, 10, 0)
    elif sys.platform == 'darwin':
        c = MulticastClient(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, 10, socket.if_nametoindex('en0'))
    else:
        c = MulticastClient(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, 10, 0)
    c.send("Hellooooooo")
