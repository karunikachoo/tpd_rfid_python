import errno
import socket
import threading
import queue
import time
import traceback

from tools.print_utils import bcol, Log
from messages.msg_struct import Wrapper
from .network_utils import NetworkUtils, TCPClientCommand, TCPClientReply

TAG = "TcpClient:"
log = Log(bcol.DKGRAY, TAG, level=Log.INFO)
# log = Log(bcol.DKGRAY, TAG)


MLC = 4


class SocketClientThread(threading.Thread):

    def __init__(self, cmd_q=None, reply_q=None, client_socket=None):
        super(SocketClientThread, self).__init__()
        # self.uuid = str(uuid.uuid4().hex)
        self.uuid = NetworkUtils.random_str(8)

        self.connected = False

        self.cmd_q = cmd_q or queue.Queue()
        self.reply_q = reply_q or queue.Queue()

        self.alive = threading.Event()
        self.alive.set()

        self.socket = client_socket

        self.handlers = {
            TCPClientCommand.CONNECT: self._handle_connect,
            TCPClientCommand.CLOSE: self._handle_close,
            TCPClientCommand.SEND: self._handle_send,
        }

        self.last_alive_check = time.time()

        self.recv_buf = bytearray()

    def run(self):
        try:
            while self.alive.isSet():
                try:
                    cmd = self.cmd_q.get(True, 0.001)        # type: TCPClientCommand
                    self.handlers[cmd.cmd](cmd)
                except queue.Empty as e:
                    if self.socket is not None:
                        self._handle_receive()

                if time.time() - self.last_alive_check > 10:
                    self.alive_check()
                    self.last_alive_check = time.time()
                if self.cmd_q.qsize() > 100:
                    log.w(self.cmd_q.qsize())

                time.sleep(0.001)
        except KeyboardInterrupt:
            self._handle_close(TCPClientCommand(TCPClientCommand.CLOSE))
            self.join()

    def join(self, timeout=None):
        self.alive.clear()
        threading.Thread.join(self, timeout)

    """
    ===========
    Quick Funcs
    ===========
    """

    def alive_check(self):
        self.send(Wrapper.ALIVE, "Alive?")

    def connect(self, ip, port):
        self.cmd_q.put(TCPClientCommand(TCPClientCommand.CONNECT, (ip, port)))

    def close(self):
        self.cmd_q.put(TCPClientCommand(TCPClientCommand.CLOSE))

    def raw_send(self, data: str):
        self.cmd_q.put(TCPClientCommand(TCPClientCommand.SEND, data))

    def send(self, msg_type, data: str):
        w = Wrapper(
            sender=self.get_sender(),
            msg_type=msg_type,
            payload=data
        )
        self.raw_send(w.to_json())

    def get_sender(self):
        try:
            return self.socket.getsockname()
        except IOError as e:
            log.e(self.uuid, str(e))
            # print(traceback.format_exc())
            # self.close()
            return None, 0

    def get_recipient(self):
        try:
            ip = self.socket.getpeername()
            return ip
        except IOError as e:
            log.e(self.uuid, str(e))
            # print(traceback.format_exc())
            # self.close()
            return "", 0

    """
    ========
    Handlers
    ========
    """

    def _handle_connect(self, cmd):
        try:
            addrinfo = socket.getaddrinfo(cmd.data[0], None)[0]

            if addrinfo[0] == socket.AF_INET:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((cmd.data[0], cmd.data[1]))
            else:
                self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
                self.socket.connect((cmd.data[0], cmd.data[1], 0, 0))

            self.connected = True
            self.reply_q.put(self._success_reply(data=cmd))
        except IOError as e:
            log.e(self.uuid, e)
            # print(traceback.format_exc())
            self.reply_q.put(self._error_reply(data=cmd))

    def _handle_close(self, cmd=None):
        self.socket.close()
        self.alive.clear()
        self.reply_q.put(self._success_reply(data=cmd))

    def _handle_send(self, cmd):
        try:
            log.d(self.uuid, "sending msg to ", self.get_recipient())
            msg = SocketClientThread.structify(message=cmd.data)
            self.socket.send(msg)
            self.reply_q.put(self._success_reply(data=cmd))

        except IOError as e:
            log.e(self.uuid, e)
            # print(traceback.format_exc())
            self.reply_q.put(self._error_reply(data=cmd))

    def _handle_receive(self):
        try:
            self.socket.settimeout(0.001)
            byte_arr = self.socket.recv(NetworkUtils.BUFFER_SIZE)

            self.recv_buf.extend(byte_arr)

            state = 1
            while state == 1:
                state = self.parse_data()

        except socket.timeout as e:
            """"""
        except socket.error as e:
            if e.args[0] == errno.EAGAIN or e.args[0] == errno.EWOULDBLOCK:
                """"""
            else:
                log.e(e)
                self.reply_q.put(self._error_reply(TCPClientCommand(TCPClientCommand.RECEIVE)))
                self.close()
                # print(traceback.format_exc())

    def parse_data(self):
        """
        Case 1:
        0xaa [num] [data] ....

        Case 2:
        0xaa [num] [data] ... 0xcc 0xaa [num] ...
        """

        if 0xaa in self.recv_buf:
            if 0xcc in self.recv_buf:       # case 2
                i0 = self.recv_buf.index(0xaa)
                # i1 = self.recv_buf.index(0xcc)

                n = int.from_bytes(self.recv_buf[i0+1: i0+MLC+1], 'big')
                i1 = i0 + (MLC + 1) + n - 1

                data = self.recv_buf[i0+MLC+1:i1]

                if len(data) == n - 1:
                    # print(data)
                    self.reply_q.put(self._recv_reply(data.decode()))
                else:
                    # print(data)
                    print(self.recv_buf)
                    self.reply_q.put(self._error_reply("WRONG DATA LENGTH RECEIVED"))
                    return 0

                self.recv_buf = self.recv_buf[i1+1:]
                return 1
            else:                           # case 1
                return 0
        else:                               # recommend discard data
            self.recv_buf.clear()
            return -1

    def _error_reply(self, data):
        return TCPClientReply(self.uuid, TCPClientReply.ERROR, data)

    def _success_reply(self, data=None):
        return TCPClientReply(self.uuid, TCPClientReply.SUCCESS, data)

    def _recv_reply(self, data=None):
        return TCPClientReply(self.uuid, TCPClientReply.RECV, data)

    @staticmethod
    def structify(message: str) -> bytearray:
        data = message.encode()
        msg = bytearray()
        msg.append(0xaa)
        msg.extend((len(data) + 1).to_bytes(MLC, 'big'))
        msg.extend(data)
        msg.append(0xcc)
        return msg