import errno
import socket
import threading
import queue
import time
import traceback

from base_node.constants import NodeConst
from base_node.networking_src.tcp_client_mt import SocketClientThread
from tools.print_utils import bcol, Log
from messages.msg_struct import Wrapper
from base_node.networking_src.network_utils import NetworkUtils, TCPClientCommand

TAG = "TcpClientIPv6:"
log = Log(bcol.DKGRAY, TAG)


class TCPClientIPv6(SocketClientThread):
    def __init__(self, cmd_q=None, reply_q=None, client_socket=None):
        super(TCPClientIPv6, self).__init__(cmd_q, reply_q, client_socket)

    def _handle_connect(self, cmd):
        try:
            self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
            self.socket.connect((cmd.data[0], cmd.data[1], 0, 0))

            self.connected = True
            self.reply_q.put(self._success_reply(data=cmd))
        except IOError as e:
            log.e(traceback.format_exc())
            # print(traceback.format_exc())
            self.reply_q.put(self._error_reply(data=cmd))


if __name__ == "__main__":
    addrinfo = socket.getaddrinfo(NodeConst.LOCALHOST_IPV6, None)[0]
    print(addrinfo)