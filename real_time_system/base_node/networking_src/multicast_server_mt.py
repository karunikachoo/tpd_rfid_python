import os
import queue
import socket
import struct
import sys
import threading
import time
import traceback

from base_node.constants import NodeConst
from base_node.networking_src.network_utils import NetworkUtils
from messages.msg_struct import Wrapper
from tools.print_utils import Log, bcol

if sys.platform == 'win32':
    socket.IPPROTO_IPV6 = 41

"""
MULTICAST ADDRESSES
FFXY::/8
X -> 4 bits for FLAGS
Y -> 4 bits for scope

X: 0 R P T
0 (reserved)
R (Rendezvous)
P (Network Prefix)
T (Transient)  ->  0 Permanent (IANA reserved) | 1 Transient (dynamic define by application)

Y -> Scope
0 (reserved)
1 (Interface-local)
2 (link-local)
3 (realm-local (iot))
4 (admin-local)
5 (site-local)
8 (organisation-local)
E (global)
F (reserved)

everything after the first 16 bits is group id
"""


TAG = "MulticastServerMT:"
log = Log(bcol.MAGENTA, TAG)


class MulticastServer(threading.Thread):
    def __init__(self, group, port, iface_index, recv_q=None):
        threading.Thread.__init__(self)

        self.ip = group
        self.port = port
        self.iface_index = iface_index
        self.recv_q = recv_q or queue.Queue()

        self.addrinfo = socket.getaddrinfo(group, None)[0]
        log.d(self.addrinfo)

        self.socket = socket.socket(self.addrinfo[0], socket.SOCK_DGRAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_IF, self.iface_index)

        self.socket.bind(('', self.port))

        log.d("socket bound to", self.socket.getsockname())

        self.bin_group = socket.inet_pton(self.addrinfo[0], self.addrinfo[4][0])

        self.join_group()

        self.alive = threading.Event()
        self.alive.set()

    def join_group(self):
        try:
            if self.addrinfo[0] == socket.AF_INET:
                mreq = self.bin_group + struct.pack('=I', socket.INADDR_ANY)
                self.socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
            else:
                mreq = struct.pack(
                    '16sI',
                    socket.inet_pton(socket.AF_INET6, self.addrinfo[4][0]),
                    self.iface_index
                )
                self.socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, mreq)

            log.d("Socket joined mcast group", self.addrinfo[4][0])
        except Exception:
            log.e(traceback.format_exc())

    def run(self):
        try:
            log.d("Multicast Server Started")
            while self.alive.is_set():
                try:
                    data = self.socket.recv(NetworkUtils.BUFFER_SIZE)
                    w = Wrapper().load(data.decode())
                    print(w)
                    self.recv_q.put(w)
                except IOError as e:
                    log.e(e)
                time.sleep(0.01)
        except KeyboardInterrupt:
            self.join()

    def join(self, timeout=None):
        self.alive.clear()
        self.socket.close()
        threading.Thread.join(self, timeout)


if __name__ == "__main__":
    if sys.platform == 'win32':
        m = MulticastServer(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, 0)
    elif sys.platform == 'darwin':
        m = MulticastServer(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, socket.if_nametoindex('en0'))
    else:
        m = MulticastServer(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, socket.if_nametoindex('wlan0'))

    m.start()

    try:
        while True:
            time.sleep(5)
    except KeyboardInterrupt:
        os._exit(1)



