import random
import string
import time


class TCPClientCommand(object):
    """
    ===========  ==================
    Cmd          Data Type
    ===========  ==================
    CONNECT      (host, port) tuple
    SEND         Data string
    RECEIVE      None
    CLOSE        None
    ===========  ==================
    """

    CONNECT, SEND, RECEIVE, CLOSE = range(4)

    def __init__(self, cmd: int, data=None):
        self.cmd = cmd
        self.data = data


class TCPClientReply(object):
    ERROR, SUCCESS, RECV = range(3)

    def __init__(self, uid, cmd, data=None):
        self.uuid = uid
        self.cmd = cmd
        self.data = data


class NetworkUtils:
    BUFFER_SIZE = 4096

    @staticmethod
    def int_to_n_bytes(n, num) -> bytearray:
        if num > 0xff**n:
            print("number too large")
            return bytearray(n)

        arr = bytearray(n)

        for i in range(n):
            arr[n-i-1] = (num >> i * 8) & 0xff
        return arr

    @staticmethod
    def n_bytes_to_int(n, byte_arr: bytearray) -> int:

        b = 0x00

        for i in range(n):
            b |= (byte_arr[i] << ((n - i - 1) * 8))

        return b

    @staticmethod
    def int_to_two_bytes(num) -> bytearray:
        if num > 65535:
            print("number too large")
            return bytearray()

        b2 = num & 0xff
        b1 = num >> 8 & 0xff

        arr = bytearray()
        arr.append(b1)
        arr.append(b2)
        return arr

    @staticmethod
    def two_bytes_to_int(byte_arr: bytearray) -> int:
        return byte_arr[0] << 8 | byte_arr[1]

    @staticmethod
    def kill_tcp_process():
        cmd = "sudo netstat -anp "

    @staticmethod
    def random_str(n):
        ret = ""
        for i in range(n):
            ret += "%02x" % random.randint(0x00, 0xff)
        return ret


if __name__ == "__main__":
    tt0 = []
    tt1 = []
    tt2 = []
    tt3 = []

    for i in range(1000):
        t0 = time.time_ns()
        ba = NetworkUtils.int_to_n_bytes(3, 98765)
        tt0.append(time.time_ns() - t0)

        t0 = time.time_ns()
        ta = (98765).to_bytes(3, byteorder='big')
        tt1.append(time.time_ns() - t0)

        t0 = time.time_ns()
        bb = NetworkUtils.n_bytes_to_int(3, ba)
        tt2.append(time.time_ns() - t0)

        t0 = time.time_ns()
        tb = int.from_bytes(ta, byteorder='big')
        tt3.append(time.time_ns() - t0)

        print(ba, ta, bb, tb)

    print(sum(tt0) / len(tt0))
    print(sum(tt1) / len(tt1))
    print(sum(tt2) / len(tt2))
    print(sum(tt3) / len(tt3))

    _arr = NetworkUtils.int_to_two_bytes(45092)
    print(_arr[0], _arr[1])
    print(hex(_arr[0]), hex(_arr[1]))

    print(NetworkUtils.two_bytes_to_int(_arr))