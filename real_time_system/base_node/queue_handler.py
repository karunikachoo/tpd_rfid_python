import queue
import threading
import time

from tools.print_utils import Log, bcol

TAG = "QueueHandler:"
log = Log(bcol.UNDERLINE, TAG)


class QueueHandler(threading.Thread):
    def __init__(self,  message_handler, recv_q=None, sleep=0.001):
        super(QueueHandler, self).__init__()

        self.msg_handler = message_handler

        self.alive = threading.Event()
        self.alive.set()

        self.sleep_dur = sleep

        self.recv_q = recv_q or queue.Queue()

    def run(self):
        try:
            while self.alive.isSet():
                try:
                    data = self.recv_q.get(True, self.sleep_dur)
                    # print(TAG, data)
                    self.msg_handler.handle_msgs(data)
                    if self.recv_q.qsize() > 20:
                        log.w("Quese size @ %d" % self.recv_q.qsize())
                except queue.Empty:
                    """"""
                time.sleep(self.sleep_dur)
        except KeyboardInterrupt:
            self.join()

    def join(self, timeout=None):
        self.alive.clear()
        threading.Thread.join(self, timeout)