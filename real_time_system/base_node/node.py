import queue
import time

from tools.print_utils import Log, bcol
from .constants import NodeConst
from .message_handler import MessageHandler
from .networking import Networking, Master
from .queue_handler import QueueHandler

"""
Architecture question...
- each device has a local master for ipc
- each LAN as a LAN master
- depending on the config the device can be a local master (if none is
  present) or a LAN master

or

- there can be multiple masters but you can configure your device to only
  target one master.
- no local master
- you can create a companion node to pass data on to other masters.

I THINK B IS BETTER

STARTUP:
if (remote master defined)
    try to connect to ip:port
else if (local master)
    connect to local master
else
    set up self as local master

MASTER:
- keep track of which node is publishing what topic
- direct devices wanting to subscribe to certain topics to the ip:port
  of the publishing node

NODE:
- publish data to topic
- keeps track of data sent
- accept and keep track of subscribers and immediately send data to
  subs

Additional features:
- Add a ping option at a particular port so that devices can use it
  to scan local network for the device

"""

TAG = "Node:"
log = Log(bcol.UNDERLINE, TAG)


class Node:
    def __init__(self, master_ip=None, master_port=None, core=False, queue_sleep=0.0001):
        self.alive = True
        self.recv_q = queue.Queue()

        self.timers = []

        self.networking = Networking((master_ip, master_port), self.recv_q, core=core)
        self.msg_handler = MessageHandler(self.networking)

        self.q_handler = QueueHandler(self.msg_handler, recv_q=self.recv_q, sleep=queue_sleep)
        self.q_handler.start()

        self.networking.init()

        while self.networking.master.state < Master.VERIFIED:
            self.sleep(0.1)

        log.i("Node Successfully Initialised")

        self.sleep(1)

    def create_publisher(self, topic, msg_type, latch=False):
        pub = self.msg_handler.pub_sub.create_publisher(topic, msg_type, latch=latch)
        return pub

    def subscribe(self, topic, msg_type, on_msg_recv_callback, pass_wrapper=False):
        sub = self.msg_handler.pub_sub.create_subscriber(topic, msg_type, on_msg_recv_callback, pass_wrapper=pass_wrapper)
        return sub

    def create_service(self, cmd_topic, reply_topic, on_cmd_recv_callback):
        return self.msg_handler.pub_sub.create_service(cmd_topic, reply_topic, on_cmd_recv_callback)

    def create_request(self, cmd_topic, reply_topic, msg_type, on_reply_recv_callback):
        return self.msg_handler.pub_sub.create_request(cmd_topic, reply_topic, msg_type, on_reply_recv_callback)

    def after(self, s, callback):
        self.timers.append([s, callback])

    def check_timers(self, sleep):
        remove = []
        for timer in self.timers:
            timer[0] -= sleep
            if timer[0] <= 0:
                timer[1]()
                remove.append(timer)

        for timer in remove:
            self.timers.remove(timer)

    def sleep(self, sleep_period):
        self.networking.tasks()
        self.msg_handler.pub_sub.housekeeping()
        self.check_timers(sleep_period)
        time.sleep(sleep_period)

    def run_loop(self, sleep_period):
        try:
            while self.alive:
                self.sleep(sleep_period)

        except KeyboardInterrupt:
            print(TAG, "END")

    @staticmethod
    def sys_parse(argv):
        ip = None
        port = NodeConst.MASTER_PORT

        if "-i" in argv:
            i0 = argv.index("-i")
            ip = str(argv[i0 + 1])

        if "-p" in argv:
            p0 = argv.index("-p")
            port = int(argv[p0 + 1])

        return ip, port


