import json
import socket
import subprocess
import sys
import time
from pprint import pprint

from base_node.networking_src.multicast_client import MulticastClient
from base_node.networking_src.multicast_server_mt import MulticastServer
from base_node.networking_src.tcp_server_ipv6_mt import TCPServerIPv6

if sys.platform == 'win32':
    import netifaces
    import winreg as wr

from messages.msg_struct import Wrapper
from tools.print_utils import bcol, Log

from base_node.constants import NodeConst
from base_node.networking_src.network_utils import NetworkUtils
from base_node.networking_src.tcp_client_handler_mt import TcpClientHandler
from base_node.networking_src.tcp_server_mt import SocketServerThread
from base_node.networking_src.udp_client import UDPBroadcast
from base_node.networking_src.udp_server_mt import UDPServerMt

TAG = "Networking:"
log = Log(bcol.CYAN, TAG)


WIN32_DRIVER_KEYWORDS_INC = ['wi-fi', "wifi", "wireless"]
WIN32_DRIVER_KEYWORDS_EXC = ['virtual']


class Master:
    SEARCHING, CONNECTING, CONNECTED, VERIFIED, SELF = range(5)

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.conn = None
        self.token = NetworkUtils.random_str(8)

        self.is_master = False
        self.last_call_ts = []
        self.discover_ts = -1

        self.state = Master.SEARCHING


class Networking:
    def __init__(self, master_addr: tuple, recv_q, core=False):
        self.master = Master(*master_addr)
        self.recv_q = recv_q
        self.core = core

        self.ipv6 = self._is_ipv6()

        self.ip_addr = self.get_local_ip(self.ipv6)
        self.port = 0

        self.client_handler = TcpClientHandler(recv_q=recv_q)
        self.client_handler.start()

        self.server = None
        self.udp_server = None

    def tasks(self):
        if len(self.master.last_call_ts) > 0:
            if time.time() - self.master.last_call_ts[0] > 2.0:
                self.on_master_verified("NOT_REPLIED", False)

        if self.master.discover_ts > 0:
            if time.time() - self.master.discover_ts > 10.0:
                self.master.ip = None
                self.master.discover_ts = -1
                self.connect_master()

    def init(self):
        if self.core:
            self._init_master()
        else:
            self.discover_on_local_network()

    def _init_master(self):
        log.i("Initialising master...")
        if self.master.conn is not None:
            log.d("Closing previous conns...")
            self.master.conn.close()
            self.master.conn = None

        self._init_tcp_server(port=NodeConst.MASTER_PORT)
        self._init_udp_server()
        self.port = NodeConst.MASTER_PORT
        self.master.is_master = True
        self.master.state = Master.SELF

        log.i("Initialised SELF as master...")

    def discover_on_local_network(self):
        self._init_tcp_server(port=0)   # init temp tcp server
        time.sleep(0.2)
        obj = {
            "ip": self.ip_addr,
            "port": self.server.port
        }

        if self.ipv6:
            client = MulticastClient(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT, 5, self.get_iface_index())
        else:
            client = UDPBroadcast(NodeConst.MASTER_UDP_PORT)

        client.send(json.dumps(obj))
        self.master.discover_ts = time.time()

    def on_discover_request_recv(self, req_ip, req_port):
        if self.master.is_master:
            msg = {
                'ip': self.ip_addr,
                'port': self.server.port
            }

            self.client_handler.send_to_ip(req_ip, req_port, Wrapper.DISCOVER_REPLY, json.dumps(msg))

    def on_discover_reply_recv(self, ip, port):
        self.master.ip = ip
        self.master.port = port
        self.master.discover_ts = -1
        self.connect_master()

    def connect_master(self):
        if self.master.ip is None:
            log.i("No remote master specified, looking locally")
            self.master.conn = self.client_handler.conn_to(
                NodeConst.LOCALHOST_IPV6 if self.ipv6 else NodeConst.LOCALHOST,
                NodeConst.MASTER_PORT,
                self._on_master_connect
            )
        else:
            log.i("Remote master specified, connecting...")
            self.master.conn = self.client_handler.conn_to(
                self.master.ip,
                self.master.port,
                self._on_master_connect
            )
        self.master.state = Master.CONNECTING

    def _on_master_connect(self, uid, success):
        if success:
            self.port = 0
            self._init_tcp_server(port=0)

            log.i("Verifying master... %s" % self.master.token)
            self.master.conn.send(Wrapper.VERIFY, self.master.token)
            self.master.last_call_ts.append(time.time())
            self.master.state = Master.CONNECTED
        else:
            self._init_master()

    def on_master_verified(self, token, verified):
        if verified:
            log.i("Master Found. %s == %s" % (self.master.token, token))
            self.master.last_call_ts.pop(0)
            self.master.state = Master.VERIFIED
        else:
            log.w("Not Master. No Master Found. %s == %s" % (self.master.token, token))
            self.master.last_call_ts.pop(0)
            self._init_master()

    def _init_tcp_server(self, port):
        if self.server is not None:
            log.d("Closing previous TCP server")
            self.server.join()

        log.i("Initialising TCP server...")
        if self.ipv6:
            self.server = TCPServerIPv6(self.client_handler, port)
        else:
            self.server = SocketServerThread(self.client_handler, port)
        self.server.start()

    def _init_udp_server(self):
        if self.udp_server is not None:
            log.d("Closing previous UDP server")
            self.udp_server.join()

        log.i("Initialising UDP server...")
        if self.ipv6:
            self.udp_server = MulticastServer(NodeConst.MCAST_IPV6, NodeConst.MASTER_UDP_PORT,
                                              self.get_iface_index(), self.recv_q)
        else:
            self.udp_server = UDPServerMt(port=NodeConst.MASTER_UDP_PORT, recv_q=self.recv_q)

        self.udp_server.start()

    @staticmethod
    def get_local_ip(ipv6):
        try:
            if ipv6:
                return Networking.ifconfigv6()
            else:
                return Networking.ifconfig()
        except:
            return Networking.get_local_ips()

    @staticmethod
    def get_local_ips():
        return [i[4][0] for i in socket.getaddrinfo(socket.gethostname(), None)]

    @staticmethod
    def get_win32_driver_names(guids) -> dict:
        names = {}
        iface_names = ['(unknown)' for i in range(len(guids))]
        # print(iface_names)

        reg = wr.ConnectRegistry(None, wr.HKEY_LOCAL_MACHINE)
        reg_key = wr.OpenKey(reg, r'SYSTEM\CurrentControlSet\Control\Class\{4d36e972-e325-11ce-bfc1-08002be10318}')
        for i in range(wr.QueryInfoKey(reg_key)[0]):
            subkey_name = wr.EnumKey(reg_key, i)
            try:
                n = int(subkey_name)
                reg_subkey = wr.OpenKey(reg_key, subkey_name)
                guid = wr.QueryValueEx(reg_subkey, 'NetCfgInstanceId')[0]
                if guid in guids:
                    idx = guids.index(guid)
                    iface_names[idx] = wr.QueryValueEx(reg_subkey, 'DriverDesc')[0]
                    names[guid] = wr.QueryValueEx(reg_subkey, 'DriverDesc')[0]
            except ValueError:
                pass
            except Exception as e:
                log.e(e)

        return names

    @staticmethod
    def get_valid_if(driver_names: dict):
        valids = []
        for guid in driver_names:
            name = driver_names[guid].lower()

            for kword in WIN32_DRIVER_KEYWORDS_INC:
                if kword in name:
                    valid = True
                    for exc_word in WIN32_DRIVER_KEYWORDS_EXC:
                        if exc_word in name:
                            valid = False

                    if valid:
                        print(guid, driver_names[guid])
                        valids.append(guid)
        return valids

    @staticmethod
    def get_win32_ip():
        if_guids = netifaces.interfaces()
        names = Networking.get_win32_driver_names(if_guids)
        ifs = Networking.get_valid_if(names)
        ips = []
        for guid in ifs:
            ip = netifaces.ifaddresses(guid)[netifaces.AF_INET]
            ips.append((ip[0]['addr'], ip[0]['broadcast'],))
        # print(ips)
        return ips

    @staticmethod
    def get_win32_ipv6():
        if_guids = netifaces.interfaces()
        names = Networking.get_win32_driver_names(if_guids)
        ifs = Networking.get_valid_if(names)
        ips = []
        for guid in ifs:
            ip = netifaces.ifaddresses(guid)[netifaces.AF_INET6]
            # for iip in ip:
            #     print(iip)
            ips.append((ip[0]['addr'], ip[0]['broadcast'],))
        # print(ips)
        return ips

    @staticmethod
    def win32_ipconfig():
        cmd = ['ipconfig']
        with subprocess.Popen(cmd, stdout=subprocess.PIPE) as p:
            out = p.communicate()[0].decode()
            addresses = out.split("Wireless LAN adapter WiFi")[1].split("Temporary IPv6 Address. . . . . . : ")[0].split("IPv6 Address. . . . . . . . . . . : ")[1:]
            # print(addresses)
            ipaddress = addresses[0].replace("\r", "").replace("\n", "").strip()
            return ipaddress


    @staticmethod
    def ifconfig():
        if sys.platform == 'win32':
            ips = Networking.get_win32_ip()
            return ips[0][0]
            # return socket.gethostbyname(socket.gethostname()), ""
        else:
            iface = ['wlan0', 'en0']
            for _if in iface:
                cmd = ['ifconfig', _if]
                with subprocess.Popen(cmd, stdout=subprocess.PIPE) as p:
                    out = p.communicate()
                    for line in out:
                        if line is not None:
                            line = line.decode()
                            if "inet " in line:
                                ip = line.split("inet ")[-1].split(" netmask")[0].strip()
                                bcast = line.split("broadcast ")[-1].split("\n")[0].strip()
                                return ip
        raise Exception("No valid IPv4 addresses found.")

    @staticmethod
    def ifconfigv6():
        if sys.platform == 'win32':
            return Networking.win32_ipconfig()
            # return socket.gethostbyname(socket.gethostname()), ""
        else:
            iface = ['wlan0', 'en0']
            for _if in iface:
                cmd = ['ifconfig', _if]
                with subprocess.Popen(cmd, stdout=subprocess.PIPE) as p:
                    out = p.communicate()
                    for lines in out:
                        if lines is not None:
                            lines = lines.decode().split("\n")
                            for line in lines:
                                if "inet6 " in line and 'dynamic' not in line and 'fe80' not in line:
                                    print(line)
                                    ip = line.split("inet6 ")[-1].split(" prefixlen")[0].strip()
                                    return ip
        raise Exception("No valid IPv6 addresses found.")

    @staticmethod
    def get_iface_index():
        if sys.platform == 'win32':
            iface = 0
        elif sys.platform == 'darwin':
            iface = socket.if_nametoindex('en0')
        else:
            iface = socket.if_nametoindex('wlan0')
        return iface

    @staticmethod
    def get_win32_wlan_if():
        p = subprocess.Popen(["ipconfig"], stdout=subprocess.PIPE)
        out = p.communicate()

        txt = out[0].decode()
        ipv4 = Networking.get_win32_ip()[0][0]
        ipv6 = Networking.get_win32_ipv6()[0][0]

        data = txt.split(ipv6)[1].split(ipv4)[0]
        link_local_ipv6 = data.split('Link-local IPv6 Address . . . . . : ')[1].split("IPv4 Address. . . . . . . . . . . :")[0]
        if_index = int(link_local_ipv6.split("%")[1].strip())
        # print(data)
        # print(link_local_ipv6)
        # print(if_index)
        return if_index

    def _is_ipv6(self):
        try:
            ip = self.ifconfigv6()
            if ip != "":
                return True
            return False
        except Exception:
            return False


if __name__ == "__main__":
    print(Networking.ifconfigv6())