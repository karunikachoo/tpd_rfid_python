import math
import numpy as np
from typing import List

from imu.quaternion import Quaternion
# from map.universal_map_handler import UniversalMapHandler
from pose_correction.correction_classes import Point, Constraint, TrackedTransform, TFEntry
from pose_correction.pose_constraint_handler import PoseConstraintHandler
# from pose_correction.transform import Transform


class TotalPathTransform:
    def __init__(self, constraint_handler: PoseConstraintHandler,
                 n_constraints: int,
                 on_transform_callback):
        self.constraint_handler = constraint_handler
        self.n_constraints = n_constraints
        self.callback = on_transform_callback

        self.ref_constraint = None

        self.transform_history = []
        self.transform = TrackedTransform()

    def reset(self):
        self.__init__(self.constraint_handler, self.n_constraints, self.callback)

    def process(self):
        constraints = self.constraint_handler.get_last_n_constraints(self.n_constraints)
        if len(constraints) > 0:
            if constraints[-1] != self.ref_constraint:
                # print(constraints)
                tfs = []

                for rot_c in constraints:       # type: Constraint
                    # rot_c = constraints[np.random.randint(0, len(constraints))]
                    non_c = [c for c in constraints if c.id != rot_c.id]

                    self._pin_point(rot_c)

                    if len(constraints) > 1:
                        self._calculate_rotations(rot_c, non_c)

                    self._fix_translation(rot_c)

                    if len(constraints) >= 4:
                        avg_err, max_err = self._calculate_error(non_c)

                        data = (avg_err, self.transform.d[0], self.transform.d[1], *list(self.transform.q), rot_c.id)
                        # print(data)
                        tfs.append(data)

                    # print(self.transform, "\n")
                if len(tfs) > 0:
                    tf_sorted = sorted(tfs, key=lambda e: e[0])
                    min_tf = tf_sorted[0]
                    # print(min_tf)
                    # print(self.transform.d, self.transform.q)
                    self.transform.d[0] = min_tf[1]
                    self.transform.d[1] = min_tf[2]

                    self.transform.q = Quaternion(*min_tf[3:7])

                self.callback(self.transform)
                self.ref_constraint = constraints[-1]

    def _pin_point(self, c: Constraint):
        p = c.point_ref  # type: Point
        dx = c.point[0] - p.point[0]
        dy = c.point[1] - p.point[1]

        self.transform.d[0] = dx
        self.transform.d[1] = dy

    def _calculate_rotations(self, pin_c: Constraint, c_list: List[Constraint]):
        angles = []
        weighted_sum = 0
        weights = 0
        for i in range(len(c_list)):
            c = c_list[i]
            angle = self._get_angle(
                pin_c.point[:2],
                self.transform.translate_point(c.point_ref.point[0], c.point_ref.point[1], 0)[:2],
                c.point[:2])
            # print(math.degrees(angle))
            angles.append(angle)

            weighted_sum += float(angle * (i + 1))
            weights += float(i+1)
        # rad = np.average(angles)
        rad = weighted_sum / weights
        self.transform.q = Quaternion.from_angle_axis(rad, 0.0, 0.0, 1.0)

    def _fix_translation(self, pin: Constraint):
        p = pin.point_ref
        v = self.transform.rotate_point(*p.point)
        dx = pin.point[0] - v[0]
        dy = pin.point[1] - v[1]
        self.transform.d[0] = dx
        self.transform.d[1] = dy

    def _calculate_error(self, constraints):
        errs = []
        for c in constraints:       # type: Constraint
            p0 = c.point[:2]
            p1 = self.transform.transform_point(c.point_ref.point[0], c.point_ref.point[1], 0)[:2]

            dx = p1[0] - p0[0]
            dy = p1[1] - p0[1]

            err = math.sqrt(dx * dx + dy * dy)
            errs.append(err)

        # self.transform_history.append(TFEntry(self.transform.copy(), errs))
        return sum(errs) / len(errs), max(errs)

    @staticmethod
    def _get_angle(p0: list, p1: list, p2: list):
        v0 = np.array(p1) - np.array(p0)
        v1 = np.array(p2) - np.array(p0)
        angle = np.math.atan2(np.linalg.det([v0, v1]), np.dot(v0, v1))

        return angle
