from imu.quaternion import Quaternion


class Transform:
    def __init__(self):
        self.d = [0, 0, 0]
        self.q = Quaternion(1, 0, 0, 0)

    def translate_point(self, x, y, z):
        return [x + self.d[0], y + self.d[1], z + self.d[2]]

    def transform_point(self, x, y, z):
        v = self.q * Quaternion(0, x, y, z) * self.q.conj()
        # print(self.d, v)
        return [v[1] + self.d[0], v[2] + self.d[1], v[3] + self.d[2]]

    def rotate_point(self, x, y, z):
        v = self.q * Quaternion(0, x, y, z) * self.q.conj()
        return v[1:]

    def transform_quaternion(self, q):
        return self.q * q * self.q.conj()
