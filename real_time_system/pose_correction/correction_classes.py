from pose_correction.transform import Transform


class Constraint:
    __slots__ = ['id', 'ts', 'point', 'radius', 'point_ref']

    def __init__(self, _id: str, ts: float, point: list, radius: float):
        self.id = _id
        self.ts = ts
        self.point = point
        self.radius = radius
        self.point_ref = None

    def __repr__(self):
        return "<Constraint %s @ %f>" % (self.id, self.ts)


class Point:
    __slots__ = ["id", "seq", "ts", "point", "q"]

    def __init__(self, uid: str, seq: int, ts: float, point: list, quaternion: list):
        self.id = uid
        self.seq = seq
        self.ts = ts
        self.point = point
        self.q = quaternion

    def __repr__(self):
        return "<Point %s: %d @ %f>" % (self.id, self.seq, self.ts)


class TrackedTransform(Transform):
    def __init__(self):
        Transform.__init__(self)
        self.constraints = []

    def __repr__(self):
        return "<Transform [%.2f, %.2f] | [%.2f, %.2f, %.2f, %.2f]>" % (self.d[0], self.d[1], self.q[0], self.q[1], self.q[2], self.q[3])

    def is_full(self, n):
        return len(self.constraints) >= n

    def copy(self):
        t = TrackedTransform()
        t.__dict__ = self.__dict__.copy()
        return t


class TFEntry:
    __slots__ = ["tf", "errs", "avg_err"]

    def __init__(self, tf: TrackedTransform, errs: list):
        self.tf = tf
        self.errs = errs
        self.avg_err = sum(errs) / len(errs)

    def __repr__(self):
        return str(self.avg_err) + " " + str(self.tf)