if __name__ == '__main__':
    from correction_classes import Point, Constraint
    import time
    import random
else:
    from pose_correction.correction_classes import Point, Constraint


class PoseConstraintHandler:
    def __init__(self):
        self.points = []

        self.internal_sequence = 0

        self.constraint_buffer = []
        self.constraints = []

    def reset(self):
        self.__init__()

    def add_entry(self, uid, ts, point, q):
        self.internal_sequence += 1
        p = Point(uid, self.internal_sequence, ts, point, q)
        self.points.append(p)

    def add_constraint(self, _id, ts, point, rad):
        c = Constraint(_id, ts, point, rad)
        self.constraint_buffer.append(c)

    def get_last_n_constraints(self, n):
        if n == -1:
            return self.constraints
        else:
            if len(self.constraints) < n:
                return self.constraints
            else:
                return self.constraints[-n:]

    def get_path(self):
        return self.constraints

    def recycle_paths(self):
        """"""

    def process(self):
        # check constraints for valid points to point to
        self._check_buffers()

    def _check_point(self, p1: Point, p0: Point):
        c_maj = []
        for c in self.constraint_buffer:            # type:  Constraint
            if p0.ts < c.ts < p1.ts:
                d0 = abs(p0.ts - c.ts)
                d1 = abs(p1.ts - c.ts)

                if d0 < d1:
                    c.point_ref = p0
                elif d1 < d0:
                    c.point_ref = p1
                c_maj.append(c)

        for c in c_maj:
            self.constraints.append(c)
            self.constraint_buffer.remove(c)

    def _check_buffers(self):
        # print(self.constraint_buffer)
        # print(self.constraints)
        for i in range(len(self.points) - 1, self._get_latest_constraint_index(), -1):
            p1 = self.points[i]
            p0 = self.points[i-1]

            # print(i, p1)

            if len(self.constraint_buffer) > 0 and self._is_buffer_within_points_range():
                self._check_point(p1, p0)
            else:
                break
        # print("pause")

    def _is_buffer_within_points_range(self):
        if len(self.points) > 0:
            p = self.points[-1]
            for c in self.constraint_buffer:
                if c.ts < p.ts:
                    return True
        return False

    def _get_latest_constraint_index(self):
        if len(self.constraints) > 0:
            c = self.constraints[-1]
            return c.point_ref.seq - self.points[0].seq
        else:
            return -1


if __name__ == '__main__':
    handlr = PoseConstraintHandler()
    for i in range(30):
        if i < 20:
            handlr.add_entry("test" + str(i), time.time(), [0, 0, 0], [1, 0, 0, 0])
            time.sleep(0.2 + float(random.randint(0, 100)) / 1000.0)
            j = random.randint(2, 5)
            if i % j == j-1:
                handlr.add_constraint(time.time(), [0, 0, 0], 1)
                time.sleep(0.1 + float(random.randint(0, 100)) / 1000.0)
        handlr.process()
        # print("")



