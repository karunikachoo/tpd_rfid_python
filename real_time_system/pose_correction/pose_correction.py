import base64
import threading
import time

from base_node.networking_src.network_utils import NetworkUtils
from map.map_constants import MapConst
from map.universal_map_handler import UniversalMapHandler
from messages.nav_msgs import MapMessage
import numpy as np

from pose_correction.correction_classes import Point, Constraint


class Clamp:
    def __init__(self, ts, point, rad):
        self.ts = ts
        self.point = point
        self.rad = rad
        self.point_id = None


class PoseCorrection:
    """
    Only fits the latest 3 or 5 clamps
    Points associated to prior clamps will be reduced using LOS checks
    """
    def __init__(self):
        self.map_handler = UniversalMapHandler()

        self.path = []
        self.ts_map = {}

        self.constraints = []

    def add_entry(self, _id, seq, ts, point, q):
        p = Point(_id, seq, ts, point, q)
        self.path.append(p)

        key = self.key_round(ts, 5)
        print(key)
        # self.ts_map[key] =

    def add_constraint(self, ts, point, rad):
        c = Constraint(ts, point, rad)
        self.constraints.append(c)

    """
    Make and PointConstraintHandler ->
    Constraint Buffer -> Add to list when point found,
    keep n constraints dump rest. handle index shifting
    sequential ids
    
    ts_map[ts] = index. if n points are removed, this one needs to be reworked.
    alternatively in the constraint, set an index. when points are removed just minus n from it.
    """

    def process(self):
        """"""
        # check clamp buffer for ts match

        # process correction
        #   - 1 pt -> translation
        #   - >1 pt -> translation + rotation

        # check for in bounds
        #   - nudge translation if not
        #   - if not in bounds, try nudge rotation

        # publish transform

    @staticmethod
    def key_round(t, base):
        return int(round(float(t) / base) * base)
