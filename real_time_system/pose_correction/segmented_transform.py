

"""
input:
- N constraints and associated points

output:
- ts for range of uid

"""
import math
import numpy as np
from typing import List

from map.universal_map_handler import UniversalMapHandler
from pose_correction.correction_classes import Point, Constraint, TrackedTransform
from pose_correction.pose_constraint_handler import PoseConstraintHandler
from pose_correction.transform import Transform


class SegmentedTransform:
    def __init__(self, constraint_handler: PoseConstraintHandler, map_handler: UniversalMapHandler, n_constraints: int):
        self.constraint_handler = constraint_handler
        self.map_handler = map_handler
        self.n_constraints = n_constraints

        self.ref_constraint = None

        self.transforms = []        # type: List[TrackedTransform]

    def process(self):
        constraints = self.constraint_handler.get_last_n_constraints(self.n_constraints)
        if len(constraints) > 0:
            if constraints[-1] != self.ref_constraint:
                if self._latest_tf_is_full():
                    t = TrackedTransform()
                    self.transforms.append(t)

                self.pin_point(constraints[0])
                if len(constraints) > 1:
                    self._calculate_rotations(constraints[0], [c for c in constraints if c != constraints[0]])

                """DO PROCESSING"""
                self.ref_constraint = constraints[-1]

    def _latest_tf_is_full(self):
        if len(self.transforms) > 0:
            return self.transforms[-1].is_full(self.n_constraints)
        return True

    def pin_point(self, c: Constraint):
        p = c.point_ref                 # type: Point
        dx = c.point[0] - p.point[0]
        dy = c.point[1] - p.point[1]

        self.transforms[-1].d[0] = dx
        self.transforms[-1].d[1] = dy
        self.transforms[-1].constraints.append(c)

        print(self.transforms[-1])

        return self.transforms[-1]

    def _calculate_rotations(self, pin_c: Constraint, c_list: List[Constraint]):
        angles = []
        t = self.transforms[-1]
        for c in c_list:
            angle = self._get_angle(
                t.translate_point(pin_c.point[0], pin_c.point[1], 0)[:2],
                t.translate_point(c.point_ref.point[0], c.point_ref.point[1], 0)[:2],
                t.translate_point(c.point[0], c.point[1], 0)[:2])
            print(math.degrees(angle))
            angles.append(angle)

    @staticmethod
    def _get_angle(p0: list, p1: list, p2: list):
        v0 = np.array(p1) - np.array(p0)
        v1 = np.array(p2) - np.array(p0)
        angle = np.math.atan2(np.linalg.det([v0, v1]), np.dot(v0, v1))
        return angle