import os
import statistics
import traceback
import numpy as np
from pprint import pprint
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


class DataProcess:
    def __init__(self, folder):
        self.folder = folder

        self.data = {}

        self.read_files()

        self.plot_data()

    def read_files(self):
        dirs = os.listdir(self.folder)
        for log in dirs:
            path = "%s/%s" % (self.folder, log)
            print(path)

            f = open(path, 'rb')
            lines = f.readlines()
            f.close()

            dists = str(log).split("_")[-1].split("cm")[0].split("x")
            dist = float(dists[0]) * float(dists[1])

            rssis = self.process_lines(lines[1:])

            for uuid in rssis:
                if uuid not in self.data:
                    self.data[uuid] = {}

                if dist not in self.data[uuid]:
                    self.data[uuid][dist] = rssis[uuid]
                else:
                    self.data[uuid][dist].append(rssis[uuid])

    def process_lines(self, lines):
        data_buffer = []
        data = {}

        beacon_found = False
        for line in lines:
            line = line.decode()
            if 'iBeacon Found' in line:
                beacon_found = True

            elif 'iBeacon End' in line and beacon_found:
                beacon_found = False
                uuid, ref, rssi = self.process_data(data_buffer)
                data_buffer = []

                if uuid not in data:
                    data[uuid] = [rssi]

                else:
                    data[uuid].append(rssi)

            elif beacon_found:
                data_buffer.append(line)

        return data

    def process_data(self, data_lines):
        uuid = ""
        ref_rssi = 0
        rssi = 0
        for line in data_lines:
            # try:
            if 'UUID::' in line:
                uuid = line.split('UUID::')[-1].replace("\r", "").replace("\n", "").strip()
            elif "Measured power (RSSI at a 1m distance):" in line:
                ref_rssi = float(line.split("Measured power (RSSI at a 1m distance):")[-1].split()[0].strip())
            elif "RSSI of packet:" in line:
                rssi = float(line.split("RSSI of packet:")[-1].split()[0].strip())
            # except Exception as e:
            #     print(traceback.format_exc())
            #     print(line)
        return uuid, ref_rssi, rssi

    def plot_data(self):
        for uuid in self.data:
            X = []
            Y = []

            x2 = []
            y2 = []

            dists = list(self.data[uuid].keys())
            dists = sorted(dists)

            print("\n\n")
            for dist in dists:
                rssi = np.array(self.data[uuid][dist])
                mean = np.mean(rssi)
                stdev = np.std(rssi)

                good_data = [r for r in rssi if abs(r - mean) / stdev < 3]

                for r in good_data:
                    X.append(dist / 100.0)
                    Y.append(r)

                    print("%f, %f" % (dist/100.0, r))

                x2.append(dist / 100.0)
                y2.append(np.mean(good_data))

            print("\n\n")

            x2.insert(0, 1.0)
            y2.insert(0, -59.0)

            # build polyfit array
            z = np.polyfit(x2, y2, 3)
            p = np.poly1d(z)
            z2 = np.polyfit(x2, y2, 1)
            p2 = np.poly1d(z2)

            x4 = np.linspace(0, 15.5, 100)
            y4 = p(x4)
            y5 = p2(x4)

            def func(x, a, b):
                # return a * np.log(x) + b
                return a * 1 / (x*x) + b
            popt, pcov = curve_fit(func, X, Y)

            print(z, z2, popt)

            fig = plt.figure()
            ax = fig.gca()
            ax.grid(alpha=0.3)

            ax.scatter(X, Y, alpha=0.2, label='Data points')

            ax.plot(x2, y2, 'rx', label='Mean')
            ax.plot(x4, y4, label='Polyfit')
            ax.plot(x4, y5, label='Linear fit')
            ax.plot(x4, func(x4, *popt), label="fit %f * ln(x) + %f" % tuple(popt))
            # ax.plot(x4, y5, label='Linear Fit')

            # ax.set_title('iBeacon RSSI (dBm) over Distance (m)')
            ax.set_ylabel('iBeacon RSSI (dBm)')
            ax.set_xlabel('Distance (m)')
            ax.set_ylim(-95, -50)

            ax.legend()

        plt.show()






if __name__ == '__main__':
    dp = DataProcess('logs')
