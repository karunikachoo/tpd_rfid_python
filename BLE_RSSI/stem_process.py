import os


class Main:
    def __init__(self, folder):
        self.beacons = {}

        self.load_data(folder)
        self.export_data()

    def export_data(self):
        for beacon in self.beacons:
            rows = ["pc, rssi\n"]
            keys = list(self.beacons[beacon].keys())
            keys = sorted(keys)
            for pos in keys:
                for rssi in self.beacons[beacon][pos]:
                    line = "%d, %d\n" % (pos, rssi)
                    rows.append(line)
            f = open("stem_beacon_%d.csv" % beacon, 'w')
            f.writelines(rows)
            f.close()

    def load_data(self, folder):
        dirs = os.listdir(folder)
        for item in dirs:
            if '.txt' in item:
                pos = int(item.split('.txt')[0])
                f = open("%s/%s" % (folder, item), 'r')
                lines = f.readlines()
                f.close()

                for line in lines:
                    cols = line.split(",")
                    ap = len(cols) - 1
                    try:
                        rssi = int(cols[ap-1])
                    except Exception as e:
                        print(e)

                    if ap not in self.beacons:
                        self.beacons[ap] = {}

                    if pos not in self.beacons[ap]:
                        self.beacons[ap][pos] = []

                    self.beacons[ap][pos].append(rssi)



if __name__ == "__main__":
    Main('logs/stem')
