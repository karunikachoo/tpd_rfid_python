import numpy as np
import matplotlib.pyplot as plt


class RollingWindow:
    def __init__(self, n_last):
        self.n_last = n_last

        self.data = []
        self.average = 0

    def push(self, point):
        self.data.append(point)

        if len(self.data) > self.n_last:
            self.data.pop(0)

        self.average = sum(self.data) / len(self.data)


f = open('test_log')
lines = f.readlines()
f.close()

dist = 0
data = {}
for line in lines:
    if '===' in line:
        dists = line.split("x")
        n = float(dists[0].split("===")[-1])
        d = float(dists[1].split("===")[0])
        dist = n * d

    elif '[' in line:
        dat = line.split()
        rssi = float(dat[0])
        if dist not in data:
            data[dist] = [rssi]
        else:
            data[dist].append(rssi)

for dist in data:
    pts = data[dist]
    r = RollingWindow(15)

    window = []
    for pt in pts:
        r.push(pt)
        window.append(r.average)

    data[dist] = window

pz = np.poly1d([-4.81261607e-02, 1.16514485e+00, -9.46919131e+00, -5.53507091e+01])
pz2 = np.poly1d([ -1.60475469, -67.25688376])


def inverse_fn(y, a, b):
    return np.exp((y-b)/a)


preds = {}
p2 = {}
p3 = {}
for dist in data:
    points = data[dist]
    print("\n\n%f\n" % dist)

    preds[dist] = []
    p2[dist] = []
    p3[dist] = []
    for pt in points:
        r = (pz - pt).roots
        x = inverse_fn(pt, -7.19693776, -66.4711244)
        r2 = (pz2 - pt).roots
        print(pt, r[-1].real, x, r2[-1].real)
        preds[dist].append(r[-1].real)
        p2[dist].append(x)
        p3[dist].append( r2[-1].real)

x = []
y1 = []
y2 = []
y3 = []
for dist in preds:
    x.append(dist/100.0)

    predictions = preds[dist]
    print(dist, np.average(predictions), np.average(p2[dist]), np.average(p3[dist]))

    y1.append(np.average(predictions))
    y2.append(np.average(p2[dist]))
    y3.append(np.average(p3[dist]))

print(x)


class PLOT:
    def __init__(self):
        fig = plt.figure()
        ax = fig.gca()
        ax.grid(alpha=0.3)

        ax.plot(x, x, 'o-', label='Reference')
        ax.plot(x, y1, 'x-', label='3rd order polyfit')
        ax.plot(x, y2, 'x-', label='a*ln(x) + b fit')
        ax.plot(x, y3, 'x-', label='linear fit')

        ax.set_xlim(0, 15)
        ax.set_ylim(0, 15)

        ax.set_xlabel('Measured distance (m)')
        ax.set_ylabel('Estimated distance (m)')

        ax.legend()

        plt.show()

if __name__ == '__main__':
    print("YO")
    p = PLOT()
    print('y no work')
