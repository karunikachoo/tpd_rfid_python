import tkinter as tk
from tkinter import filedialog
import json
import os


class AP(tk.Frame):
    def __init__(self, master, i, del_callback, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master

        self.del_callback = del_callback

        self.label = tk.Label(self, text="SoftAP_%02d" % (i + 1))
        self.label.grid(row=0, column=0, sticky='we')

        self.x = tk.Entry(self, text="")
        self.x.grid(row=0, column=1, sticky='we')

        self.y = tk.Entry(self, text="")
        self.y.grid(row=0, column=2, sticky='we')

        self.delbtn = tk.Button(self, text='Delete', command=self.delete)
        self.delbtn.grid(row=0, column=3, sticky='we')

        self.rssi = tk.Entry(self, text="")
        self.rssi.grid(row=0, column=4, sticky='we')

    def delete(self):
        self.del_callback(self)


class RSSISimulator(tk.Toplevel):
    def __init__(self, master, plotter, **kwargs):
        tk.Toplevel.__init__(self, master, **kwargs)
        # self.geometry('400x400')
        self.master = master
        self.plotter = plotter

        row = 0

        self.add_btn = tk.Button(self, text='add AP', command=self.add)
        self.add_btn.grid(row=row, column=0, sticky='we')

        self.load_btn = tk.Button(self, text='load', command=self.load)
        self.load_btn.grid(row=row, column=2, sticky='we')

        row += 1

        self.frame = tk.Frame(self)
        self.frame.grid(row=row, column=0, columnspan=3)

        self.aps = []

        self.after(100, self.simulate_data)

    def simulate_data(self):
        
        self.after(100, self.simulate_data)

    def delete_ap(self, ap):
        self.aps.remove(ap)
        ap.destroy()

    def add(self):
        ap = AP(self.frame, len(self.aps), self.delete_ap)
        ap.grid(row=len(self.aps), column=0, columnspan=5)
        self.aps.append(ap)

    def load(self):
        filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Load Config", filetypes=(("JSON file", "*.json"),))
        print(filename)
        f = open(filename, 'r')
        data = f.read()
        config = json.loads(data)

        for i in range(len(self.aps)):
            self.delete_ap(self.aps[i])

        for item in config:
            ap = AP(self.frame, item['i'], self.delete_ap)
            ap.grid(row=len(self.aps), column=0, columnspan=3)

            self.aps.append(ap)

        for item in config:
            ap = self.aps[item['i']]

            ap.x.delete_pt(0, tk.END)
            ap.x.insert(0, str(item['x']))

            ap.y.delete_pt(0, tk.END)
            ap.y.insert(0, str(item['y']))


