import math


class LinearModel:
    def __init__(self, max_rssi, grad):
        self.max = max_rssi
        self.grad = grad

    def predict(self, rssi):
        r = (rssi - self.max) / self.grad
        if r < 0:
            return 0
        else:
            return r

    def json(self):
        return {
            'type': 'linear',
            'kwargs': [self.max, self.grad]
        }


class LogModel:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def predict(self, rssi):
        r = math.exp((rssi - self.b) / self.a)

        if r < 0:
            return 0
        else:
            return r

    def reverse(self, r):
        rssi = self.a * math.log(r) + self.b

        if rssi > 0.01:
            return 0.01
        else:
            return r

    def json(self):
        return {
            'type': 'log',
            'kwargs': [self.a, self.b]
        }


class PathLossModel:
    def __init__(self, p_rx0, n):
        self.prx0 = p_rx0
        self.n = n

    def predict(self, rssi):
        r = 10 ** ((self.prx0 - rssi) / (10 * self.n))

        if r < 0:
            return 0
        else:
            return r

    def reverse(self, r):
        rssi = self.prx0 - 10 * self.n * math.log10(r)

        if rssi > 0.01:
            return 0.01
        else:
            return r

    def json(self):
        return {
            'type': 'pathloss',
            'kwargs': [self.prx0, self.n]
        }


class Model:

    TYPES = ['linear', 'log', 'pathloss']

    @staticmethod
    def create(label, kwargs):
        if label == 'linear':
            return LinearModel(*kwargs)
        elif label == 'log':
            return LogModel(*kwargs)
        elif label == 'pathloss':
            return PathLossModel(*kwargs)
