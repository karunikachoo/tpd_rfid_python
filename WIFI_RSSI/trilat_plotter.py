import math
import tkinter as tk

# from ap_node import TriLatNode


class Grids:
    def __init__(self, canvas: tk.Canvas, w, h, color="#ccc"):
        self.c = canvas
        self.w = w
        self.h = h
        self.color = color

        self.rows = []
        self.cols = []

        self.cs_x = self.c.create_line([0, 0, 0, self.h], fill="#333", width=2)
        self.cs_y = self.c.create_line([0, 0, self.w, 0], fill="#333", width=2)

    def update(self, px_p_m, offset: list):
        n_cols = int(math.ceil(self.w / px_p_m))
        n_rows = int(math.ceil(self.h / px_p_m))

        diff_rows = n_rows - len(self.rows)
        diff_cols = n_cols - len(self.cols)

        if diff_rows > 0:
            for i in range(diff_rows):
                line = self.c.create_line([0, 0, 0, 0], fill=self.color)
                self.rows.append(line)
        elif diff_rows < 0:
            for i in range(abs(diff_rows)):
                self.c.delete(self.rows[-1])
                self.rows.pop(-1)

        if diff_cols > 0:
            for i in range(diff_cols):
                line = self.c.create_line([0, 0, 0, 0], fill=self.color)
                self.cols.append(line)
        elif diff_cols < 0:
            for i in range(abs(diff_cols)):
                self.c.delete(self.cols[-1])
                self.cols.pop(-1)

        x_off = offset[0] % px_p_m
        y_off = offset[1] % px_p_m
        # x_off = 0
        # y_off = 0

        for i in range(len(self.rows)):
            y = i * px_p_m + y_off
            c = [0, y, self.w, y]
            # print('y:', c)
            self.c.coords(self.rows[i], c)

        for i in range(len(self.cols)):
            x = i * px_p_m + x_off
            c = [x, 0, x, self.h]
            # print('x:', c)
            self.c.coords(self.cols[i], c)

        self.c.coords(self.cs_x, [offset[0], 0, offset[0], self.h])
        self.c.coords(self.cs_y, [0, offset[1], self.w, offset[1]])


class TriLatPlotter(tk.Canvas):
    REFR_PERIOD = 40  # ms

    def __init__(self, master, width, height, **kwargs):
        tk.Canvas.__init__(self, master, width=width, height=height, bd=3, highlightbackground="#aaa", **kwargs)
        self.w = width
        self.h = height

        self.px_p_m = 100
        self.offset = [width/2, width/2]

        self.grids = Grids(self, self.w, self.h)
        self.grids.update(self.px_p_m, self.offset)

        self.aps = {}

        self.mouse_xy = [0, 0]
        self.prev_offset = [0, 0]
        self.bind('<Button-1>', self.click)
        self.bind('<ButtonRelease-1>', self.release)
        self.bind('<B1-Motion>', self.handle_click_drag)
        self.bind('<MouseWheel>', self.scroll)

        self.mouse_move_xy = [0, 0]
        self.mouse_click_xy = [0, 0]
        self.sta_xy = [0, 0]
        self.bind('<Motion>', self.handle_mouse_move)

        self.after(self.REFR_PERIOD, self.draw)

    def draw(self):
        self.update_aps()
        self.after(self.REFR_PERIOD, self.draw)

    def update_aps(self):
        for i in self.aps:
            self.aps[i].draw()

    def update_grids(self):
        self.grids.update(self.px_p_m, self.offset)

    def register_ap(self, node):
        if node.uuid not in self.aps:
            self.aps[node.uuid] = APObject(self, node)

    def deregister_ap(self, node):
        if node.uuid in self.aps:
            self.delete(self.aps[node.uuid].rect)
            self.delete(self.aps[node.uuid].circ)
            self.aps.pop(node.uuid)

    def scroll(self, evt):
        self.px_p_m += evt.delta
        if self.px_p_m < 1:
            self.px_p_m = 1

        self.master.sc.set(self.px_p_m)
        self.update_grids()

    def click(self, evt):
        self.mouse_xy[0] = evt.x
        self.mouse_xy[1] = evt.y
        self.prev_offset[0] = self.offset[0]
        self.prev_offset[1] = self.offset[1]

        self.mouse_click_xy[0] = evt.x
        self.mouse_click_xy[1] = evt.y

    def release(self, evt):
        self.mouse_xy[0] = 0
        self.mouse_xy[1] = 0

        if self.mouse_click_xy[0] == evt.x and self.mouse_click_xy[1] == evt.y:
            self.set_sta_point()

    def handle_click_drag(self, evt):
        self.offset[0] = self.prev_offset[0] + evt.x - self.mouse_xy[0]
        self.offset[1] = self.prev_offset[1] + evt.y - self.mouse_xy[1]
        # print("off", self.offset)
        self.update_grids()

    def handle_mouse_move(self, evt):
        self.mouse_move_xy[0] = evt.x - self.offset[0]
        self.mouse_move_xy[1] = evt.y - self.offset[1]

    def set_sta_point(self):
        self.sta_xy[0] = self.mouse_move_xy[0] / self.px_p_m
        self.sta_xy[1] = self.mouse_move_xy[1] / self.px_p_m
        print(self.sta_xy)


class APObject:
    W = 15
    H = 10

    def __init__(self, canvas: TriLatPlotter, node):
        self.canvas = canvas
        self.node = node

        self.c = [0, 0]
        self.r = 10

        xy = node.get_xy()
        self.c[0] = xy[0] * self.canvas.px_p_m + self.canvas.offset[0]
        self.c[1] = xy[1] * self.canvas.px_p_m + self.canvas.offset[1]

        self.rect = self.canvas.create_rectangle(
            self.c[0] - self.W/2,
            self.c[1] - self.H/2,
            self.c[0] + self.W/2,
            self.c[1] + self.H/2,
            fill='gray', outline=''
        )
        self.circ = self.canvas.create_oval(
            self.c[0] - self.r / 2,
            self.c[1] - self.r / 2,
            self.c[0] + self.r / 2,
            self.c[1] + self.r / 2,
            outline='#9CDA22',
            )

    def draw(self):
        self.update_xy()
        self.update_circle()

    def update_xy(self):
        xy = self.node.get_xy()
        self.c[0] = xy[0] * self.canvas.px_p_m + self.canvas.offset[0]
        self.c[1] = -xy[1] * self.canvas.px_p_m + self.canvas.offset[1]

        self.canvas.coords(self.rect,
                           self.c[0] - self.W / 2,
                           self.c[1] - self.H / 2,
                           self.c[0] + self.W / 2,
                           self.c[1] + self.H / 2
                           )

        self.canvas.coords(self.circ,
                           self.c[0] - self.r,
                           self.c[1] - self.r,
                           self.c[0] + self.r,
                           self.c[1] + self.r,
                           )

    def update_circle(self):
        if self.node.detected:
            self.r = self.node.model.predict(self.node.rssi) * self.canvas.px_p_m
        else:
            self.r = 0

        self.canvas.coords(self.circ,
                           self.c[0] - self.r,
                           self.c[1] - self.r,
                           self.c[0] + self.r,
                           self.c[1] + self.r,
                           )


class TriLatPlotterFrame(tk.Frame):
    """
    As the canvas plots the data +x left +y down. the conversion from standard +x left to +y up -> +x left +y down.
    """
    def __init__(self, master, width, height, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        self.sc = tk.Scale(self, from_=0, to=300, orient=tk.HORIZONTAL)
        self.sc.grid(row=0, column=0, columnspan=4, sticky='we')
        self.sc.bind('<ButtonRelease-1>', self.zoom)

        self.plotter = TriLatPlotter(self, width=width, height=height)
        self.plotter.grid(row=1, column=0, columnspan=5, sticky='we')

        self.sc.set(self.plotter.px_p_m)

    def zoom(self, evt):
        """"""
        self.plotter.px_p_m = int(self.sc.get())
        self.plotter.update_grids()
