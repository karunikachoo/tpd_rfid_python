

class RollingAverageFilter:
    def __init__(self, n):
        self.n = n

        self.list = []

    def push(self, x):
        self.list.append(x)

        d = len(self.list) - self.n
        if d > 0:
            for i in range(d):
                self.list.pop(0)

        return sum(self.list) / len(self.list)


class RollingMedianFilter:
    def __init__(self, size):
        """ odd size """
        self.size = size

        self.index = 0

        if self.size % 2 == 0:
            print("PLS INPUT AN ODD NUMBER FOR SIZE")
        else:
            self.index = int((self.size - 1) / 2)

        self.list = []

    def push(self, x):
        self.list.append(x)
        try:
            d = len(self.list) - self.size
            if d > 0:
                for i in range(d):
                    self.list.pop(0)
            elif d < 0:
                return x

            unique = list(set(self.list))
            median = unique[self.index]
            return median
        except:
            return x
