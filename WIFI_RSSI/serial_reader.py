import os, queue, sys, threading, time, traceback, serial
import tkinter as tk


FAST_SLEEP = 0.0001


class bcol:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class CMD:
    SEARCH = 100
    CONNECT = 110
    LOOP = 120

    def __init__(self, cmd, data):
        self.cmd = cmd
        self.data = data


class SerialReader(threading.Thread):
    def __init__(self, main, line_callback):
        super(SerialReader, self).__init__()
        self.main = main
        self.line_callback = line_callback

        self.alive = threading.Event()
        self.alive.set()

        self.port = None
        self.serial = None

        self.queue = queue.Queue()
        self.current_cmd = CMD(None, None)

        self.queue.put(CMD(CMD.SEARCH, None))

    def stop(self):
        self.alive.clear()

    def run(self):
        # self.wait_for_dev()
        try:
            while self.alive.is_set():
                cmd = self.current_cmd
                if not self.queue.empty():
                    cmd = self.queue.get(True, FAST_SLEEP)

                self.handle_cmd(cmd)
        except KeyboardInterrupt:
            os._exit(0)
        # os._exit(0)

    def handle_cmd(self, cmd: CMD):
        if cmd.cmd == CMD.SEARCH:
            if cmd.cmd != self.current_cmd.cmd:
                self.main.status_disconnected()
                print("Scanning devices ", end='', flush=True)

            print(".", end='', flush=True)
            ports = []
            if sys.platform.startswith('win'):
                ports = self.list_com()
            elif sys.platform.startswith("linux") or sys.platform.startswith('cygwin'):
                ports = self.list_dev("ttyUSB")
            elif sys.platform.startswith("darwin"):
                ports = self.list_dev("cu.")

            self.main.update_ports(ports)
            time.sleep(1)

        elif cmd.cmd == CMD.CONNECT:
            self.port = cmd.data
            if self.connect():
                self.main.status_connected()
                self.put(CMD(CMD.LOOP, None))
            else:
                self.main.status_disconnected()
                self.put(CMD(CMD.SEARCH, None))
            time.sleep(0.1)

        elif cmd.cmd == CMD.LOOP:
            self.read_comms()
            time.sleep(FAST_SLEEP)

        self.current_cmd = cmd

    def put(self, cmd: CMD):
        self.queue.put(cmd)

    def connect(self):
        try:
            self.serial = serial.Serial(self.port, 115200, timeout=0.1)
            print(bcol.OKGREEN + ("\n===== CONNECTED TO %s =====" % self.port) + bcol.ENDC)
            return True
        except serial.SerialException as e:
            print(traceback.format_exc())
            return False

    def read_comms(self):
        try:
            line = self.serial.readline()
        except serial.serialutil.SerialException as e:
            print(bcol.FAIL + "E: Device Disconnected!" + bcol.ENDC)
            self.serial.close()
            self.put(CMD(CMD.SEARCH, None))
            return

        try:
            line = line.decode()
        except:
            line = ""

        if line != "":
            self.line_callback(line)

    @staticmethod
    def list_com():
        ports = ['COM%d' % (i + 1) for i in range(256)]
        ret = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                ret.append(port)
            except (OSError, serial.SerialException):
                pass
        return ret

    @staticmethod
    def list_dev(pattern):
        ret = []
        devs = os.listdir("/dev")
        for dev in devs:
            if dev.startswith(pattern):
                ret.append("/dev/" + dev)
        return ret


class SerialObject(tk.Frame):
    def __init__(self, master, line_callback, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.line_callback = line_callback

        self.serial = SerialReader(self, self.line_callback)

        self.dev_var = tk.StringVar(self)
        self.dev_var.set("Select PORT")
        self.dev_dropdown = tk.OptionMenu(self, self.dev_var, "Select PORT")
        self.dev_dropdown.grid(row=0, column=2, sticky="E")

        self.connect_btn = tk.Button(self, text="Connect to PORT", command=self.connect_to_port)
        self.connect_btn.grid(row=0, column=3, sticky="W")

        self.dev_status = tk.Label(self, text="disconnected")
        self.dev_status.grid(row=0, column=4, sticky="W")

        # self.processor.start()
        self.serial.start()

    def status_connected(self):
        # cmd = DISP_CMD(Display.CMD_SERIAL_REGISTER, {
        #     "uid": self.uid,
        #     "cb": self.set_line_color
        # })
        # self.disp.put_cmd(cmd)
        self.connect_btn.config(state=tk.NORMAL)
        self.dev_status.config(text="CONNECTED", fg="Green")

    def status_disconnected(self):
        self.dev_status.config(text="disconnected", fg="Red")
        self.dev_var.set("Select PORT")
        self.connect_btn.config(state=tk.NORMAL)

    def connect_to_port(self):
        self.dev_status.config(text="connecting...", fg="Grey")
        self.serial.put(CMD(CMD.CONNECT, self.dev_var.get()))
        self.connect_btn.config(state=tk.DISABLED)

    def update_ports(self, dev_list):
        menu = self.dev_dropdown['menu']
        menu.delete(0, 'end')
        # menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.dev_var.set(name))
        else:
            self.dev_var.set("Select PORT")


def print_line(line):
    print(line)


if __name__ == '__main__':
    try:
        root = tk.Tk()
        so = SerialObject(root, print_line)
        so.pack()
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)