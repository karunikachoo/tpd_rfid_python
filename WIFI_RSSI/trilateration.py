import os
import time
import tkinter as tk
import traceback

from ap_node import TriLatNode, NodeManager
from serial_reader import SerialObject
from simulator import RSSISimulator
from trilat_plotter import TriLatPlotterFrame


class Trilateration(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.pack(fill=tk.BOTH, expand=1)

        row = 0

        self.sim = None
        self.enable_sim = False

        self.sim_btn = tk.Button(self, text='Enable Sim', command=self.toggle_sim)
        self.sim_btn.grid(row=row, column=0, sticky='we')

        row += 1

        self.so = SerialObject(self, self.line_callback)
        self.so.grid(row=row, column=0, columnspan=5, sticky='we')

        self.plotter = TriLatPlotterFrame(self, 500, 500)
        self.plotter.grid(row=row, column=6, columnspan=5, rowspan=10)

        row += 1

        self.node_man = NodeManager(master=self)
        self.node_man.grid(row=row, column=0, columnspan=5, sticky='we')

        row += 1

        self.count = 0
        self.count_label = tk.Label(self, text='0')
        self.count_label.grid(row=row, column=0)

        self.pause_btn = tk.Button(self, text='Pause', command=self.pause_toggle)
        self.reset_btn = tk.Button(self, text='Clear', command=self.clear_log)
        self.pause_btn.grid(row=row, column=3)
        self.reset_btn.grid(row=row, column=4)

        row += 1

        self.is_print = True
        self.log = tk.Text(self, width=5, height=10, bd=3, highlightbackground="#aaa")
        self.log.grid(row=row, column=0, columnspan=5, sticky='we')

    def toggle_sim(self):
        self.enable_sim = not self.enable_sim
        if self.enable_sim:
            self.sim_btn.config(text='Disable Sim')
            self.sim = RSSISimulator(self, self.plotter)
        else:
            self.sim_btn.config(text='Enable Sim')
            self.sim.destroy()
            self.sim = None

    def line_callback(self, line):
        data = line.split(",")
        if len(data) > 1 and self.is_print:
            line = "%s, %s" % (time.time(), line)
            self.log_line(line)
            for i in range(len(data)):
                try:
                    rssi = int(data[i].strip())
                    self.node_man.update_rssi(i, rssi)
                except Exception as e:
                    # self.node_man.update_rssi(i, 0)
                    pass

    def clear_log(self):
        self.count = 0
        self.count_label.config(text='%d' % self.count)
        self.log.delete(1.0, tk.END)

    def pause_toggle(self):
        self.is_print = not self.is_print
        if self.is_print:
            self.pause_btn.config(text="Pause")
        else:
            self.pause_btn.config(text="Resume")

    def log_line(self, line):
        self.count += 1
        self.count_label.config(text='%d' % self.count)
        self.log.insert(tk.END, line.rstrip() + '\n')
        self.log.see(tk.END)

    def get_plotter(self):
        return self.plotter


if __name__ == '__main__':
    try:
        root = tk.Tk()
        Trilateration(root)
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)