import json
import os
import tkinter as tk
import uuid
from tkinter import filedialog

from filter import RollingMedianFilter, RollingAverageFilter
from rssi_models import LinearModel, Model
from trilat_plotter import TriLatPlotter, TriLatPlotterFrame


class NodeManager(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master

        self.nodes = []

        self.add_btn = tk.Button(self, text='Add AP', command=self.add_node)
        self.add_btn.grid(row=0, column=0, sticky='we')

        self.save_btn = tk.Button(self, text='Save Config', command=self.save)
        self.load_btn = tk.Button(self, text='Load Config', command=self.load)

        self.save_btn.grid(row=0, column=1, sticky='we')
        self.load_btn.grid(row=0, column=2, sticky='we')

    def save(self):
        config = []
        for node in self.nodes:
            config.append({
                'i': node.i,
                'x': float(node.x.get()),
                'y': float(node.y.get()),
                'model': node.model.json()
            })
        # print(config)
        filename = filedialog.asksaveasfilename(initialdir=os.getcwd(), title="Save Config", filetypes=(("JSON file", "*.json"),))

        if '.json' not in filename:
            filename += '.json'
        f = open(filename, 'w')
        f.write(json.dumps(config))
        f.close()

    def load(self):
        filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Load Config", filetypes=(("JSON file", "*.json"),))
        print(filename)
        f = open(filename, 'r')
        data = f.read()
        f.close()
        config = json.loads(data)

        for i in range(len(self.nodes)):
            self.del_node(self.nodes[0])

        for item in config:
            node = TriLatNode(self, item['i'], self.del_node)
            node.grid(row=len(self.nodes) + 1, column=0, columnspan=3)
            node.x.delete(0, tk.END)
            node.x.insert(0, str(item['x']))

            node.y.delete(0, tk.END)
            node.y.insert(0, str(item['y']))

            node.model = Model.create(item['model']['type'], item['model']['kwargs'])
            self.master.get_plotter().plotter.register_ap(node)
            self.nodes.append(node)

    def del_node(self, node):
        self.nodes.remove(node)
        self.master.get_plotter().plotter.deregister_ap(node)
        node.destroy()

    def add_node(self):
        node = TriLatNode(self, len(self.nodes), self.del_node)
        node.grid(row=len(self.nodes) + 1, column=0, columnspan=3)
        node.model = Model.create('pathloss', [-40, 1.8])
        self.master.get_plotter().plotter.register_ap(node)
        self.nodes.append(node)

    def update_rssi(self, i, rssi):
        # print(i)
        diff = (i + 1) - len(self.nodes)
        if diff > 0:
            for i in range(diff):
                self.add_node()
        self.nodes[i].set_detected(True)
        self.nodes[i].update_rssi(rssi)

    def not_detected(self, i):
        self.nodes[i].set_detected(False)


class TriLatNode(tk.Frame):
    def __init__(self, master, i, on_del_callback=None, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.uuid = uuid.uuid4()
        self.i = i
        self.on_delete_callback = on_del_callback

        self.avg_filt = RollingAverageFilter(25)
        self.filter = RollingMedianFilter(5)
        self.model = None
        self.rssi = -62
        self.detected = False

        x_label = tk.Label(self, text="SoftAP_%02d -> X" % (i + 1))
        x_label.grid(row=0, column=0, sticky='we')

        self.x = tk.Entry(self, text="", width=5)
        self.x.grid(row=0, column=1, sticky='we')

        y_label = tk.Label(self, text="Y")
        y_label.grid(row=0, column=2, sticky='we')

        self.y = tk.Entry(self, text="", width=5)
        self.y.grid(row=0, column=3, sticky='we')

        self.delbtn = tk.Button(self, text='Delete', command=self.del_self)
        self.delbtn.grid(row=0, column=4, sticky='we')

        self.rssi_label = tk.Label(self, text='%d dBm' % self.rssi)
        self.rssi_label.grid(row=0, column=5, sticky='we')

    def set_detected(self, b):
        self.detected = b

    def update_rssi(self, rssi):
        rssi = self.avg_filt.push(rssi)
        rssi = self.filter.push(rssi)
        self.rssi_label.config(text='%d dBm' % rssi)
        self.rssi = rssi

    def get_xy(self):
        try:
            return [float(self.x.get()), float(self.y.get())]
        except Exception:
            return [0, 0]

    def del_self(self):
        if self.on_delete_callback is not None:
            self.on_delete_callback(self)