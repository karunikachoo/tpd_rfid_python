import math
import os
import random
import threading
import time

TAG = 'input_listener  :'


class InputListener(threading.Thread):
    def __init__(self, main):
        super(InputListener, self).__init__()
        """Custom initialiser"""

        self.user_input = ['']
        self.main = main
        self.sleep_duration = 0.2

    def run(self):
        self.get_user_input(self.user_input)
        while True:
            if self.user_input[0] is not None:
                cmd = self.user_input[0]
                self.cmd_handler(cmd)
                self.user_input = [None]
                self.get_user_input(self.user_input)
            time.sleep(self.sleep_duration)

    def get_user_input(self, user_input_ref):
        """Get user input"""
        user_input_ref[0] = input("Plz Input: ")

    def cmd_handler(self, cmd):
        """Handles commands"""
        try:
            if cmd.startswith('d:'):
                data = cmd.split(":")
                dist = float(data[1])
                self.main.measure_tag_rssi(dist)
            elif cmd.startswith("attn:"):
                data = cmd.split(":")
                attn = int(data[1])
                self.main.set_attn(attn)
            elif cmd == "undo":
                self.main.undo_round()
            elif cmd == "save":
                self.main.write_to_csv()
            elif cmd == "kill -f":
                os._exit(0)
        except KeyboardInterrupt:
            os._exit(0)

        except Exception as e:
            print("ERROR", str(e))