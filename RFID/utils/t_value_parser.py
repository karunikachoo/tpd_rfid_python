import os


class TDist:
    def __init__(self):
        path = os.path.dirname(os.path.realpath(__file__))
        f = open(path + "/t_dist_norm.csv")
        lines = f.readlines()
        f.close()

        self.data = {}

        for line in lines:
            data = line.split(",")
            self.data[data[0]] = []
            for dat in data[1:]:
                self.data[data[0]].append(dat)

    def get_val(self, alpha, dof):
        i = dof - 1
        if i > len(self.data) - 1:
            return float(self.data[str(alpha)][-1].replace("\n", ""))
        return float(self.data[str(alpha)][i].replace("\n", ""))


class Parser:
    def __init__(self):
        f = open("t_dist.csv")
        lines = f.readlines()
        f.close()

        data = {}

        for dat in lines[0].split(" ")[1:]:
            data[dat] = []

        keys = list(data.keys())
        for line in lines:
            ldat = line.split(" ")[1:]
            for i in range(len(ldat)):
                data[keys[i]].append(ldat[i].replace("\n", ""))

        line_str = []
        for key in data:
            line = ", ".join(data[key]) + "\n"
            line_str.append(line)
        f = open("t_dist_norm.csv", "w")
        f.writelines(line_str)
        f.close()


if __name__ == "__main__":
    Parser()
