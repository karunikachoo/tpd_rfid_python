import sys
import subprocess
import pkg_resources


if __name__ == '__main__':
    req = {'playsound'}
    installed = {pkg.key for pkg in pkg_resources.working_set}
    missing = req - installed

    if missing:
        print('Installing missing modules: ', *missing)
        python = sys.executable
        subprocess.check_call([python, '-m', 'pip', 'install', '--user', *missing])

    from ppe_check_vis import start_vis
    start_vis()
