
import socket
from const.const import TagProps as Props

TAG_LIST_TEMPLATE = "TagListCustomFormat = %s \nTagListFormat = Custom\n"


class NetworkComm:
    def __init__(self):
        self.s = None
        self.ip_addr = ""
        self.port = 0
        self.tags_list_format = ""

        self.set_tag_list_format([
            Props.TAG_ID_WITH_SPACE,
            Props.TX,
            Props.RX,
            Props.RSSI,
            Props.DIR,
        ])

    def set_tag_list_format(self, params: list):
        param_str = ", ".join(params)
        self.tags_list_format = TAG_LIST_TEMPLATE % param_str
        if self.s is not None:
            self.netcat(self.tags_list_format)

    def connect(self, ip_addr: str, port: int):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip_addr = ip_addr
        self.port = port
        self.s.connect((ip_addr, int(port)))

        self.netcat("alien\n")
        self.netcat("password_set\n")
        self.netcat("AntennaSequence = 0 1 2 3\n")
        self.netcat(self.tags_list_format)

    def netcat(self, content: str):
        data = bytes()
        try:
            self.s.sendall(content.encode())
        except BrokenPipeError as e:
            print(e, "reconnecting")
            self.connect(self.ip_addr, self.port)
        self.s.settimeout(0.05)
        i = 0
        while True:
            try:
                data += self.s.recv(1024)
            except:
                break
        return data

    def send_cmd(self, cmd: str):
        return self.netcat(cmd + "\n").decode()
