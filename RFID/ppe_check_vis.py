import tkinter as tk
from main_mt import RFID, Tag
from playsound import playsound

PPE = ['Helmet', 'Goggles', 'Vest', 'Gloves', 'Boots']
# EXAMPLE_EPCS = [
#     [
#         "E200 3098 1517 0018 1340 91DC",
#         "E2X0 3098 1517 0044 1340 9199",
#         "E20X 3098 1517 0122 1340 910C",
#     ], [
#         "E20X 3098 1517 0087 1350 8E11",
#         "E200 3098 1517 0074 1340 916C",
#         "E200 3098 1517 0027 1350 8D93",
#     ], [
#         "E20X 3098 1517 0039 1350 8DB1",
#         "E200 3098 1517 0044 1350 8DBB",
#         "E20X 3098 1517 0116 1340 9109",
#     ], [
#         "E200 3098 1517 0024 1340 91CB",
#         "E200 3098 1517 0099 1340 9134",
#         "E200 3098 1517 0087 1340 9142",
#     ], [
#         "E200 3098 1517 0093 1340 9131",
#         "E20X 3098 1517 0032 1350 8DA9",
#         "E200 3098 1517 0074 1350 8DFA",
#     ],
# ]
EXAMPLE_EPCS = [
    [

    ], [
        "E200 3098 1517 0207 1340 9052",
        "E200 3098 1517 0213 1340 9041",
    ], [
        "E200 3098 1517 0037 1340 91A1",
        "E200 3098 1517 0061 1340 9171",
        "E200 3098 1517 0043 1340 91A4",
        "E200 3098 1517 0067 1340 9174",
    ], [

    ], [
        "E200 3098 1517 0201 1340 9063",
        "E200 3098 1517 0242 1340 901C",
        "E200 3098 1517 0236 1340 9019",
        "E200 3098 1517 0150 1360 8C45",
        "E200 3098 1517 0230 1340 902A",
        "E200 3098 1517 0138 1540 7A63",
        "E200 3098 1517 0131 1540 7A7C",
        "E200 3098 1517 0224 1340 903B",

    ],
]


class EPCClass(tk.Frame):
    def __init__(self, parent, name, **kw):
        tk.Frame.__init__(self, parent, **kw)
        self.parent = parent
        self.name = name

        self.label = tk.Label(self, text=name)
        self.label.pack()

        self.epcs = tk.Text(self, width=5, height=15)
        self.epcs.pack(fill=tk.X, expand=True)

        self.raw = ""
        self.epc_list = []

        self.epcs.tag_configure('MATCH', foreground='green')

    def set_text(self, txt):
        self.epcs.delete('1.0', tk.END)
        self.epcs.insert('1.0', txt)

    def verify_epc(self, epc_list):
        txt = self.epcs.get('1.0', tk.END)
        if txt != self.raw:
            self.raw = txt.replace("\n\n", '\n')
            self.epc_list = [x for x in self.raw.split("\n") if len(x) > 0]

        valid = []
        if len(self.epc_list) > 0:
            res = [False] * len(self.epc_list)

            for i in range(len(self.epc_list)):
                res[i] = self.epc_list[i] in epc_list
                if res[i]:
                    if self.epc_list[i] not in valid:
                        valid.append(self.epc_list[i])

            for i in range(len(res)):
                if res[i]:
                    # self.epcs.tag_add('MATCH', ind, ind + float(len(self.epc_list[i])))
                    self.epcs.tag_add('MATCH', float(i + 1), float(i + 2))

                else:
                    self.epcs.tag_remove('MATCH', float(i + 1), float(i + 2))

            if any(res):
                self.label.config(fg='green')
                return True, valid
            else:
                self.label.config(fg='red')
                return False, valid
        else:
            self.label.config(fg='black')
            return True, valid


class Main(tk.Frame):
    def __init__(self, parent, **kw):
        tk.Frame.__init__(self, parent, **kw)
        self.parent = parent
        self.pack(expand=True, fill=tk.BOTH)

        self.rfid = RFID()
        self.scan = {}
        self.ts = 0
        self.success_count = 0

        row = 0

        WIDTH = 1250
        self.canvas = tk.Canvas(self, width=WIDTH, height=200)
        self.canvas.grid(row=row, column=0, columnspan=5, sticky='we')

        self.label = self.canvas.create_text([WIDTH/2, 100], text="%s" % "Fail", font=("Arial", 100), fill="#888888")
        self.ts_label = self.canvas.create_text([WIDTH-10, 10], text=str(self.ts), font=('Arial', 50), anchor='ne', fill='#888888')

        row += 1

        self.cls = []

        for i in range(len(PPE)):
            ppe_class = EPCClass(self, PPE[i])
            ppe_class.grid(row=row, column=len(self.cls), sticky='we')
            ppe_class.set_text('\n'.join(EXAMPLE_EPCS[i]))
            self.cls.append(ppe_class)

        row += 1

        self.log = tk.Text(self, width=5, height=10)
        self.log.grid(row=row, column=0, columnspan=5, sticky='we')

        self.after(500, self.continuous_scan)

    def timestamp(self):
        self.ts += 1
        self.canvas.itemconfig(self.ts_label, text='%d' % self.ts)

    def continuous_scan(self):
        try:
            self.rfid.netcat_wait_avail()
            self.timestamp()
            self.rfid.netcat('T\n', self.scan_cb)
            self.after(500, self.continuous_scan)
        except Exception as e:
            print(e)

    def scan_cb(self, data: bytes):
        data = data.decode()
        tags = data.split("</TAG>")
        # print(data)
        self.parse_tags(tags)

    def parse_tags(self, tags: list):
        """<TAG>1, 1, 925.2, <EPC>E200 1021 3303 0201 1330 8F46</EPC>"""
        self.scan = {}
        for tag_str in tags:
            if "<TAG>" in tag_str:
                tag = Tag()
                tag.parse(tag_str)
                if tag.epc not in self.scan:
                    self.scan[tag.epc] = [tag]
                else:
                    add = True
                    for t in self.scan[tag.epc]:
                        if t.tx == tag.tx and t.rx == tag.rx:
                            t.update_rssi(tag)
                            add = False
                    if add:
                        self.scan[tag.epc].append(tag)

        res = []
        log = []
        for epc in self.cls:
            present, valid = epc.verify_epc(list(self.scan))
            res.append(present)

            for tag_epc in valid:
                tags = self.scan[tag_epc]
                for tag in tags:
                    row = '%d, %d, %d, %d, %f, %s' % (tag.ms, self.ts, tag.tx, tag.rx, tag.rssi, tag.epc)
                    log.append(row)

        self.log.insert(tk.END, '\n'.join(log) + '\n')
        self.log.see(tk.END)

        if all(res):
            self.success_count += 1
            if self.success_count >= 3:
                self.canvas.itemconfig(self.label, text='Passed', fill='green')
                # playsound('sound.wav')
                playsound('beep.mp3')
                self.success_count = 0
        else:
            self.canvas.itemconfig(self.label, text='Fail', fill='red')
            self.success_count = 0


def start_vis():
    root = tk.Tk()
    m = Main(root)
    root.mainloop()


if __name__ == '__main__':
    start_vis()
