import threading
import os
import queue
import socket
import time


SERVER_IP = "127.0.0.1"
PORT = 65432


USER = {"alien": "password"}
TAG_DATA = """
<TAG>0, 0, 2867.3, <EPC>E200 3098 1517 0087 1350 8E11</EPC></TAG>
<TAG>0, 0, 2271.6, <EPC>E200 3098 1517 0074 1350 8DFA</EPC></TAG>
<TAG>0, 0, 1096.5, <EPC>E200 3098 1517 0043 1340 91A4</EPC></TAG>
<TAG>0, 0, 2180.8, <EPC>E200 3098 1517 0086 1350 8E0C</EPC></TAG>
<TAG>0, 0, 2376.7, <EPC>E200 3098 1517 0021 1350 8D84</EPC></TAG>
<TAG>0, 0, 1870.2, <EPC>E200 3098 1517 0018 1340 91DC</EPC></TAG>
<TAG>0, 0, 2587.3, <EPC>E200 3098 1517 0068 1350 8DEB</EPC></TAG>
<TAG>0, 0, 2802.7, <EPC>E200 3098 1517 0027 1350 8D93</EPC></TAG>
<TAG>0, 0, 1540.3, <EPC>E200 3098 1517 0044 1340 9199</EPC></TAG>
<TAG>0, 0, 1591.0, <EPC>E200 3098 1517 0032 1350 8DA9</EPC></TAG>
<TAG>0, 0, 2421.6, <EPC>E200 3098 1517 0039 1350 8DB1</EPC></TAG>
<TAG>0, 0, 2030.3, <EPC>E200 3098 1517 0062 1350 8DDC</EPC></TAG>
<TAG>0, 0, 1207.0, <EPC>E200 3098 1517 0074 1340 916C</EPC></TAG>
<TAG>0, 0, 1039.5, <EPC>E200 3098 1517 0044 1350 8DBB</EPC></TAG>
<TAG>0, 0, 2614.2, <EPC>E200 3098 1517 0080 1350 8E09</EPC></TAG>
<TAG>0, 0, 1827.6, <EPC>E200 3098 1517 0012 1340 91D9</EPC></TAG>
<TAG>0, 0, 715.4, <EPC>E200 3098 1517 0159 1340 90B2</EPC></TAG>
<TAG>0, 0, 1542.4, <EPC>E200 3098 1517 0122 1340 910C</EPC></TAG>
<TAG>0, 0, 1984.7, <EPC>E200 3098 1517 0045 1350 8DB4</EPC></TAG>
<TAG>0, 0, 1594.5, <EPC>E200 3098 1517 0032 1340 91BB</EPC></TAG>
<TAG>0, 0, 1429.1, <EPC>E200 3098 1517 0024 1340 91CB</EPC></TAG>
<TAG>0, 0, 1668.8, <EPC>E200 3098 1517 0015 1350 8D81</EPC></TAG>
<TAG>0, 0, 1258.8, <EPC>E200 3098 1517 0087 1340 9142</EPC></TAG>
<TAG>0, 0, 1497.1, <EPC>E200 3098 1517 0116 1340 9109</EPC></TAG>
<TAG>0, 0, 1328.3, <EPC>E200 4006 0904 0086 1270 96F4</EPC></TAG>
<TAG>0, 0, 2235.0, <EPC>E200 3098 1517 0020 1350 8D8B</EPC></TAG>
<TAG>0, 0, 2003.7, <EPC>E200 3098 1517 0051 1350 8DC3</EPC></TAG>
<TAG>0, 0, 2892.8, <EPC>E200 3098 1517 0033 1350 8DA2</EPC></TAG>
<TAG>0, 0, 1232.3, <EPC>E200 3098 1517 0037 1340 91A1</EPC></TAG>
<TAG>0, 0, 1470.7, <EPC>E200 3098 1517 0110 1340 911A</EPC></TAG>
<TAG>0, 0, 1540.0, <EPC>E200 3098 1517 0081 1340 9153</EPC></TAG>
<TAG>0, 0, 874.6, <EPC>E200 3098 1517 0213 1340 9041</EPC></TAG>
<TAG>0, 0, 1351.5, <EPC>E200 3098 1517 0057 1350 8DD2</EPC></TAG>
<TAG>0, 0, 1401.2, <EPC>E200 4006 0904 0088 1270 9701</EPC></TAG>
<TAG>0, 0, 1006.4, <EPC>E200 3098 1517 0038 1350 8DAC</EPC></TAG>
<TAG>0, 0, 1455.3, <EPC>E200 3098 1517 0025 1340 91C3</EPC></TAG>
<TAG>0, 0, 1000.2, <EPC>E200 3098 1517 0099 1340 9134</EPC></TAG>
<TAG>0, 0, 893.5, <EPC>E200 3098 1517 0050 1350 8DCA</EPC></TAG>
<TAG>0, 0, 1103.3, <EPC>E200 3098 1517 0230 1340 902A</EPC></TAG>
<TAG>0, 0, 1593.4, <EPC>E200 3098 1517 0026 1350 8D9A</EPC></TAG>
<TAG>0, 0, 1591.3, <EPC>E200 3098 1517 0038 1340 91AA</EPC></TAG>
<TAG>0, 0, 713.5, <EPC>E200 3098 1517 0063 1350 8DE1</EPC></TAG>
<TAG>0, 0, 1544.3, <EPC>E200 3098 1517 0093 1340 9131</EPC></TAG>
"""


class ClientThread(threading.Thread):
    def __init__(self, conn, addr):
        super(ClientThread, self).__init__()
        self.conn = conn
        self.addr = addr
        self.alive = threading.Event()
        self.alive.set()

        self.username_set = False
        self.password_set = False

        self.username = ""

    def run(self):
        try:
            print("Connected to ", self.addr)
            while self.alive:
                cmd = self.conn.recv(1024)
                ret = self.handle_cmd(cmd.decode())
                print(ret)
                self.conn.sendall(ret.encode())

        except KeyboardInterrupt:
            self.alive.clear()

    def handle_cmd(self, cmd):
        ret = ""
        if cmd.startswith("T"):
            ret = TAG_DATA + "Alien>"
        else:
            ret = cmd + "Alien>"

        return ret


class SocketServerThread(threading.Thread):
    def __init__(self):
        super(SocketServerThread, self).__init__()
        self.alive = threading.Event()
        self.alive.set()

        self.s = None

    def setup(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((SERVER_IP, PORT))
        self.s.listen()

    def run(self):
        try:
            self.setup()
            while self.alive.is_set():
                conn, addr = self.s.accept()
                cli = ClientThread(conn, addr)
                cli.start()

                time.sleep(0.001)
        except KeyboardInterrupt:
            self.alive.clear()


if __name__ == "__main__":
    s = SocketServerThread()
    s.start()