import time

from networking.network_communicator import NetworkComm
import queue
from const.const import TagProps as Props
from utils.input_listener import InputListener
import os


class Tag:
    def __init__(self):
        self.id = 0
        self.rx = 0
        self.rssi = 0
        self.raw = ""

    def parse_line(self, line: str):
        data = line.split(", ")
        self.id = data[0]
        self.rx = int(data[1])
        self.rssi = float(data[2])
        self.raw = line


class Main:
    def __init__(self):
        """"""
        print(os.getcwd())
        self.comms = NetworkComm()
        self.comms.set_tag_list_format([
            Props.TAG_ID_WITH_SPACE,
            Props.RX,
            Props.TX,
            Props.RSSI
        ])
        self.queue = queue.Queue()
        # self.input_listener = InputListener(self)
        # self.input_listener.start()

        self.attenuation = 0
        self.filter_ids = [
            "E200 3098 1517 0112 1350 8E49",
            "E200 3098 1517 0138 1360 8C67"
        ]

        self.data_out = ["dist, attn, id, rx, rssi\n"]
        self.temp_data = []

        self.user_input = ['']

    def set_attn(self, attn: int):
        if attn < 0 or attn > 150:
            print("Wrong AttenValue")
            return

        print(self.comms.send_cmd("RFAttenuation = %d" % attn))
        self.attenuation = attn

    def measure_tag_rssi(self, dist: float):
        self.extend_data()

        data = []
        for i in range(30):
            tags = self.detect_tags()
            for tag in tags:
                data.append("%f, %d, %s\n" % (dist, self.attenuation, tag.raw))
                print(tag.raw)
            time.sleep(0.5)
        self.temp_data = data

    def undo_round(self):
        self.temp_data.clear()

    def extend_data(self):
        if len(self.temp_data) > 0:
            self.data_out.extend(self.temp_data)
            self.temp_data.clear()

    def write_to_csv(self):
        self.extend_data()

        for line in self.data_out:
            print(line)
        f = open("data_out_%d.csv" % int(time.time()), "w")
        f.writelines(self.data_out)
        f.close()
        print("data_complete_writing")

    def detect_tags(self):
        tags = []
        data = self.comms.send_cmd("T").splitlines()
        for line in data:
            for iid in self.filter_ids:
                if iid in line:
                    tag = Tag()
                    tag.parse_line(line)
                    print(line)
                    tags.append(tag)
        return tags

    def run(self):
        self.comms.connect("192.168.1.208", 23)

        self.get_user_input(self.user_input)
        try:
            while True:
                if self.user_input[0] is not None:
                    cmd = self.user_input[0]
                    self.cmd_handler(cmd)
                    self.user_input = [None]
                    self.get_user_input(self.user_input)
                time.sleep(0.2)

        except KeyboardInterrupt:
            pass

    def get_user_input(self, user_input_ref):
        """Get user input"""
        user_input_ref[0] = input("Plz Input: ")

    def cmd_handler(self, cmd):
        """Handles commands"""
        try:
            if cmd.startswith('d:'):
                data = cmd.split(":")
                dist = float(data[1])
                self.measure_tag_rssi(dist)
            elif cmd.startswith("attn:"):
                data = cmd.split(":")
                attn = int(data[1])
                self.set_attn(attn)
            elif cmd == "undo":
                self.undo_round()
            elif cmd == "save":
                self.write_to_csv()
            elif cmd == "kill -f":
                os._exit(0)
        except KeyboardInterrupt:
            os._exit(0)

        except Exception as e:
            print("ERROR", str(e))


if __name__ == "__main__":
    main = Main()
    main.run()
