import os
import queue
import socket
import threading
import time
import uuid
#
TCP_IP = "192.168.1.208"
PORT = 23

# TCP_IP = "127.0.0.1"
# PORT = 65432

"""860 - 980 MHz"""

TAGS = {
    "E200 3098 1517 0207 1340 9052": "Goggles Left",
    "E200 3098 1517 0213 1340 9041": "Goggles Right",
    "E200 3098 1517 0201 1340 9063": "Shoes Top",
    "E200 3098 1517 0242 1340 901C": "Shoes Ankle Right",
}


class Tag:
    def __init__(self, epc="", tx=0, rx=0, rssi=0, ms=0):
        self.epc = epc
        self.ms = ms
        self.tx = tx
        self.rx = rx
        self.rssi = rssi
        self.raw = ""

    def update_rssi(self, tag):
        self.rssi = tag.rssi
        self.raw = tag.raw

    def parse(self, tag: str):
        """<TAG>1, 1, 925.2, <EPC>E200 1021 3303 0201 1330 8F46</EPC>"""
        self.raw = tag.split("<TAG>")[1]
        try:
            self.epc = self.raw.split("<EPC>")[1].split("</EPC>")[0]
            data = self.raw.split("<EPC>")[0].split(",")
            self.ms = int(data[0].strip())
            self.tx = int(data[1].strip())
            self.rx = int(data[2].strip())
            self.rssi = float(data[3].strip())
        except Exception as e:
            print(e)

class MSG:
    def __init__(self, msg, uid="", cb=None):
        self.msg = msg.encode()
        self.cb = cb
        self.uid = uid


class SocketClientThread(threading.Thread):
    def __init__(self, ip, port):
        super(SocketClientThread, self).__init__()
        self.alive = threading.Event()
        self.alive.set()

        self.ip = ip
        self.port = port

        self.recv_callback = self.example_recv
        self.disconnected_cb = self.example_disconnect

        self.s = None
        self.queue = queue.Queue()

    def connect(self, ip, port):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip, int(port)))

    def put(self, msg: MSG):
        self.queue.put(msg)

    def example_recv(self, data):
        print(data)

    def example_disconnect(self):
        print("disconnected -- reconnecting")

    def run(self):
        try:
            while self.alive.is_set():
                try:
                    """SEND MSG"""
                    msg = self.queue.get(True, 0.0001)  # type: MSG
                    try:
                        self.s.sendall(msg.msg)
                        if msg.cb is not None:
                            msg.cb(msg.uid)
                    except Exception as e:
                        print(e)
                        self.connect(self.ip, self.port)
                        self.disconnected_cb()
                        self.s.sendall(msg.msg)
                        if msg.cb is not None:
                            msg.cb(msg.uid)
                except queue.Empty as e:
                    """ """

                """READ FROM SOCKET"""
                try:
                    self.s.settimeout(0.0001)
                    data = self.s.recv(1024)
                    self.recv_callback(data)
                except socket.timeout:
                    """"""

                time.sleep(0.0001)
        except KeyboardInterrupt as e:
            self.alive.clear()
            os._exit(1)


class RFID:
    def __init__(self):
        self.t0 = 0
        self.t1 = 0
        self.current_uid = ""
        self.netcat_avail = True
        self.netcat_cb = None
        self.data = bytes()

        self.perf_times = []
        self.perf = []

        self.scan = {}

        self.client = SocketClientThread(TCP_IP, PORT)

        self.client.recv_callback = self.recv
        self.client.disconnected_cb = self.login

        self.client.connect(TCP_IP, PORT)
        self.login()

        self.client.start()

    def recv(self, data):
        print(data.decode())

    def netcat(self, msg: str, cb):
        self.netcat_avail = False
        self.netcat_cb = cb
        self.current_uid = str(uuid.uuid4())

        msg = MSG(msg, self.current_uid, self.netcat_t0)
        self.client.recv_callback = self.netcat_recv
        self.client.put(msg)

    def netcat_wait_avail(self):
        while not self.netcat_avail:
            if time.time() - self.t0 > 5:
                self.netcat_avail = True
            time.sleep(0.0001)

    def netcat_t0(self, uid: str):
        self.t0 = time.time()

    def netcat_recv(self, data: bytes):
        self.data += data
        # print(data.decode())
        if b'Alien>' in data:
            self.t1 = time.time()
            self.netcat_avail = True
            if self.netcat_cb is not None:
                self.netcat_cb(self.data)
            self.data = bytes()

    def login(self):
        self.netcat("alien\n", self.recv)
        self.netcat("password\n", self.recv)
        self.netcat("AntennaSequence = 0 1 2 3\n", self.recv)
        self.netcat("TagListCustomFormat = <TAG> ${MSEC1}, ${TX}, ${RX}, ${RSSI}, <EPC>${TAGIDW}</EPC></TAG> \nTagListFormat = Custom\n", self.recv)
        self.netcat("RFAttenuation=150", self.recv)

    def scan_tags(self):
        self.netcat("T\n", self.recv)

    def continuous_scan(self):
        time.sleep(0.5)
        try:
            while True:
                self.netcat_wait_avail()
                self.netcat("T\n", self.scan_cb)
        except KeyboardInterrupt:
            os._exit(1)

    def scan_cb(self, data: bytes):
        data = data.decode()
        tags = data.split("</TAG>")
        # print(data)
        self.parse_tags(tags)
        print(len(tags) - 1)
        dt = self.t1 - self.t0
        print(dt)
        print("\n\n")

    def parse_tags(self, tags: list):
        """<TAG>1, 1, 925.2, <EPC>E200 1021 3303 0201 1330 8F46</EPC>"""
        self.scan = {}
        for tag_str in tags:
            if "<TAG>" in tag_str:
                tag = Tag()
                tag.parse(tag_str)
                if tag.epc not in self.scan:
                    self.scan[tag.epc] = [tag]
                else:
                    add = True
                    for t in self.scan[tag.epc]:
                        if t.tx == tag.tx and t.rx == tag.rx:
                            t.update_rssi(tag)
                            add = False
                    if add:
                        self.scan[tag.epc].append(tag)

        for epc in self.scan:
            if epc in TAGS:
                print("%s : %s" % (epc, TAGS[epc]))
                for t in self.scan[epc]:
                    print("-- tx:%d rx:%d rssi:%f" % (t.tx, t.rx, t.rssi))
                print("")

    def perf_test(self):
        time.sleep(0.5)
        self.perf_times = []
        self.perf = []
        for i in range(35):
            self.netcat_wait_avail()
            self.netcat("T\n", self.perf_cb)

        print(self.perf_times)
        # with open("10_1m.csv", "a+") as f:
        #     f.writelines(self.perf)
        #     f.close()
        # print(self.perf)

    def perf_cb(self, data: bytes):
        data = data.decode()
        tags = data.split("</TAG>")
        print(data)
        print(len(tags) - 1)
        dt = self.t1 - self.t0
        print(dt)
        self.perf_times.append(dt)
        self.perf.append("%d, %d, %f\n" % (90, len(tags) - 1, dt))

    def rssi_measurement(self):
        """"""


if __name__ == "__main__":
    rfid = RFID()
    rfid.continuous_scan()
    # rfid.perf_test()
