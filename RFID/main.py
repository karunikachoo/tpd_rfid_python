""" ALIEN ALR9900 """

import socket
import time
from subprocess import check_output

"""
INTERNAL TAGS:
Tag:E200 3098 1517 0102 1370 89B0, Disc:1969/12/31 16:25:47.494, Last:1969/12/31 16:25:47.494, Count:1, Ant:0, Proto:2
Tag:E200 3098 1517 0096 1370 89AD, Disc:1969/12/31 16:25:47.502, Last:1969/12/31 16:25:47.502, Count:1, Ant:0, Proto:2

EXTERNAL TAGS:
Tag:E200 3098 1517 0138 1360 8C67, Disc:1969/12/31 16:25:04.241, Last:1969/12/31 16:25:04.241, Count:1, Ant:0, Proto:2
Tag:E200 3098 1517 0180 1360 8C18, Disc:1969/12/31 16:27:47.021, Last:1969/12/31 16:27:47.021, Count:1, Ant:0, Proto:2


WALLET TAGS:
Tag:E200 3098 1517 0174 1360 8C15, Disc:1969/12/31 16:28:25.644, Last:1969/12/31 16:28:25.644, Count:1, Ant:0, Proto:2
"""

TCP_IP = "192.168.1.208"
PORT = 23

TAGS = {
    "E200 3098 1517 0138 1360 8C67": "HIGH VIS JACKET 1",
    "E200 3098 1517 0180 1360 8C18": "HIGH VIS JACKET 2"
}


class Tag:
    def __init__(self, tx=-1, rx=-1, epc="0", rssi=-1, _dir="0"):
        self.tx_id = int(tx)
        self.rx_id = int(rx)
        self.epc = epc
        self.rssi = int(rssi)
        self.dir = _dir
        self.ts = time.time()

    def parse(self, string:str):
        data = string.split(", ")
        self.tx_id = int(data[0])
        self.rx_id = int(data[1])
        self.rssi = float(data[2])
        self.epc = data[3]
        self.dir = data[4]


class RFID:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect_socket()

        self.tags = {}

    def connect_socket(self):
        self.s.connect((TCP_IP, int(PORT)))

        self.netcat("alien\n")
        self.netcat("password_set\n")
        self.netcat("AntennaSequence = 0 1 2 3\n")
        self.netcat("TagListCustomFormat = ${TX}, ${RX}, ${RSSI}, ${TAGIDW}, ${DIR} \nTagListFormat = Custom\n")

    def netcat(self, content: str):
        data = bytes()
        self.s.sendall(content.encode())
        self.s.settimeout(0.05)
        i = 0
        while True:
            try:
                data += self.s.recv(1024)
            except:
                break
        return data

    def scan_tags(self):
        ret = self.netcat("T\n").decode()
        self.process_data(ret)

    def process_data(self, data: str):
        data = data.splitlines()
        # print(data)

        for line in data:
            if line == "":
                print("broken line")
            else:
                self.get_tag(line)
        self.print()

    def get_tag(self, line:str):
        tag = Tag()
        # print(line)
        try:
            tag.parse(line)
        except Exception as e:
            # print("Error parsing tag: " + str(e))
            return

        if tag.epc not in self.tags:
            self.tags[tag.epc] = {
                "latest": tag,
                "count": 0,
                "log": []
            }

        self.tags[tag.epc]["latest"] = tag
        self.tags[tag.epc]["log"].insert(0, tag)
        self.tags[tag.epc]["count"] += 1
        return tag

    def print(self):
        print("\n\n===========================\n")
        # print(self.tags)
        for key in self.tags.keys():
            if key in TAGS:
                tag = self.tags[key]["latest"]
                print("%30s %5d %5d   %5.1f %10.0f %5s %s" % (tag.epc, tag.tx_id, tag.rx_id, tag.rssi, tag.ts, tag.dir.strip(), TAGS[key]))

    def run(self):
        while True:
            try:
                self.scan_tags()

                time.sleep(0.5)
            except KeyboardInterrupt:
                break
            except BrokenPipeError:
                self.connect_socket()


if __name__ == "__main__":
    rfid = RFID()
    rfid.run()



