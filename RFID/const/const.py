
"""
${TAGIDW}   Tag ID with a space between each pair of bytes: 8000 00FE 8010 2AB7
${TAGIDB}   Tag ID with a space between each byte: 80 00 00 FE 80 10 2A B7
${TAGID}    Tag ID with no spaces at all: 800000FE80102AB7

${DATE1}    Discovery date of tag, in format YY/MM/DD
${TIME1}    Discovery time of tag, in format hh:mm:ss
${MSEC1}    Discovery date & time of tag, in milliseconds since 1/1/1970.
${DATE2}    Last-seen date of tag, in format YY/MM/DD
${TIME2}    Last-seen time of tag, in format hh:mm:ss
${MSEC2}    Last-seen date & time of tag, in milliseconds since 1/1/1970.

${COUNT}    Read Count of tags, i.e., how many times the tag has been read

${TX}       (Transmit) Antenna the tag was last seen at
${RX}       (Receive) Antenna where tag was last seen

${PROTO#}   Integer value indicating the tag's protocol (0 = Class0, 1 = Class1/Gen1, 2 = Class2/Gen2)
${PROTO}    String representation of the tag's protocol
${PCWORD}   PC Word (Class1/Gen2)

${RSSI}     Tag RSSI measurement
${RSSI_MAX} The maximum RSSI value for this tag.

${NAME}     The reader's ReaderName
${HOST}     The reader's Hostname
${IP}       The reader's IP address
${MAC}      The reader's MAC address

${SPEED}        Tag speed (m/s) – impacts read performance
${SPEED_MIN}    The minimum (most negative) speed measured for the tag
${SPEED_MAX}    The maximum (most positive) speed measured for the tag
${SPEED_TOP}    The top speed (regardless of direction) for the tag
${DIR}          Direction: "-" (approach), "+" (recede), or "0" (stationary)

${G2DATA1...4}  Tag data corresponding to the 1st, 2nd, 3rd, or 4th AcqG2TagData entry
${G2OPS}        Results from all AcqG2Ops that were executed, entries separated by vertical bars (|).
${G2OPS1...8}   Results corresponding to 1 of the 8 numbered AcqG2Ops
${AUTH}         Tag's Alien dynamic authentication data (includes tag manufacturer's id code) Refer to the description of the TagAuthcommand formoredetails.
${XPC}          XPC Word(s) (Class1/Gen2)
${FREQ}         Frequency the tag was acquired on (MHz)
"""


class TagProps:
    """
    ${TAGIDW}   Tag ID with a space between each pair of bytes: 8000 00FE 8010 2AB7
    ${TAGIDB}   Tag ID with a space between each byte: 80 00 00 FE 80 10 2A B7
    ${TAGID}    Tag ID with no spaces at all: 800000FE80102AB7
    """
    TAG_ID_WITH_SPACE = "${TAGIDW}"
    TAG_ID_NO_SPACE = "${TAGID}"
    TAG_ID_SPACE_BY_BYTE = "${TAGIDB}"

    """
    ${DATE1}    Discovery date of tag, in format YY/MM/DD
    ${TIME1}    Discovery time of tag, in format hh:mm:ss
    ${MSEC1}    Discovery date & time of tag, in milliseconds since 1/1/1970.
    ${DATE2}    Last-seen date of tag, in format YY/MM/DD
    ${TIME2}    Last-seen time of tag, in format hh:mm:ss
    ${MSEC2}    Last-seen date & time of tag, in milliseconds since 1/1/1970.
    """
    DATE_DISCOVERED = "${DATE1}"
    TIME_DISCOVERED = "${TIME1}"
    MSEC_DISCOVERED = "${MSEC1}"
    DATE_LAST_SEEN = "${DATE2}"
    TIME_LAST_SEEN = "${TIME2}"
    MSEC_LAST_SEEN = "${MSEC2}"

    """
    ${COUNT}    Read Count of tags, i.e., how many times the tag has been read
    """
    READ_COUNT = "${COUNT}"

    """
    ${TX}       (Transmit) Antenna the tag was last seen at
    ${RX}       (Receive) Antenna where tag was last seen
    """
    TX = "${TX}"
    RX = "${RX}"

    """
    ${RSSI}     Tag RSSI measurement
    ${RSSI_MAX} The maximum RSSI value for this tag.
    """
    RSSI = "${RSSI}"
    RSSI_MAX = "${RSSI_MAX}"

    """
    ${NAME}     The reader's ReaderName
    ${HOST}     The reader's Hostname
    ${IP}       The reader's IP address
    ${MAC}      The reader's MAC address
    """
    READER_NAME = "${NAME}"
    HOSTNAME = "${HOST}"
    READER_IP_ADDR = "${IP}"
    READER_MAC_ADDR = "${MAC}"

    """
    ${SPEED}        Tag speed (m/s) – impacts read performance
    ${SPEED_MIN}    The minimum (most negative) speed measured for the tag
    ${SPEED_MAX}    The maximum (most positive) speed measured for the tag
    ${SPEED_TOP}    The top speed (regardless of direction) for the tag
    ${DIR}          Direction: "-" (approach), "+" (recede), or "0" (stationary)
    """
    SPEED = "${SPEED}"
    SPEED_MIN = "${SPEED_MIN}"
    SPEED_MAX = "${SPEED_MAX}"
    SPEED_TOP = "${SPEED_TOP}"
    T = "${DIR}"
