import math
import matplotlib.pyplot as plt
import numpy as np

from utils.t_value_parser import TDist


class Tag:
    def __init__(self):
        self.epc = ""
        self.rx = 0
        self.rssi = 0
        self.dist = 0
        self.attn = 0
        self.raw = ""

    def __repr__(self):
        return repr((self.epc, self.dist, self.rssi))

    def parse_line(self, line: str):
        data = line.split(",")
        self.dist = float(data[0].strip())
        self.attn = int(data[1].strip())
        self.epc = data[2].strip()
        self.rx = int(data[3])
        self.rssi = float(data[5])
        self.raw = line


class TagHolder:
    def __init__(self, tag: Tag):
        self.avg = tag.rssi
        self.tags = [tag]
        self.pdiff = 0.0
        self.pd2 = 0.0


TAG_ID = ["E200 3098 1517 0112 1350 8E49",
          "E200 3098 1517 0138 1360 8C67"]


class Analyser:
    def __init__(self):
        self.t_dist = TDist()

        f0 = open("nov7_stick_test.csv")
        f1 = open("nov7_human_test.csv")
        lines0 = f0.readlines()[1:]
        lines1 = f1.readlines()[1:]
        f0.close()
        f1.close()

        tags_stick = self.parse_data(lines0)
        tags_human = self.parse_data(lines1)

        # STICK TEST
        r_sq = self.compare_fixes(tags_stick[TAG_ID[0]])
        plt.show()

        dd = self.parse_tags_for_plot(tags_stick, TAG_ID[1])

        x, y, ret = self.plot_avgs(['Stick', 'Stationary @ 4.75m'], *dd, r_sq=r_sq)

        i = 0
        for tag in tags_stick:
            print(tag)
            stick_dist = self.group_by_dist(tags_stick[tag])
            x, err = self.calc_error_bars(stick_dist)
            vv = ret[i]
            plt.errorbar(vv[0], vv[1], yerr=err)
            i += 1

        plt.show()

        # HUMAN TEST
        r_sq = self.compare_fixes(tags_human[TAG_ID[1]])
        plt.show()
        dd = self.parse_tags_for_plot(tags_human, TAG_ID[1])

        x, y, ret = self.plot_avgs(['Human Vest', 'Stationary @ 4.75m'], *dd, r_sq=r_sq)

        i = 0
        for tag in tags_stick:
            print(tag)
            stick_dist = self.group_by_dist(tags_stick[tag])
            x, err = self.calc_error_bars(stick_dist)
            vv = ret[i]
            plt.errorbar(vv[0], vv[1], yerr=max(err))
            i += 1

        plt.show()

    def calc_error_bars(self, dist_group: dict):
        errors = []
        x = []
        dist_group = self.w_knn_groups(dist_group, discard=True)
        for dist in dist_group:
            tags = dist_group[dist]
            tv = self.t_dist.get_val(0.025, len(tags) - 1)
            rssis = [t.rssi for t in tags]

            x.append(float(dist))
            if len(rssis) > 0:
                x_bar = np.mean(rssis)
                sd = np.std(rssis)
                err = tv * sd / math.sqrt(x_bar)
                print("%10s %10f" % (dist, err))

                errors.append(err)
            else:
                errors.append(0)
            # for tag in tags:
        return x, errors

    @staticmethod
    def plot_avgs(labels: list, *avgs, r_sq=None):
        i = 0
        y_maxes = []
        ret = []
        for avg in avgs:
            # print(avg)
            x = []
            y = []
            for key in avg:
                # print(key)
                d = float(key)
                x.append(d)
                y.append(avg[key].avg)

            plt.plot(x, y, label=labels[i])
            i += 1
            y_maxes.append(max(y))

            ret.append([x, y])
        y_max = max(y_maxes)

        # Plot 1/R**2
        x = []
        y = []
        if r_sq is None:
            key = list(avgs[0].keys())[0]
            key_end = list(avgs[0].keys())[-1]
            max_avg = avgs[0][key].avg * (float(key) ** 2)

            maxes = []
            amax = avgs[0][key]
            akey = float(key)
            for k in avgs[0]:
                a = avgs[0][k]
                if a.avg > amax.avg:
                    amax = a
                    akey = float(k)
            max_avg2 = amax.avg * (akey ** 2)

            # print(max_avg)
            # print(max_avg2)
            # print(sum(maxes))

            for d in range(int(float(key)), int(float(key_end))):
                x.append(d)
                y.append(max_avg2 / (d * d))
        else:
            x = r_sq[0]
            y = r_sq[1]

        plt.plot(x, y, label="1/r^2")
        plt.legend(loc="upper right")

        axes = plt.gca()
        axes.set_ylim([0, y_max * 1.1])

        # plt.show()
        return x, y, ret

    @staticmethod
    def parse_tags_for_plot(tags: dict, ref_id):
        epc_list = list(tags.keys())
        epc_list.remove(ref_id)

        data = sorted(tags[ref_id], key=lambda t: (t.dist, t.rssi))
        data_avgs = Analyser.average_sorted(data)

        dd = (data_avgs,)

        for epc in epc_list:
            dat = sorted(tags[epc], key=lambda t: (t.dist, t.rssi))
            dat_avgs = Analyser.average_sorted(dat)

            dd = dd + (dat_avgs,)

        return dd

    @staticmethod
    def parse_data(lines):
        tags = {}
        for line in lines:
            tag = Tag()
            tag.parse_line(line)
            if tag.epc not in tags:
                tags[tag.epc] = [tag]
            else:
                tags[tag.epc].append(tag)
        return tags

    def compare_fixes(self, tags: list):
        data = sorted(tags, key=lambda t: (t.dist, t.rssi))
        data_avgs = self.average_sorted(data)

        groups = self.group_by_dist(data)
        group_ori = groups
        for i in range(1):
            print("\n\n === ITER %d ===" % i)
            groups = self.w_knn_groups(groups)
        group_avgs = self.average_groups(groups)

        limit_groups = self.limit_group(group_ori)
        limit_group_avgs = self.average_groups(limit_groups)

        for key in data_avgs:
            d = data_avgs[key]
            lg = limit_group_avgs[key]
            try:
                g = group_avgs[key]
                # print("%s % 10.5f % 10.5f % 10.5f" % (key, d.avg, lg.avg, g.avg))
            except:
                # print("%s % 10.5f % 10.5f XXXXXXXX" % (key, lg.avg, d.avg))
                continue

        return self.plot_avgs(["Original", "w-knn shifted", "range limited"], data_avgs, group_avgs, limit_group_avgs)

    @staticmethod
    def limit_group(groups, range_percent=80.0):
        g2 = {}
        for key in groups:
            data = groups[key]
            count = int(len(data) * range_percent / 100)
            pad = len(data) - count
            fpad = math.floor(pad / 2.0)
            epad = len(data) - math.ceil(pad / 2.0)
            # print(fpad, epad, pad)
            g2[key] = data[fpad:epad]
        # print(g2)
        return g2

    @staticmethod
    def group_by_dist(data):
        dists = {}
        for tag in data:
            if str(tag.dist) in dists:
                dists[str(tag.dist)].append(tag)
            else:
                dists[str(tag.dist)] = [tag]
        return dists

    def w_knn_groups(self, groups: dict, discard=False):
        keys = list(groups.keys())

        dists = {keys[0]: groups[keys[0]]}
        for i in range(1, len(keys)):
            kprev = keys[i - 1]
            kcur = keys[i]

            prev = groups[kprev]
            cur = groups[kcur]

            dists[kcur] = []

            for tag in cur:
                if self.w_knn(tag, cur, prev):
                    dists[kcur].append(tag)
                elif not discard:
                    # print(tag, "OUTLIER %s -> %s" % (kcur, kprev))
                    dists[kprev].append(tag)
        return dists

    @staticmethod
    def w_knn(p, cur, prev, k=10):
        dists = []
        for t in cur:
            if t != p:
                # print("SAME", theta)
                d = abs(t.rssi - p.rssi)
                dists.append((d, 1))
        for t in prev:
            d = abs(t.rssi - p.rssi)
            dists.append((d, 0))

        if k > 0:
            closest = sorted(dists, key=lambda x: x[0])[:k]
        else:
            closest = sorted(dists, key=lambda x: x[0])

        g0 = 0
        g1 = 0

        for d in closest:
            if d[1] == 0:
                g0 += (1 / (d[0] + 0.00001))
            elif d[1] == 1:
                g1 += (1 / (d[0] + 0.00001))

        return g1 > g0

    def average_groups(self, groups):
        avgs = {}
        for key in groups:
            data = groups[key]
            a = None
            for t in data:
                if key not in avgs:
                    avgs[key] = TagHolder(t)
                    a = avgs[key]
                else:
                    a = avgs[key]
                    c = len(a.tags)
                    a.avg = (a.avg * c + t.rssi) / (c + 1)
                    a.tags.append(t)

                    diff = (t.rssi - a.avg) / a.avg * 100
                    try:
                        a.pd2 = (diff - a.pdiff) / a.pdiff * 100
                    except ZeroDivisionError:
                        pass
                    a.pdiff = diff
                # print("%5s % 6.6f % 10.2f % 5.2f % 5.2f % 5.2f " % (key, theta.rssi, theta.dist * theta.rssi, a.avg, a.pdiff, a.pd2))
            if a is not None:
                """"""
                # print("% 10s % 10.5f" % (key, a.avg))
        return avgs

    @staticmethod
    def average_sorted(data):
        avgs = {}
        for tag in data:
            avg = None
            if str(tag.dist) not in avgs:
                avgs[str(tag.dist)] = TagHolder(tag)
                avg = avgs[str(tag.dist)]
            else:
                avg = avgs[str(tag.dist)]
                c = len(avg.tags)

                avg.avg = (avg.avg * c + tag.rssi) / (c + 1)
                avg.tags.append(tag)

                diff = (tag.rssi - avg.avg) / avg.avg * 100
                try:
                    avg.pd2 = (diff - avg.pdiff) / avg.pdiff * 100
                except ZeroDivisionError:
                    pass
                avg.pdiff = diff

            # print("% 5.0f % 6.6f % 10.2f % 5.2f % 5.2f % 5.2f " % (tag.dist, tag.rssi, tag.dist * tag.rssi, avg.avg, avg.pdiff, avg.pd2))
        for key in avgs:
            """"""
            # print("% 10s % 10.5f" % (key, avgs[key].avg))
        return avgs





if __name__ == '__main__':
    Analyser()
