import json
import os
import pickle
import random

import pandas as pd
import keras
from keras import Input
from keras.layers import Dense, Conv1D, Dropout, MaxPooling1D, Flatten
from keras.models import Model
import numpy as np
from sklearn.preprocessing import MinMaxScaler, LabelBinarizer
from sklearn.svm import SVC, NuSVC

from datahandler import DataHandler

MODEL_LB_BIN_TEMPLATE = "%s_lb_bin.pkl"

WEIGHT_FILE_TEMPLATE = "%s.h5"

MODEL_FILE_TEMPLATE = "%s.json"

LABEL = 'labels'


class PositionerML:
    def __init__(self, name, epochs=0, csvs=None, train_percent=0.8, svm=False):
        # self.datahandler = DataHandler(["LOG", "ROOM"], os.getcwd() + "/csi_data", "{}{:d}LOG_{:d}_{:d}")
        self.name = name
        self.epochs = epochs
        self.csvs = csvs
        self.train_percent = train_percent
        self.svm = svm

        self.lb_bin = None
        self.model = None   # type: Model
        self.train = []
        self.test = []
        self.cats = []

    def load_model(self):
        try:
            if self.svm:
                return self.load_svm_model()
            else:
                f = open(MODEL_FILE_TEMPLATE % self.name, 'r')
                model_json = f.read()
                f.close()

                print("Model Found. Loading...")

                self.model = keras.models.model_from_json(model_json)
                self.model.load_weights(WEIGHT_FILE_TEMPLATE % self.name)
                self.model.compile(optimizer='adam',
                                   loss='categorical_crossentropy',
                                   metrics=['accuracy'])

                try:
                    lb = open(MODEL_LB_BIN_TEMPLATE % self.name, 'rb')
                    self.lb_bin = pickle.load(lb)
                    print("lb_binarizer found. using...")
                except FileNotFoundError as e:
                    print(e)

                return True
        except FileNotFoundError as e:
            print(e)
            return False

    def save_model(self):
        model_json = self.model.to_json()
        with open(MODEL_FILE_TEMPLATE % self.name, 'w') as json_file:
            json_file.write(model_json)
            json_file.close()

        self.model.save_weights(WEIGHT_FILE_TEMPLATE % self.name)

        with open(MODEL_LB_BIN_TEMPLATE % self.name, 'wb') as jf:
            pickle.dump(self.lb_bin, jf)
            jf.close()

    def train_model(self, conv=False):
        if self.svm:
            self.train_svm_model()
        else:
            if conv:
                self.reshape_data_for_conv()
            if not self.load_model():
                print("Model NOT Found. Creating...")

                if conv:
                    self.model = self.create_conv_model(self.train[0], self.cats)
                else:
                    self.model = self.create_model(self.train[0], self.cats)

                self.model.compile(optimizer='adam',
                                   loss='categorical_crossentropy',
                                   metrics=['accuracy'])

            self.model.fit(self.train[0], self.train[1],
                           validation_data=(self.test[0], self.test[1]),
                           epochs=self.epochs, batch_size=100)

            scores = self.model.evaluate(self.train[0], self.train[1])
            print("%s: %.2f%%" % (self.model.metrics_names[1], scores[1] * 100))

            self.save_model()

    def test_model(self, conv=False):
        if self.svm:
            self.test_svm_model()
        else:
            if self.load_model():
                test_x = self.test[0]       # type: pd.DataFrame
                test_y = self.test[1]       # type: pd.DataFrame
                j = random.randint(0, test_x.shape[0]-2)
                print(j, test_x.shape)

                if conv:
                    test_x = self.reshape_conv(test_x)

                ref = self.lb_bin.inverse_transform(test_y)
                prediction = self.model.predict(test_x)
                vals = self.lb_bin.inverse_transform(prediction)
                count = 0
                pos = 0
                neg = 0
                miss = {}

                for i in range(len(vals)):
                    p = vals[i]
                    y = ref[i]
                    if p in y or y in p or y == p:
                        pos += 1
                    else:
                        neg += 1
                        label = "%s <- %s" % (p, y)
                        # print(neg, label)
                        if label not in miss:
                            miss[label] = 1
                        else:
                            miss[label] += 1

                    count += 1

                labels = [""]
                counts = [0]
                for label in miss:
                    c = miss[label]
                    for i in range(len(counts)):
                        if c > counts[i]:
                            counts.insert(i, c)
                            labels.insert(i, label)
                            break

                for i in range(len(labels)):
                    print(labels[i], counts[i])
                print(count, pos, neg, ":", pos / count)

    def predict(self, x):
        if self.svm:
            return self.svm_predict(x)
        else:
            p = self.model.predict(x)
            return self.lb_bin.inverse_transform(p)

    def preprocess(self, binarize=True):
        print("Pre-processing data....")
        dfs = []
        for csv in self.csvs:
            df = pd.read_csv(csv, sep=",", header=None)

            cols = ["x%d" % x for x in range(len(df.columns) - 1)]
            cols.append("labels")
            df.columns = cols

            df['x0'] = 0
            dfs.append(df)

        df, self.cats = self.compile_category_data(dfs)
        self.train, self.test, self.lb_bin = self.prepare_datasets(df, self.train_percent, self.lb_bin, binarize)

    def reshape_data_for_conv(self):
        self.train[0] = self.reshape_conv(self.train[0])
        self.test[0] = self.reshape_conv(self.test[0])

    @staticmethod
    def reshape_conv(data):
        if type(data) == pd.DataFrame:
            return data.to_numpy().reshape((data.shape[0], data.shape[1], 1))
        else:
            return data.reshape((data.shape[0], data.shape[1], 1))

    @staticmethod
    def prepare_datasets(df: pd.DataFrame, train_percent, lb_bin, binarize=True):
        print("Preparing datasets for use")
        # randomize data
        df = df.sample(frac=1).reset_index(drop=True)
        r, c = df.shape
        data_cols = df.columns[:-1]

        # split data into train and test
        train = df[:int(r * train_percent)]
        test = df[int(r * train_percent):]

        # scale data between 0-1
        # cs = MinMaxScaler()
        # train_x = cs.fit_transform(train[data_cols])
        # test_x = cs.fit_transform(test[data_cols])
        train_x = train[data_cols]
        test_x = test[data_cols]

        # binarize labels
        if binarize:
            if lb_bin is None:
                print("lb_binarizer NOT found. creating from data...")
                lb_bin = LabelBinarizer().fit(df[LABEL])
            train_y = lb_bin.transform(train[LABEL])
            test_y = lb_bin.transform(test[LABEL])
        else:
            train_y = train[LABEL].to_numpy()
            test_y = test[LABEL].to_numpy()

        print(type(train_x), type(test_x))

        return [train_x, train_y], [test_x, test_y], lb_bin

    @staticmethod
    def compile_category_data(dfs) -> [pd.DataFrame, list]:
        # limit it to lowest dataset
        label_counts = {}

        for df in dfs:
            labels = df[LABEL].value_counts().keys().tolist()
            counts = df[LABEL].value_counts().tolist()
            print(labels, counts)

            for (lb, c) in zip(labels, counts):
                if lb not in label_counts:
                    label_counts[lb] = [c]
                else:
                    label_counts[lb].append(c)
        # limit to lowest and combine columns of all dfs
        combined_df_rows = []
        # print(label_counts)
        for lb in label_counts:
            min_count = min(label_counts[lb])

            lb_datasets = []
            lb_labels = None
            for df in dfs:
                data_cols = df.columns[:-1]

                lb_data = df.loc[df[('%s' % LABEL)] == lb]
                lb_data = lb_data.reset_index(drop=True)

                rows = lb_data.shape[0]
                n_drop = rows - min_count
                if n_drop > 0:
                    lb_data = lb_data.drop([rows - n_drop - 1, rows - 1])

                lb_labels = lb_data[LABEL]

                lb_datasets.append(lb_data[data_cols])

            lb_datasets.append(lb_labels)
            df = pd.concat(lb_datasets, axis=1, sort=False)

            new_columns = ["x%d" % x for x in range(df.shape[1] - 1)]
            new_columns.append("%s" % LABEL)
            df.columns = new_columns

            # print(lb, df.shape, df.columns)

            combined_df_rows.append(df)
        df = pd.concat(combined_df_rows)
        print(df.shape, df.columns)

        cats = list(label_counts.keys())
        return df, cats

    def load_svm_model(self):
        try:
            lb = open("%s.pkl" % self.name, 'rb')
            self.model = pickle.load(lb)
            print("SVM model found")
            return True
        except FileNotFoundError as e:
            print(e)
            return False

    def train_svm_model(self, nu=False):
        if not self.load_svm_model():
            print("SVM Model not found. creating from scratch...")
            if nu:
                self.model = NuSVC(gamma='auto')
            else:
                self.model = SVC(gamma='auto')

        print("Training svm model", self.train[0].shape, self.train[1].shape)
        if nu:
            for _nu in np.arange(1.0, 0.05, -0.05):
                self.model = NuSVC(nu=_nu)
                try:
                    self.model.fit(self.train[0], self.train[1])
                    break
                except ValueError as e:
                    print("nu {} not feasible".format(_nu))
        else:
            self.model.fit(self.train[0], self.train[1])

        with open("%s.pkl" % self.name, 'wb') as jf:
            pickle.dump(self.model, jf)

    def svm_predict(self, data):
        return self.model.predict(data)

    def test_svm_model(self):
        """
        INITIAL_DATASETS -> c:3600 +ve:3077 -ve:523 : 0.8547222222222223
        """
        if self.load_svm_model():
            pred = self.model.predict(self.test[0])
            count = 0
            pos = 0
            neg = 0
            miss = {}
            for i in range(len(pred)):
                p = pred[i]
                y = self.test[1][i]
                if p in y or y in p or y == p:
                    pos += 1
                else:
                    neg += 1
                    label = "%s <- %s" % (p, y)
                    if label not in miss:
                        miss[label] = 1
                    else:
                        miss[label] += 1
                count += 1

            order = [""]
            counts = [0]
            for label in miss:
                c = miss[label]
                for i in range(len(counts)):
                    if c > counts[i]:
                        counts.insert(i, c)
                        order.insert(i, label)
                        break

            for i in range(len(counts)):
                print(order[i], counts[i])

            print(count, pos, neg, ":", pos / count)

    @staticmethod
    def create_conv_model(train_x: pd.DataFrame, unique_cats):
        input_a = Input(shape=train_x.shape[1:])
        x = Conv1D(filters=64, kernel_size=2, activation='relu')(input_a)
        x = Conv1D(filters=64, kernel_size=2, activation='relu')(x)
        # x = Conv1D(filters=64, kernel_size=3, activation='relu')(x)
        x = Dropout(0.3)(x)
        x = MaxPooling1D(pool_size=2)(x)
        x = Flatten()(x)
        x = Dense(128, activation='relu')(x)
        # x = Dense(64, activation='relu')(x)
        # x = Dense(32, activation='relu')(x)
        x = Dense(len(unique_cats), activation='softmax')(x)
        x = Model(inputs=input_a, outputs=x)

        return x

    @staticmethod
    def create_model(train_x: pd.DataFrame, unique_cats):
        input_a = Input(shape=(train_x.shape[1],))
        x = Dense(512, activation='relu')(input_a)
        x = Dense(256, activation='relu')(x)
        x = Dense(128, activation='relu')(x)
        x = Dense(64, activation='relu')(x)
        x = Dense(32, activation='relu')(x)
        x = Dense(len(unique_cats), activation='softmax')(x)
        x = Model(inputs=input_a, outputs=x)

        return x


if __name__ == "__main__":
    # Dense model
    # p0 = PositionerML("rel_room_model_dense", 500, ["csi_data3/REL_ROOM_TRAIN001.csv", "csi_data3/REL_ROOM_TRAIN002.csv"], 0.8)
    # p0.preprocess()
    # p0.train_model()
    #
    # # Convolution Model
    # p1 = PositionerML("room_model_conv", 500, ["csi_data2/ROOM_TEST_DATA1.csv", "csi_data2/ROOM_TEST_DATA2.csv"], 0.8)
    # p1.preprocess()
    # p1.train_model(conv=True)

    # NuSVM
    # p2 = PositionerML("rel_room_model_nusvm", 500, ["csi_data3/REL_ROOM_TRAIN001.csv", "csi_data3/REL_ROOM_TRAIN002.csv"], 0.8, svm=True)
    # p2.preprocess(binarize=False)
    # p2.train_svm_model(nu=True)

    # RBF SVM
    # p3 = PositionerML("rel_room_model_rbfsvm", 500, ["csi_data3/REL_ROOM_TRAIN001.csv", "csi_data3/REL_ROOM_TRAIN002.csv"], 0.9, svm=True)
    # p3.preprocess(binarize=False)
    # p3.train_svm_model()

    # Test models
    t0 = PositionerML("room_model_dense", 500, ["csi_data3/ROOM_TEST001.csv", "csi_data3/ROOM_TEST002.csv"], 0.01)
    # t1 = PositionerML("room_model_conv", 500, ["csi_data2/ROOM_TEST_DATA1.TEST.csv", "csi_data2/ROOM_TEST_DATA2.TEST.csv"], 0.01)
    t2 = PositionerML("room_model_nusvm", 500, ["csi_data3/ROOM_TEST001.csv", "csi_data3/ROOM_TEST002.csv"], 0.01, svm=True)
    t3 = PositionerML("room_model_rbfsvm", 500, ["csi_data3/ROOM_TEST001.csv", "csi_data3/ROOM_TEST002.csv"], 0.01, svm=True)

    t0.load_model()
    # t1.load_model()
    t2.load_model()
    t3.load_model()

    t0.preprocess()
    # t1.preprocess()
    t2.preprocess(binarize=False)
    t3.preprocess(binarize=False)

    t0.test_model()
    # t1.test_model(conv=True)
    t2.test_model()
    t3.test_model()

    # p = PositionerML("room_model_nusvm_full", 200, ["csi_data/ROOM_FULL_DATA1.csv", "csi_data/ROOM_FULL_DATA2.csv"], 0.7, svm=True)
    # p = PositionerML("room_model_full", 1000, ["csi_data/ROOM_FULL_DATA1.csv", "csi_data/ROOM_FULL_DATA2.csv"], 0.7)
    # p = PositionerML("room_model_full", 1000, ["csi_test/ROOM_TEST_DATA1.csv", "csi_test/ROOM_TEST_DATA2.csv"], 0.01)
    # p = PositionerML("room_model_nusvm_full", 1000, ["csi_test/ROOM_TEST_DATA1.csv", "csi_test/ROOM_TEST_DATA2.csv"], 0.01, svm=True)
    # p = PositionerML("kitchen_model_svm_rbs", 1000, ["csi_data/KITCHEN_DATA1.csv", "csi_data/KITCHEN_DATA2.csv"], 0.7, svm=True)
    # p = PositionerML("kitchen_model_conv", 1000, ["csi_data/KITCHEN_DATA1.csv", "csi_data/KITCHEN_DATA2.csv"], 0.7)
    # p.preprocess(binarize=False)
    # p.preprocess()
    # p.train_model(conv=False)
    # p.train_svm_model()
    # p.test_svm_model()
    # p.train_model(conv=True)

    # for i in range(1):
    #     p.test_model()
