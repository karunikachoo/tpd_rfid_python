import json
import math
import tkinter as tk


class LocationBtn(tk.Canvas):
    def __init__(self, master, name, onclick, **kwargs):
        tk.Canvas.__init__(self, master, width=200, height=100)
        self.name = name
        self.onclick = onclick

        self.label = self.create_text([100, 50], text=self.name, font=("Arial", 24), fill="#888888")
        self.bind('<Button-1>', self.clicked)

    def clicked(self, evt):
        self.onclick(self.name)


class LocationChooser(tk.Toplevel):
    def __init__(self, main, master):
        tk.Toplevel.__init__(self, master)
        self.main = main
        self.title("LocationVis")

        label1 = tk.Label(self, text="File path: ./")
        label1.grid(row=0, column=0, stick="e")

        self.file_entry = tk.Entry(self)
        self.file_entry.grid(row=0, column=1, stick="we")

        label2 = tk.Label(self, text=".json")
        label2.grid(row=0, column=2, stick="w")

        self.load_btn = tk.Button(self, text="Load Locations", command=self.load_locations)
        self.load_btn.grid(row=0, column=3, stick="w")

        self.export_btn = tk.Button(self, text="Export Locations", command=self.export_locations)
        self.export_btn.grid(row=0, column=4, stick="w")

        label0 = tk.Label(self, text="Room Name:")
        label0.grid(row=1, column=0, sticky="e")

        self.room_entry = tk.Entry(self, text="")
        self.room_entry.grid(row=1, column=1, sticky="we")

        label = tk.Label(self, text="Location Name:")
        label.grid(row=2, column=0, sticky="e")

        self.name_entry = tk.Entry(self, text="")
        self.name_entry.grid(row=2, column=1, sticky="we")

        self.add_btn = tk.Button(self, text="Add Location", command=self.create_location)
        self.add_btn.grid(row=2, column=3, stick="w")

        self.frame = tk.Frame(self)
        self.frame.grid(row=3, column=0, columnspan=3)

        self.locations = []
        self.rows_per_col = 4

    def on_location_selected(self, name):
        # print(name)     # pass to serial names
        room = self.room_entry.get()
        
        for serial in self.main.serials:
            file_prefix = "%s_%s%03d_" % (room, name, serial.i + 1)
            # print(file_prefix)
            serial.set_prefix_text(file_prefix)

    def load_locations(self):
        try:
            with open("./%s.json" % self.file_entry.get(), 'r') as f:
                ds = f.read()
                data = json.loads(ds)

                self.room_entry.delete(0, tk.END)
                self.room_entry.insert(0, data["room"])

                for loc in self.locations:
                    loc.destroy()

                self.locations = []

                for name in data["names"]:
                    self.add_location(name)
        except Exception as e:
            print(e)

    def export_locations(self):
        room = self.room_entry.get()
        names = []
        for loc in self.locations:
            names.append(loc.name)

        data = {
            'room': room,
            'names': names
        }

        ds = json.dumps(data)

        with open("./%s.json" % self.file_entry.get(), 'w+') as f:
            f.write(ds)
            f.close()

    def create_location(self):
        name = self.name_entry.get()
        self.add_location(name)

    def add_location(self, name):
        if name != "":
            btn = LocationBtn(self.frame, name, self.on_location_selected)
            btn.grid(row=len(self.locations) % self.rows_per_col, column=math.floor(len(self.locations) / self.rows_per_col))
            self.locations.append(btn)
