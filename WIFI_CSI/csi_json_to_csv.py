import json
import os

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np


class Converter:
    def __init__(self, folder, ang=False):
        self.folder = folder
        files = self.list_files(os.getcwd() + "/%s" % folder, ["LOG", "ROOM"])

        for filename in files:
            data = self.read_data(folder, filename)
            self.plot_data(folder, filename, data, save=True)
            if ang:
                self.plot_ang(folder, filename, data, save=True)

    @staticmethod
    def plot_data(folder, filename, data, save=False):
        fig = plt.figure()
        amp_plt = fig.gca(projection='3d')

        # fig1 = plt.figure()
        # ang_plt = fig1.gca(projection='3d')

        amp_z = []
        # ang_z = []

        y = 0
        for entry in data:
            amp = [x for x in entry["amp"][:64]]
            # ang = [x for x in entry["ang"][:64]]

            amp[0] = 0

            amp_z.append(amp)
            # ang_z.append(ang)
            y += 1

        X, Y = np.meshgrid(range(64), range(y))
        Z = np.array(amp_z)

        cmap = plt.get_cmap("viridis")

        amp_plt.plot_surface(X, Y, Z, cmap=cmap, linewidth=0, antialiased=False)
        #
        # Z1 = np.array(ang_z)
        # ang_plt.plot_surface(X, Y, Z1, cmap=cmap, linewidth=0, antialiased=False)

        if save:
            if folder != "" and folder is not None:
                path = "%s/%s.png" % (folder, filename)
            else:
                path = "%s.png" % filename

            plt.savefig(path)
            # amp_plt.save_fig(filename + "amp.png")
            # ang_plt.save_fig(filename + "ang.png")
        else:
            plt.show()

    @staticmethod
    def plot_ang(folder, filename, data, save=False):
        fig = plt.figure()
        amp_plt = fig.gca(projection='3d')

        # fig1 = plt.figure()
        # ang_plt = fig1.gca(projection='3d')

        amp_z = []
        # ang_z = []

        y = 0
        for entry in data:
            # amp = [x for x in entry["amp"][:64]]
            ang = [x for x in entry["ang"][:64]]

            ang[0] = 0

            amp_z.append(ang)
            # ang_z.append(ang)
            y += 1

        X, Y = np.meshgrid(range(64), range(y))
        Z = np.array(amp_z)

        cmap = plt.get_cmap("viridis")

        amp_plt.plot_surface(X, Y, Z, cmap=cmap, linewidth=0, antialiased=False)
        #
        # Z1 = np.array(ang_z)
        # ang_plt.plot_surface(X, Y, Z1, cmap=cmap, linewidth=0, antialiased=False)

        if save:
            if folder != "" and folder is not None:
                path = "%s/%s_ANG.png" % (folder, filename)
            else:
                path = "%s_ANG.png" % filename

            plt.savefig(path)
            # amp_plt.save_fig(filename + "amp.png")
            # ang_plt.save_fig(filename + "ang.png")
        else:
            plt.show()

    @staticmethod
    def list_files(path, fixes):
        dirs = os.listdir(path)
        ret = []
        for f in dirs:
            if ".json" in f:
                for fix in fixes:
                    if fix not in f:
                        continue

                ret.append(f.split(".json")[0])
        return ret

    @staticmethod
    def read_data(folder, filename):
        if folder != "" and folder is not None:
            path = "%s/%s.json" % (folder, filename)
        else:
            path = "%s.json" % filename
        f = open(path, "r")

        data = f.read()
        f.close()

        return json.loads(data)


if __name__ == "__main__":
    Converter("csi_data3", ang=True)
