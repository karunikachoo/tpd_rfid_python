import os
import queue
import sys
import threading
import time
import traceback
from math import sqrt, atan2

import serial


class bcol:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class CMD:
    SEARCH = 100
    CONNECT = 110
    LOOP = 120

    def __init__(self, cmd, data):
        self.cmd = cmd
        self.data = data


class ESPSerial(threading.Thread):
    def __init__(self, main, pattern="ttyUSB"):
        super(ESPSerial, self).__init__()
        self.main = main
        self.pattern = pattern
        self.alive = threading.Event()
        self.alive.set()

        self.port = None
        self.serial = None
        self.prev_lines = []

        self.amps = []
        self.angs = []

        self.queue = queue.Queue()
        self.current_cmd = CMD(None, None)

        self.queue.put(CMD(CMD.SEARCH, None))

    def stop(self):
        self.alive.clear()

    def run(self):
        # self.wait_for_dev()
        try:
            while self.alive.is_set():
                cmd = self.current_cmd
                if not self.queue.empty():
                    cmd = self.queue.get(True, 0.05)

                self.handle_cmd(cmd)
        except KeyboardInterrupt:
            os._exit(0)
        # os._exit(0)

    def handle_cmd(self, cmd: CMD):
        if cmd.cmd == CMD.SEARCH:
            if cmd.cmd != self.current_cmd.cmd:
                self.main.status_disconnected()
                print("Scanning devices ", end='', flush=True)

            print(".", end='', flush=True)
            ports = []
            if sys.platform.startswith('win'):
                ports = self.list_com()
            elif sys.platform.startswith("linux") or sys.platform.startswith('cygwin'):
                ports = self.list_dev(self.pattern)
            elif sys.platform.startswith("darwin"):
                ports = self.list_dev("cu.SLAB")

            self.main.update_ports(ports)
            time.sleep(2)

        elif cmd.cmd == CMD.CONNECT:
            self.port = cmd.data
            if self.connect():
                self.main.status_connected()
                self.put(CMD(CMD.LOOP, None))
            else:
                self.main.status_disconnected()
                self.put(CMD(CMD.SEARCH, None))
            time.sleep(0.1)

        elif cmd.cmd == CMD.LOOP:
            self.read_comms()
            time.sleep(0.05)

        self.current_cmd = cmd

    def put(self, cmd: CMD):
        self.queue.put(cmd)

    def connect(self):
        try:
            self.serial = serial.Serial(self.port, 115200, timeout=0.1)
            print(bcol.OKGREEN + ("\n===== CONNECTED TO %s =====" % self.port) + bcol.ENDC)
            self.prev_lines = ""
            return True
        except serial.SerialException as e:
            print(traceback.format_exc())
            return False

    def read_comms(self):
        try:
            line = self.serial.readline()
        except serial.serialutil.SerialException as e:
            print(bcol.FAIL + "E: Device Disconnected!" + bcol.ENDC)
            self.serial.close()
            self.put(CMD(CMD.SEARCH, None))
            return

        try:
            line = line.decode()
        except:
            line = ""

        if line != "":
            # print(line)
            self.prev_lines += line

        if "</CSI>" in self.prev_lines:
            # print(".", end="", flush=True)
            # print(self.prev_lines)
            # print(theta.toc())
            data = self.process_lines_for_csi(self.prev_lines)
            for d in data:
                self.main.put_data(d["amp"], d["ang"], d["re"], d["im"])

            self.prev_lines = ""

    @staticmethod
    def process_lines_for_csi(lines: str):
        ret = []
        try:
            csis = lines.split("<CSI>")
            valid_csi = [line for line in csis if "</CSI>" in line and "</len>" in line]
            for csi in valid_csi:
                count = int(csi.split("<len>")[1].split("</len>")[0])
                data = [x for x in csi.split("</len>")[1].split("</CSI>")[0].split()]
                data = ESPSerial.clean_data(data)
                if count == len(data):
                    csi_data = ESPSerial.process_csi(data)
                    if csi_data is not None:
                        amp, ang, re, im = csi_data
                        ret.append({
                            "amp": amp,
                            "ang": ang,
                            "re": re,
                            "im": im
                        })
                    else:
                        print("Packet discarded")

        except Exception as e:
            print(bcol.FAIL + lines + bcol.ENDC)
            print(bcol.FAIL + "Process Lines for CSI:" + str(e) + bcol.ENDC)
            print("Packet discarded")

        return ret

    @staticmethod
    def clean_data(str_data: list):
        skip = []
        ret = []
        for i in range(len(str_data)):
            s = str_data[i]     # type: str

            if i in skip:
                pass
            try:
                x = int(s)
                ret.append(x)
            except Exception as e:
                print(s)
                if '--' in s:
                    x = int('-' + s.replace("--", ""))
                    ret.append(x)
                elif s == '-':
                    s += str_data[i+1]
                    x = int(s)
                    skip.append(i+1)
                    ret.append(x)
                elif '-' in s:
                    xs = s.split("-")
                    if xs[0] == "" and len(xs) == 3:
                        x0 = int('-' + xs[1])
                        x1 = int('-' + xs[2])
                        print(x0, x1)
                        ret.append(x0)
                        ret.append(x1)
                    elif len(xs) == 2:
                        x0 = int(xs[0])
                        x1 = int('-' + xs[1])
                        print(x0, x1)
                        ret.append(x0)
                        ret.append(x1)
                else:
                    print(bcol.FAIL + "clean_data: " + str(e) + bcol.ENDC)
                    pass
        return ret

    @staticmethod
    def process_csi(data: list):
        list_im = []
        list_real = []
        try:
            for i in range(len(data)):
                if i % 2 == 0:
                    list_im.append(data[i])
                else:
                    list_real.append(data[i])
        except Exception as e:
            print(bcol.WARNING + str(data) + bcol.ENDC)
            print(bcol.WARNING + str(e) + bcol.ENDC)

        list_amp = []
        list_ang = []
        try:
            for i in range(int(len(data) / 2)):
                list_amp.append(sqrt(list_im[i] ** 2 + list_real[i] ** 2))
                list_ang.append(atan2(list_im[i], list_real[i]))
        except Exception as e:
            print(e)
            return None
        return list_amp, list_ang, list_real, list_im

    @staticmethod
    def list_com():
        ports = ['COM%d' % (i + 1) for i in range(256)]
        ret = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                ret.append(port)
            except (OSError, serial.SerialException):
                pass
        return ret

    @staticmethod
    def list_dev(pattern):
        ret = []
        devs = os.listdir("/dev")
        for dev in devs:
            if dev.startswith(pattern):
                ret.append("/dev/" + dev)
        return ret