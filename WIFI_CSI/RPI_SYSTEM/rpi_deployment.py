import os
import tkinter as tk
import traceback
import uuid

from esp_serial import CMD, ESPSerial


"""
Implement ML Model on Pi -> Load and Run
Average Predictions on N Predictions and only publish changes
"""


class SerialObj(tk.Frame):

    STEP_ARR = ["|", "\\", "--", "/"]

    def __init__(self, master, **kwargs):
        super(SerialObj, self).__init__(master, **kwargs)
        self.master = master
        self.uid = str(uuid.uuid4())

        self.serial = ESPSerial(self, "ttyUSB")

        prefix_label = tk.Label(self, text="File Prefix: ")
        prefix_label.grid(row=0, column=0, sticky="E")

        self.prefix_input = tk.Entry(self, text="")
        self.prefix_input.grid(row=0, column=1, sticky="W")

        self.dev_var = tk.StringVar(self)
        self.dev_var.set("Select PORT")
        self.dev_dropdown = tk.OptionMenu(self, self.dev_var, "Select PORT")
        self.dev_dropdown.grid(row=0, column=2, sticky="E")

        self.connect_btn = tk.Button(self, text="Connect to PORT", command=self.connect_to_port)
        self.connect_btn.grid(row=0, column=3, sticky="W")

        self.dev_status = tk.Label(self, text="disconnected")
        self.dev_status.grid(row=0, column=4, sticky="W")

        self.step = 0
        self.data_step = tk.Label(self, text="%s" % self.STEP_ARR[self.step], width=5)
        self.data_step.grid(row=0, column=5, sticky="W")

    def status_connected(self):
        self.connect_btn.config(state=tk.NORMAL)
        self.dev_status.config(text="CONNECTED", fg="Green")

    def status_disconnected(self):
        self.dev_status.config(text="disconnected", fg="Red")
        self.dev_var.set("Select PORT")
        self.connect_btn.config(state=tk.NORMAL)

    def connect_to_port(self):
        self.dev_status.config(text="connecting...", fg="Grey")
        self.serial.put(CMD(CMD.CONNECT, self.dev_var.get()))
        self.connect_btn.config(state=tk.DISABLED)

    def update_step(self):
        self.step = (self.step + 1) % len(self.STEP_ARR)
        self.data_step.config(text="%s" % self.STEP_ARR[self.step])

    def update_ports(self, dev_list):
        menu = self.dev_dropdown['menu']
        menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.dev_var.set(name))
        else:
            self.dev_var.set("Select PORT")

    def put_data(self, amp, ang, re, im):
        self.update_step()


class Vis(tk.Frame):
    def __init__(self, parent, **kwargs):
        tk.Frame.__init__(self, **kwargs)

        self.pack(side=tk.LEFT, fill=tk.BOTH, expand=True, padx=10, pady=10)
        self.parent = parent
        self.serials = []

        folder_label = tk.Label(self, text="Folder:")
        folder_label.grid(row=0, column=0, sticky="E")

        self.folder_input = tk.Entry(self, text="csi_data")
        self.folder_input.grid(row=0, column=1, sticky="WE", columnspan=2)
        self.folder_input.insert(0, "csi_data")

        self.add_serial_btn = tk.Button(self, text="Add Serial Port", command=self.create_serial)
        self.add_serial_btn.grid(row=1, column=2, sticky="WE")
        self.remove_serial_btn = tk.Button(self, text="Remove Last Port", command=self.remove_serial)
        self.remove_serial_btn.grid(row=1, column=1, sticky="WE")

        self.create_serial()

    def remove_serial(self):
        serial = self.serials[-1]   # type: SerialObj
        self.serials.remove(serial)
        serial.serial.stop()
        serial.destroy()

    def create_serial(self):
        serial = SerialObj(self)
        serial.grid(row=2 + len(self.serials), column=1, columnspan=6, sticky="W")
        serial.serial.start()
        self.serials.append(serial)


if __name__ == "__main__":
    try:
        root = tk.Tk()
        vis = Vis(root)
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)