import parse
import json
import os
import random
import numpy as np


class DataPrep:
    ROOM_PATTERN = "{}{:d}LOG_{:d}_{:d}"

    TRAIN = "TRAIN"
    VALID = "VALID"
    TEST = "TEST"

    def __init__(self, folder, pattern, filters, output_prefix):
        self.path = folder
        self.pattern = parse.compile(pattern)
        self.filters = filters
        self.output_prefix = output_prefix

        self.lowest_count = 1000000
        self.data = {}

        self.portioned = {}

    def export_data(self):
        for i in self.portioned:
            for section in self.portioned[i]:
                rows = []
                for j in range(len(self.portioned[i][section]['x'])):
                    amp_arr = [str(x) for x in self.portioned[i][section]['x'][j]]
                    amp_str = ','.join(amp_arr)
                    row_str = amp_str + ',' + self.portioned[i][section]['y'][j] + '\n'
                    # print(row_str)
                    rows.append(row_str)

                filename = "%sROOM_%s00%s.csv" % (self.output_prefix, section, i)
                with open("%s/%s" % (self.path, filename), 'w+') as f:
                    f.writelines(rows)
                    f.close()

    def portion_data(self):
        self.portioned = {}
        train_count = int(self.lowest_count * 0.5)
        validation_count = int(self.lowest_count * 0.25)
        test_count = int(self.lowest_count * 0.25)

        for i in self.data:
            self.portioned[i] = {
                self.TRAIN: {
                    "x": [],
                    "y": []
                },
                self.VALID: {
                    "x": [],
                    "y": []
                },
                self.TEST: {
                    "x": [],
                    "y": []
                }
            }

            # training = {
            #     'x': [],
            #     'y': []
            # }
            # validation = {
            #     'x': [],
            #     'y': []
            # }
            # testing = {
            #     'x': [],
            #     'y': []
            # }

            for label in self.data[i]:
                train = {
                    'x': [],
                    'y': []
                }
                valid = {
                    'x': [],
                    'y': []
                }
                test = {
                    'x': [],
                    'y': []
                }
                for e in self.data[i][label]:
                    if len(train['x']) < train_count:
                        train['x'].append(e)
                        train['y'].append(label)
                    elif len(valid['x']) < validation_count:
                        valid['x'].append(e)
                        valid['y'].append(label)
                    elif len(test['x']) < test_count:
                        test['x'].append(e)
                        test['y'].append(label)

                self.portioned[i][self.TRAIN]['x'].extend(train['x'])
                self.portioned[i][self.TRAIN]['y'].extend(train['y'])
                self.portioned[i][self.VALID]['x'].extend(valid['x'])
                self.portioned[i][self.VALID]['y'].extend(valid['y'])
                self.portioned[i][self.TEST]['x'].extend(test['x'])
                self.portioned[i][self.TEST]['y'].extend(test['y'])

                print(label)
                print(train_count, validation_count, test_count)
                print(len(train['x']), len(valid['x']), len(test['x']))
            print(i)
            print(len(self.portioned[i][self.TRAIN]['x']), len(self.portioned[i][self.VALID]['x']), len(self.portioned[i][self.TEST]['x']))

        # print(self.portioned)

    def make_relative(self, relative_label):
        for i in self.data:
            ref_data = self.data[i][relative_label]
            pots = []

            for x in ref_data[0]:
                pots.append([x])

            for row in ref_data[1:]:
                for j in range(len(row)):
                    pots[j].append(row[j])

            ref_pot = []
            for pot in pots:
                avg_pot = sum(pot) / len(pot)
                ref_pot.append(avg_pot)

            r_ref = np.array(ref_pot)

            for label in self.data[i]:
                for j in range(len(self.data[i][label])):
                    row = self.data[i][label][j]
                    r1 = np.array(row)
                    dr = r1 - r_ref         # type: np.ndarray
                    self.data[i][label][j] = dr.tolist()

    def compile_data(self):
        self.data = {}
        files = self.list_files(self.path, self.filters)
        self.lowest_count = 1000000
        for file in files:
            names = self.pattern.search(file)
            num = str(names[1])
            label = names[0]

            if num not in self.data:
                self.data[num] = {}

            if label not in self.data[num]:
                self.data[num][label] = []

            data = self.read_data(self.path, file)
            for element in data:
                amp = element["amp"][:64]

                self.data[num][label].append(amp)

            random.shuffle(self.data[num][label])

            if len(data) < self.lowest_count:
                self.lowest_count = len(data)

            print(label, num, len(data))

    @staticmethod
    def list_files(path, filters):
        dirs = os.listdir(path)
        ret = []
        for f in dirs:
            bools = [x in f for x in filters]
            if all(bools):
                ret.append(f)
                print("appending: %s" % f)
        return ret

    @staticmethod
    def read_data(path, filename):
        f = open("%s/%s" % (path, filename), "r")

        data = f.read()
        f.close()

        return json.loads(data)


if __name__ == "__main__":
    d = DataPrep(os.getcwd() + '/csi_data3', "{}{:d}_LOG_{:d}_{:d}.json", ['LOG', 'ROOM', '.json'], "REL_")
    d.compile_data()
    d.make_relative("ROOM_EMPTY")
    d.portion_data()
    d.export_data()
