import queue
import sys
import time
# import numpy as np
from math import sqrt, atan2
import serial
import threading
import os
import matplotlib.pyplot as plt

from displayer import Data


class bcol:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class CMD:
    SEARCH = 100
    CONNECT = 110
    LOOP = 120

    def __init__(self, cmd, data):
        self.cmd = cmd
        self.data = data


class CSISerial(threading.Thread):

    def __init__(self, main):
        super(CSISerial, self).__init__()
        self.main = main
        self.alive = threading.Event()
        self.alive.set()

        self.port = None
        self.serial = None
        self.prev_lines = []

        self.amps = []
        self.angs = []

        self.queue = queue.Queue()
        self.current_cmd = CMD(None, None)

        self.queue.put(CMD(CMD.SEARCH, None))

    def handle_cmd(self, cmd: CMD):
        if cmd.cmd == CMD.SEARCH:
            if cmd.cmd != self.current_cmd.cmd:
                print(sys.platform)
                self.main.status_disconnected()
                print("Scanning devices ", end='', flush=True)

            print(".", end='', flush=True)
            self.main.update_ports(self.list_dev("cu.SLAB"))
            time.sleep(2)

        elif cmd.cmd == CMD.CONNECT:
            self.port = cmd.data
            if self.connect():
                self.main.status_connected()
                self.put(CMD(CMD.LOOP, None))
            else:
                self.main.status_disconnected()
                self.put(CMD(CMD.SEARCH, None))
            time.sleep(0.1)

        elif cmd.cmd == CMD.LOOP:
            self.read_comms()
            time.sleep(0.005)

        self.current_cmd = cmd

    def stop(self):
        self.alive.clear()

    def run(self):
        # self.wait_for_dev()
        try:
            while self.alive.is_set():
                cmd = self.current_cmd
                if not self.queue.empty():
                    cmd = self.queue.get(True, 0.005)

                self.handle_cmd(cmd)
        except KeyboardInterrupt:
            os._exit(0)
        # os._exit(0)

    def put(self, cmd: CMD):
        self.queue.put(cmd)

    def connect(self):
        try:
            self.serial = serial.Serial(self.port, 115200, timeout=0.1)
            print(bcol.OKGREEN + ("\n===== CONNECTED TO %s =====" % self.port) + bcol.ENDC)
            self.prev_lines = ""
            return True
        except serial.SerialException:
            return False

    def read_comms(self):
        try:
            line = self.serial.readline()
        except serial.serialutil.SerialException as e:
            print(bcol.FAIL + "E: Device Disconnected!" + bcol.ENDC)
            self.serial.close()
            self.put(CMD(CMD.SEARCH, None))
            return

        try:
            line = line.decode()
        except:
            line = ""

        if line != "":
            # print(line)
            self.prev_lines += line

        if "</CSI>" in self.prev_lines:
            # print(".", end="", flush=True)
            # print(self.prev_lines)
            # print(theta.toc())
            data = self.process_lines_for_csi(self.prev_lines)
            if data is not None:
                amp, ang, re, im = data
                self.main.put_data(amp, ang, re, im)

            self.prev_lines = ""

    # def start_serial_comms(self):
    #     self.connect()
    #     try:
    #         while True:
    #             # theta.tic()
    #             self.read_comms()
    #
    #             time.sleep(0.05)
    #     except KeyboardInterrupt:
    #         os._exit(0)
    #     os._exit(0)

    def process_lines_for_csi(self, lines: str):
        ind = lines.find("<CSI>")
        line = lines[ind:]

        try:
            data = (line.split("</len>")[1].split("</CSI>")[0]).split()
        except Exception as e:
            print(e)
            return None
        # print(data)

        list_im = []
        list_real = []
        try:
            for i in range(len(data)):
                if i % 2 == 0:
                    list_im.append(int(data[i]))
                else:
                    list_real.append(int(data[i]))
        except Exception as e:
            print(e)

        list_amp = []
        list_ang = []
        try:
            for i in range(int(len(data) / 2)):
                list_amp.append(sqrt(list_im[i] ** 2 + list_real[i] ** 2))
                list_ang.append(atan2(list_im[i], list_real[i]))
        except Exception as e:
            print(e)
            return None
        return list_amp, list_ang, list_real, list_im

    # def search_devices(self):
    #     print(sys.platform)
    #     print("Searching for devices ", end='', flush=True)
    #     try:
    #         while self.port is None:
    #             print(".", end='', flush=True)
    #             self.main.update_ports(self.list_dev("cu.SLAB"))
    #             time.sleep(3)
    #     except KeyboardInterrupt:
    #         os._exit(0)

    # def wait_for_dev(self):
    #     print(sys.platform)
    #     print("Waiting for device ", end='', flush=True)
    #     try:
    #         while True:
    #             print(".", end='', flush=True)
    #             ret = self.check_dev()
    #             if ret is not None:
    #                 self.port = ret
    #                 break
    #             time.sleep(3)
    #     except KeyboardInterrupt:
    #         os._exit(0)

    @staticmethod
    def list_dev(pattern):
        ret = []
        devs = os.listdir("/dev")
        for dev in devs:
            if dev.startswith(pattern):
                ret.append("/dev/" + dev)
        return ret

    @staticmethod
    def check_dev():
        ret = None
        devs = os.listdir("/dev")
        for dev in devs:
            if dev.startswith("cu.SLAB"):
                ret = "/dev/" + dev
                print("\nFound: " + bcol.OKBLUE + ret + bcol.ENDC)
                break
        return ret



