import os
import queue
import threading
import time
import tkinter as tk
import traceback
import uuid

from math import sqrt, atan2

from displayer import DISP_CMD, Display, Data
from esp_serial_csi import ESPSerial, CMD, bcol


class CSIHandler(threading.Thread):
    def __init__(self, csi_cb):
        super(CSIHandler, self).__init__()
        self.alive = threading.Event()
        self.alive.set()

        self.cb = csi_cb

        self.queue = queue.Queue()

    def put(self, addr, data):
        self.queue.put({
            "addr": addr,
            "data": data
        })

    def run(self):
        try:
            while self.alive.is_set():
                try:
                    """
                    data = {
                        "addr": ""
                        "data": []
                    }
                    """

                    data = self.queue.get(True, 0.001)
                    amp, ang, real, im = self.process_csi(data["data"])
                    csi = {
                        'amp': amp,
                        'ang': ang,
                        'real': real,
                        'im': im,
                        "x": 0,
                        "y": 0,
                        "addr": data["addr"]
                    }
                    self.cb(csi)
                except queue.Empty:
                    """"""
                # print(".")
                time.sleep(0.001)
        except KeyboardInterrupt:
            self.alive.clear()

    @staticmethod
    def process_csi(data: list):
        list_im = []
        list_real = []
        try:
            for i in range(len(data)):
                if i % 2 == 0:
                    list_im.append(data[i])
                else:
                    list_real.append(data[i])
        except Exception as e:
            print(bcol.WARNING + str(data) + bcol.ENDC)
            print(bcol.WARNING + str(e) + bcol.ENDC)

        list_amp = []
        list_ang = []
        try:
            for i in range(int(len(data) / 2)):
                list_amp.append(sqrt(list_im[i] ** 2 + list_real[i] ** 2))
                list_ang.append(atan2(list_im[i], list_real[i]))
        except Exception as e:
            print(e)
            return None
        return list_amp[:192], list_ang[:192], list_real[:192], list_im[:192]


""" 188 + 168 """


class SerialObj(tk.Frame):
    def __init__(self, master, i, displayer, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.disp = displayer
        self.i = i
        self.uid = str(uuid.uuid4())
        self.color = "Green"

        self.serial = ESPSerial(main=self, callback=self.process_active_csi)

        self.csi_handler = CSIHandler(self.csi_callback)
        self.csi_handler.start()

        prefix_label = tk.Label(self, text="File Name: ")
        prefix_label.grid(row=0, column=0, sticky="E")

        self.prefix_input = tk.Entry(self, text="")
        self.prefix_input.grid(row=0, column=1, sticky="W")

        self.dev_var = tk.StringVar(self)
        self.dev_var.set("Select PORT")
        self.dev_dropdown = tk.OptionMenu(self, self.dev_var, "Select PORT")
        self.dev_dropdown.grid(row=0, column=2, sticky="E")

        self.connect_btn = tk.Button(self, text="Connect to PORT", command=self.connect_to_port)
        self.connect_btn.grid(row=0, column=3, sticky="W")

        self.dev_status = tk.Label(self, text="disconnected")
        self.dev_status.grid(row=0, column=4, sticky="W")

        self.record_btn = tk.Button(self, text="Record", command=self.record)
        self.record_btn.grid(row=0, column=5, sticky="W")
        self.record_btn.config(state=tk.DISABLED)

        self.x = tk.StringVar()
        self.y = tk.StringVar()

        self.x_input = tk.Entry(self, text="0", textvariable=self.x)
        self.x_input.grid(row=0, column=6, sticky="WE")

        self.y_input = tk.Entry(self, text="0", textvariable=self.y)
        self.y_input.grid(row=0, column=7, sticky="WE")

        # self.line_color = tk.Label(self, text="---")
        # self.line_color.grid(row=0, column=5, sticky="W")

        self.recording = False
        self.record_buffer = []

        self.addrs = []

    def write_to_file(self):
        """"""
        f = open("%s.csv" % self.prefix_input.get(), "a+")
        f.writelines(self.record_buffer)
        f.close()
        self.record_buffer = []

    def record(self):
        """"""
        if not self.recording:
            self.record_btn.config(text="Stop Record")
            self.record_buffer = []
            self.recording = True
        else:
            self.record_btn.config(text="Record")
            self.recording = False
            self.write_to_file()

    def csi_callback(self, csi):
        """"""
        # print(csi["amp"])
        self.disp.put(Data(csi["addr"], [x for x in range(192)], csi["amp"][:192]))
        if self.recording:
            row = "\"%s\", %s, %s, \"amp\", %s, \"ang\", %s, \"re\", %s, \"im\", %s\n" \
                  % (csi["addr"], self.x_input.get(), self.y_input.get(), ", ".join([str(x) for x in csi["amp"]]), ", ".join([str(x) for x in csi["ang"]]), ", ".join([str(x) for x in csi["real"]]), ", ".join([str(x) for x in csi["im"]]))
            # print(row)
            self.record_buffer.append(row)
            print(len(self.record_buffer))

    def process_active_csi(self, lines: str):
        """"""
        try:
            lines = lines.split("</CSI>")
            for line in lines:
                if "<CSI>" in line:
                    line = line.split("<CSI>")[1]
                    address = line.split("<address>")[1].split("</address>")[0]
                    d_len = int(line.split("<len>")[1].split("</len>")[0])
                    if d_len == 384:
                        data = [int(x) for x in line.split("</len>")[1].split()]

                        if address not in self.addrs:
                            print(address)
                            cmd = DISP_CMD(Display.CMD_SERIAL_REGISTER, {
                                "uid": address,
                                "cb": None
                            })
                            self.disp.put_cmd(cmd)
                            self.addrs.append(address)

                        self.csi_handler.put(address, data)
                    # print(address, len(data), d_len)

        except Exception as e:
            print(bcol.WARNING + str(e) + bcol.ENDC)

    def set_prefix_text(self, txt):
        self.prefix_input.delete(0, tk.END)
        self.prefix_input.insert(0, txt)

    def set_line_color(self, color):
        self.color = color
        # self.line_color.config(fg=self.color)

    def status_connected(self):
        # cmd = DISP_CMD(Display.CMD_SERIAL_REGISTER, {
        #     "uid": self.uid,
        #     "cb": self.set_line_color
        # })
        # self.disp.put_cmd(cmd)
        self.record_btn.config(state=tk.NORMAL)
        self.connect_btn.config(state=tk.NORMAL)
        self.dev_status.config(text="CONNECTED", fg="Green")

    def status_disconnected(self):
        self.dev_status.config(text="disconnected", fg="Red")
        self.dev_var.set("Select PORT")
        self.connect_btn.config(state=tk.NORMAL)

    def connect_to_port(self):
        self.dev_status.config(text="connecting...", fg="Grey")
        self.serial.put(CMD(CMD.CONNECT, self.dev_var.get()))
        self.connect_btn.config(state=tk.DISABLED)

    def update_ports(self, dev_list):
        menu = self.dev_dropdown['menu']
        menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.dev_var.set(name))
        else:
            self.dev_var.set("Select PORT")


class Vis(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, **kwargs)
        self.pack(side=tk.LEFT, fill=tk.BOTH, expand=True, padx=10, pady=10)
        self.parent = parent
        self.parent.title("ACTIVE CSI Vis")

        self.disp = Display(self, Display.MODE_AMP)
        self.disp.grid(row=0, column=0, columnspan=4)

        self.serials = []
        self.create_serial(self.disp)

    def remove_serial(self):
        serial = self.serials[-1]   # type: SerialObj
        self.serials.remove(serial)
        serial.serial.stop()
        serial.destroy()

    def create_serial(self, displayer):
        serial = SerialObj(self, len(self.serials), displayer)
        serial.grid(row=1 + len(self.serials), column=3, columnspan=6, sticky="W")
        serial.serial.start()
        self.serials.append(serial)


if __name__ == "__main__":
    try:
        root = tk.Tk()
        vis = Vis(root)
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)