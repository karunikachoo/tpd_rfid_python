import math
import statistics
import time
from matplotlib import animation
from pprint import pprint

from mpl_toolkits import mplot3d
import numpy as np
from matplotlib import pyplot as plt

DF_DATA = [12.706, 4.303, 3.182, 2.776, 2.571,
           2.447, 2.365, 2.306, 2.262, 2.228,
           2.201, 2.179, 2.16, 2.145, 2.131,
           2.12, 2.11, 2.101, 2.093, 2.086,
           2.08, 2.074, 2.069, 2.064, 2.06,
           2.056, 2.052, 2.048, 2.045, 1.96]

CENTRE_CARRIER_INDEX = 32
CHANNEL = 0     # CHANNEL 1
CHANNEL_CENTRE_FREQS = [2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484]     # MHz
C = 299792458   # m/s
SUBCARRIER_INDEX = [x for x in range(-64, 0)]
# SUBCARRIER_INDEX = [x for x in range(-32, 32)]
# VALID_RANGE = [5, -4]     # -5 gives invalid data
VALID_RANGE = [6, -5]   # -58 to -6

OFFSETS = {
    "24:00:00:00:00:01": [0, 0],
    "24:01:01:01:01:01": [3.56, 0]
}


class DataProcessing:
    def __init__(self, file_path):
        self.file_path = file_path

        f = open(file_path, 'r')
        self.raw_data = f.readlines()

        self.data = {}
        self.data_by_dist = {}

        self.dist_ani = None

    def plot_phase_offsets(self):
        for key in [list(self.data.keys())[1]]:
            section = self.data[key]
            print("\n\n", key, ":", len(section))

            indices = SUBCARRIER_INDEX[VALID_RANGE[0]:VALID_RANGE[1]]

            dist_section = self.data_by_dist[key]
            dist_keys = list(dist_section.keys())
            for i in range(len(dist_keys)):

                fig = plt.figure(figsize=(10, 5), dpi=80)
                ax = fig.add_subplot(131, projection='polar')
                ax2 = fig.add_subplot(133)
                ax3 = fig.add_subplot(132)
                ax.set_ylim(0, 7)

                dist = dist_section[dist_keys[i]]

                for ii in range(len(dist)):
                    sample = dist[ii]
                    angs = sample['ang'][VALID_RANGE[0]:VALID_RANGE[1]]
                    rs = [sample['r']] * len(angs)

                    sc, = ax.plot(angs, rs, 'bo', alpha=0.01)
                    ax2.plot(indices, angs)
                    ax3.plot(indices, sample['raw_ang'][VALID_RANGE[0]:VALID_RANGE[1]])

                ax.set_title("%s -> %s @ %f" % (key, dist_keys[i], dist[0]['r']))

                plt.show(block=True)

    def animate_phase_offsets(self):
        for key in self.data:
            section = self.data[key]
            print("\n\n", key, ":", len(section))

            dist_section = self.data_by_dist[key]
            dist_keys = list(dist_section.keys())
            for i in range(len(dist_keys)):

                fig = plt.figure()
                ax = fig.add_subplot(111, projection='polar')
                ax.set_ylim(0, 7)

                sc, = ax.plot([], [], '.', alpha=0.75)

                dist = dist_section[dist_keys[i]]

                ax.set_title("%s -> %s" % (key, dist_keys[i]))

                def animate(ii):
                    sample = dist[ii]
                    angs = sample['ang'][VALID_RANGE[0]:VALID_RANGE[1]]
                    rs = [sample['r']] * len(angs)
                    sc.set_data(angs, rs)
                    return sc,

                print(len(dist))
                self.dist_ani = animation.FuncAnimation(fig, animate, frames=len(dist), interval=500, blit=True, repeat=False)

                plt.show(block=True)

    def process_data(self):
        # 600 605 606
        # for key in self.data:
        for key in ["24:00:00:00:00:01"]:
            section = self.data[key]
            print("\n\n", key, ":", len(section))

            fig = plt.figure()

            indices = SUBCARRIER_INDEX[VALID_RANGE[0]:VALID_RANGE[1]]

            # for i in range(len(section)):
            # for i in range(600, 610):
            #     sample = section[i]
            #     amps = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]]
            #     angs = sample['ang'][VALID_RANGE[0]:VALID_RANGE[1]]
            #
            #     new_angs = self.calibrate_ang(angs, indices)
            #
            #     plt.plot(indices, angs, '.')
            #     plt.plot(indices, new_angs, '-')
            #     plt.title("%s->%d" % (key, i))

            dist_section = self.data_by_dist[key]
            dist_keys = list(dist_section.keys())

            for i in range(len(dist_keys)):
                fig = plt.figure()
                dist = dist_section[dist_keys[i]]
                for ii in range(len(dist)):
                    sample = dist[ii]
                    amps = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]]
                    angs = sample['ang'][VALID_RANGE[0]:VALID_RANGE[1]]

                    new_angs, diff_angs = self.calibrate_ang(angs, indices)

                    plt.plot(indices, angs, '*', dashes=[1, 1], alpha=0.25)
                    # plt.plot(indices, new_angs, '-')
                    plt.title("%s -> %d : %5.2f : %5.2f -> %5.2f" % (key, i,  sample['x'], sample['y'], sample['r']))

                if (i % 10) == 9:
                    plt.show()

    def check_particular_dataset(self, i):
        indices = SUBCARRIER_INDEX[VALID_RANGE[0]:VALID_RANGE[1]]
        for key in self.data_by_dist:
            dist_section = self.data_by_dist[key]
            dist_keys = list(dist_section.keys())
            fig = plt.figure()
            dist = dist_section[dist_keys[i]]

            for ii in range(len(dist)):
                sample = dist[ii]
                amps = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]]
                angs = sample['ang'][VALID_RANGE[0]:VALID_RANGE[1]]

                new_angs, diff_angs = self.calibrate_ang(angs, indices)

                diffs = [angs[iii] - angs[iii - 1] for iii in range(1, len(angs))]
                print(ii)
                pprint(diffs)

                plt.plot(indices, angs, '*', dashes=[1, 1], alpha=0.25)
                plt.plot(indices, new_angs, '-')
                plt.title("%s -> %d : %5.2f : %5.2f -> %5.2f" % (key, ii, sample['x'], sample['y'], sample['r']))
                # if ii % 10 == 9:
                #     plt.show()
                #     fig = plt.figure()
            plt.show()

    def plot_amplitudes(self):
        indices = SUBCARRIER_INDEX[VALID_RANGE[0]:VALID_RANGE[1]]
        for key in self.data_by_dist:
            device = self.data_by_dist[key]

            for d_key in device:
                dists = device[d_key]

                fig = plt.figure()
                ax = fig.gca(projection='3d')

                amps = []

                for sample in dists:
                    amp = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]]
                    amps.append(amp)

                X, Y = np.meshgrid(range(len(indices)), range(len(dists)))
                Z = np.array(amps)

                cmap = plt.get_cmap("viridis")

                ax.plot_surface(X, Y, Z, cmap=cmap, linewidth=0, antialiased=False)
                ax.set_title("%s  %s" % (key, d_key))

                plt.show()

    def plot_subcarrier(self):
        indices = SUBCARRIER_INDEX[VALID_RANGE[0]:VALID_RANGE[1]]
        for key in self.data:
            dev = self.data[key]
            print(key)

            for i in range(VALID_RANGE[0], 64 + VALID_RANGE[1]):
                fig = plt.figure()
                ax = fig.gca()

                x = []
                y = []
                for sample in dev:
                    x.append(sample['r'])
                    y.append(sample['amp'][i])

                ax.plot(x, y, '.')
                ax.set_title("Subcarrier %d for %s" % (SUBCARRIER_INDEX[i], key))
                plt.xlabel('Distance (m)')
                plt.ylabel('Amplitude')

                plt.savefig("figs/%s_sub_carrier_%d_amp_dist.png" % (key.replace(":", ""), SUBCARRIER_INDEX[i]))
                plt.show()

    def naive_multipath(self):
        indices = SUBCARRIER_INDEX[VALID_RANGE[0]:VALID_RANGE[1]]
        # for key in sorted(self.data.keys()):
        for key in self.data:
            dev = self.data[key]
            print(key)

            for sample in dev:
                r = sample['r']
                amps = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]]
                angs = sample['ang'][VALID_RANGE[0]:VALID_RANGE[1]]

                phase_shifts = [self.compute_phase_shift(ind, CHANNEL_CENTRE_FREQS[0], r) for ind in indices]
                mod_shifts = [phase_shift % (2 * math.pi) for phase_shift in phase_shifts]
                mod_shifts = self.reshape_ang(mod_shifts)

                fit_shifts = [self.fitting_phase_shift(ind, r) for ind in indices]
                diff_0 = angs[0] - fit_shifts[0]
                fit_shifts = [fit + diff_0 for fit in fit_shifts]

                sin_angs = [angs[i] - (fit_shifts[i] - fit_shifts[0]) for i in range(len(angs))]

                min_a, max_a = min(sin_angs), max(sin_angs)
                print("%5d, %10.5f : %10.5f" % (dev.index(sample), max(angs)-min(angs), max_a - min_a))

                sample['shift'] = diff_0

                fig = plt.figure()
                ax = fig.gca()
                ax.plot(indices, mod_shifts)
                ax.plot(indices, angs)
                ax.plot(indices, fit_shifts)
                ax.plot(indices, sin_angs)
                ax.set_title("%s @ %.2fm " % (key, r))
                # ax.set_ylim(-math.pi, 3 * math.pi)
                plt.xlabel("subcarrier index")
                plt.ylabel("phase (rad.)")
                plt.show()

        for key in self.data:
            dev = self.data[key]
            print(key)

            dists = []
            amps = []

            x = []
            y = []
            ii = 6

            wlen = C / (CHANNEL_CENTRE_FREQS[0] * 1000000.0)
            allow_dist = 0.01      # m
            phase_allow = 2 * math.pi * allow_dist / wlen
            print(phase_allow / math.pi * 180.0)
            for sample in dev:
                # if abs(sample['shift']) < phase_allow:
                if abs(sample['shift']) < 5 / 180.0 * math.pi:
                    # amp = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]]
                    amp = sample['amp'][VALID_RANGE[0]:VALID_RANGE[1]][ii]
                    # dists.extend([sample['r']] * len(amp))
                    dists.append(sample['r'])
                    amps.append(amp)

                    # x.append(sample['r'])
                    # avg.append(sum(amp) / float(len(amp)))

            fig = plt.figure()
            ax = fig.gca()
            ax.scatter(dists, amps, alpha=0.25)
            ax.set_title('%s subcarrier %d' % (key, indices[ii]))
            ax.set_xlabel('Distance (m)')
            ax.set_ylabel('CSI Amplitude')
            # print(indices[10])

        plt.show()


    def brute_force_fitting(self, angs, indices, dist):
        fit_shifts = [self.fitting_phase_shift(ind, dist) for ind in indices]
        diff_0 = angs[0] - fit_shifts[0]
        fit_shifts = [fit + diff_0 for fit in fit_shifts]
        sin_angs = [angs[i] - fit_shifts[i] for i in range(len(angs))]


    def fitting_phase_shift(self, sub_idx, dist):
        return 2 * math.pi * dist / C * 0.3125 * sub_idx * 10 ** 6

    def compute_phase_shift(self, sub_idx, chan_freq, dist):
        sub_freq = (chan_freq + 0.3125 * sub_idx) * 10**6           # MHz -> Hz
        wavelength = C / sub_freq
        phase_shift = 2 * math.pi * dist / wavelength
        return phase_shift

    def sort_data(self):
        for row in self.raw_data:
            data = row.split(",")
            mac = data[0].replace("\"", "")
            if mac not in self.data:
                self.data[mac] = []

            offset = OFFSETS[mac]

            x = float(data[1].strip()) - offset[0]
            y = float(data[2].strip()) - offset[1]
            r = math.sqrt(x*x + y*y)

            raw_angs = [float(data[4+65+i].strip()) for i in range(64)]
            angs = self.reshape_ang(raw_angs)

            amps = [float(data[4+i].strip()) for i in range(64)]

            sample = {
                'r': r,
                'x': x,
                'y': y,
                'x_raw': float(data[1].strip()),
                'y_raw': float(data[2].strip()),
                'amp': amps,
                'ang': angs,
                'raw_ang': raw_angs
            }
            self.data[mac].append(sample)

            if mac not in self.data_by_dist:
                self.data_by_dist[mac] = {}
            key = "%.2f:%.2f" % (sample['x_raw'], sample['y_raw'])
            if key not in self.data_by_dist[mac]:
                self.data_by_dist[mac][key] = [sample]
            else:
                self.data_by_dist[mac][key].append(sample)

    @staticmethod
    def calibrate_ang(angs, indices):
        zeta = (angs[-1] - angs[0]) / (indices[-1] - indices[0])
        b = 1 / len(angs) * sum(angs)
        calibrated_phi = [angs[i] - zeta * indices[i] - b for i in range(len(angs))]
        diff_phi = [angs[i] - calibrated_phi[i] for i in range(len(angs))]
        # print(max(diff_phi) - min(diff_phi))
        return calibrated_phi, diff_phi

    @staticmethod
    def reshape_ang(angs):
        # from +-pi to 0->2pi
        ret = []
        for i in range(len(angs)):
            if angs[i] < 0:
                ret.append(2 * math.pi + angs[i])
            else:
                ret.append(angs[i])

        # unwrap 2pi
        for i in range(1, len(ret)):
            if ret[i] - ret[i-1] > 1.5 * math.pi:
                ret[i] = ret[i] - 2 * math.pi
            elif ret[i-1] - ret[i] > 1.5 * math.pi:
                ret[i] = ret[i] + 2 * math.pi

            # if ret[i] - ret[i-1] > 0.65 * math.pi:
            #     ret[i] = ret[i] - math.pi
            # elif ret[i-1] - ret[i] > 0.65 * math.pi:
            #     ret[i] = ret[i] + math.pi
        return ret


if __name__ == "__main__":
    d = DataProcessing("ap0_0_sta356_0cm.csv")
    d.sort_data()
    # d.plot_amplitudes()
    # d.plot_subcarrier()
    d.naive_multipath()
    # d.process_data()
    # d.plot_phase_offsets()
    # d.animate_phase_offsets()
    # d.reshape_phi()
    # d.plot_data()
    # d.check_particular_dataset(8)
    # d.process_data()
    # d.offset_by_null_carrier()
