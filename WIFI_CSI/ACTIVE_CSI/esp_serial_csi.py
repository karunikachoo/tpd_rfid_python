import os
import queue
import sys
import threading
import time
import traceback
from math import sqrt, atan2

import serial


class bcol:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class CMD:
    SEARCH = 100
    CONNECT = 110
    LOOP = 120

    def __init__(self, cmd, data):
        self.cmd = cmd
        self.data = data


class ESPSerial(threading.Thread):
    def __init__(self, main, callback, pattern="ttyUSB"):
        super(ESPSerial, self).__init__()
        self.main = main
        self.callback = callback
        self.pattern = pattern
        self.alive = threading.Event()
        self.alive.set()

        self.port = None
        self.serial = None
        self.prev_lines = []

        self.amps = []
        self.angs = []

        self.queue = queue.Queue()
        self.current_cmd = CMD(None, None)

        self.queue.put(CMD(CMD.SEARCH, None))

    def stop(self):
        self.alive.clear()

    def run(self):
        # self.wait_for_dev()
        try:
            while self.alive.is_set():
                cmd = self.current_cmd
                if not self.queue.empty():
                    cmd = self.queue.get(True, 0.0005)

                self.handle_cmd(cmd)
        except KeyboardInterrupt:
            os._exit(0)
        # os._exit(0)

    def handle_cmd(self, cmd: CMD):
        if cmd.cmd == CMD.SEARCH:
            if cmd.cmd != self.current_cmd.cmd:
                self.main.status_disconnected()
                print("Scanning devices ", end='', flush=True)

            print(".", end='', flush=True)
            ports = []
            if sys.platform.startswith('win'):
                ports = self.list_com()
            elif sys.platform.startswith("linux") or sys.platform.startswith('cygwin'):
                ports = self.list_dev(self.pattern)
            elif sys.platform.startswith("darwin"):
                ports = self.list_dev("cu.SLAB")

            self.main.update_ports(ports)
            time.sleep(2)

        elif cmd.cmd == CMD.CONNECT:
            self.port = cmd.data
            if self.connect():
                self.main.status_connected()
                self.put(CMD(CMD.LOOP, None))
            else:
                self.main.status_disconnected()
                self.put(CMD(CMD.SEARCH, None))
            time.sleep(0.1)

        elif cmd.cmd == CMD.LOOP:
            self.read_comms()
            time.sleep(0.0005)

        self.current_cmd = cmd

    def put(self, cmd: CMD):
        self.queue.put(cmd)

    def connect(self):
        try:
            self.serial = serial.Serial(self.port, 115200, timeout=0.1)
            print(bcol.OKGREEN + ("\n===== CONNECTED TO %s =====" % self.port) + bcol.ENDC)
            self.prev_lines = ""
            return True
        except serial.SerialException as e:
            print(traceback.format_exc())
            return False

    def read_comms(self):
        try:
            line = self.serial.readline()
        except serial.serialutil.SerialException as e:
            print(bcol.FAIL + "E: Device Disconnected!" + bcol.ENDC)
            self.serial.close()
            self.put(CMD(CMD.SEARCH, None))
            return

        try:
            line = line.decode()
        except:
            line = ""

        if line != "":
            self.prev_lines += line

        if "<CSI>" in self.prev_lines and "</CSI>" in self.prev_lines:
            self.callback(self.prev_lines)

            self.prev_lines = ""

    @staticmethod
    def process_csi(data: list):
        list_im = []
        list_real = []
        try:
            for i in range(len(data)):
                if i % 2 == 0:
                    list_im.append(data[i])
                else:
                    list_real.append(data[i])
        except Exception as e:
            print(bcol.WARNING + str(data) + bcol.ENDC)
            print(bcol.WARNING + str(e) + bcol.ENDC)

        list_amp = []
        list_ang = []
        try:
            for i in range(int(len(data) / 2)):
                list_amp.append(sqrt(list_im[i] ** 2 + list_real[i] ** 2))
                list_ang.append(atan2(list_im[i], list_real[i]))
        except Exception as e:
            print(e)
            return None
        return list_amp, list_ang, list_real, list_im

    @staticmethod
    def list_com():
        ports = ['COM%d' % (i + 1) for i in range(256)]
        ret = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                ret.append(port)
            except (OSError, serial.SerialException):
                pass
        return ret

    @staticmethod
    def list_dev(pattern):
        ret = []
        devs = os.listdir("/dev")
        for dev in devs:
            if dev.startswith(pattern):
                ret.append("/dev/" + dev)
        return ret