import json
import os
import parse
import pandas as pd


class DataHandler:
    ROOM_PATTERN = "{}{:d}LOG_{:d}_{:d}"

    def __init__(self, fixes, path, pattern, limit=-1, data_split=1.0):
        # self.fixes = ["LOG", "ROOM"]
        # self.path = os.getcwd() + "/csi_data"
        # self.pattern = parse.compile(self.ROOM_PATTERN)

        self.limit = limit
        self.fixes = fixes
        self.path = path
        self.data_split = data_split
        self.pattern = parse.compile(pattern)

        self.data = {}
        self.train = {}

    def export_simple(self, filename):
        dataset, test = self.get_datasets()

        for key in dataset:
            data = dataset[key]

            rows = []
            for i in range(len(data["Y"])):
                line = ','.join([str(x) for x in data["X"][i]]) + ',' + data["Y"][i] + "\n"
                rows.append(line)
            name = "%s%s.csv" % (filename, key)
            with open(name, "w") as f:
                f.writelines(rows)
                f.close()

        for key in test:
            data = test[key]

            rows = []
            for i in range(len(data["Y"])):
                line = ','.join([str(x) for x in data["X"][i]]) + ',' + data["Y"][i] + "\n"
                rows.append(line)
            name = "%s%s.TEST.csv" % (filename, key)
            with open(name, "w") as f:
                f.writelines(rows)
                f.close()

    def get_datasets(self):
        files = self.list_files(self.path, self.fixes)
        print(files)
        self.parse_labels(files)

        for filename in files:
            data = self.read_data(self.path, filename)
            self.parse_data(filename, data)

        return self.data, self.train

    def parse_data(self, filename, data):
        r = self.pattern.parse(filename)
        num = str(r[1])
        label = r[0]
        # counts = [len(e["amp"]) for e in data]
        #
        # cc = {}
        # for c in counts:
        #     if c not in cc:
        #         cc[c] = 1
        #     else:
        #         cc[c] += 1
        # max_cols = 0
        # col_count = 0
        # for c in cc:
        #     if cc[c] > col_count:
        #         cv = cc[c]
        #         max_cols = c
        #
        # print(filename, max_cols)

        train_count = int(len(data) * self.data_split)
        test_count = len(data) - train_count

        print(filename, len(data), train_count, test_count)

        for e in data:
            if self.limit > 0:
                amp = [x for x in e["amp"][:self.limit]]
            else:
                amp = e["amp"]

            if train_count >= 0:
                self.data[num]["X"].append(amp)
                self.data[num]["Y"].append(label)
                train_count -= 1
            else:
                self.train[num]["X"].append(amp)
                self.train[num]["Y"].append(label)

    def parse_labels(self, files):
        type_num = []
        for filename in files:
            r = self.pattern.parse(filename)
            if str(r[1]) not in type_num:
                type_num.append(str(r[1]))

        for num in type_num:
            if num not in self.data:
                self.data[num] = {
                    "X": [],
                    "Y": []
                }
            if num not in self.train:
                self.train[num] = {
                    "X": [],
                    "Y": []
                }

    @staticmethod
    def list_files(path, fixes):
        dirs = os.listdir(path)
        ret = []
        for f in dirs:
            if ".json" in f:
                bools = [x in f for x in fixes]
                if all(bools):
                    ret.append(f.split(".json")[0])
                    print("appending: %s" % f)
        return ret

    @staticmethod
    def read_data(path, filename):
        f = open("%s/%s.json" % (path, filename), "r")

        data = f.read()
        f.close()

        return json.loads(data)


if __name__ == "__main__":
    d = DataHandler(["LOG", "ROOM"], os.getcwd() + "/csi_data2", "{}{:d}_LOG_{:d}_{:d}", limit=64, data_split=0.8)
    # d.get_datasets()
    d.export_simple("csi_data2/ROOM_TEST_DATA")

