import os
import queue
import time
import tkinter as tk
import traceback
import uuid

from RPI_SYSTEM.esp_serial import ESPSerial, CMD, bcol
from displayer import Display, Data, DISP_CMD
from location_vis import LocationChooser
from recorder import Recorder
from vis_ml import VisML


class SerialObj(tk.Frame):
    def __init__(self, master, i, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.i = i
        self.uid = str(uuid.uuid4())
        self.color = "Green"

        self.serial = ESPSerial(self)

        prefix_label = tk.Label(self, text="%03d Prefix: " % (self.i + 1))
        prefix_label.grid(row=0, column=0, sticky="E")

        self.prefix_input = tk.Entry(self, text="")
        self.prefix_input.grid(row=0, column=1, sticky="W")

        self.dev_var = tk.StringVar(self)
        self.dev_var.set("Select PORT")
        self.dev_dropdown = tk.OptionMenu(self, self.dev_var, "Select PORT")
        self.dev_dropdown.grid(row=0, column=2, sticky="E")

        self.connect_btn = tk.Button(self, text="Connect to PORT", command=self.connect_to_port)
        self.connect_btn.grid(row=0, column=3, sticky="W")

        self.dev_status = tk.Label(self, text="disconnected")
        self.dev_status.grid(row=0, column=4, sticky="W")

        self.line_color = tk.Label(self, text="---")
        self.line_color.grid(row=0, column=5, sticky="W")

    def set_prefix_text(self, txt):
        self.prefix_input.delete(0, tk.END)
        self.prefix_input.insert(0, txt)

    def set_line_color(self, color):
        self.color = color
        self.line_color.config(fg=self.color)

    def status_connected(self):
        cmd = DISP_CMD(Display.CMD_SERIAL_REGISTER, {
            "uid": self.uid,
            "cb": self.set_line_color
        })
        self.master.amp_disp.put_cmd(cmd)
        self.master.ang_disp.put_cmd(cmd)
        self.connect_btn.config(state=tk.NORMAL)
        self.dev_status.config(text="CONNECTED", fg="Green")

    def status_disconnected(self):
        self.dev_status.config(text="disconnected", fg="Red")
        self.dev_var.set("Select PORT")
        self.connect_btn.config(state=tk.NORMAL)

    def connect_to_port(self):
        self.dev_status.config(text="connecting...", fg="Grey")
        self.serial.put(CMD(CMD.CONNECT, self.dev_var.get()))
        self.connect_btn.config(state=tk.DISABLED)

    def update_ports(self, dev_list):
        menu = self.dev_dropdown['menu']
        menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.dev_var.set(name))
        else:
            self.dev_var.set("Select PORT")

    def put_data(self, amp, ang, re, im):
        # d_amp = Data(self.uid, [x for x in range(64)], amp[:64])
        d_amp = Data(self.uid, [x for x in range(128)], amp[:128])
        # d_amp = Data(self.uid, [x for x in range(len(amp))], amp)

        d_ang = Data(self.uid, [x for x in range(128)], ang[:128])
        # d_ang = Data(self.uid, re[:128], im[:128])

        data = {
            "amp": amp,
            "ang": ang,
            "re": re,
            "im": im,
            "x": 0,
            "y": 0,
            "time": time.time_ns(),
            "uid": self.uid,
            "prefix": self.prefix_input.get()
        }

        ml_amp = amp[:64]
        ml_amp[0] = 0

        if self.master.vis_ml is not None:
            self.master.vis_ml.put(self.uid, ml_amp)
        self.master.put_data(d_amp, d_ang, data)


class Vis(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, **kwargs)
        self.pack(side=tk.LEFT, fill=tk.BOTH, expand=True, padx=10, pady=10)
        self.parent = parent
        self.recorder = Recorder()
        self.parent.title("Vis")

        self.serials = []

        self.count = {}
        self.count_max = 0
        self.record = False
        self.pos_x = 0
        self.pos_y = 0
        self.t_start = 0
        self.serial_buffer = {}

        self.press_recorder = None
        self.vis_ml = None
        self.location = None

        self.amp_disp = Display(self, Display.MODE_AMP)
        self.amp_disp.grid(row=0, column=0, columnspan=4)

        self.ang_disp = Display(self, Display.MODE_ANG)
        self.ang_disp.grid(row=0, column=4, columnspan=4)

        self.launch_frame = tk.Frame(self)
        self.launch_frame.grid(row=0, column=8)
        self.recorder_btn = tk.Button(self.launch_frame, text="Show Recorder", command=self.show_rec)
        self.ml_btn = tk.Button(self.launch_frame, text="Show Predictor", command=self.show_ml)
        self.loc_btn = tk.Button(self.launch_frame, text="Show Locations", command=self.show_loc)

        self.recorder_btn.grid(row=0, column=0, sticky="we")
        self.ml_btn.grid(row=1, column=0, sticky="we")
        self.loc_btn.grid(row=2, column=0, sticky="we")

        self.create_serial()
        self.create_serial()

        folder_label = tk.Label(self, text="Folder:")
        folder_label.grid(row=1, column=0, sticky="E")

        self.folder_input = tk.Entry(self, text="csi_data")
        self.folder_input.grid(row=1, column=1, sticky="WE", columnspan=2)
        self.folder_input.insert(0, "csi_data")

        self.add_serial_btn = tk.Button(self, text="Add Serial Port", command=self.create_serial)
        self.add_serial_btn.grid(row=2, column=2, sticky="WE")
        self.remove_serial_btn = tk.Button(self, text="Remove Last Port", command=self.remove_serial)
        self.remove_serial_btn.grid(row=2, column=1, sticky="WE")

        self.recorder.start()
        # self.serial.start()

    def show_rec(self):
        self.press_recorder = PressRecorder(self, self.parent)

    def show_ml(self):
        self.vis_ml = VisML(self, self.parent)

    def show_loc(self):
        self.location = LocationChooser(self, self.parent)

    def remove_serial(self):
        serial = self.serials[-1]   # type: SerialObj
        self.serials.remove(serial)
        serial.serial.stop()
        serial.destroy()

    def create_serial(self):
        serial = SerialObj(self, len(self.serials))
        serial.grid(row=1 + len(self.serials), column=3, columnspan=6, sticky="W")
        serial.serial.start()
        self.serials.append(serial)

    def put_data(self, d_amp: Data, d_ang: Data, data):
        self.amp_disp.put(d_amp)

        self.ang_disp.put(d_ang)

        data["x"] = self.pos_x
        data["y"] = self.pos_y

        if self.record:
            if self.count[d_ang.uid] < self.count_max:
                self.recorder.put(data)
                self.count[d_ang.uid] += 1

                min_count = min(self.count.values())
                self.press_recorder.update_record_count(min_count)

            maxed = [x >= self.count_max for x in self.count.values()]

            if all(maxed):
                self.stop_record()

    def start_record(self, x, y, n):
        if not self.record:
            self.record = True
            self.count = {}

            for s in self.serials:
                self.count[s.uid] = 0

            self.t_start = time.time()
            self.pos_x = x
            self.pos_y = y
            self.count_max = n

    def stop_record(self):
        print("Data collection over %f mins" % ((time.time() - self.t_start) / 60))
        self.record = False
        self.recorder.folder = self.folder_input.get() + "/"
        self.recorder.put("save")
        self.press_recorder.canvas.config(background="#ffffff")


class PressRecorder(tk.Toplevel):
    def __init__(self, main: Vis, master):
        tk.Toplevel.__init__(self, master)
        self.main = main

        # self.frame = tk.Frame(self)
        self.title('Press Recorder')
        self.geometry('500x500')

        self.canvas = tk.Canvas(self)
        self.canvas.pack(fill=tk.BOTH, expand=1)

        self.counter = self.canvas.create_text([250, 250], text="%d" % 0, font=("Arial", 100), fill="#888888")

        self.x = tk.Entry(self)
        self.x.pack(side=tk.LEFT)

        self.y = tk.Entry(self)
        self.y.pack(side=tk.LEFT)

        self.n = tk.Entry(self)
        self.n.pack(side=tk.LEFT)

        self.x.insert(0, "0")
        self.y.insert(0, "0")
        self.n.insert(0, "10")

        self.canvas.bind('<Button-1>', self.handle_mouse)
        self.canvas.bind('<ButtonRelease-1>', self.handle_mouse)

    def update_record_count(self, count):
        self.canvas.itemconfig(self.counter, text="%d" % count)
        self.canvas.coords(self.counter, [self.canvas.winfo_width()/2, self.canvas.winfo_height()/2])

    def handle_mouse(self, evt):
        if evt.type == tk.EventType.ButtonPress and not self.main.record:
            try:
                x = int(self.x.get())
                y = int(self.y.get())
                n = int(self.n.get())
                self.main.start_record(x, y, n)

                self.canvas.config(background="#333333")
            except Exception:
                print(bcol.FAIL + "Invalid values x, y, or z" +  bcol.ENDC)
                pass
            # self.main.start_record()
        # elif evt.type == tk.EventType.ButtonRelease:
        #     self.canvas.config(background="#ffffff")
        #     self.main.stop_record()


if __name__ == "__main__":
    try:
        root = tk.Tk()
        vis = Vis(root)
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)