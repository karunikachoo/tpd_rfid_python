import tkinter as tk
import traceback
import uuid
import queue

import numpy as np

from ml_classifier import PositionerML


class ML_CMD:
    CMD_PREDICT = 100

    def __init__(self, cmd, data):
        self.cmd = cmd
        self.data = data


class Chooser(tk.Frame):
    def __init__(self, main, i, **kwargs):
        tk.Frame.__init__(self, main, **kwargs)
        self.main = main
        self.i = i

        label = tk.Label(self, text="STA%03d" % (i + 1))
        label.grid(row=0, column=0, sticky="E")

        self.sel_var = tk.StringVar(self)
        self.sel_var.set("Select PORT")
        self.dropdown = tk.OptionMenu(self, self.sel_var, "Select PORT", command=self.port_selected)
        self.dropdown.grid(row=0, column=1, sticky="WE")

    def port_selected(self, value):
        self.sel_var.set(value)
        self.main.port_selected(self.i, value)

    def update_ports(self, dev_list):
        menu = self.dropdown['menu']
        menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.port_selected(name))
        else:
            self.sel_var.set("Select PORT")


class VisML(tk.Toplevel):
    def __init__(self, main, master):
        tk.Toplevel.__init__(self, master)
        self.main = main

        self.title("Machine Learning Predictor")

        self.conv = False

        self.p = PositionerML("room_model_rbfsvm", svm=True)
        self.p.load_model()

        self.queue = queue.Queue()

        self.serials = {}
        self.serialmap = {}
        self.data_buffer = []

        self.input_buffer = []
        self.predict_buffer = []

        self.data_db = {}

        self.choosers = []
        self.create_btn = tk.Button(self, text="Add Datastream", command=self.create_serial_chooser)
        self.create_btn.grid(row=1, column=0, sticky="we")

        self.create_serial_chooser()
        self.create_serial_chooser()

        self.canvas = tk.Canvas(self, width=1000, height=200)
        self.canvas.grid(row=0, column=0, sticky="we")

        self.label = self.canvas.create_text([500, 100], text="%s" % "???", font=("Arial", 100), fill="#888888")

        self.after(50, self.loop)
        self.after(200, self.update_serials)
        self.after(200, self.average_predictions)

    def port_selected(self, i, value):
        for uid in self.serials:
            if self.serials[uid] == value:
                self.serialmap[uid] = i

    def put(self, uid, data):
        if uid in self.serialmap:
            self.data_buffer[self.serialmap[uid]] = data

        if 0 not in self.data_buffer:
            self.parse_data(self.data_buffer)
            self.data_buffer = [0] * len(self.choosers)

    def parse_data(self, buff):
        data = []
        for d in buff:
            data.extend(d)
        data = np.array(data)
        data = data.reshape(1, -1)
        # data.reshape(shape=(1, len(data)))
        if self.conv:
            data = self.p.reshape_conv(data)

        self.queue.put(ML_CMD(ML_CMD.CMD_PREDICT, data))

    def loop(self):
        """"""
        if not self.queue.empty():
            cmd = self.queue.get(True, timeout=0.005)
            if type(cmd) == ML_CMD:
                self.handle_cmd(cmd)

        self.after(50, self.loop)

    def handle_cmd(self, cmd):
        if cmd.cmd == ML_CMD.CMD_PREDICT:
            self.predict(cmd.data)

    def update_serials(self):
        dev_list = []
        for serial in self.main.serials:
            if serial.serial.port is not None:
                self.serials[serial.uid] = serial.serial.port
                dev_list.append(serial.serial.port)

        for chooser in self.choosers:
            chooser.update_ports(dev_list)

        self.after(200, self.update_serials)

    def create_serial_chooser(self):
        chooser = Chooser(self, len(self.choosers))
        chooser.grid(row=2 + chooser.i, column=0, columnspan=2, sticky='we')
        self.choosers.append(chooser)
        self.data_buffer = [0] * len(self.choosers)

    def predict(self, data):
        uid = uuid.uuid4()
        try:
            pred = self.p.predict(data)
            self.predict_buffer.append((uid, pred, data))

            diff = len(self.predict_buffer) - 20
            if diff > 0:
                for i in range(diff):
                    self.predict_buffer.pop(0)

            print(uid, pred)
        except Exception as e:
            print(e)

    def average_predictions(self):
        if len(self.predict_buffer) > 10:
            counts = {}
            for tup in self.predict_buffer[-10:]:   # 10 obj window ~~ past 2.5s
                uid, pred, data = tup
                if pred[0] not in counts:
                    counts[pred[0]] = 1
                else:
                    counts[pred[0]] += 1

            count_max = 0
            label = "???"
            for pred in counts:
                if counts[pred] > count_max:
                    count_max = counts[pred]
                    label = pred

            self.canvas.itemconfig(self.label, text=label)
        self.after(100, self.average_predictions)


