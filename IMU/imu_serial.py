import os
import queue
import sys
import threading
import time
import traceback
from math import sqrt, atan2

import serial


FAST_SLEEP = 0.0001

class bcol:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class CMD:
    SEARCH = 100
    CONNECT = 110
    LOOP = 120

    def __init__(self, cmd, data):
        self.cmd = cmd
        self.data = data


class IMUSerial(threading.Thread):
    def __init__(self, main, line_callback):
        super(IMUSerial, self).__init__()
        self.main = main
        self.line_callback = line_callback

        self.alive = threading.Event()
        self.alive.set()

        self.port = None
        self.serial = None

        self.queue = queue.Queue()
        self.current_cmd = CMD(None, None)

        self.queue.put(CMD(CMD.SEARCH, None))

    def stop(self):
        self.alive.clear()

    def run(self):
        # self.wait_for_dev()
        try:
            while self.alive.is_set():
                cmd = self.current_cmd
                if not self.queue.empty():
                    cmd = self.queue.get(True, FAST_SLEEP)

                self.handle_cmd(cmd)
        except KeyboardInterrupt:
            os._exit(0)
        # os._exit(0)

    def handle_cmd(self, cmd: CMD):
        if cmd.cmd == CMD.SEARCH:
            if cmd.cmd != self.current_cmd.cmd:
                self.main.status_disconnected()
                print("Scanning devices ", end='', flush=True)

            print(".", end='', flush=True)
            ports = []
            if sys.platform.startswith('win'):
                ports = self.list_com()
            elif sys.platform.startswith("linux") or sys.platform.startswith('cygwin'):
                ports = self.list_dev("ttyUSB")
            elif sys.platform.startswith("darwin"):
                ports = self.list_dev("cu.usbmodem")

            self.main.update_ports(ports)
            time.sleep(1)

        elif cmd.cmd == CMD.CONNECT:
            self.port = cmd.data
            if self.connect():
                self.main.status_connected()
                self.put(CMD(CMD.LOOP, None))
            else:
                self.main.status_disconnected()
                self.put(CMD(CMD.SEARCH, None))
            time.sleep(0.1)

        elif cmd.cmd == CMD.LOOP:
            self.read_comms()
            time.sleep(FAST_SLEEP)

        self.current_cmd = cmd

    def put(self, cmd: CMD):
        self.queue.put(cmd)

    def connect(self):
        try:
            self.serial = serial.Serial(self.port, 115200, timeout=0.1)
            print(bcol.OKGREEN + ("\n===== CONNECTED TO %s =====" % self.port) + bcol.ENDC)
            return True
        except serial.SerialException as e:
            print(traceback.format_exc())
            return False

    def read_comms(self):
        try:
            line = self.serial.readline()
        except serial.serialutil.SerialException as e:
            print(bcol.FAIL + "E: Device Disconnected!" + bcol.ENDC)
            self.serial.close()
            self.put(CMD(CMD.SEARCH, None))
            return

        try:
            line = line.decode()
        except:
            line = ""

        if line != "":
            self.line_callback(line)

    @staticmethod
    def list_com():
        ports = ['COM%d' % (i + 1) for i in range(256)]
        ret = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                ret.append(port)
            except (OSError, serial.SerialException):
                pass
        return ret

    @staticmethod
    def list_dev(pattern):
        ret = []
        devs = os.listdir("/dev")
        for dev in devs:
            if dev.startswith(pattern):
                ret.append("/dev/" + dev)
        return ret