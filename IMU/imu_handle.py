import os
from scipy import integrate
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
import numpy as np
import math
import transformations as tf

"""
2020-01-04:
DOWN->DOWN->UP->UP->STRAIGHT->STRAIGHT->STRAIGHT->JUMP

BACK:
- 4, 5 : DOWN
- 6, 7 : UP 
- 8, 9, 10 : STRAIGHT
- 11 : JUMP

HEAD:
- 3, 4 : DOWN
- 5, 6 : UP
- 7, 8, 9 : STRAIGHT
- 10 :  JUMP

RIGHT_FOOT:
- 2, 3 : DOWN
- 4, 5 : UP
- 6 : STRAIGHT
- 11: JUMP
"""


def normalize(v, tolerance=0.00001):
    print(v)
    mag2 = sum(n * n for n in v)
    mag = 1
    if abs(mag2 - 1.0) > tolerance:
        mag = math.sqrt(mag2)
        if mag > tolerance:
            v = tuple(n / mag for n in v)
    return v, mag


def q_mult(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
    z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
    return w, x, y, z


def q_conjugate(q):
    w, x, y, z = q
    return w, -x, -y, -z


def qv_mult(q1, v1):
    q2 = (0.0,) + v1
    return q_mult(q_mult(q1, q2), q_conjugate(q1))[1:]


def q_diff(q0, q):
    # q0 * diff -> q
    return q_mult(q, (q_conjugate(q0)))


def trapz(x, y, initial_val=0):
    ret = [initial_val]
    for i in range(1, len(x)):
        r = 0.5 * (x[i] - x[i-1]) * (y[i] + y[i-1])
        ret.append(r)
    return ret


class RunningVariance:
    def __init__(self, x0):
        self.M = x0
        self.S = 0
        self.k = 1

    def push(self, x):
        self.k += 1

        Mk = self.M + (x - self.M) / self.k
        Sk = self.S + (x - self.M) * (x - Mk)

        self.M = Mk
        self.S = Sk

    def variance(self):
        return self.S / (self.k - 1)


class Data:
    def __init__(self):
        self.ms = 0
        self.a = [0.0, 0.0, 0.0]
        self.dyn_a = [0.0, 0.0, 0.0]
        self.q = [0.0, 0.0, 0.0, 0.0]       # [w, x, y, z]
        self.euler = [0.0, 0.0, 0.0]
        self.heading = 0.0

    def load_from_floats(self, float_array):
        self.ms = float_array[0]
        self.a = float_array[1:4]
        self.dyn_a = float_array[4:7]
        self.q = float_array[7:11]
        self.euler = float_array[11:14]
        self.heading = float_array[-1]


class IMUHandler:
    def __init__(self, path):
        self.path = path

        self.data = self.read_data(path)
        self.ts = None
        self.ax = None
        self.ay = None
        self.az = None
        self.q = None

    @staticmethod
    def read_data(path):
        f = open(path)
        lines = f.readlines()
        f.close()

        ret = []
        for line in lines:
            data_str_array = line.split(",")
            data_float_array = [float(col.strip()) for col in data_str_array]

            data = Data()
            data.load_from_floats(data_float_array)

            ret.append(data)

        return ret

    @staticmethod
    def calculate_running_variance(data):
        """
        This can be used when trying to measure covariances for the Extended Kalman Filter.
        Run this on the error between measured and real data to get the variance of the error.
        We can use that to create an estimate of the covariance.
        """
        rv = RunningVariance(data[0])

        variance = [0.0]

        for i in range(1, len(data)):
            rv.push(data[i])
            variance.append(rv.variance())

        return variance

    def compile_data(self):
        ts = []
        ax = []
        ay = []
        az = []
        q = []

        for data in self.data:
            ts.append(data.ms)

            ax.append(data.dyn_a[0])
            ay.append(data.dyn_a[1])
            az.append(data.dyn_a[2])

            q.append(data.q)

        ts = [t - ts[0] for t in ts]

        self.ts = ts.copy()
        self.ax = ax.copy()
        self.ay = ay.copy()
        self.az = az.copy()
        self.q = q.copy()

    def truncate_data(self, time_ms):
        i = 0
        for t in self.ts:
            if t >= time_ms:
                break
            i += 1

        self.ts = self.ts[i:]
        self.ax = self.ax[i:]
        self.ay = self.ay[i:]
        self.az = self.az[i:]
        self.q = self.q[i:]

    def integrate_accel(self, ts, ax, ay, az):
        vx = [0, *integrate.cumtrapz(ax, ts)]
        vy = [0, *integrate.cumtrapz(ay, ts)]
        vz = [0, *integrate.cumtrapz(az, ts)]

        dx = [0, *integrate.cumtrapz(vx, ts)]
        dy = [0, *integrate.cumtrapz(vy, ts)]
        dz = [0, *integrate.cumtrapz(vz, ts)]
        print(len(dx), len(vx), len(ax))

        plt.figure()
        s0 = plt.subplot(311)
        s1 = plt.subplot(312)
        s2 = plt.subplot(313)

        s0.plot(ts, ax)
        s1.plot(ts, ay)
        s2.plot(ts, az)

        plt.figure()
        s0 = plt.subplot(311)
        s1 = plt.subplot(312)
        s2 = plt.subplot(313)

        s0.plot(ts, vx)
        s1.plot(ts, vy)
        s2.plot(ts, vz)

        plt.figure()
        s0 = plt.subplot(311)
        s1 = plt.subplot(312)
        s2 = plt.subplot(313)

        s0.plot(ts, dx)
        s1.plot(ts, dy)
        s2.plot(ts, dz)

        plt.show()

    def test_quaternion_transform(self):
        q_array = [
            [1, 0, 0, 0],
            # [0.998, 0.044, 0.044, 0.002],         # 5, 5, 0
            # [0.992, 0.087, 0.087, 0.008],         # 10, 10, 0
            # [0.983, 0.129, 0.129, 0.017],         # etc...
            # [0.970, 0.171, 0.171, 0.030],
            # [0.953, 0.211, 0.211, 0.047],
            # [0.933, 0.250, 0.250, 0.067],
            # [0.910, 0.287, 0.287, 0.090],
            # [0.883, 0.321, 0.321, 0.117],
            [0.854, 0.354, 0.354, 0.146],         # 45, 45, 0

            # [0.821, 0.383, 0.383, 0.179],
            # [0.787, 0.410, 0.410, 0.213],

            # [0.985, 0, 0, 0.174]
        ]

        q0 = tuple(q_array[0])
        q = tuple(q_array[-1])
        ref_cs = self.axis_global_to_local(q0)
        rot_cs = self.axis_global_to_local(q)

        v = 0.5, 0.5, 0
        d = 1, 0, 1
        v2 = qv_mult(q, v)

        g0 = self.project_to_global((0, 0, 0), q, d)
        vg = self.project_to_global(v, q, d)
        vg2 = qv_mult(q, v)
        # vg = qv_mult(q_conjugate(q), tuple(vg))
        print(np.linalg.norm(vg))

        print(vg)
        print(vg2)

        fig = plt.figure()
        ax = p3.Axes3D(fig)

        ax.set_xlim(-1.1, 1.1)
        ax.set_ylim(-1.1, 1.1)
        ax.set_zlim(-1.1, 1.1)

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        self.plot_cs(ax, (0, 0, 0), ref_cs)
        self.plot_cs(ax, d, rot_cs + np.array(d), alpha=0.5)
        ax.plot([d[0], v2[0] + d[0]], [d[1], v2[1] + d[1]], [d[2], v2[2] + d[2]], 'c', alpha=0.5)
        ax.plot([0, vg[0]], [0, vg[1]], [0, vg[2]], color='violet')

        plt.show()

    @staticmethod
    def axis_global_to_local(q):
        cs = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
        q = tuple(q)
        c2 = []

        for axis in range(3):
            v = tuple(cs[axis])
            x, y, z = qv_mult(q, v)
            c2.append([x, y, z])
        return c2

    @staticmethod
    def project_to_global(v, q, d):
        # q = q_conjugate(q)
        q0 = list(q[1:])
        q0.append(q[0])

        H = tf.quaternion_matrix(q0)

        print(H)
        for i in range(3):
            H[i, 3] = d[i]

        print(H)

        v = list(v)
        v.append(1.0)
        v0 = np.dot(H, v)

        return v0[:3]
        # cs = IMUHandler.axis_global_to_local(q_conjugate(q))
        #
        # scalars = []
        # for axis in range(3):
        #     print(cs[axis])
        #     s = np.dot(v, cs[axis]) / np.linalg.norm(cs[axis])
        #     scalars.append(s)
        #
        # return tuple(scalars)

        # q_inv = q_conjugate(q)
        # vx = 1, 0, 0
        # vy = 0, 1, 0
        # vz = 0, 0, 1
        #
        # # back transform the axes to global frame
        # vx = qv_mult(q_inv, vx)
        # vy = qv_mult(q_inv, vy)
        # vz = qv_mult(q_inv, vz)
        #
        # v, mag = normalize(v)
        #
        # # project the vector onto each axis
        # x = np.dot(v, vx) / np.linalg.norm(vx)
        # y = np.dot(v, vy) / np.linalg.norm(vy)
        # z = np.dot(v, vz) / np.linalg.norm(vz)

        # return x * mag, y * mag, z * mag

    def transform_acc_to_global(self):
        ax = []
        ay = []
        az = []
        for i in range(len(self.ts)):
            v = (self.ax[i], self.ay[i], self.az[i])

            v, mag = normalize(v)

            # if i > 0:
            #     q0 = self.q[i - 1]
            #     q = self.q[i]
            #
            #     # dq = q_conjugate(q_diff(q0, q))
            #     dq = q_diff(q0, q)
            #
            #     v2 = qv_mult(dq, v)
            #     ag = tuple(n * mag for n in v2)
            #
            #     ax.append(ag[0])
            #     ay.append(ag[1])
            #     az.append(ag[2])
            # else:
                # v2 = qv_mult(q_conjugate(self.q[i]), v)
            v2 = qv_mult(self.q[i], v)
            ag = tuple(n * mag for n in v2)

            ax.append(ag[0])
            ay.append(ag[1])
            az.append(ag[2])

        return ax, ay, az

    def animate_position(self):
        a_x, a_y, a_z = self.transform_acc_to_global()

        s = [t / 1000.0 for t in self.ts]

        vx = trapz(s, [a * 9.81 for a in a_x])
        vy = trapz(s, [a * 9.81 for a in a_y])
        vz = trapz(s, [a * 9.81 for a in a_z])

        # vx = [0, *integrate.cumtrapz([a * 9.81 for a in a_x], s)]
        # vy = [0, *integrate.cumtrapz([a * 9.81 for a in a_y], s)]
        # vz = [0, *integrate.cumtrapz([a * 9.81 for a in a_z], s)]
        #
        # dx = trapz(s, vx)
        # dy = trapz(s, vy)
        # dz = trapz(s, vz)

        dx = [0, *integrate.cumtrapz(vx, s)]
        dy = [0, *integrate.cumtrapz(vy, s)]
        dz = [0, *integrate.cumtrapz(vz, s)]

        # cdx = [sum(dx[:i]) for i in range(len(dx))]
        # cdy = [sum(dy[:i]) for i in range(len(dy))]
        # cdz = [sum(dz[:i]) for i in range(len(dz))]

        fig = plt.figure()
        ax = p3.Axes3D(fig)

        ax.set_xlim(-1.1, 1.1)
        ax.set_ylim(-1.1, 1.1)
        ax.set_zlim(-1.1, 1.1)

        line, = ax.plot([0, 0], [0, 0], [0, 0], 'c')

        shadow_count = 15
        axes_array = []
        for i in range(shadow_count):
            X, = ax.plot([0, 0], [0, 0], [0, 0], 'r')
            Y, = ax.plot([0, 0], [0, 0], [0, 0], 'g')
            Z, = ax.plot([0, 0], [0, 0], [0, 0], 'b')
            axes = [X, Y, Z]
            axes_array.append(axes)

        def shadow_update_no_orientation(ii):
            # p = poses[ii]
            axs = axes_array[ii % shadow_count]
            for axis in range(3):
                xs = [dx[ii], dx[ii] + (0.1 if axis == 0 else 0)]
                ys = [dy[ii], dy[ii] + (0.1 if axis == 1 else 0)]
                zs = [dz[ii], dz[ii] + (0.1 if axis == 2 else 0)]
                axs[axis].set_data(xs, ys)
                axs[axis].set_3d_properties(zs)
            line.set_data(dx[:ii], dy[:ii])
            line.set_3d_properties(dz[:ii])
            ax.set_title("%d ms" % self.ts[ii])

        ani = animation.FuncAnimation(fig, shadow_update_no_orientation, len(dx), interval=6, repeat=False)

        plt.show()

    def animate_quaternions(self):
        cs = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
        poses = []

        for i in range(len(self.q)):
            # for i in range(100, 500):
            q = tuple(self.q[i])
            c2 = []

            for axis in range(3):
                v = tuple(cs[axis])
                x, y, z = qv_mult(q, v)
                c2.append([x, y, z])
            # print(c2)
            poses.append(c2)

        fig = plt.figure()
        ax = p3.Axes3D(fig)

        ax.set_xlim(-1.1, 1.1)
        ax.set_ylim(-1.1, 1.1)
        ax.set_zlim(-1.1, 1.1)

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        #
        # X, = ax.plot([0, 1], [0, 0], [0, 0], 'r')
        # Y, = ax.plot([0, 0], [0, 1], [0, 0], 'g')
        # Z, = ax.plot([0, 0], [0, 0], [0, 1], 'b')
        #
        # axes = [X, Y, Z]
        # colors = ['r', 'g', 'b']

        shadow_count = 2
        axes_array = []
        for i in range(shadow_count):
            X, = ax.plot([0, 0], [0, 0], [0, 0], 'r')
            Y, = ax.plot([0, 0], [0, 0], [0, 0], 'g')
            Z, = ax.plot([0, 0], [0, 0], [0, 0], 'b')
            axes = [X, Y, Z]
            axes_array.append(axes)

        # def update(ii):
        #     p = poses[ii]
        #     for axis in range(3):
        #         xs = [0, p[axis][0]]
        #         ys = [0, p[axis][1]]
        #         zs = [0, p[axis][2]]
        #         axes[axis].set_data(xs, ys)
        #         axes[axis].set_3d_properties(zs)
        #
        #     ax.set_title("%d ms" % self.ts[ii])

        def shadow_update(ii):
            p = poses[ii]
            axs = axes_array[ii % shadow_count]
            for axis in range(3):
                xs = [0, p[axis][0]]
                ys = [0, p[axis][1]]
                zs = [0, p[axis][2]]
                axs[axis].set_data(xs, ys)
                axs[axis].set_3d_properties(zs)
            print("%d ms" % self.ts[ii], end='\r')
            # ax.set_title("%d ms" % self.ts[ii])

        ani = animation.FuncAnimation(fig, shadow_update, len(poses), interval=10)

        plt.show()

    @staticmethod
    def plot_cs(ax, origin, pose, alpha=1.0):
        colors = ['red', 'green', 'blue']
        for axis in range(3):
            xs = [origin[0], pose[axis][0]]
            ys = [origin[1], pose[axis][1]]
            zs = [origin[2], pose[axis][2]]
            ax.plot(xs, ys, zs, color=colors[axis], alpha=alpha)

    def plot_acceleration(self):
        ts = []

        ax = []
        ay = []
        az = []

        dax = []
        day = []
        daz = []

        # dcax = []
        # dcay = []
        # dcaz = []
        for data in self.data:
            ts.append(data.ms)

            # q = [0.0, 0.0, 0.0, 0.0]
            # q[0] = data.q[1]
            # q[1] = data.q[2]
            # q[2] = data.q[3]
            # q[3] = data.q[0]
            #
            # a = self.compensate_gravity(data.a, q)
            # dcax.append(a[0])
            # dcay.append(a[1])
            # dcaz.append(a[2])

            ax.append(data.a[0])
            ay.append(data.a[1])
            az.append(data.a[2])

            dax.append(data.dyn_a[0])
            day.append(data.dyn_a[1])
            daz.append(data.dyn_a[2])

        ts = [(t - ts[0]) / 1000 for t in ts]

        fig = plt.figure()

        ax0 = plt.subplot(311)
        ax1 = plt.subplot(312)
        ax2 = plt.subplot(313)

        ax0.plot(ts, ax, label='Absolute')
        ax0.plot(ts, dax, label='Dynamic')
        # ax0.plot(ts, dcax)
        ax1.plot(ts, ay)
        ax1.plot(ts, day)
        # ax1.plot(ts, dcay)
        ax2.plot(ts, az)
        ax2.plot(ts, daz)
        # ax2.plot(ts, dcaz)

        # ax0.set_title(self.path)
        ax2.set_xlabel('Time (s)')
        ax0.set_ylabel('ax')
        ax1.set_ylabel('ay')
        ax2.set_ylabel('az')

        ax0.legend()

    @staticmethod
    def compensate_gravity(acc, q):
        g = [0.0, 0.0, 0.0]

        g[0] = 2 * (q[1] * q[3] - q[0] * q[2])
        g[1] = 2 * (q[0] * q[1] + q[2] * q[3])
        g[2] = q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]

        # compensate accelerometer readings with the expected direction of gravity
        return [acc[0] - g[0], acc[1] - g[1], acc[2] - g[2]]


class RollingWindow:
    def __init__(self, n_last):
        self.n_last = n_last

        self.data = []
        self.average = 0

    def push(self, point):
        self.data.append(point)

        if len(self.data) > self.n_last:
            self.data.pop(0)

        self.average = sum(self.data) / len(self.data)


G = 9.80665  # m/s^2


class NoisePlot:
    def __init__(self):
        f = open('imu_offset/imu_noise.txt')
        lines = f.readlines()
        f.close()

        ms_arr = []
        dyn_ax = []
        dyn_ay = []
        dyn_az = []

        self.rx = RollingWindow(25)
        self.ry = RollingWindow(25)
        self.rz = RollingWindow(25)

        for line in lines:
            data = [float(d) for d in line.split()]
            ms, ax, ay, az = data

            self.rx.push(ax)
            self.rx.push(ay)
            self.rx.push(az)

            ms_arr.append(ms)
            dyn_ax.append(self.rx.average * G)
            dyn_ay.append(self.ry.average * G)
            dyn_az.append(self.rz.average * G)

        ts = [(t - ms_arr[0]) / 1000.0 for t in ms_arr]

        fig = plt.figure()
        sub1 = fig.add_subplot(311)
        sub2 = fig.add_subplot(312)
        sub3 = fig.add_subplot(313)

        sub1.plot(ts, dyn_ax, 'r', label='ax')
        sub2.plot(ts, dyn_ay, 'g', label='ay')
        sub3.plot(ts, dyn_az, 'b', label='az')

        sub2.set_ylabel('Acceleration (m/s^2)')

        sub3.set_xlabel('Time (s)')
        sub1.legend()
        sub2.legend()
        sub3.legend()
        plt.show()


if __name__ == '__main__':
    # NoisePlot()
    h = IMUHandler('data/RIGHT_FOOT_UP_STAIRS01.TXT')
    h.plot_acceleration()
    plt.show()
    # h = IMUHandler('data/LOG17.TXT')
    # h.compile_data()
    # h.test_quaternion_transform()
    # # h.truncate_data(7100)
    # h.animate_position()
    # h.animate_quaternions()
    # h.integrate_accel(h.ts, h.ax, h.ay, h.az)



