"""
CODE IS BASED ON WORK FROM MADGWICK GAIT ANALYSIS
"""
import math
from pprint import pprint

from file_reader import FileReader
import numpy as np
from scipy import signal

from quaternion import Quaternion
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3


class IMU:
    def __init__(self):
        self.ms = []
        self.acc = []
        self.gyro = []
        self.mag = []
        self.q = []
        self.euler = []
        self.heading = []

    def get_entry(self, i):
        return self.ms[i], self.acc[i], self.gyro[i], self.mag[i], self.q[i], self.euler[i], self.heading[i]

    def add_entry(self, ms, acc, gyro, mag, q, euler, heading):
        self.ms.append(ms)
        self.acc.append(acc)
        self.gyro.append(gyro)
        self.mag.append(mag)
        self.q.append(q)
        self.euler.append(euler)
        self.heading.append(heading)

    def pop_entry(self, index):
        self.ms.pop(index)
        self.acc.pop(index)
        self.gyro.pop(index)
        self.mag.pop(index)
        self.q.pop(index)
        self.euler.pop(index)
        self.heading.pop(index)

    def get_quaternion_columns(self):
        cols = ()
        for i in range(len(self.q)):
            q = self.q[i]
            for j in range(4):
                if len(cols) == j:
                    cols += ([], )
                cols[j].append(q[j])
        return cols

    def get_columns(self, data_list):
        cols = ()
        for i in range(len(data_list)):
            row = data_list[i]
            for j in range(len(row)):
                if len(cols) == j:
                    cols += ([],)
                cols[j].append(row[j])
        return cols


ACC_LABELS = ['ax', 'ay', 'az']
VEL_LABELS = ['vx', 'vy', 'vz']
DIS_LABELS = ['sx', 'sy', 'sz']
COLORS = ['r', 'g', 'b', 'orange']


class PostProcess:
    def __init__(self, file_path):
        self.file_path = file_path
        self.imu = IMU()

        self.load_data()
        self.process_data()

    def load_data(self):
        data = FileReader(self.file_path).read()
        for d in data:
            # ms, ax, ay, az, dax, day, daz, gx, gy, gz, mx, my, mz, qw, qx, qy, qz, ex, ey, ez, h = d
            ms, ax, ay, az, gx, gy, gz, mx, my, mz, qw, qx, qy, qz, ex, ey, ez, h = d
            self.imu.add_entry(ms, [ax, ay, az], [gx, gy, gz], [mx, my, mz], Quaternion(qw, qx, qy, qz), [ex, ey, ez], h)

    def process_data(self):
        self.remove_calibration_period(0)

        # local_acc = self.rolling_window(self.imu.acc, 25, 25)
        # imu = self.reproduce_range_from_roll(local_acc)

        imu = self.imu

        fig = plt.figure()
        acc = imu.get_columns(imu.acc)
        # acc2 = imu.get_columns(self.imu.acc)
        plt.title("Local Acc")
        for i in range(3):
            s = plt.subplot('31' + str(i+1))
            s.plot(imu.ms, acc[i])
            # s.plot(self.imu.ms, acc2[i])

        # plt.show()

        global_acc = self.global_transform(imu.acc, imu.q)

        # fig = plt.figure()
        # acc = imu.get_columns(global_acc)
        # plt.title("Global Acc")
        # for i in range(3):
        #     s = plt.subplot('31' + str(i + 1))
        #     s.plot(imu.ms, acc[i])

        dyn_acc = self.g_removal(global_acc, 1.0229357798165137)

        fig = plt.figure("Global Dynamic Acc")
        acc = imu.get_columns(dyn_acc)
        for i in range(3):
            s = plt.subplot('31' + str(i + 1))
            s.set_ylabel(ACC_LABELS[i])
            s.plot(imu.ms, acc[i], COLORS[i])

        # plt.show()

        q_label = ["qw", "qx", "qy", "qz"]
        q_s = imu.get_quaternion_columns()
        fig = plt.figure("Quaternions")
        for i in range(len(q_s)):
            s = plt.subplot(str(len(q_s)) + '1' + str(i+1))
            s.set_ylabel(q_label[i])
            s.plot(imu.ms, q_s[i], COLORS[i])

        stationary, mags = self.stationary_points(dyn_acc, 0.053516819571865444)

        fig = plt.figure()
        ax = fig.gca()
        acc = imu.get_columns(dyn_acc)
        ax.plot(imu.ms, mags)
        ax.plot(imu.ms, stationary, alpha=0.5)

        dyn_acc_ms2 = self.g_to_ms2(dyn_acc, 9.81)

        vel = self.integrate_acc(imu.ms, dyn_acc_ms2, stationary)

        plt.figure('Velocity')
        vel_disp = imu.get_columns(vel)
        for i in range(3):
            s = plt.subplot('31' + str(i + 1))
            s.set_ylabel(VEL_LABELS[i])
            s.plot(imu.ms, vel_disp[i], COLORS[i])
        # plt.show()

        start, end = self.find_non_stationary_periods(stationary)

        print(start)
        print(end)

        vel_drift = self.calc_vel_drift(vel, start, end)

        plt.figure('Velocity Drift Correction')
        vel_disp = imu.get_columns(vel_drift)
        for i in range(3):
            s = plt.subplot('31' + str(i + 1))
            s.set_ylabel(VEL_LABELS[i] + ' Drift')
            s.plot(imu.ms, vel_disp[i], COLORS[i])
        # plt.show()

        vel_drift_corrected = self.adjust_velocity_drift(vel, vel_drift)
        # vel_drift_corrected = vel

        plt.figure("Drift Corrected Velocity")
        vel_disp = imu.get_columns(vel_drift_corrected)
        for i in range(3):
            s = plt.subplot('31' + str(i + 1))
            s.set_ylabel(VEL_LABELS[i])
            s.plot(imu.ms, vel_disp[i], COLORS[i])
        # plt.show()

        pos = self.integrate_vel(imu.ms, vel_drift_corrected)

        fig = plt.figure("Displacement")
        disp = imu.get_columns(pos)
        for i in range(3):
            s = plt.subplot('31' + str(i + 1))
            s.set_ylabel(DIS_LABELS[i])
            s.plot(imu.ms, disp[i], COLORS[i])
        # plt.show()

        xy_mag = []
        for i in range(len(disp[0])):
            r = math.sqrt(disp[0][i] ** 2 + disp[1][i] ** 2)
            xy_mag.append(r)

        fig = plt.figure("XY Displacement Mag")
        fig.gca().plot(imu.ms, xy_mag)

        rows = []
        for i in range(len(pos)):
            row = [imu.ms[i]]
            row.extend(pos[i])
            row.extend(imu.q[i])
            line = ', '.join([str(col) for col in row]) + '\n'
            rows.append(line)

        print(xy_mag[-1])

        f = open("%s_disp.csv" % self.file_path, 'w')
        f.writelines(rows)
        f.close()

        plt.show()

    @staticmethod
    def integrate_vel(ms, vel):
        """"""
        pos = [[0, 0, 0]]
        for i in range(1, len(ms)):
            dt = (ms[i] - ms[i-1]) / 1000
            s0 = pos[i-1]

            sx = s0[0] + vel[i][0] * dt
            sy = s0[1] + vel[i][1] * dt
            sz = s0[2] + vel[i][2] * dt

            pos.append([sx, sy, sz])

        return pos

    @staticmethod
    def adjust_velocity_drift(vel, vel_drift):
        ret = []
        for i in range(len(vel)):
            vx, vy, vz = vel[i]
            dx, dy, dz = vel_drift[i]

            ret.append([vx-dx, vy-dy, vz-dz])

        return ret

    @staticmethod
    def calc_vel_drift(vel, start, end):
        if len(end) > len(start):
            end.pop(0)

        vel_drift = np.zeros((len(vel), 3))
        for i in range(len(end)):
            vx, vy, vz = vel[end[i]-1]
            elem = end[i] - start[i]
            # print(vx, vy, vz)
            drift_rate = [vx/elem, vy/elem, vz/elem]
            # print(drift_rate)

            for ind in range(0, elem):
                ddd = [
                    drift_rate[0] * (ind + 1),
                    drift_rate[1] * (ind + 1),
                    drift_rate[2] * (ind + 1)
                ]
                # drift.append(ddd)
                # print(ind, ddd)
                vel_drift[start[i] + ind][0] = ddd[0]
                vel_drift[start[i] + ind][1] = ddd[1]
                vel_drift[start[i] + ind][2] = ddd[2]
        return vel_drift

    @staticmethod
    def find_non_stationary_periods(stationary):
        """
        1 = stationary
        0 = non-stationary
                0 0 0 0 0 0 0 1 1 1  1  1  0  0  0  0  0  0  1 1 1  0 0 0
        diff:   0 0 0 0 0 0 0 1 0 0  0  0 -1  0  0  0  0  0  1 0 0 -1 0 0
                0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
        """
        diff = [0]
        start = []
        end = []
        for i in range(1, len(stationary)):
            d = stationary[i] - stationary[i-1]
            if d == -1:
                start.append(i)
            elif d == 1:
                end.append(i)
            diff.append(d)

        return start, end

    @staticmethod
    def integrate_acc(ms, acc, stationary):
        # dts = [0]
        vel = [[0, 0, 0]]
        for i in range(1, len(ms)):
            dt = (ms[i] - ms[i - 1]) / 1000.0

            if stationary[i]:
                vx = 0
                vy = 0
                vz = 0
            else:
                ax, ay, az = acc[i]
                v0 = vel[i - 1]
                vx = v0[0] + ax * dt
                vy = v0[1] + ay * dt
                vz = v0[2] + az * dt

            vel.append([vx, vy, vz])
            # dts.append(dt)

        return vel


    @staticmethod
    def stationary_points(dyn_acc, thresh):
        mags = []
        for i in range(len(dyn_acc)):
            ax, ay, az = dyn_acc[i]
            mag = math.sqrt(ax*ax + ay*ay + az*az)
            mags.append(mag)

        stationary = [m < thresh for m in mags]

        return stationary, mags

    @staticmethod
    def g_removal(global_acc, g):
        dacc = [[a[0], a[1], a[2] - g] for a in global_acc]

        return dacc

    @staticmethod
    def global_transform(local_acc, q):
        dacc = []
        for i in range(len(local_acc)):
            ax, ay, az = local_acc[i]

            v = q[i] * Quaternion(0, ax, ay, az) * q[i].conj()
            dacc.append(v[1:])

        return dacc

    @staticmethod
    def g_to_ms2(acc, g):
        return [[a[0] * g, a[1] * g, a[2] * g] for a in acc]

    def reproduce_range_from_roll(self, roll_acc):
        mags = [math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]) for a in roll_acc]
        # pprint(mags)
        keep = [m > 0 for m in mags]
        # pprint(keep)
        imu = IMU()

        for i in range(len(self.imu.ms)):
            if keep[i]:
                ms, acc, gyro, mag, q, euler, h = self.imu.get_entry(i)
                imu.add_entry(ms, roll_acc[i], gyro, mag, q, euler, h)

        return imu

    @staticmethod
    def rolling_window(acc, n_before, n_after):
        roll = []
        for i in range(len(acc)):
            if n_before <= i < len(acc)-n_after:
                buf = acc[i - n_before:i + n_after]
                buf_sum = [0, 0, 0]
                for en in buf:
                    buf_sum[0] += en[0]
                    buf_sum[1] += en[1]
                    buf_sum[2] += en[2]
                av = [buf_sum[0] / len(buf), buf_sum[1] / len(buf), buf_sum[2] / len(buf)]
                roll.append(av)
            else:
                roll.append([0, 0, 0])

        return roll

    def remove_calibration_period(self, t_ms):
        ind = 0
        for i in range(len(self.imu.ms)):
            if self.imu.ms[i] - self.imu.ms[0] > t_ms:
                ind = i
                break

        for i in range(ind):
            self.imu.pop_entry(0)


if __name__ == '__main__':
    PostProcess('../real_time_system/imu/data/5a01_extended_run_1.txt')
