import math
import numpy as np
import matplotlib.pyplot as plt


class IMUOffset:
    def __init__(self):
        self.data = {}
        self.headers = []

        self.group = {}
        self.averages = {}
        self.stdevs = {}
        self.errors = {}

        self.load_data()
        self.group_data(30)
        self.grouping_averages()
        self.grouping_stdev()
        self.histogram()
        # self.group_histogram()
        # self.plot_data()
        self.error()

    def error(self):
        for header in self.headers:
            err = 1.96 * self.stdevs[header]["avgs_stdev"] / math.sqrt(len(self.averages[header]["avgs"]) - 1)
            self.errors[header] = err
            print(header, err)

    def plot_data(self):
        for header in self.headers:
            fig = plt.figure()
            ax = fig.gca()
            ax.plot(self.data[header])

            plt.show()

    def group_histogram(self):
        for header in self.headers:
            plt.figure()
            # hist, edge = np.histogram(self.averages[header]['avgs'])
            plt.hist(self.averages[header]['avgs'], bins=1000)
            plt.title(header)

        plt.show()

    def histogram(self):
        for i in range(len(self.headers)):
            if i % 4 == 0:
                plt.figure()
            ind = i % 4

            s = plt.subplot(2, 2, ind + 1)
            header = self.headers[i]

            hist, edge = np.histogram(self.data[header], bins=1000)
            # print(hist, edge)
            plt.plot(edge[1:], hist)
            plt.title(header)
        plt.show()

    def grouping_stdev(self):
        for header in self.headers:
            self.stdevs[header] = {
                "pop_stdev": 0,
                "avgs_stdev": 0,
                "group_stdev": [],
            }
            self.stdevs[header]['pop_stdev'] = np.std(self.data[header])
            self.stdevs[header]['avgs_stdev'] = np.std(self.averages[header]["avgs"])
            for group in self.group[header]:
                gstdev = np.std(group)
                self.stdevs[header]["group_stdev"].append(gstdev)

            print(header, self.stdevs[header]['pop_stdev'], self.stdevs[header]['avgs_stdev'])


    def grouping_averages(self):
        for header in self.headers:
            self.averages[header] = {
                "avgs": [],
                "avg_of_avgs": 0
            }
            for group in self.group[header]:
                avg = np.mean(group)
                self.averages[header]["avgs"].append(avg)

            self.averages[header]["avg_of_avgs"] = np.mean(self.averages[header]["avgs"])
            print(header, self.averages[header]["avg_of_avgs"])

    def group_data(self, n):
        for header in self.headers:
            self.group[header] = []

            data = self.data[header]

            set_count = math.floor(len(data) / n)

            print(header, set_count)
            for i in range(set_count):
                group = data[i * n: (i + 1) * n]
                self.group[header].append(group)

    def load_data(self):
        f = open('imu_offset/combined.csv', 'r')
        data = f.readlines()

        self.headers = data[0].strip().replace("\r", "").replace("\n", "").split(",")

        for header in self.headers:
            self.data[header] = []

        for row in data[1:]:
            cols = row.split(",")
            for i in range(len(cols)):
                try:
                    self.data[self.headers[i]].append(float(cols[i]))
                except:
                    """"""


if __name__ == "__main__":
    IMUOffset()
