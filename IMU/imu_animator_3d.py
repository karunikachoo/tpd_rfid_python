import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation

from quaternion import Quaternion
import numpy as np


class Plotter:
    def __init__(self):
        self.path = None
        self.ms = []
        self.disp = []
        self.q = []

        self.fig = None
        self.ax = None
        self.axes = None
        self.disp_path = None

        # self.mins = [0, 0, 0]
        # self.maxs = [0, 0, 0]

    def load_data(self, path):
        self.path = path
        f = open(self.path, 'r')
        lines = f.readlines()
        f.close()

        disp = []

        for line in lines:
            data = [float(x.strip()) for x in line.split(",")]
            ms, sx, sy, sz, qw, qx, qy, qz = data
            self.ms.append(ms)
            disp.append([sx, sy, sz])

            q = Quaternion(qw, qx, qy, qz)
            self.q.append(q)

        self.disp = np.array(disp)

    def animate(self):
        # self.fig = plt.figure(figsize=plt.figaspect(1.0))
        self.fig = plt.figure()
        self.ax = p3.Axes3D(self.fig)
        # self.ax.set_aspect('equal', 'box')

        # self.ax.set_xlim(-1, 0)
        # self.ax.set_ylim(0, 3.1)
        # self.ax.set_zlim(0, 3.1)

        fixed_cs = self.axis_local_to_global(Quaternion(1, 0, 0, 0), scale=0.1)
        self.axes = self.plot_cs(self.ax, [0, 0, 0], fixed_cs)
        self.disp_path, = self.ax.plot([0, 0], [0, 0], [0, 0], color='cyan')

        ani = animation.FuncAnimation(self.fig, self.update_animation, len(self.q), interval=6, repeat=True)
        # ani.save(self.path + '.mp4', writer='ffmpeg')
        plt.show()

    def update_animation(self, ii):
        self.ax.view_init(elev=10., azim=(ii / 5) % 360)
        q = self.q[ii]
        pose = self.axis_local_to_global(q, scale=0.5)
        origin = self.disp[ii][:]
        self.update_cs(self.axes, origin, pose)
        x = self.disp[0:ii, 0]
        y = self.disp[0:ii, 1]
        z = self.disp[0:ii, 2]
        self.disp_path.set_data(x, y)
        self.disp_path.set_3d_properties(z)

        # self.mins[0] = min(x)
        # self.maxs[0] = max(x)
        #
        # self.mins[1] = min(y)
        # self.maxs[1] = max(y)
        #
        # self.mins[2] = min(z)
        # self.maxs[2] = max(z)
        if len(x) > 0:
            lx, ly, lz = self.compute_lims(x, y, z)
            self.ax.set_xlim(*lx)
            self.ax.set_ylim(*ly)
            self.ax.set_zlim(*lz)
        return self.fig

    def compute_lims(self, x, y, z):
        mx = [min(x), max(x)]
        my = [min(y), max(y)]
        mz = [min(z), max(z)]
        d = [mx[1] - mx[0], my[1] - my[0], mz[1] - mz[0]]
        dist = max(d)
        return [mx[0], mx[0] + dist], [my[0], my[0] + dist], [mz[0], mz[0] + dist]

    @staticmethod
    def update_cs(axes, origin, pose):
        for axis in range(3):
            xs = [origin[0], pose[axis][0] + origin[0]]
            ys = [origin[1], pose[axis][1] + origin[1]]
            zs = [origin[2], pose[axis][2] + origin[2]]
            axes[axis].set_data(xs, ys)
            axes[axis].set_3d_properties(zs)

    @staticmethod
    def plot_cs(ax, origin, pose, alpha=1.0, colors=None):
        if colors is None:
            colors = ['red', 'green', 'blue']
        ret = []
        for axis in range(3):
            xs = [origin[0], pose[axis][0] + origin[0]]
            ys = [origin[1], pose[axis][1] + origin[1]]
            zs = [origin[2], pose[axis][2] + origin[2]]
            axs, = ax.plot(xs, ys, zs, color=colors[axis], alpha=alpha)
            ret.append(axs)
        return ret

    @staticmethod
    def axis_local_to_global(q: Quaternion, scale=1.0):
        """
        generates a coordinate system around (0, 0, 0)
        """
        cs = [[scale, 0.0, 0.0], [0.0, scale, 0.0], [0.0, 0.0, scale]]
        c2 = []

        for axis in range(3):
            q2 = Quaternion(0, cs[axis][0], cs[axis][1], cs[axis][2])
            h = q * q2 * q.conj()
            c2.append(h[1:])
        return c2


if __name__ == '__main__':
    p = Plotter()
    p.load_data("right_foot/azi_mar8_up_single_3_right.txt_disp.csv")
    p.animate()