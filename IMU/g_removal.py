import math

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

G = 9.806


class GRemoval:
    def __init__(self, filename, header=False):
        self.filename = filename
        self.data = pd.read_csv(filename)   # type: pd.DataFrame

        self.dt = self.get_dts(self.data['ms'])
        # print(self.dt)
        # print(len([x for x in self.dt if x > 15]))

        ax = self.data['x']
        ay = self.data['y']
        az = self.data['z']
        self.a_mag = self.get_mag(ax, ay, az)
        # print(self.a_mag)

        self.theta, mag = self.get_angle(ax[0], ay[0], az[0])
        self.G = mag[0]
        tarr, dyn_accel = self.iterate(self.data.to_numpy())

        data = self.data.to_numpy()
        ii = data[:, 0]
        ax = plt.subplot(311)
        ay = plt.subplot(312)
        az = plt.subplot(313)

        ax.plot(ii, dyn_accel[:, 0])
        ax.plot(ii, data[:, 1])
        ay.plot(ii, dyn_accel[:, 1])
        ay.plot(ii, data[:, 2])
        az.plot(ii, dyn_accel[:, 2])
        az.plot(ii, data[:, 3])

        plt.show()

    def iterate(self, data: np.ndarray):
        theta = [self.theta[0], self.theta[1], self.theta[2]]
        tarr = [theta]
        g0 = np.array([0, 0, 0])
        ret = []
        for i in range(0, len(data)):
            row = data[i]

            g1 = row[4:7]
            if i > 0:
                avg_g = (g1 + g0) / 2
                d_theta = avg_g * self.dt[i] / 180 * math.pi
            else:
                d_theta = np.array([0, 0, 0])
            theta += d_theta
            # print(theta)
            tarr.append([theta[0], theta[1], theta[2]])

            gz = self.G * math.sin(theta[1])
            gy = self.G * math.sin(theta[0]) * math.cos(theta[1])
            gx = self.G * math.cos(theta[0]) * math.cos(theta[1])

            g = np.array([gx, gy, gz])
            # gs = self.G * np.sin(theta)
            # gc = self.G * np.cos(theta)

            print("====")
            # print(g)
            # print(gs)
            # print(gc)
            print("====")

            da = row[1:4] - g
            ret.append(da)
            print(row[1:4])
            print(da)
            g0 = g1
        return tarr, np.array(ret)

    @staticmethod
    def get_angle(x, y, z):
        print(x, y, z)
        mag = GRemoval.get_mag([x], [y], [z])
        tx = math.atan2(-y, z)
        ty = math.atan2(z, x)
        tz = math.atan2(y, x)

        print(tx * 180 / math.pi, ty * 180 / math.pi, tz * 180 / math.pi)
        print(tx, ty, tz)
        print(mag)
        return [tx, ty, tz], mag

    @staticmethod
    def get_mag(x, y, z):
        ret = []
        for i in range(len(x)):
            mag = math.sqrt(x[i] * x[i] + y[i] * y[i] + z[i] * z[i])
            ret.append(mag)
        return ret

    @staticmethod
    def get_dts(data):
        ret = [0]
        for i in range(len(data)):
            t1 = data[i]
            if i > 0:
                t0 = data[i - 1]
                dt = t1 - t0
                ret.append(dt / 1000.0)
        return ret


if __name__ == '__main__':
    # g = GRemoval('DROP TEST.csv')
    g = GRemoval('DATA FOOT.csv')
