import math
import numpy as np
from quaternion import Quaternion

G_MAG = 1.023354149     #g
G = 9.80665             # m/s^2


class RollingWindow:
    def __init__(self, n_last):
        self.n_last = n_last

        self.data = []
        self.average = 0

    def push(self, point):
        self.data.append(point)

        if len(self.data) > self.n_last:
            self.data.pop(0)

        self.average = sum(self.data) / len(self.data)


class VelItem:
    def __init__(self, ms, vel):
        self.ms = ms
        self.vel = vel

    def __truediv__(self, b):
        b = float(b)
        return [self.vel[0] / b, self.vel[1] / b, self.vel[2] / b]


class GaitAnalysis:
    def __init__(self):
        self.a_roll = [RollingWindow(25), RollingWindow(25), RollingWindow(25)]

        self.ms_prev = -1
        self.acc_prev = [0, 0, 0]
        self.vel_prev = [0, 0, 0]
        self.d = [0, 0, 0]

        self.was_stationary = False

        self.vel_buffer = []
        self.d_buffer = []

    def process_data(self, ms, ax, ay, az, q: Quaternion):
        # Filtering using Rolling Window
        a = [ax, ay, az]
        a_avg = []
        for axis in range(3):
            self.a_roll[axis].push(a[axis])
            a_avg.append(self.a_roll[axis].average)

        # Transform to global frame
        acc = Quaternion(0, a_avg[0], a_avg[1], a_avg[2])
        global_acc = q * acc * q.conj()
        gax, gay, gaz = global_acc[1:]

        gaz -= G_MAG

        acc_mag = math.sqrt(gax * gax + gay * gay + gaz * gaz)

        vx, vy, vz, sx, sy, sz = self.integrate_acceleration(ms, gax, gay, gaz, acc_mag < 0.08)

        return gax, gay, gaz, vx, vy, vz, sx, sy, sz

    """
    This one takes in the global dynamic acceleration
    TODO: Create a separate thread to do non-linear processing 
    of the integrations. Then callback to display_update
    """
    def integrate_acceleration(self, ms, gax, gay, gaz, stationary: bool):
        if self.ms_prev < 0:
            self.ms_prev = ms
            # self.acc_prev = [gax, gay, gaz]
            # self.vel_prev = [0, 0, 0]
            self.vel_buffer.append(VelItem(ms, [0, 0, 0]))
            self.was_stationary = True
            return None

        if not self.was_stationary and stationary:
            self.was_stationary = True

            # compute velocity drift
            drift_rate = self.vel_buffer[-1] / len(self.vel_buffer)
            for i in range(len(self.vel_buffer)):
                drift = (i + 1) * drift_rate
                self.vel_buffer = self.vel_buffer[i] - drift
                self.d_buffer.append()

        elif stationary and self.was_stationary:
            """do nothing"""

        else:
            self.was_stationary = False

            acc = [gax * G, gay * G, gaz * G]
            dt = (ms - self.ms_prev) / 1000

            v = [0, 0, 0]
            for i in range(3):
                v[i] = acc[i] * dt

            self.vel_buffer.append(VelItem(ms, v))

            self.ms_prev = ms
        # vx = (acc[0] + self.acc_prev[0]) / 2.0 * dt
        # vy = (acc[1] + self.acc_prev[1]) / 2.0 * dt
        # vz = (acc[2] + self.acc_prev[2]) / 2.0 * dt
        #
        # sx = (vx + self.vel_prev[0]) / 2.0 * dt
        # sy = (vy + self.vel_prev[1]) / 2.0 * dt
        # sz = (vz + self.vel_prev[2]) / 2.0 * dt

        sx = vx * dt

        # print(gax, gay, gaz, vx, vy, vz, sx, sy, sz)

        self.ms_prev = ms
        self.acc_prev = acc
        self.vel_prev = [vx, vy, vz]

        return vx, vy, vz, sx, sy, sz

