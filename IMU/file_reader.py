

class FileReader:
    def __init__(self, file_path):
        self.file_path = file_path

    def read(self):
        f = open(self.file_path, 'r')
        lines = f.readlines()

        data = []
        for line in lines:
            cols = line.split(",")
            d = [float(c.strip()) for c in cols]
            data.append(d)
        return data
