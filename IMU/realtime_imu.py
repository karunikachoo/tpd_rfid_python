import os
import queue
import threading
import time
import tkinter as tk
import traceback

import numpy as np
import math
from scipy import signal

from gait_analysis import GaitAnalysis
from quaternion import Quaternion
from imu_serial import IMUSerial, CMD, bcol
from madgwickahrs import MadgwickAHRS

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation


G = 9.80665     # m/s^2
FAST_SLEEP = 0.0001
SAMPLE_PERIOD = 1/100


class IMUProcessor(threading.Thread):
    def __init__(self, update_callback):
        super(IMUProcessor, self).__init__()
        self.update_callback = update_callback

        self.alive = threading.Event()
        self.alive.set()

        self.gait_analysis = GaitAnalysis()

        self.queue = queue.Queue()

    def put(self, line):
        self.queue.put(line)

    def stop(self):
        self.alive.clear()

    def run(self):
        # self.wait_for_dev()
        try:
            while self.alive.is_set():
                if not self.queue.empty():
                    line = self.queue.get(True, FAST_SLEEP)
                    self.process_line(line)

                time.sleep(FAST_SLEEP)

        except KeyboardInterrupt:
            os._exit(0)

    def process_line(self, line):
        """

        ms, ax, ay, az, dax, day, daz, gx, gy, gz, mx, my, mz, qw, qx, qy, qz, ex, ey, ez, h
        """
        try:
            data_str_arr = line.split(",")
            data_float_arr = [float(col.strip()) for col in data_str_arr]
            ms, ax, ay, az, gx, gy, gz, mx, my, mz, qw, qx, qy, qz, ex, ey, ez, h = data_float_arr
            q = Quaternion(qw, qx, qy, qz)

            c = Quaternion(0, 1, 0, 0)
            v = (q * c * q.conj())[1:]
            angle = math.atan2(v[1], v[0]) / math.pi * 180.0
            print(angle, q.to_euler123()[2] / math.pi * 180.0)

            # gax, gay, gaz, vx, vy, vz, sx, sy, sz = self.gait_analysis.process_data(ms, ax, ay, az, q)
            #
            self.update_callback(ms, 0, 0, 0, 0, 0, 0, 0, 0, 0, q)

        except Exception as e:
            print(traceback.format_exc())
            print(bcol.FAIL + "ERR: bad line: \n" + line + bcol.ENDC)

    @staticmethod
    def compensate_gravity(acc, q):
        g = [0.0, 0.0, 0.0]

        g[0] = 2 * (q[1] * q[3] - q[0] * q[2])
        g[1] = 2 * (q[0] * q[1] + q[2] * q[3])
        g[2] = q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]

        # compensate accelerometer readings with the expected direction of gravity
        return [acc[0] - g[0], acc[1] - g[1], acc[2] - g[2]]

    def line_callback(self, line: str):
        self.queue.put(line)


class SerialObject(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.master = master
        self.pack()

        self.plotter = Plotter(self, self.master)
        self.processor = IMUProcessor(self.data_callback)
        self.serial = IMUSerial(self, line_callback=self.processor.process_line)

        self.dev_var = tk.StringVar(self)
        self.dev_var.set("Select PORT")
        self.dev_dropdown = tk.OptionMenu(self, self.dev_var, "Select PORT")
        self.dev_dropdown.grid(row=0, column=2, sticky="E")

        self.connect_btn = tk.Button(self, text="Connect to PORT", command=self.connect_to_port)
        self.connect_btn.grid(row=0, column=3, sticky="W")

        self.dev_status = tk.Label(self, text="disconnected")
        self.dev_status.grid(row=0, column=4, sticky="W")

        self.reset = tk.Button(self, text='RESET', command=self.plotter.reset)
        self.reset.grid(row=0, column=5, sticky='WE')

        # self.processor.start()
        self.serial.start()

    def data_callback(self, ms, ax, ay, az, vx, vy, vz, sx, sy, sz, q):
        self.plotter.update_data(ms, ax, ay, az, vx, vy, vz, sx, sy, sz, q)

    def status_connected(self):
        # cmd = DISP_CMD(Display.CMD_SERIAL_REGISTER, {
        #     "uid": self.uid,
        #     "cb": self.set_line_color
        # })
        # self.disp.put_cmd(cmd)
        self.connect_btn.config(state=tk.NORMAL)
        self.dev_status.config(text="CONNECTED", fg="Green")

    def status_disconnected(self):
        self.dev_status.config(text="disconnected", fg="Red")
        self.dev_var.set("Select PORT")
        self.connect_btn.config(state=tk.NORMAL)

    def connect_to_port(self):
        self.dev_status.config(text="connecting...", fg="Grey")
        self.serial.put(CMD(CMD.CONNECT, self.dev_var.get()))
        self.connect_btn.config(state=tk.DISABLED)

    def update_ports(self, dev_list):
        menu = self.dev_dropdown['menu']
        # menu.delete_pt(0, 'end')
        if len(dev_list) > 0:
            for dev in dev_list:
                menu.add_command(label=dev, command=lambda name=dev: self.dev_var.set(name))
        else:
            self.dev_var.set("Select PORT")


# class Plotter(tk.Toplevel):
class Plotter:
    def __init__(self, main, master):
        # tk.Toplevel.__init__(self, master)
        self.main = main

        # self.title("Plotter")
        # self.geometry("600x600")

        self.ms = None
        self.u = [0, 0, 0]
        self.d = [0, 0, 0]

        # axes

        self.q = Quaternion(1, 0, 0, 0)
        self.cs = self.axis_local_to_global(self.q)
        self.line_x = [0, 0]
        self.line_y = [0, 0]
        self.line_z = [0, 0]

        self.a_cs = self.cs.copy()
        self.a_g_vec = [0, 0, 0]

        # Plotting
        self.fig = plt.Figure()

        self.canvas = FigureCanvasTkAgg(self.fig, master=master)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        self.ax = p3.Axes3D(self.fig)

        self.ax.set_xlim(-1.1, 1.1)
        self.ax.set_ylim(-1.1, 1.1)
        self.ax.set_zlim(-1.1, 1.1)

        self.ax.set_xlabel('x')
        self.ax.set_ylabel('y')
        self.ax.set_zlabel('z')

        self.ref_cs = self.axis_local_to_global(Quaternion(1, 0, 0, 0))
        # self.plot_cs(self.ax, (0, 0, 0), self.ref_cs, alpha=0.5)

        self.cs_ax = self.plot_cs(self.ax, self.d, self.cs, alpha=0.25)
        self.line, = self.ax.plot(self.line_x, self.line_y, self.line_z, 'c')

        self.a_cs_ax = self.plot_cs(self.ax, self.d, self.a_cs, colors=['c', 'c', 'c'])
        self.a_line, = self.ax.plot(
            [self.d[0], self.a_g_vec[0] + self.d[0]],
            [self.d[1], self.a_g_vec[1] + self.d[1]],
            [self.d[2], self.a_g_vec[2] + self.d[2]], 'r')

        # self.ani = animation.FuncAnimation(self.fig, self.draw_plot, interval=125)
        self.ani = animation.FuncAnimation(self.fig, self.draw_plot, interval=100)

    def update_data(self, ms, ax, ay, az, vx, vy, vz, sx, sy, sz, q: Quaternion):
        self.q = q
        self.cs = self.axis_local_to_global(q)

        # self.d[0] += sx
        # self.d[1] += sy
        # self.d[2] += sz
        self.ms = ms

    def draw_plot(self, ii):
        # print(ii)
        self.update_cs(self.cs_ax, self.d, self.cs)
        self.update_cs(self.a_cs_ax, self.d, self.a_cs)

        # self.line.set_data(self.line_x, self.line_y)
        # self.line.set_3d_properties(self.line_z)

        # self.a_line.set_data([self.d[0], self.a_g_vec[0] + self.d[0]], [self.d[1], self.a_g_vec[1] + self.d[1]])
        # self.a_line.set_3d_properties([self.d[2], self.a_g_vec[2] + self.d[2]])

    def reset(self):
        self.d = [0, 0, 0]

    def integrate_step(self, a_global_vec, a_mag, ms):
        # convert accel from g -> m/s^2
        if self.ms is None:
            " Wait for at least one time steps "
            return

        a_vec = [a * G * a_mag for a in a_global_vec]
        # print(a_vec)
        dt = (ms - self.ms) / 1000.0

        v = [0, 0, 0]
        for i in range(3):
            ds = self.u[i] * dt + 0.5 * a_vec[i] * dt * dt
            v[i] = self.u[i] + a_vec[i] * dt
            self.d[i] += ds

        self.u = v

    @staticmethod
    def update_cs(axes, origin, pose):
        for axis in range(3):
            xs = [origin[0], pose[axis][0] + origin[0]]
            ys = [origin[1], pose[axis][1] + origin[1]]
            zs = [origin[2], pose[axis][2] + origin[2]]
            axes[axis].set_data(xs, ys)
            axes[axis].set_3d_properties(zs)

    @staticmethod
    def plot_cs(ax, origin, pose, alpha=1.0, colors=None):
        if colors is None:
            colors = ['red', 'green', 'blue']
        ret = []
        for axis in range(3):
            xs = [origin[0], pose[axis][0] + origin[0]]
            ys = [origin[1], pose[axis][1] + origin[1]]
            zs = [origin[2], pose[axis][2] + origin[2]]
            axs, = ax.plot(xs, ys, zs, color=colors[axis], alpha=alpha)
            ret.append(axs)
        return ret

    @staticmethod
    def axis_local_to_global(q: Quaternion):
        """
        generates a coordinate system around (0, 0, 0)
        :param q:
        :return:
        """
        cs = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]
        c2 = []

        for axis in range(3):
            q2 = Quaternion(0, cs[axis][0], cs[axis][1], cs[axis][2])
            h = q * q2 * q.conj()
            c2.append(h[1:])
        return c2


if __name__ == "__main__":
    try:
        root = tk.Tk()
        so = SerialObject(root)
        root.mainloop()
    except Exception as e:
        print(traceback.format_exc())
        os._exit(0)
    os._exit(0)

